package app.tp6.ejerciciod;

import java.math.BigDecimal;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.zkoss.bind.annotation.Immutable;

import app.utils.Observable;
import app.utils.Observer;
import app.utils.TablaIntegracionNumerica;
import app.Loggable;
import app.tp5ytp6.Alumno;
import app.tp5ytp6.AlumnoFactory;
import app.tp5ytp6.ColaAlumnos;
import app.tp5ytp6.ColaMaquinas;
import app.tp5ytp6.EventoFactory;
import app.tp5ytp6.FinMantenimientoFactory;
import app.tp5ytp6.Maquina;
import app.tp5ytp6.MaquinaFactory;
import app.tp5ytp6.Reloj;
import app.tp5ytp6.Tecnico;
import app.tp5ytp6.TecnicoFactory;
import app.tp5ytp6.VectorEstadosFinMantenimientoFactory;
import app.utils.DetalleTabla;
import app.utils.GestorColas;
import app.utils.TablaVectorEstados;
import app.utils.VectorEstados;
import app.utils.distribucion.TipoDistribucion;

@Component
public class TP6EjercicioDImpl implements TP6EjercicioD {

	static final Logger LOG = LoggerFactory.getLogger(TP6EjercicioDImpl.class);
	private Long numeroIteracionesAMostrar;
	private Integer cantidadMaquinas, cantidadTecnicos;
	private BigDecimal horaDesdeAMostrar, cantidadDeTiempoASimular;
	private DetalleTabla detalleTabla;
	private Set<Maquina> maquinas;
	private Set<Tecnico> tecnicos;
	private List<Observer> observers;
	private boolean changed;
	private Map<Object, TablaIntegracionNumerica> tablasDeIntegracionNumerica;
	@Autowired
	private TP6EjercicioDResultados tP6EjercicioDResultados;
	@Autowired
	private ApplicationContext applicationContext;
	@Autowired
	private Reloj reloj;
	@Autowired
	private GestorColas gestorColas;
	@Autowired
	private TablaVectorEstados tablaVectorEstados;
	@Autowired
	private AlumnoFactory alumnoFactory;
	@Autowired
	private MaquinaFactory maquinaFactory;
	@Autowired
	private TecnicoFactory tecnicoFactory;
	@Autowired
	private EventoFactory<?> llegadaAlumnoFactory;
	@Autowired
	private EventoFactory<?> finInscripcionFactory;
	@Autowired
	private EventoFactory<?> llegadaTecnicoFactory;
	@Autowired
	private FinMantenimientoFactory finMantenimientoFactoryTP6EjercicioD;
	@Autowired
	private VectorEstados<?> vectorEstadosAlumno;
	@Autowired
	private VectorEstados<?> vectorEstadosMaquina;
	@Autowired
	private VectorEstados<?> vectorEstadosTecnico;
	@Autowired
	private VectorEstados<?> vectorEstadosLlegadaAlumnoFactory;
	@Autowired
	private VectorEstados<?> vectorEstadosFinInscripcionFactory;
	@Autowired
	private VectorEstados<?> vectorEstadosLlegadaTecnicoFactory;
	@Autowired
	private VectorEstadosFinMantenimientoFactory vectorEstadosFinMantenimientoFactoryTP6EjercicioD;
	@Autowired
	private VectorEstados<?> vectorEstadosLlegadaAlumno;
	@Autowired
	private VectorEstados<?> vectorEstadosFinInscripcion;
	@Autowired
	private VectorEstados<?> vectorEstadosLlegadaTecnico;
	@Autowired
	private VectorEstados<?> vectorEstadosFinMantenimiento;
	@Autowired
	private VectorEstados<?> vectorEstadosGestorColas;
	@Autowired
	private VectorEstados<?> vectorEstadosReloj;
	private BigDecimal demoraInscripcionMinima, demoraInscripcionMaxima, llegadaAlumnoMedia, llegadaTecnicoMedia,
			llegadaTecnicoError, demoraMantenimientoMedia, demoraMantenimientoDesviacion;
	private String h;

	public TP6EjercicioDImpl() {
		super();
		observers = new ArrayList<>();
		changed = true;
		maquinas = new HashSet<>();
		tecnicos = new HashSet<>();
		demoraInscripcionMinima = new BigDecimal("5");
		demoraInscripcionMaxima = new BigDecimal("8");
		llegadaAlumnoMedia = new BigDecimal("2");
		llegadaTecnicoMedia = new BigDecimal("1");
		llegadaTecnicoError = new BigDecimal("3");
		demoraMantenimientoMedia = new BigDecimal("3");
		demoraMantenimientoDesviacion = new BigDecimal("10");
		cantidadDeTiempoASimular = new BigDecimal("800");
		horaDesdeAMostrar = new BigDecimal("0");
		numeroIteracionesAMostrar = 18l;
		tablasDeIntegracionNumerica = new HashMap<>();
		h = "0.01";
	}

	@Bean
	@Scope(value = "prototype")
	static public TP6EjercicioD tP6EjercicioD() {
		return new TP6EjercicioDImpl();
	}

	@PostConstruct
	private void initTablasDeIntegracion() {
		tablasDeIntegracionNumerica.put(new BigDecimal("1000"), (TablaIntegracionNumerica) applicationContext.getBean("tablaIntegracionNumerica"));
		tablasDeIntegracionNumerica.put(new BigDecimal("1500"), (TablaIntegracionNumerica) applicationContext.getBean("tablaIntegracionNumerica"));
		tablasDeIntegracionNumerica.put(new BigDecimal("2000"), (TablaIntegracionNumerica) applicationContext.getBean("tablaIntegracionNumerica"));
	}

	@PostConstruct
	private void initReloj() {
		reloj.addObserver(this);
		Set<EventoFactory<?>> eventoFactories = new HashSet<>();
		eventoFactories.add(llegadaAlumnoFactory);
		eventoFactories.add(llegadaTecnicoFactory);
		reloj.resetear(eventoFactories);
	}

	@PostConstruct
	private void initGestorColas() {
		gestorColas.addObserver(vectorEstadosGestorColas);
		gestorColas.addObserver(vectorEstadosAlumno);
		gestorColas.agregarCola(
				new ColaMaquinas(new ArrayDeque<Maquina>(), new ArrayDeque<Tecnico>(), "Cola de máquinas", 5));
		gestorColas.agregarCola(
				new ColaAlumnos(new ArrayDeque<Alumno>(), new ArrayDeque<Maquina>(), "Cola de alumnos", 5));
	}

	@PostConstruct
	private void initAlumnoFactory() {
		alumnoFactory.addObserver(vectorEstadosAlumno);
	}

	@PostConstruct
	private void initMaquinaFactory() {
		maquinaFactory.addObserver(vectorEstadosMaquina);
		maquinaFactory.addObserver(finInscripcionFactory);
		maquinaFactory.addObserver(vectorEstadosFinInscripcionFactory);
		maquinaFactory.addObserver(gestorColas);
	}

	@PostConstruct
	private void initTecnicoFactory() {
		tecnicoFactory.addObserver(vectorEstadosTecnico);
		tecnicoFactory.addObserver(llegadaTecnicoFactory);
		tecnicoFactory.addObserver(finMantenimientoFactoryTP6EjercicioD);
		tecnicoFactory.addObserver(vectorEstadosLlegadaTecnicoFactory);
		tecnicoFactory.addObserver(vectorEstadosFinMantenimientoFactoryTP6EjercicioD);
		tecnicoFactory.addObserver(gestorColas);
	}

	@PostConstruct
	private void initLlegadaAlumnoFactory() {
		addObserver(llegadaAlumnoFactory);
		llegadaAlumnoFactory.addObserver(vectorEstadosLlegadaAlumno);
		llegadaAlumnoFactory.addObserver(vectorEstadosLlegadaAlumnoFactory);
		llegadaAlumnoFactory.addObserver(reloj);
		llegadaAlumnoFactory.addObserver(gestorColas);
	}

	@PostConstruct
	private void initFinInscripcionFactory() {
		addObserver(finInscripcionFactory);
		finInscripcionFactory.addObserver(vectorEstadosFinInscripcion);
		finInscripcionFactory.addObserver(vectorEstadosFinInscripcionFactory);
		finInscripcionFactory.addObserver(reloj);
		finInscripcionFactory.addObserver(gestorColas);
	}

	@PostConstruct
	private void initLlegadaTecnicoFactory() {
		addObserver(llegadaTecnicoFactory);
		llegadaTecnicoFactory.addObserver(vectorEstadosLlegadaTecnico);
		llegadaTecnicoFactory.addObserver(vectorEstadosLlegadaTecnicoFactory);
		llegadaTecnicoFactory.addObserver(reloj);
		llegadaTecnicoFactory.addObserver(gestorColas);
	}

	@PostConstruct
	private void initFinMantenimientoFactory() {
		addObserver(finMantenimientoFactoryTP6EjercicioD);
		finMantenimientoFactoryTP6EjercicioD.addObserver(vectorEstadosFinMantenimiento);
		finMantenimientoFactoryTP6EjercicioD.addObserver(vectorEstadosFinMantenimientoFactoryTP6EjercicioD);
		finMantenimientoFactoryTP6EjercicioD.addObserver(reloj);
		finMantenimientoFactoryTP6EjercicioD.addObserver(gestorColas);
	}

	@PostConstruct
	private void initVectorEstadosReloj() {
		vectorEstadosReloj.addObserver(this);
		addObserver(vectorEstadosReloj);
	}

	@PostConstruct
	private void initVectorEstadosAlumno() {
		addObserver(vectorEstadosAlumno);
		vectorEstadosAlumno.addObserver(this);
	}

	@PostConstruct
	private void initVectorEstadosMaquina() {
		addObserver(vectorEstadosMaquina);
		vectorEstadosMaquina.addObserver(this);
	}

	@PostConstruct
	private void initVectorEstadosTecnico() {
		addObserver(vectorEstadosTecnico);
		vectorEstadosTecnico.addObserver(this);
	}

	@PostConstruct
	private void initVectorEstadosGestorColas() {
		vectorEstadosGestorColas.addObserver(this);
		addObserver(vectorEstadosGestorColas);
	}

	@PostConstruct
	private void initVectorEstadosLlegadaAlumnoFactory() {
		addObserver(vectorEstadosLlegadaAlumnoFactory);
		vectorEstadosLlegadaAlumnoFactory.addObserver(this);
	}

	@PostConstruct
	private void initVectorEstadosFinInscripcionFactory() {
		addObserver(vectorEstadosFinInscripcionFactory);
		vectorEstadosFinInscripcionFactory.addObserver(this);
	}

	@PostConstruct
	private void initVectorEstadosLlegadaTecnicoFactory() {
		addObserver(vectorEstadosLlegadaTecnicoFactory);
		vectorEstadosLlegadaTecnicoFactory.addObserver(this);
	}

	@PostConstruct
	private void initVectorEstadosFinMantenimientoFactory() {
		addObserver(vectorEstadosFinMantenimientoFactoryTP6EjercicioD);
		vectorEstadosFinMantenimientoFactoryTP6EjercicioD.addObserver(this);
	}

	@PostConstruct
	private void initVectorEstadosLlegadaAlumno() {
		addObserver(vectorEstadosLlegadaAlumno);
		vectorEstadosLlegadaAlumno.addObserver(this);
	}

	@PostConstruct
	private void initVectorEstadosFinInscripcion() {
		addObserver(vectorEstadosFinInscripcion);
		vectorEstadosFinInscripcion.addObserver(this);
	}

	@PostConstruct
	private void initVectorEstadosLlegadaTecnico() {
		addObserver(vectorEstadosLlegadaTecnico);
		vectorEstadosLlegadaTecnico.addObserver(this);
	}

	@PostConstruct
	private void initVectorEstadosFinMantenimiento() {
		addObserver(vectorEstadosFinMantenimiento);
		vectorEstadosFinMantenimiento.addObserver(this);
	}

	@Override
	public void solucionar() throws Exception {
		doLog("trace", "solucionar()", "[{]", null, null);
		finMantenimientoFactoryTP6EjercicioD.setH(h);
		cargarParametrosDeEntrada();
		detalleTabla = (DetalleTabla) applicationContext.getBean("detalleTabla");
		notifyObservers(tablaVectorEstados);
		construirMaquinas(getCantidadMaquinas());
		construirTecnicos(getCantidadTecnicos());
		reloj.iniciar();
		while (reloj.getHorasProximas().get(0).compareTo(cantidadDeTiempoASimular) < 0)
			reloj.next();
		finalizar();
		doLog("trace", "solucionar()", "[}]", null, null);
	}

	private void cargarParametrosDeEntrada() {
		setCantidadMaquinas(5);
		setCantidadTecnicos(1);
		finInscripcionFactory
				.setTipoDistribucion((TipoDistribucion) applicationContext.getBean("distribucionUniforme"));
		finInscripcionFactory.getGeneradorPseudoaleatorio()
				.setExtremoInferior(getDemoraInscripcionMinima().doubleValue());
		finInscripcionFactory.getGeneradorPseudoaleatorio()
				.setExtremoSuperior(getDemoraInscripcionMaxima().doubleValue());

		llegadaAlumnoFactory
				.setTipoDistribucion((TipoDistribucion) applicationContext.getBean("distribucionExponencial"));
		llegadaAlumnoFactory.getGeneradorPseudoaleatorio().setLambda(1d / llegadaAlumnoMedia.doubleValue());

		llegadaTecnicoFactory
				.setTipoDistribucion((TipoDistribucion) applicationContext.getBean("distribucionUniforme"));
		llegadaTecnicoFactory.getGeneradorPseudoaleatorio().setExtremoInferior(
				getLlegadaTecnicoMedia().multiply(new BigDecimal("60").subtract(llegadaTecnicoError)).doubleValue());
		llegadaTecnicoFactory.getGeneradorPseudoaleatorio().setExtremoSuperior(
				getLlegadaTecnicoMedia().multiply(new BigDecimal("60").add(llegadaTecnicoError)).doubleValue());

		finMantenimientoFactoryTP6EjercicioD.setTablasDeIntegracionNumerica(tablasDeIntegracionNumerica);
	}

	private void construirMaquinas(int cantidadMaquinas) {
		doLog("trace", "construirMaquinas(int cantidadMaquinas)", "[{]", null, null);
		for (int i = 0; i < cantidadMaquinas; i++)
			try {
				Maquina maquina = maquinaFactory.getObject();
				maquina.entrarEnServicio(reloj.getHoraActual());
				maquinas.add(maquina);
			} catch (Exception e) {
				e.printStackTrace();
			}
		changed = true;
		notifyObservers(maquinas);
		doLog("trace", "construirMaquinas(int cantidadMaquinas)", "[}]", null, null);
	}

	private void construirTecnicos(int cantidadTecnicos) {
		doLog("trace", "construirTecnicos(int cantidadTecnicos)", "[{]", null, null);
		for (int i = 0; i < cantidadTecnicos; i++)
			try {
				tecnicos.add(tecnicoFactory.getObject());
			} catch (Exception e) {
				e.printStackTrace();
			}
		changed = true;
		notifyObservers(tecnicos);
		doLog("trace", "construirTecnicos(int cantidadTecnicos)", "[}]", null, null);
	}

	@Override
	public boolean hasResultadosSimulacion() {
		return tP6EjercicioDResultados.hasDetalleTablaFinal();
	}

	@Override
	public void reiniciar() {
		// TODO
	}

	@Override
	public String getH() {
		return h;
	}

	@Override
	public TP6EjercicioDResultados getResultadosSimulacion() {
		return tP6EjercicioDResultados;
	}

	@Override
	public TablaVectorEstados getTabla() {
		return tablaVectorEstados;
	}

	@Override
	@Immutable
	public BigDecimal getHoraDesdeAMostrar() {
		return horaDesdeAMostrar;
	}

	@Override
	public void setH(String h) {
		this.h = h;
	}

	@Override
	public void setHoraDesdeAMostrar(BigDecimal horaDesdeAMostrar) {
		this.horaDesdeAMostrar = horaDesdeAMostrar;
	}

	@Override
	@Immutable
	public BigDecimal getCantidadDeTiempoASimular() {
		return cantidadDeTiempoASimular;
	}

	@Override
	public void setCantidadDeTiempoASimular(BigDecimal cantidadDeTiempoASimular) {
		this.cantidadDeTiempoASimular = cantidadDeTiempoASimular;
	}

	@Override
	public Long getNumeroIteracionesAMostrar() {
		return numeroIteracionesAMostrar;
	}

	@Override
	public void setNumeroIteracionesAMostrar(Long numeroIteracionesAMostrar) {
		this.numeroIteracionesAMostrar = numeroIteracionesAMostrar;
	}

	@Override
	public Integer getCantidadMaquinas() {
		return cantidadMaquinas;
	}

	@Override
	public Integer getCantidadTecnicos() {
		return cantidadTecnicos;
	}

	@Override
	public void setCantidadTecnicos(Integer cantidadTecnicos) {
		this.cantidadTecnicos = cantidadTecnicos;
	}

	@Override
	public void setCantidadMaquinas(Integer cantidadMaquinas) {
		this.cantidadMaquinas = cantidadMaquinas;
	}

	@Override
	public void update(Observable o, Object arg) {
		doLog("trace", "update(Observable o, Object arg)", "[{]", null, null);
		if (arg instanceof DetalleTabla)
			update((DetalleTabla) arg);
		else if (o instanceof Reloj && arg instanceof BigDecimal) {
			update((Reloj) o);
		}
		doLog("trace", "update(Observable o, Object arg)", "[}]", null, null);
	}

	private void update(DetalleTabla detalleTabla) {
		doLog("trace", "update(DetalleTabla detalleTabla)", "[{]", null, null);
		parse(detalleTabla);
		this.detalleTabla = tablaVectorEstados.acumular(detalleTabla, this.detalleTabla);
		doLog("trace", "update(DetalleTabla arg)", "[}]", null, null);
	}

	// Agrega al detalle actual los valores del detalleTabla
	private void parse(DetalleTabla detalleTabla) {
		doLog("trace", "parse(DetalleTabla detalleTabla)", "[{]", null, null);
		for (Object key : detalleTabla.getValores().keySet())
			this.detalleTabla.getValores().put(key, detalleTabla.getValores().get(key));
		doLog("trace", "parse(DetalleTabla detalleTabla)", "[}]", null, null);
	}

	private void actualizarTabla() {
		doLog("trace", "actualizarTabla()", "[{]", null, null);
		if (reloj.getHoraActual().compareTo(getHoraDesdeAMostrar()) >= 0
				&& tablaVectorEstados.getDetalles().size() < numeroIteracionesAMostrar
				&& !tablaVectorEstados.getDetalles().contains(detalleTabla))
			tablaVectorEstados.getDetalles().add(detalleTabla);
		doLog("trace", "actualizarTabla()", "[}]", null, null);
	}

	private void update(Reloj reloj) {
		doLog("trace", "update(Reloj reloj)", "[{]", null, null);
		changed = true;
		notifyObservers(reloj.getHoraActual());
		if (!reloj.getHoraActual().equals(new BigDecimal("0")))
			detalleTabla = (DetalleTabla) applicationContext.getBean("detalleTabla");
		changed = true;
		notifyObservers(reloj);
		actualizarTabla();
		doLog("trace", "update(Reloj reloj)", "[}]", null, null);
	}

	private void finalizar() {
		reloj.parar();
		for (Maquina maquina : maquinas)
			maquina.salirDeServicio(reloj.getHoraActual());
		tP6EjercicioDResultados.setMaquinas(maquinas);
		tP6EjercicioDResultados.calcular(detalleTabla);
	}

	@Override
	public void addObserver(Observer o) {
		observers.add(o);
	}

	@Override
	public void removeObserver(Observer o) {
		observers.remove(o);
	}

	@Override
	public int countObservers() {
		return observers.size();
	}

	@Override
	public void deleteObserver(Observer o) {
		observers.remove(o);
	}

	@Override
	public void deleteObservers() {
		observers.clear();
	}

	@Override
	public boolean hasChanged() {
		return changed;
	}

	@Override
	public void notifyObservers() {
		doLog("trace", "notifyObservers()", "[{]", null, null);
		if (hasChanged())
			for (Observer obs : observers)
				obs.update(this, null);
		clearChanged();
		doLog("trace", "notifyObservers()", "[}]", null, null);
	}

	@Override
	public void notifyObservers(Object o) {
		doLog("trace", "notifyObservers(Object o)", "[{]", null, null);
		if (hasChanged())
			for (Observer obs : observers)
				obs.update(this, o);
		clearChanged();
		doLog("trace", "notifyObservers(Object o)", "[}]", null, null);
	}

	protected void clearChanged() {
		changed = false;
	}

	private void doLog(String level, String signature, String corchetes, String nombreVariable, Loggable loggable) {
		switch (level) {
		case "trace":
			if (LOG.isTraceEnabled())
				LOG.trace(toLogMsg(signature, corchetes, nombreVariable, loggable));
			break;
		case "debug":
			if (LOG.isDebugEnabled())
				LOG.debug(toLogMsg(signature, corchetes, nombreVariable, loggable));
			break;
		case "info":
			if (LOG.isInfoEnabled())
				LOG.info(toLogMsg(signature, corchetes, nombreVariable, loggable));
			break;
		case "warn":
			if (LOG.isWarnEnabled())
				LOG.warn(toLogMsg(signature, corchetes, nombreVariable, loggable));
			break;
		case "error":
			if (LOG.isErrorEnabled())
				LOG.error(toLogMsg(signature, corchetes, nombreVariable, loggable));
			break;
		}
	}

	private String toLogMsg(String signature, String corchetes, String nombreVariable, Loggable loggable) {
		StringBuffer s = new StringBuffer(signature);
		if (corchetes != null)
			s.append(" " + corchetes);
		if (nombreVariable != null)
			s.append(" " + "{ l = " + nombreVariable + " }");
		if (nombreVariable != null && loggable == null)
			s.append(nombreVariable + ": " + loggable);
		else if (nombreVariable != null && loggable != null)
			s.append(loggable.toLogString("l"));
		return s.toString();
	}

	@Override
	public String toLogString(String nombreVariable) {
		return this.toString();
	}

	@Override
	public void setDemoraInscripcionMinima(BigDecimal demoraInscripcionMinima) {
		this.demoraInscripcionMinima = demoraInscripcionMinima;
	}

	@Override
	@Immutable
	public BigDecimal getDemoraInscripcionMinima() {
		return demoraInscripcionMinima;
	}

	@Override
	public void setDemoraInscripcionMaxima(BigDecimal demoraInscripcionMaxima) {
		this.demoraInscripcionMaxima = demoraInscripcionMaxima;
	}

	@Override
	@Immutable
	public BigDecimal getDemoraInscripcionMaxima() {
		return demoraInscripcionMaxima;
	}

	@Override
	public void setLlegadaAlumnoMedia(BigDecimal llegadaAlumnoMedia) {
		this.llegadaAlumnoMedia = llegadaAlumnoMedia;
	}

	@Override
	@Immutable
	public BigDecimal getLlegadaAlumnoMedia() {
		return llegadaAlumnoMedia;
	}

	@Override
	public void setLlegadaTecnicoMedia(BigDecimal llegadaTecnicoMedia) {
		this.llegadaTecnicoMedia = llegadaTecnicoMedia;
	}

	@Override
	@Immutable
	public BigDecimal getLlegadaTecnicoMedia() {
		return llegadaTecnicoMedia;
	}

	@Override
	public void setLlegadaTecnicoError(BigDecimal llegadaTecnicoError) {
		this.llegadaTecnicoError = llegadaTecnicoError;
	}

	@Override
	@Immutable
	public BigDecimal getLlegadaTecnicoError() {
		return llegadaTecnicoError;
	}

	@Override
	public TablaIntegracionNumerica getTablaIntegracionNumerica(BigDecimal key) {
		return tablasDeIntegracionNumerica.get(key);
	}

}
