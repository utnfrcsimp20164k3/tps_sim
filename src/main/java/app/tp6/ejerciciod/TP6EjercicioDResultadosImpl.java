package app.tp6.ejerciciod;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.HashSet;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import app.Loggable;
import app.tp5ytp6.Maquina;
import app.utils.DetalleTabla;
import app.utils.TablaVectorEstados;

@Component
public class TP6EjercicioDResultadosImpl implements TP6EjercicioDResultados {

	static final Logger LOG = LoggerFactory.getLogger(TP6EjercicioDResultadosImpl.class);
	private DetalleTabla detalleTabla;
	@Autowired
	private TablaVectorEstados tablaVectorEstados;
	@Autowired
	private ApplicationContext applicationContext;
	private Set<Maquina> maquinas;

	public TP6EjercicioDResultadosImpl() {
		super();
	}

	@Bean
	@Scope(value = "prototype")
	static public TP6EjercicioDResultados tP6EjercicioDResultados() {
		return new TP6EjercicioDResultadosImpl();
	}

	@Override
	public boolean hasDetalleTablaFinal() {
		return detalleTabla != null;
	}

	@Override
	public void calcular(DetalleTabla detalleTabla) {
		this.detalleTabla = detalleTabla;
		construirTabla();
	}

	@Override
	public void setMaquinas(Set<Maquina> maquinas) {
		this.maquinas = maquinas;
	}

	@Override
	public TablaVectorEstados getTabla() {
		return tablaVectorEstados;
	}

	private void construirTabla() {
		doLog("trace", "construirTabla()", "[{]", null, null);
		DetalleTabla detalle = (DetalleTabla) applicationContext.getBean("detalleTabla");
		BigDecimal ratio = new BigDecimal("0");
		for (Maquina maquina : maquinas) {
			tablaVectorEstados.registrarPermanente(maquina);
			Set<Object> aux = new HashSet<>();
			aux.add(maquina);
			aux.add("Inscriptos");
			tablaVectorEstados.crearColumna(aux);
			aux = new HashSet<>();
			aux.add(maquina);
			aux.add("Inscriptos");
			detalle.getValores().put(aux, maquina.getContadorInscripciones());
			aux = new HashSet<>();
			aux.add(maquina);
			aux.add("Tiempo [']");
			tablaVectorEstados.crearColumna(aux);
			aux = new HashSet<>();
			aux.add(maquina);
			aux.add("Tiempo [']");
			detalle.getValores().put(aux, maquina.getTiempoEnServicio().setScale(2, RoundingMode.HALF_UP));
			aux = new HashSet<>();
			aux.add(maquina);
			aux.add("Tiempo [h]");
			tablaVectorEstados.crearColumna(aux);
			aux = new HashSet<>();
			aux.add(maquina);
			aux.add("Tiempo [h]");
			detalle.getValores().put(aux, maquina.getTiempoEnServicio()
					.divide(new BigDecimal("60"), 2, RoundingMode.HALF_UP).setScale(2, RoundingMode.HALF_UP));
			aux = new HashSet<>();
			aux.add(maquina);
			aux.add("Inscriptos / h");
			tablaVectorEstados.crearColumna(aux);
			aux = new HashSet<>();
			aux.add(maquina);
			aux.add("Inscriptos / h");
			ratio = ratio.add(new BigDecimal(maquina.getContadorInscripciones())
					.divide(maquina.getTiempoEnServicio().divide(new BigDecimal("60"), 2, RoundingMode.HALF_UP), 2,
							RoundingMode.HALF_UP)
					.setScale(2, RoundingMode.HALF_UP));
			detalle.getValores()
					.put(aux,
							new BigDecimal(maquina.getContadorInscripciones())
									.divide(maquina.getTiempoEnServicio().divide(new BigDecimal("60"), 2,
											RoundingMode.HALF_UP), 2, RoundingMode.HALF_UP)
									.setScale(2, RoundingMode.HALF_UP));
		}
		tablaVectorEstados.crearColumna("Contador de alumnos que llegaron");
		detalle.getValores().put("Contador de alumnos que llegaron",
				detalleTabla.getValores().get("Contador de alumnos que llegaron"));
		tablaVectorEstados.crearColumna("Contador de alumnos que se fueron");
		detalle.getValores().put("Contador de alumnos que se fueron",
				detalleTabla.getValores().get("Contador de alumnos que se fueron"));
		tablaVectorEstados.crearColumna("Porcentaje de alumnos que llegaron");
		detalle.getValores().put("Porcentaje de alumnos que llegaron",
				new BigDecimal((Long) detalleTabla.getValores().get("Contador de alumnos que se fueron"))
						.divide(new BigDecimal(
								(Long) detalleTabla.getValores().get("Contador de alumnos que llegaron")), 4,
								RoundingMode.HALF_UP)
						.multiply(new BigDecimal("100")).setScale(2, RoundingMode.HALF_UP));
		detalle.getValores().put("Inscriptos / h", ratio);
		tablaVectorEstados.getDetalles().add(detalle);
		doLog("trace", "construirTabla()", "[}]", null, null);
	}

	private void doLog(String level, String signature, String corchetes, String nombreVariable, Loggable loggable) {
		switch (level) {
		case "trace":
			if (LOG.isTraceEnabled())
				LOG.trace(toLogMsg(signature, corchetes, nombreVariable, loggable));
			break;
		case "debug":
			if (LOG.isDebugEnabled())
				LOG.debug(toLogMsg(signature, corchetes, nombreVariable, loggable));
			break;
		case "info":
			if (LOG.isInfoEnabled())
				LOG.info(toLogMsg(signature, corchetes, nombreVariable, loggable));
			break;
		case "warn":
			if (LOG.isWarnEnabled())
				LOG.warn(toLogMsg(signature, corchetes, nombreVariable, loggable));
			break;
		case "error":
			if (LOG.isErrorEnabled())
				LOG.error(toLogMsg(signature, corchetes, nombreVariable, loggable));
			break;
		}
	}

	private String toLogMsg(String signature, String corchetes, String nombreVariable, Loggable loggable) {
		StringBuffer s = new StringBuffer(signature);
		if (corchetes != null)
			s.append(" " + corchetes);
		if (nombreVariable != null)
			s.append(" " + "{ l = " + nombreVariable + " }");
		if (nombreVariable != null && loggable == null)
			s.append(nombreVariable + ": " + loggable);
		else if (nombreVariable != null && loggable != null)
			s.append(loggable.toLogString("l"));
		return s.toString();
	}
}
