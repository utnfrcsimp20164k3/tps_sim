package app.tp6.ejerciciod.presentacion;

import org.zkoss.zul.ListModelList;

import app.utils.TablaVectorEstados;

public interface TablaVectoresEstadoMTP6EjercicioD {

	void setTabla(TablaVectorEstados tablaVectorEstados);
	
	Object getEncabezadoA();

	Object getEncabezadoB();

	Object getEncabezadoC();

	Object getEncabezadoD();

	Object getEncabezadoE();

	Object getEncabezadoF();

	Object getEncabezadoG();

	Object getEncabezadoH();

	Object getEncabezadoI();

	Object getEncabezadoJ();

	Object getEncabezadoK();

	Object getEncabezadoL();

	Object getEncabezadoM();

	Object getEncabezadoN();

	Object getEncabezadoO();

	Object getEncabezadoP();

	Object getEncabezadoQ();

	Object getEncabezadoR();

	Object getEncabezadoS();

	Object getEncabezadoT();

	Object getEncabezadoU();

	Object getEncabezadoV();

	Object getEncabezadoW();

	Object getEncabezadoX();

	Object getEncabezadoY();

	Object getEncabezadoZ();

	Object getEncabezadoAA();

	Object getEncabezadoAB();

	Object getEncabezadoAC();

	Object getEncabezadoAD();

	Object getEncabezadoAE();

	Object getEncabezadoAF();

	Object getEncabezadoAG();

	Object getEncabezadoAH();

	Object getEncabezadoAI();

	Object getEncabezadoAJ();

	Object getEncabezadoAK();

	Object getEncabezadoAL();

	ListModelList<TablaVectoresEstadoDetalleMTP6EjercicioD> getDetalles();

	void clear();

}
