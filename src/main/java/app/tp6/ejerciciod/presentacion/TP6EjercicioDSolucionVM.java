package app.tp6.ejerciciod.presentacion;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.DependsOn;
import org.zkoss.bind.annotation.GlobalCommand;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.bind.annotation.SmartNotifyChange;
import org.zkoss.spring.SpringUtil;
import org.zkoss.zk.ui.select.annotation.VariableResolver;
import org.zkoss.zk.ui.select.annotation.WireVariable;

import app.Loggable;
import app.presentacion.ConversorDeNumeroIteraciones;
import app.presentacion.InputValidator;
import app.presentacion.Observable;
import app.presentacion.Observer;
import app.presentacion.ResourceBundle;
import app.presentacion.ResourceIndex;
import app.presentacion.SidebarTreeItem;
import app.tp5ytp6.presentacion.ConversorDeHoras;
import app.tp5ytp6.presentacion.ConversorDeMinutos;
import app.tp5ytp6.presentacion.ConversorDeSegundos;
import app.tp5ytp6.presentacion.IngresarCantidadDeTiempoASimularValidator;
import app.tp5ytp6.presentacion.IngresarDemoraInscripcionMaximaValidator;
import app.tp5ytp6.presentacion.IngresarDemoraInscripcionMinimaValidator;
import app.tp5ytp6.presentacion.IngresarDemoraMantenimientoDesviacionValidator;
import app.tp5ytp6.presentacion.IngresarDemoraMantenimientoMediaValidator;
import app.tp5ytp6.presentacion.IngresarHoraDesdeAMostrarValidator;
import app.tp5ytp6.presentacion.IngresarLlegadaAlumnoMediaValidator;
import app.tp5ytp6.presentacion.IngresarLlegadaTecnicoErrorValidator;
import app.tp5ytp6.presentacion.IngresarLlegadaTecnicoMediaValidator;
import app.tp5ytp6.presentacion.IngresarNumeroIteracionesAMostrarValidator;
import app.tp6.ejerciciod.TP6EjercicioD;

@VariableResolver(org.zkoss.zkplus.spring.DelegatingVariableResolver.class)
public class TP6EjercicioDSolucionVM implements Observer {

	private static final String TILDE_SRC = "./images/check_mark.png";
	private static final String ADVERTENCIA_SRC = "./images/warning.png";
	private static final String ASTERISCO_SRC = "./images/asterisk_red.png";
	static final Logger LOG = LoggerFactory.getLogger(TP6EjercicioDSolucionVM.class);
	@WireVariable
	private ResourceIndex resourceIndex;
	private ResourceBundle rB;
	@WireVariable
	private InputValidator ingresarDemoraInscripcionMinimaValidator;
	@WireVariable
	private InputValidator ingresarDemoraInscripcionMaximaValidator;
	@WireVariable
	private InputValidator ingresarLlegadaAlumnoMediaValidator;
	@WireVariable
	private InputValidator ingresarLlegadaTecnicoMediaValidator;
	@WireVariable
	private InputValidator ingresarLlegadaTecnicoErrorValidator;
	@WireVariable
	private InputValidator ingresarDemoraMantenimientoMediaValidator;
	@WireVariable
	private InputValidator ingresarDemoraMantenimientoDesviacionValidator;
	@WireVariable
	private InputValidator ingresarCantidadDeTiempoASimularValidator;
	@WireVariable
	private InputValidator ingresarHoraDesdeAMostrarValidator;
	@WireVariable
	private InputValidator ingresarNumeroIteracionesAMostrarValidator;
	private String ingresarDemoraInscripcionMinimaIconSrc, ingresarDemoraInscripcionMaximaIconSrc,
			ingresarLlegadaAlumnoMediaIconSrc, ingresarLlegadaTecnicoMediaIconSrc, ingresarLlegadaTecnicoErrorIconSrc,
			ingresarDemoraMantenimientoMediaIconSrc, ingresarDemoraMantenimientoDesviacionIconSrc,
			ingresarCantidadDeTiempoASimularIconSrc, ingresarHoraDesdeAMostrarIconSrc,
			ingresarNumeroIteracionesAMostrarIconSrc;
	private boolean demoraInscripcionMinimaValidada, demoraInscripcionMaximaValidada, llegadaAlumnoMediaValidada,
			llegadaTecnicoMediaValidada, llegadaTecnicoErrorValidada, demoraMantenimientoMediaValidada,
			demoraMantenimientoDesviacionValidada, cantidadDeTiempoASimularValidada, horaDesdeAMostrarValidada,
			numeroIteracionesAMostrarValidada;
	@WireVariable
	private TP6EjercicioD tP6EjercicioD;
	@WireVariable
	private ConversorDeHoras conversorDeHoras;
	@WireVariable
	private ConversorDeMinutos conversorDeMinutos;
	@WireVariable
	private ConversorDeSegundos conversorDeSegundos;
	@WireVariable
	private ConversorDeNumeroIteraciones conversorDeNumeroIteraciones;
	private boolean mostrandoResultados;

	@Init
	public void initSetup() {
		rB = resourceIndex.getResourceBundle("pqfjyh");
		ingresarDemoraInscripcionMinimaValidator.addObserver(this);
		ingresarDemoraInscripcionMaximaValidator.addObserver(this);
		ingresarLlegadaAlumnoMediaValidator.addObserver(this);
		ingresarLlegadaTecnicoMediaValidator.addObserver(this);
		ingresarLlegadaTecnicoErrorValidator.addObserver(this);
		ingresarDemoraMantenimientoMediaValidator.addObserver(this);
		ingresarDemoraMantenimientoDesviacionValidator.addObserver(this);
		ingresarCantidadDeTiempoASimularValidator.addObserver(this);
		ingresarHoraDesdeAMostrarValidator.addObserver(this);
		ingresarNumeroIteracionesAMostrarValidator.addObserver(this);
		inicializarIconosValidaciones();
		inicializarValidaciones();
	}

	private void inicializarValidaciones() {
		setDemoraInscripcionMinimaValidada(true);
		setDemoraInscripcionMaximaValidada(true);
		setLlegadaAlumnoMediaValidada(true);
		setLlegadaTecnicoMediaValidada(true);
		setLlegadaTecnicoErrorValidada(true);
		setDemoraMantenimientoMediaValidada(true);
		setDemoraMantenimientoDesviacionValidada(true);
		setCantidadDeTiempoASimularValidada(true);
		setHoraDesdeAMostrarValidada(true);
		setNumeroIteracionesAMostrarValidada(true);
	}

	private void inicializarIconosValidaciones() {
		setIngresarDemoraInscripcionMinimaIconSrc(TILDE_SRC);
		setIngresarDemoraInscripcionMaximaIconSrc(TILDE_SRC);
		setIngresarLlegadaAlumnoMediaIconSrc(TILDE_SRC);
		setIngresarLlegadaTecnicoMediaIconSrc(TILDE_SRC);
		setIngresarLlegadaTecnicoErrorIconSrc(TILDE_SRC);
		setIngresarDemoraMantenimientoMediaIconSrc(TILDE_SRC);
		setIngresarDemoraMantenimientoDesviacionIconSrc(TILDE_SRC);
		setIngresarCantidadDeTiempoASimularIconSrc(TILDE_SRC);
		setIngresarHoraDesdeAMostrarIconSrc(TILDE_SRC);
		setIngresarNumeroIteracionesAMostrarIconSrc(TILDE_SRC);
	}

	@GlobalCommand
	@SmartNotifyChange({ "demoraInscripcionMinimaValidada", "ingresarDemoraInscripcionMinimaIconSrc",
			"demoraInscripcionMaximaValidada", "ingresarDemoraInscripcionMaximaIconSrc", "llegadaAlumnoMediaValidada",
			"ingresarLlegadaAlumnoMediaIconSrc", "llegadaTecnicoMediaValidada", "ingresarLlegadaTecnicoMediaIconSrc",
			"llegadaTecnicoErrorValidada", "ingresarLlegadaTecnicoErrorIconSrc", "demoraMantenimientoMediaValidada",
			"ingresarDemoraMantenimientoMediaIconSrc", "demoraMantenimientoDesviacionValidada",
			"ingresarDemoraMantenimientoDesviacionIconSrc", "cantidadDeTiempoASimularValidada",
			"ingresarCantidadDeTiempoASimularIconSrc", "horaDesdeAMostrarValidada", "ingresarHoraDesdeAMostrarIconSrc",
			"numeroIteracionesAMostrarValidada", "ingresarNumeroIteracionesAMostrarIconSrc" })
	public void showContent(@BindingParam("page") SidebarTreeItem page) {
		if (page == null || page.getUri() == null || !page.getUri().equals(rB.getZul()))
			clear();
	}

	@Command
	@SmartNotifyChange({ "mostrandoResultados", "demoraInscripcionMinimaValidada",
			"ingresarDemoraInscripcionMinimaIconSrc", "demoraInscripcionMaximaValidada",
			"ingresarDemoraInscripcionMaximaIconSrc", "llegadaAlumnoMediaValidada", "ingresarLlegadaAlumnoMediaIconSrc",
			"llegadaTecnicoMediaValidada", "ingresarLlegadaTecnicoMediaIconSrc", "llegadaTecnicoErrorValidada",
			"ingresarLlegadaTecnicoErrorIconSrc", "demoraMantenimientoMediaValidada",
			"ingresarDemoraMantenimientoMediaIconSrc", "demoraMantenimientoDesviacionValidada",
			"ingresarDemoraMantenimientoDesviacionIconSrc", "cantidadDeTiempoASimularValidada",
			"ingresarCantidadDeTiempoASimularIconSrc", "horaDesdeAMostrarValidada", "ingresarHoraDesdeAMostrarIconSrc",
			"numeroIteracionesAMostrarValidada", "ingresarNumeroIteracionesAMostrarIconSrc" })
	public void clear() {
		tP6EjercicioD.reiniciar();
		setDemoraInscripcionMinimaValidada(false);
		setDemoraInscripcionMaximaValidada(false);
		setLlegadaAlumnoMediaValidada(false);
		setLlegadaTecnicoMediaValidada(false);
		setLlegadaTecnicoErrorValidada(false);
		setDemoraMantenimientoMediaValidada(false);
		setDemoraMantenimientoDesviacionValidada(false);
		setCantidadDeTiempoASimularValidada(false);
		setHoraDesdeAMostrarValidada(false);
		setNumeroIteracionesAMostrarValidada(false);
		setMostrandoResultados(false);
		inicializarIconosValidaciones();
		inicializarValidaciones();
	}

	@Command
	@NotifyChange({ "tp6EjercicioD", "resultadosSrc", "mostrandoResultados" })
	public void correr() throws Exception {
		doLog("trace", "correr()", "[{]", null, null);
		tP6EjercicioD.solucionar();
		setMostrandoResultados(true);
		doLog("trace", "correr()", "[}]", null, null);
	}

	@Command
	@SmartNotifyChange("mostrandoResultados")
	public void limpiarResultados() {
		setMostrandoResultados(false);
		BindUtils.postGlobalCommand(null, null, "limpiarTabla", null);
		BindUtils.postGlobalCommand(null, null, "limpiarResumenFinal", null);
	}

	@Command
	public void validarParametrosDeLaSalida(@BindingParam("form") TP6EjercicioD form) {
	}

	@Command
	public void notificarCambioParametroDeLaSalida() {
	}

	// "Validación"
	@Override
	public void update(Observable o, Object arg) {
		if (o instanceof IngresarDemoraInscripcionMinimaValidator)
			updateValidacion((IngresarDemoraInscripcionMinimaValidator) o);
		if (o instanceof IngresarDemoraInscripcionMaximaValidator)
			updateValidacion((IngresarDemoraInscripcionMaximaValidator) o);
		if (o instanceof IngresarLlegadaAlumnoMediaValidator)
			updateValidacion((IngresarLlegadaAlumnoMediaValidator) o);
		if (o instanceof IngresarLlegadaTecnicoMediaValidator)
			updateValidacion((IngresarLlegadaTecnicoMediaValidator) o);
		if (o instanceof IngresarLlegadaTecnicoErrorValidator)
			updateValidacion((IngresarLlegadaTecnicoErrorValidator) o);
		if (o instanceof IngresarDemoraMantenimientoMediaValidator)
			updateValidacion((IngresarDemoraMantenimientoMediaValidator) o);
		if (o instanceof IngresarDemoraMantenimientoDesviacionValidator)
			updateValidacion((IngresarDemoraMantenimientoDesviacionValidator) o);
		if (o instanceof IngresarCantidadDeTiempoASimularValidator)
			updateValidacion((IngresarCantidadDeTiempoASimularValidator) o);
		if (o instanceof IngresarHoraDesdeAMostrarValidator)
			updateValidacion((IngresarHoraDesdeAMostrarValidator) o);
		if (o instanceof IngresarNumeroIteracionesAMostrarValidator)
			updateValidacion((IngresarNumeroIteracionesAMostrarValidator) o);
	}

	private void updateValidacion(IngresarDemoraInscripcionMinimaValidator val) {
		String iconoSrc = registrarResultadoValidacion((InputValidator) val);
		ingresarDemoraInscripcionMinimaIconSrc = iconoSrc;
		if (iconoSrc != null && iconoSrc.equals(TILDE_SRC) || iconoSrc == null)
			setDemoraInscripcionMinimaValidada(true);
		else
			setDemoraInscripcionMinimaValidada(false);
		BindUtils.postNotifyChange(null, null, this, "ingresarDemoraInscripcionMinimaIconSrc");
		BindUtils.postNotifyChange(null, null, this, "demoraInscripcionMinimaValidada");
	}

	private void updateValidacion(IngresarDemoraInscripcionMaximaValidator val) {
		String iconoSrc = registrarResultadoValidacion((InputValidator) val);
		ingresarDemoraInscripcionMaximaIconSrc = iconoSrc;
		if (iconoSrc != null && iconoSrc.equals(TILDE_SRC) || iconoSrc == null)
			setDemoraInscripcionMaximaValidada(true);
		else
			setDemoraInscripcionMaximaValidada(false);
		BindUtils.postNotifyChange(null, null, this, "ingresarDemoraInscripcionMaximaIconSrc");
		BindUtils.postNotifyChange(null, null, this, "demoraInscripcionMaximaValidada");
	}

	private void updateValidacion(IngresarLlegadaAlumnoMediaValidator val) {
		String iconoSrc = registrarResultadoValidacion((InputValidator) val);
		ingresarLlegadaAlumnoMediaIconSrc = iconoSrc;
		if (iconoSrc != null && iconoSrc.equals(TILDE_SRC) || iconoSrc == null)
			setLlegadaAlumnoMediaValidada(true);
		else
			setLlegadaAlumnoMediaValidada(false);
		BindUtils.postNotifyChange(null, null, this, "ingresarLlegadaAlumnoMediaIconSrc");
		BindUtils.postNotifyChange(null, null, this, "llegadaAlumnoMediaValidada");
	}

	private void updateValidacion(IngresarLlegadaTecnicoMediaValidator val) {
		String iconoSrc = registrarResultadoValidacion((InputValidator) val);
		ingresarLlegadaTecnicoMediaIconSrc = iconoSrc;
		if (iconoSrc != null && iconoSrc.equals(TILDE_SRC) || iconoSrc == null)
			setLlegadaTecnicoMediaValidada(true);
		else
			setLlegadaTecnicoMediaValidada(false);
		BindUtils.postNotifyChange(null, null, this, "ingresarLlegadaTecnicoMediaIconSrc");
		BindUtils.postNotifyChange(null, null, this, "llegadaTecnicoMediaValidada");
	}

	private void updateValidacion(IngresarLlegadaTecnicoErrorValidator val) {
		String iconoSrc = registrarResultadoValidacion((InputValidator) val);
		ingresarLlegadaTecnicoErrorIconSrc = iconoSrc;
		if (iconoSrc != null && iconoSrc.equals(TILDE_SRC) || iconoSrc == null)
			setLlegadaTecnicoErrorValidada(true);
		else
			setLlegadaTecnicoErrorValidada(false);
		BindUtils.postNotifyChange(null, null, this, "ingresarLlegadaTecnicoErrorIconSrc");
		BindUtils.postNotifyChange(null, null, this, "llegadaTecnicoErrorValidada");
	}

	private void updateValidacion(IngresarDemoraMantenimientoMediaValidator val) {
		String iconoSrc = registrarResultadoValidacion((InputValidator) val);
		ingresarDemoraMantenimientoMediaIconSrc = iconoSrc;
		if (iconoSrc != null && iconoSrc.equals(TILDE_SRC) || iconoSrc == null)
			setDemoraMantenimientoMediaValidada(true);
		else
			setDemoraMantenimientoMediaValidada(false);
		BindUtils.postNotifyChange(null, null, this, "ingresarDemoraMantenimientoMediaIconSrc");
		BindUtils.postNotifyChange(null, null, this, "demoraMantenimientoMediaValidada");
	}

	private void updateValidacion(IngresarDemoraMantenimientoDesviacionValidator val) {
		String iconoSrc = registrarResultadoValidacion((InputValidator) val);
		ingresarDemoraMantenimientoDesviacionIconSrc = iconoSrc;
		if (iconoSrc != null && iconoSrc.equals(TILDE_SRC) || iconoSrc == null)
			setDemoraMantenimientoDesviacionValidada(true);
		else
			setDemoraMantenimientoDesviacionValidada(false);
		BindUtils.postNotifyChange(null, null, this, "ingresarDemoraMantenimientoDesviacionIconSrc");
		BindUtils.postNotifyChange(null, null, this, "demoraMantenimientoDesviacionValidada");
	}

	private void updateValidacion(IngresarCantidadDeTiempoASimularValidator val) {
		String iconoSrc = registrarResultadoValidacion((InputValidator) val);
		ingresarCantidadDeTiempoASimularIconSrc = iconoSrc;
		if (iconoSrc != null && iconoSrc.equals(TILDE_SRC) || iconoSrc == null)
			setCantidadDeTiempoASimularValidada(true);
		else
			setCantidadDeTiempoASimularValidada(false);
		BindUtils.postNotifyChange(null, null, this, "ingresarCantidadDeTiempoASimularIconSrc");
		BindUtils.postNotifyChange(null, null, this, "cantidadDeTiempoASimularValidada");
	}

	private void updateValidacion(IngresarHoraDesdeAMostrarValidator val) {
		String iconoSrc = registrarResultadoValidacion((InputValidator) val);
		ingresarHoraDesdeAMostrarIconSrc = iconoSrc;
		if (iconoSrc != null && iconoSrc.equals(TILDE_SRC) || iconoSrc == null)
			setHoraDesdeAMostrarValidada(true);
		else
			setHoraDesdeAMostrarValidada(false);
		BindUtils.postNotifyChange(null, null, this, "ingresarHoraDesdeAMostrarIconSrc");
		BindUtils.postNotifyChange(null, null, this, "horaDesdeAMostrarValidada");
	}

	private void updateValidacion(IngresarNumeroIteracionesAMostrarValidator val) {
		String iconoSrc = registrarResultadoValidacion((InputValidator) val);
		ingresarNumeroIteracionesAMostrarIconSrc = iconoSrc;
		if (iconoSrc != null && iconoSrc.equals(TILDE_SRC) || iconoSrc == null)
			setNumeroIteracionesAMostrarValidada(true);
		else
			setNumeroIteracionesAMostrarValidada(false);
		BindUtils.postNotifyChange(null, null, this, "ingresarNumeroIteracionesAMostrarIconSrc");
		BindUtils.postNotifyChange(null, null, this, "numeroIteracionesAMostrarValidada");
	}

	private String registrarResultadoValidacion(InputValidator val) {
		String res = null;
		if (!val.isIncorrecta() && !val.isNula())
			res = TILDE_SRC;
		else if (val.isIncorrecta())
			res = ADVERTENCIA_SRC;
		else if (val.isNula() && val.isObligatoria())
			res = ASTERISCO_SRC;
		else if (val.isNula() && !val.isObligatoria())
			res = null;
		return res;
	}

	@DependsOn({ "demoraInscripcionMinimaValidada" })
	public boolean isValidacionOk() {
		return (demoraInscripcionMinimaValidada || (!getIngresarDemoraInscripcionMinimaValidator().isObligatoria()
				&& getIngresarDemoraInscripcionMinimaIconSrc() == null))
				&& (demoraInscripcionMaximaValidada || (!getIngresarDemoraInscripcionMaximaValidator().isObligatoria()
						&& getIngresarDemoraInscripcionMaximaIconSrc() == null))
				&& (llegadaAlumnoMediaValidada || (!getIngresarLlegadaAlumnoMediaValidator().isObligatoria()
						&& getIngresarLlegadaAlumnoMediaIconSrc() == null))
				&& (llegadaTecnicoMediaValidada || (!getIngresarLlegadaTecnicoMediaValidator().isObligatoria()
						&& getIngresarLlegadaTecnicoMediaIconSrc() == null))
				&& (llegadaTecnicoErrorValidada || (!getIngresarLlegadaTecnicoErrorValidator().isObligatoria()
						&& getIngresarLlegadaTecnicoErrorIconSrc() == null))
				&& (demoraMantenimientoMediaValidada || (!getIngresarDemoraMantenimientoMediaValidator().isObligatoria()
						&& getIngresarDemoraMantenimientoMediaIconSrc() == null))
				&& (demoraMantenimientoDesviacionValidada
						|| (!getIngresarDemoraMantenimientoDesviacionValidator().isObligatoria()
								&& getIngresarDemoraMantenimientoDesviacionIconSrc() == null))
				&& (cantidadDeTiempoASimularValidada || (!getIngresarCantidadDeTiempoASimularValidator().isObligatoria()
						&& getIngresarCantidadDeTiempoASimularIconSrc() == null))
				&& (horaDesdeAMostrarValidada || (!getIngresarHoraDesdeAMostrarValidator().isObligatoria()
						&& getIngresarHoraDesdeAMostrarIconSrc() == null))
				&& (numeroIteracionesAMostrarValidada || (!getIngresarNumeroIteracionesAMostrarValidator().isObligatoria()
						&& getIngresarNumeroIteracionesAMostrarIconSrc() == null))
				;
	}

	public InputValidator getIngresarDemoraInscripcionMinimaValidator() {
		return ingresarDemoraInscripcionMinimaValidator;
	}

	public InputValidator getIngresarDemoraInscripcionMaximaValidator() {
		return ingresarDemoraInscripcionMaximaValidator;
	}

	public InputValidator getIngresarLlegadaAlumnoMediaValidator() {
		return ingresarLlegadaAlumnoMediaValidator;
	}

	public InputValidator getIngresarLlegadaTecnicoMediaValidator() {
		return ingresarLlegadaTecnicoMediaValidator;
	}

	public InputValidator getIngresarLlegadaTecnicoErrorValidator() {
		return ingresarLlegadaTecnicoErrorValidator;
	}

	public InputValidator getIngresarDemoraMantenimientoMediaValidator() {
		return ingresarDemoraMantenimientoMediaValidator;
	}

	public InputValidator getIngresarDemoraMantenimientoDesviacionValidator() {
		return ingresarDemoraMantenimientoDesviacionValidator;
	}

	public InputValidator getIngresarCantidadDeTiempoASimularValidator() {
		return ingresarCantidadDeTiempoASimularValidator;
	}

	public InputValidator getIngresarHoraDesdeAMostrarValidator() {
		return ingresarHoraDesdeAMostrarValidator;
	}

	public InputValidator getIngresarNumeroIteracionesAMostrarValidator() {
		return ingresarNumeroIteracionesAMostrarValidator;
	}

	@DependsOn("demoraInscripcionMinimaValidada")
	public String getDefaultTooltiptextIngresarDemoraInscripcionMinima() {
		String res = null;
		if (getIngresarDemoraInscripcionMinimaValidator().isObligatoria() && !demoraInscripcionMinimaValidada)
			res = "Este campo es obligatorio.";
		else if (demoraInscripcionMinimaValidada)
			res = "Probabilidad válida.";
		return res;
	}

	@DependsOn("demoraInscripcionMaximaValidada")
	public String getDefaultTooltiptextIngresarDemoraInscripcionMaxima() {
		String res = null;
		if (getIngresarDemoraInscripcionMaximaValidator().isObligatoria() && !demoraInscripcionMaximaValidada)
			res = "Este campo es obligatorio.";
		else if (demoraInscripcionMaximaValidada)
			res = "Probabilidad válida.";
		return res;
	}

	@DependsOn("llegadaAlumnoMediaValidada")
	public String getDefaultTooltiptextIngresarLlegadaAlumnoMedia() {
		String res = null;
		if (getIngresarLlegadaAlumnoMediaValidator().isObligatoria() && !llegadaAlumnoMediaValidada)
			res = "Este campo es obligatorio.";
		else if (llegadaAlumnoMediaValidada)
			res = "Probabilidad válida.";
		return res;
	}

	@DependsOn("llegadaTecnicoMediaValidada")
	public String getDefaultTooltiptextIngresarLlegadaTecnicoMedia() {
		String res = null;
		if (getIngresarLlegadaTecnicoMediaValidator().isObligatoria() && !llegadaTecnicoMediaValidada)
			res = "Este campo es obligatorio.";
		else if (llegadaTecnicoMediaValidada)
			res = "Probabilidad válida.";
		return res;
	}

	@DependsOn("llegadaTecnicoErrorValidada")
	public String getDefaultTooltiptextIngresarLlegadaTecnicoError() {
		String res = null;
		if (getIngresarLlegadaTecnicoErrorValidator().isObligatoria() && !llegadaTecnicoErrorValidada)
			res = "Este campo es obligatorio.";
		else if (llegadaTecnicoErrorValidada)
			res = "Probabilidad válida.";
		return res;
	}

	@DependsOn("demoraMantenimientoMediaValidada")
	public String getDefaultTooltiptextIngresarDemoraMantenimientoMedia() {
		String res = null;
		if (getIngresarDemoraMantenimientoMediaValidator().isObligatoria() && !demoraMantenimientoMediaValidada)
			res = "Este campo es obligatorio.";
		else if (demoraMantenimientoMediaValidada)
			res = "Probabilidad válida.";
		return res;
	}

	@DependsOn("demoraMantenimientoDesviacionValidada")
	public String getDefaultTooltiptextIngresarDemoraMantenimientoDesviacion() {
		String res = null;
		if (getIngresarDemoraMantenimientoDesviacionValidator().isObligatoria()
				&& !demoraMantenimientoDesviacionValidada)
			res = "Este campo es obligatorio.";
		else if (demoraMantenimientoDesviacionValidada)
			res = "Probabilidad válida.";
		return res;
	}

	@DependsOn("cantidadDeTiempoASimularValidada")
	public String getDefaultTooltiptextIngresarCantidadDeTiempoASimular() {
		String res = null;
		if (getIngresarCantidadDeTiempoASimularValidator().isObligatoria() && !cantidadDeTiempoASimularValidada)
			res = "Este campo es obligatorio.";
		else if (cantidadDeTiempoASimularValidada)
			res = "Probabilidad válida.";
		return res;
	}

	@DependsOn("horaDesdeAMostrarValidada")
	public String getDefaultTooltiptextIngresarHoraDesdeAMostrar() {
		String res = null;
		if (getIngresarHoraDesdeAMostrarValidator().isObligatoria() && !horaDesdeAMostrarValidada)
			res = "Este campo es obligatorio.";
		else if (horaDesdeAMostrarValidada)
			res = "Probabilidad válida.";
		return res;
	}

	@DependsOn("numeroIteracionesAMostrarValidada")
	public String getDefaultTooltiptextIngresarNumeroIteracionesAMostrar() {
		String res = null;
		if (getIngresarNumeroIteracionesAMostrarValidator().isObligatoria() && !numeroIteracionesAMostrarValidada)
			res = "Este campo es obligatorio.";
		else if (numeroIteracionesAMostrarValidada)
			res = "Probabilidad válida.";
		return res;
	}

	public String getIngresarDemoraInscripcionMinimaIconSrc() {
		return ingresarDemoraInscripcionMinimaIconSrc;
	}

	public String getIngresarDemoraInscripcionMaximaIconSrc() {
		return ingresarDemoraInscripcionMaximaIconSrc;
	}

	public String getIngresarLlegadaAlumnoMediaIconSrc() {
		return ingresarLlegadaAlumnoMediaIconSrc;
	}

	public String getIngresarLlegadaTecnicoMediaIconSrc() {
		return ingresarLlegadaTecnicoMediaIconSrc;
	}

	public String getIngresarLlegadaTecnicoErrorIconSrc() {
		return ingresarLlegadaTecnicoErrorIconSrc;
	}

	public String getIngresarDemoraMantenimientoMediaIconSrc() {
		return ingresarDemoraMantenimientoMediaIconSrc;
	}

	public String getIngresarDemoraMantenimientoDesviacionIconSrc() {
		return ingresarDemoraMantenimientoDesviacionIconSrc;
	}

	public String getIngresarCantidadDeTiempoASimularIconSrc() {
		return ingresarCantidadDeTiempoASimularIconSrc;
	}

	public String getIngresarHoraDesdeAMostrarIconSrc() {
		return ingresarHoraDesdeAMostrarIconSrc;
	}

	public String getIngresarNumeroIteracionesAMostrarIconSrc() {
		return ingresarNumeroIteracionesAMostrarIconSrc;
	}

	public boolean isDemoraInscripcionMinimaValidada() {
		return demoraInscripcionMinimaValidada;
	}

	public boolean isDemoraInscripcionMaximaValidada() {
		return demoraInscripcionMaximaValidada;
	}

	public boolean isLlegadaAlumnoMediaValidada() {
		return llegadaAlumnoMediaValidada;
	}

	public boolean isLlegadaTecnicoMediaValidada() {
		return llegadaTecnicoMediaValidada;
	}

	public boolean isLlegadaTecnicoErrorValidada() {
		return llegadaTecnicoErrorValidada;
	}

	public boolean isDemoraMantenimientoMediaValidada() {
		return demoraMantenimientoMediaValidada;
	}

	public boolean isDemoraMantenimientoDesviacionValidada() {
		return demoraMantenimientoDesviacionValidada;
	}

	public boolean isCantidadDeTiempoASimularValidada() {
		return cantidadDeTiempoASimularValidada;
	}

	public boolean isHoraDesdeAMostrarValidada() {
		return horaDesdeAMostrarValidada;
	}

	public boolean isNumeroIteracionesAMostrarValidada() {
		return horaDesdeAMostrarValidada;
	}

	public void setIngresarDemoraInscripcionMinimaIconSrc(String ingresarDemoraInscripcionMinimaIconSrc) {
		this.ingresarDemoraInscripcionMinimaIconSrc = ingresarDemoraInscripcionMinimaIconSrc;
	}

	public void setIngresarDemoraInscripcionMaximaIconSrc(String ingresarDemoraInscripcionMaximaIconSrc) {
		this.ingresarDemoraInscripcionMaximaIconSrc = ingresarDemoraInscripcionMaximaIconSrc;
	}

	public void setIngresarLlegadaAlumnoMediaIconSrc(String ingresarLlegadaAlumnoMediaIconSrc) {
		this.ingresarLlegadaAlumnoMediaIconSrc = ingresarLlegadaAlumnoMediaIconSrc;
	}

	public void setIngresarLlegadaTecnicoMediaIconSrc(String ingresarLlegadaTecnicoMediaIconSrc) {
		this.ingresarLlegadaTecnicoMediaIconSrc = ingresarLlegadaTecnicoMediaIconSrc;
	}

	public void setIngresarLlegadaTecnicoErrorIconSrc(String ingresarLlegadaTecnicoErrorIconSrc) {
		this.ingresarLlegadaTecnicoErrorIconSrc = ingresarLlegadaTecnicoErrorIconSrc;
	}

	public void setIngresarDemoraMantenimientoMediaIconSrc(String ingresarDemoraMantenimientoMediaIconSrc) {
		this.ingresarDemoraMantenimientoMediaIconSrc = ingresarDemoraMantenimientoMediaIconSrc;
	}

	public void setIngresarDemoraMantenimientoDesviacionIconSrc(String ingresarDemoraMantenimientoDesviacionIconSrc) {
		this.ingresarDemoraMantenimientoDesviacionIconSrc = ingresarDemoraMantenimientoDesviacionIconSrc;
	}

	public void setIngresarCantidadDeTiempoASimularIconSrc(String ingresarCantidadDeTiempoASimularIconSrc) {
		this.ingresarCantidadDeTiempoASimularIconSrc = ingresarCantidadDeTiempoASimularIconSrc;
	}

	public void setIngresarHoraDesdeAMostrarIconSrc(String ingresarHoraDesdeAMostrarIconSrc) {
		this.ingresarHoraDesdeAMostrarIconSrc = ingresarHoraDesdeAMostrarIconSrc;
	}

	public void setIngresarNumeroIteracionesAMostrarIconSrc(String ingresarNumeroIteracionesAMostrarIconSrc) {
		this.ingresarNumeroIteracionesAMostrarIconSrc = ingresarNumeroIteracionesAMostrarIconSrc;
	}

	public void setDemoraInscripcionMinimaValidada(boolean demoraInscripcionMinimaValidada) {
		this.demoraInscripcionMinimaValidada = demoraInscripcionMinimaValidada;
	}

	public void setDemoraInscripcionMaximaValidada(boolean demoraInscripcionMaximaValidada) {
		this.demoraInscripcionMaximaValidada = demoraInscripcionMaximaValidada;
	}

	public void setLlegadaAlumnoMediaValidada(boolean llegadaAlumnoMediaValidada) {
		this.llegadaAlumnoMediaValidada = llegadaAlumnoMediaValidada;
	}

	public void setLlegadaTecnicoMediaValidada(boolean llegadaTecnicoMediaValidada) {
		this.llegadaTecnicoMediaValidada = llegadaTecnicoMediaValidada;
	}

	public void setLlegadaTecnicoErrorValidada(boolean llegadaTecnicoErrorValidada) {
		this.llegadaTecnicoErrorValidada = llegadaTecnicoErrorValidada;
	}

	public void setDemoraMantenimientoMediaValidada(boolean demoraMantenimientoMediaValidada) {
		this.demoraMantenimientoMediaValidada = demoraMantenimientoMediaValidada;
	}

	public void setDemoraMantenimientoDesviacionValidada(boolean demoraMantenimientoDesviacionValidada) {
		this.demoraMantenimientoDesviacionValidada = demoraMantenimientoDesviacionValidada;
	}

	public void setCantidadDeTiempoASimularValidada(boolean cantidadDeTiempoASimularValidada) {
		this.cantidadDeTiempoASimularValidada = cantidadDeTiempoASimularValidada;
	}

	public void setHoraDesdeAMostrarValidada(boolean horaDesdeAMostrarValidada) {
		this.horaDesdeAMostrarValidada = horaDesdeAMostrarValidada;
	}

	public void setNumeroIteracionesAMostrarValidada(boolean numeroIteracionesAMostrarValidada) {
		this.numeroIteracionesAMostrarValidada = numeroIteracionesAMostrarValidada;
	}

	public ConversorDeHoras getConversorDeHoras() {
		return conversorDeHoras;
	}

	public ConversorDeMinutos getConversorDeMinutos() {
		return conversorDeMinutos;
	}

	public ConversorDeSegundos getConversorDeSegundos() {
		return conversorDeSegundos;
	}

	public ConversorDeNumeroIteraciones getConversorDeNumeroIteraciones() {
		return conversorDeNumeroIteraciones;
	}

	public String getResultadosSrc() {
		return mostrandoResultados ? rB.getPagePathname("resultados.") : null;
	}

	public String getInputSrc() {
		return rB.getPagePathname("input");
	}

	@DependsOn("resultadosSrc")
	public String getResultadosSimulacionSrc() {
		String src = null;
		if (tP6EjercicioD != null && tP6EjercicioD.hasResultadosSimulacion())
			src = rB.getPagePathname("resultadossimulacion.");
		return src;
	}

	@DependsOn("resultadosSrc")
	public String getTablaVectoresEstadoSrc() {
		String src = null;
		if (tP6EjercicioD != null && tP6EjercicioD.getTabla() != null)
			src = rB.getPagePathname("tablavectoresestado.");
		return src;
	}

	@DependsOn("resultadosSrc")
	public String getTablaIntegracionNumericaSrc() {
		String src = null;
		if (tP6EjercicioD != null && tP6EjercicioD.getTabla() != null)
			src = rB.getPagePathname("tablaintegracionnumerica.");
		return src;
	}

	public void setInputSrc(String inputSrc) {
	}

	public TP6EjercicioD gettP6EjercicioD() {
		if (tP6EjercicioD == null)
			tP6EjercicioD = (TP6EjercicioD) SpringUtil.getBean("tP6EjercicioD");
		return tP6EjercicioD;
	}

	public void settp6EjercicioD(TP6EjercicioD tp6EjercicioD) {
		this.tP6EjercicioD = tp6EjercicioD;
	}

	public boolean isMostrandoResultados() {
		return mostrandoResultados;
	}

	public void setMostrandoResultados(boolean mostrandoResultados) {
		this.mostrandoResultados = mostrandoResultados;
	}

	@DependsOn({ "mostrandoResultados" })
	public boolean isCorrerHabilitado() {
		return !mostrandoResultados && isValidacionOk();
	}

	public String getKey1() {
		return "1000";
	}

	public String getKey2() {
		return "1500";
	}

	public String getKey3() {
		return "2000";
	}

	private void doLog(String level, String signature, String corchetes, String nombreVariable, Loggable loggable) {
		switch (level) {
		case "trace":
			if (LOG.isTraceEnabled())
				LOG.trace(toLogMsg(signature, corchetes, nombreVariable, loggable));
			break;
		case "debug":
			if (LOG.isDebugEnabled())
				LOG.debug(toLogMsg(signature, corchetes, nombreVariable, loggable));
			break;
		case "info":
			if (LOG.isInfoEnabled())
				LOG.info(toLogMsg(signature, corchetes, nombreVariable, loggable));
			break;
		case "warn":
			if (LOG.isWarnEnabled())
				LOG.warn(toLogMsg(signature, corchetes, nombreVariable, loggable));
			break;
		case "error":
			if (LOG.isErrorEnabled())
				LOG.error(toLogMsg(signature, corchetes, nombreVariable, loggable));
			break;
		}
	}

	private String toLogMsg(String signature, String corchetes, String nombreVariable, Loggable loggable) {
		StringBuffer s = new StringBuffer(signature);
		if (corchetes != null)
			s.append(" " + corchetes);
		if (nombreVariable != null)
			s.append(" " + "{ l = " + nombreVariable + " }");
		if (nombreVariable != null && loggable == null)
			s.append(nombreVariable + ": " + loggable);
		else if (nombreVariable != null && loggable != null)
			s.append(loggable.toLogString("l"));
		return s.toString();
	}
	
}
