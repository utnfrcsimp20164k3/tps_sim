package app.tp6.ejerciciod.presentacion;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.bind.annotation.Init;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.select.annotation.VariableResolver;
import org.zkoss.zk.ui.select.annotation.WireVariable;

import app.presentacion.ResourceBundle;
import app.presentacion.ResourceIndex;

@VariableResolver(org.zkoss.zkplus.spring.DelegatingVariableResolver.class)
public class TP6EjercicioDVM {

	@WireVariable
	private ResourceIndex resourceIndex;
	static final Logger LOG = LoggerFactory.getLogger(TP6EjercicioDVM.class);
	private ResourceBundle rB;

	@Init
	public void initSetup() {
		rB = resourceIndex.getResourceBundle("pqfjyh");
	}

	@Command
	public void detachWindow(@ContextParam(ContextType.VIEW) Component view) {
		view.detach();
	}

	public String getEnunciadoSrc() {
		return rB.getPagePathname("enunciado.");
	}

	public String getSolucionSrc() {
		return rB.getPagePathname("solucion.");
	}

	public String getNombreEjercicio() {
		return rB.getNombre();
	}

	public String getNombrePaquete() {
		LOG.debug("getNombrePaquete() [0] rB: " + rB);
		LOG.debug("getNombrePaquete() [0] rB.getPaquete(): " + rB.getPaquete());
		return rB.getPaquete().getNombre();
	}

}
