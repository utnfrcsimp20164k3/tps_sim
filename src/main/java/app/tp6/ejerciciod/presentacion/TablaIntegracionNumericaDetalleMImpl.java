package app.tp6.ejerciciod.presentacion;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import app.Loggable;
import app.utils.DetalleTabla;

@Component
public class TablaIntegracionNumericaDetalleMImpl implements TablaIntegracionNumericaDetalleM {

	static final Logger LOG = LoggerFactory.getLogger(TablaIntegracionNumericaDetalleMImpl.class);
	private DetalleTabla detalleTabla;

	public TablaIntegracionNumericaDetalleMImpl() {
		super();
	}

	@Bean
	@Scope("prototype")
	static public TablaIntegracionNumericaDetalleM tablaIntegracionNumericaDetalleM() {
		return new TablaIntegracionNumericaDetalleMImpl();
	}

	public void setDetalleTabla(DetalleTabla detalleTabla) {
		this.detalleTabla = detalleTabla;
	}

	@Override
	public Object getValorA() {
		return detalleTabla.getValores().get("t");
	}

	@Override
	public Object getValorB() {
		return detalleTabla.getValores().get("A");
	}

	@Override
	public Object getValorC() {
		return detalleTabla.getValores().get("A/dt");
	}

	@Override
	public Object getValorD() {
		return detalleTabla.getValores().get("Ai+1");
	}

	private void doLog(String level, String signature, String corchetes, String nombreVariable, Loggable loggable) {
		switch (level) {
		case "trace":
			if (LOG.isTraceEnabled())
				LOG.trace(toLogMsg(signature, corchetes, nombreVariable, loggable));
			break;
		case "debug":
			if (LOG.isDebugEnabled())
				LOG.debug(toLogMsg(signature, corchetes, nombreVariable, loggable));
			break;
		case "info":
			if (LOG.isInfoEnabled())
				LOG.info(toLogMsg(signature, corchetes, nombreVariable, loggable));
			break;
		case "warn":
			if (LOG.isWarnEnabled())
				LOG.warn(toLogMsg(signature, corchetes, nombreVariable, loggable));
			break;
		case "error":
			if (LOG.isErrorEnabled())
				LOG.error(toLogMsg(signature, corchetes, nombreVariable, loggable));
			break;
		}
	}

	private String toLogMsg(String signature, String corchetes, String nombreVariable, Loggable loggable) {
		StringBuffer s = new StringBuffer(signature);
		if (corchetes != null)
			s.append(" " + corchetes);
		if (nombreVariable != null)
			s.append(" " + "{ l = " + nombreVariable + " }");
		if (nombreVariable != null && loggable == null)
			s.append(nombreVariable + ": " + loggable);
		else if (nombreVariable != null && loggable != null)
			s.append(loggable.toLogString("l"));
		return s.toString();
	}

}
