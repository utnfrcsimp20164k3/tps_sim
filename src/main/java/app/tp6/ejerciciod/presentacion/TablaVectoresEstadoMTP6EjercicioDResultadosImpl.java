package app.tp6.ejerciciod.presentacion;

import java.util.Collection;
import java.util.Collections;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.zkoss.zul.ListModelList;

import app.Loggable;
import app.tp5ytp6.Maquina;
import app.tp5ytp6.MaquinaImpl;
import app.utils.DetalleTabla;
import app.utils.TablaVectorEstados;

@Component
public class TablaVectoresEstadoMTP6EjercicioDResultadosImpl implements TablaVectoresEstadoMTP6EjercicioDResultados {

	static final Logger LOG = LoggerFactory.getLogger(TablaVectoresEstadoMTP6EjercicioDResultadosImpl.class);
	TablaVectorEstados tabla;
	@Autowired
	ApplicationContext applicationContext;
	
	public TablaVectoresEstadoMTP6EjercicioDResultadosImpl() {
		super();
	}

	@Bean
	@Scope("prototype")
	static public TablaVectoresEstadoMTP6EjercicioDResultados tablaVectoresEstadoMTP6EjercicioDResultados() {
		return new TablaVectoresEstadoMTP6EjercicioDResultadosImpl();
	}

	@Override
	public void setTabla(TablaVectorEstados tabla) {
		this.tabla = tabla;
	}

	@Override
	public Object getEncabezadoA() {
		return "Llegaron";
	}

	@Override
	public Object getEncabezadoB() {
		return "Se fueron";
	}

	@Override
	public Object getEncabezadoC() {
		return "Se fueron [%]";
	}

	@Override
	public Object getEncabezadoD() {
		return "Inscriptos / h";
	}

	@Override
	public Object getEncabezadoE() {
		return "Inscriptos";
	}

	@Override
	public Object getEncabezadoF() {
		return "Tiempo [']";
	}

	@Override
	public Object getEncabezadoG() {
		return "Tiempo [h]";
	}

	@Override
	public Object getEncabezadoH() {
		return "Inscriptos / h";
	}

	@Override
	public Object getEncabezadoI() {
		return "Inscriptos";
	}

	@Override
	public Object getEncabezadoJ() {
		return "Tiempo [']";
	}

	@Override
	public Object getEncabezadoK() {
		return "Tiempo [h]";
	}

	@Override
	public Object getEncabezadoL() {
		return "Inscriptos / h";
	}

	@Override
	public Object getEncabezadoM() {
		return "Inscriptos";
	}

	@Override
	public Object getEncabezadoN() {
		return "Tiempo [']";
	}

	@Override
	public Object getEncabezadoO() {
		return "Tiempo [h]";
	}

	@Override
	public Object getEncabezadoP() {
		return "Inscriptos / h";
	}

	@Override
	public Object getEncabezadoQ() {
		return "Inscriptos";
	}

	@Override
	public Object getEncabezadoR() {
		return "Tiempo [']";
	}

	@Override
	public Object getEncabezadoS() {
		return "Tiempo [h]";
	}

	@Override
	public Object getEncabezadoT() {
		return "Inscriptos / h";
	}

	@Override
	public Object getEncabezadoU() {
		return "Inscriptos";
	}

	@Override
	public Object getEncabezadoV() {
		return "Tiempo [']";
	}

	@Override
	public Object getEncabezadoW() {
		return "Tiempo [h]";
	}

	@Override
	public Object getEncabezadoX() {
		return "Inscriptos / h";
	}

	@Override
	public ListModelList<TablaVectoresEstadoDetalleMTP6EjercicioDResultados> getDetalles() {
		doLog("trace", "getDetalles()", "[{]", null, null);
		ListModelList<TablaVectoresEstadoDetalleMTP6EjercicioDResultados> modelo = new ListModelList<>();
		for (DetalleTabla d : tabla.getDetalles()) {
			TablaVectoresEstadoDetalleMTP6EjercicioDResultados dM = (TablaVectoresEstadoDetalleMTP6EjercicioDResultados) applicationContext.getBean("tablaVectoresEstadoDetalleMTP6EjercicioDResultados");
			dM.getMaquinas().addAll((Collection<? extends Maquina>) tabla.obtenerPermanentes(MaquinaImpl.class));
			Collections.sort(dM.getMaquinas(), MaquinaImpl.comparador());
			dM.setDetalleTabla(d);
			modelo.add(dM);
		}
		doLog("trace", "getDetalles()", "[}]", null, null);
		return modelo;
	}

	@Override
	public void clear() {
		tabla.getDetalles().clear();
	}

	private void doLog(String level, String signature, String corchetes, String nombreVariable, Loggable loggable) {
		switch (level) {
		case "trace":
			if (LOG.isTraceEnabled())
				LOG.trace(toLogMsg(signature, corchetes, nombreVariable, loggable));
			break;
		case "debug":
			if (LOG.isDebugEnabled())
				LOG.debug(toLogMsg(signature, corchetes, nombreVariable, loggable));
			break;
		case "info":
			if (LOG.isInfoEnabled())
				LOG.info(toLogMsg(signature, corchetes, nombreVariable, loggable));
			break;
		case "warn":
			if (LOG.isWarnEnabled())
				LOG.warn(toLogMsg(signature, corchetes, nombreVariable, loggable));
			break;
		case "error":
			if (LOG.isErrorEnabled())
				LOG.error(toLogMsg(signature, corchetes, nombreVariable, loggable));
			break;
		}
	}

	private String toLogMsg(String signature, String corchetes, String nombreVariable, Loggable loggable) {
		StringBuffer s = new StringBuffer(signature);
		if (corchetes != null)
			s.append(" " + corchetes);
		if (nombreVariable != null)
			s.append(" " + "{ l = " + nombreVariable + " }");
		if (nombreVariable != null && loggable == null)
			s.append(nombreVariable + ": " + loggable);
		else if (nombreVariable != null && loggable != null)
			s.append(loggable.toLogString("l"));
		return s.toString();
	}

}
