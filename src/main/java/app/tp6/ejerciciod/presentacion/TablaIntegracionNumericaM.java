package app.tp6.ejerciciod.presentacion;

import java.math.BigDecimal;

import org.zkoss.zul.ListModelList;

import app.utils.TablaIntegracionNumerica;

public interface TablaIntegracionNumericaM {

	void setTabla(TablaIntegracionNumerica tabla);
	
	BigDecimal getH();

	Object getEncabezadoA();

	Object getEncabezadoB();

	Object getEncabezadoC();

	Object getEncabezadoD();

	ListModelList<TablaIntegracionNumericaDetalleM> getDetalles();

	void clear();

}
