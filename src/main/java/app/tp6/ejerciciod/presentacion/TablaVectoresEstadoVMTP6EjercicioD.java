package app.tp6.ejerciciod.presentacion;

import org.zkoss.bind.Converter;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.GlobalCommand;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.SmartNotifyChange;
import org.zkoss.zk.ui.select.annotation.VariableResolver;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.ListModelList;

import app.presentacion.ConversorDeDouble;
import app.presentacion.ConversorDeInt;
import app.presentacion.ConversorDeLong;
import app.tp6.ejerciciod.TP6EjercicioD;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@VariableResolver(org.zkoss.zkplus.spring.DelegatingVariableResolver.class)
public class TablaVectoresEstadoVMTP6EjercicioD {

	static final Logger LOG = LoggerFactory.getLogger(TablaVectoresEstadoVMTP6EjercicioD.class);
	@WireVariable
	TablaVectoresEstadoMTP6EjercicioD tablaVectoresEstadoMTP6EjercicioD;
	@WireVariable
	ConversorDeInt conversorDeInt;
	@WireVariable
	ConversorDeDouble conversorDeDouble;
	@WireVariable
	ConversorDeLong conversorDeLong;

	@Init
	public void initSetup(@BindingParam("tP6EjercicioD") TP6EjercicioD tP6EjercicioD) {
		tablaVectoresEstadoMTP6EjercicioD.setTabla(tP6EjercicioD.getTabla());
	}
	
	@GlobalCommand
	@SmartNotifyChange("tablaVectoresEstadoM")
	public void limpiarTablaVectoresEstado() {
		tablaVectoresEstadoMTP6EjercicioD.clear();
	}

	public ListModelList<TablaVectoresEstadoDetalleMTP6EjercicioD> getDetalles() {
		return tablaVectoresEstadoMTP6EjercicioD.getDetalles();
	}

	public Converter<?, ?, ?> getConversorDeInt() {
		return conversorDeInt;
	}

	public Converter<?, ?, ?> getConversorDeLong() {
		return conversorDeLong;
	}

	public Converter<?, ?, ?> getConversorDeDouble() {
		return conversorDeDouble;
	}
	
	public TablaVectoresEstadoMTP6EjercicioD getTablaVectoresEstadoM() {
		return tablaVectoresEstadoMTP6EjercicioD;
	}

}
