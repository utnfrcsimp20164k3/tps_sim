package app.tp6.ejerciciod.presentacion;

import java.util.Collection;
import java.util.Collections;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.zkoss.zul.ListModelList;

import app.Loggable;
import app.tp5ytp6.Maquina;
import app.tp5ytp6.MaquinaImpl;
import app.tp5ytp6.Tecnico;
import app.tp5ytp6.TecnicoImpl;
import app.utils.DetalleTabla;
import app.utils.TablaVectorEstados;

@Component
public class TablaVectoresEstadoMTP6EjercicioDImpl implements TablaVectoresEstadoMTP6EjercicioD {

	static final Logger LOG = LoggerFactory.getLogger(TablaVectoresEstadoMTP6EjercicioDImpl.class);
	private TablaVectorEstados tabla;
	@Autowired
	private ApplicationContext applicationContext;
	
	public TablaVectoresEstadoMTP6EjercicioDImpl() {
		super();
	}

	@Bean
	@Scope("prototype")
	static public TablaVectoresEstadoMTP6EjercicioD tablaVectoresEstadoMTP6EjercicioD() {
		return new TablaVectoresEstadoMTP6EjercicioDImpl();
	}

	@Override
	public void setTabla(TablaVectorEstados tabla) {
		this.tabla = tabla;
	}

	@Override
	public Object getEncabezadoA() {
		return "Evento";
	}

	@Override
	public Object getEncabezadoB() {
		return "Reloj";
	}

	@Override
	public Object getEncabezadoC() {
		return "Rnd";
	}

	@Override
	public Object getEncabezadoD() {
		return "Tiempo";
	}

	@Override
	public Object getEncabezadoE() {
		return "Hora";
	}

	@Override
	public Object getEncabezadoF() {
		return "Rnd";
	}

	@Override
	public Object getEncabezadoG() {
		return "Tiempo";
	}

	@Override
	public Object getEncabezadoH() {
		return "Hora";
	}

	@Override
	public Object getEncabezadoI() {
		return "Rnd";
	}

	@Override
	public Object getEncabezadoJ() {
		return "Tiempo";
	}

	@Override
	public Object getEncabezadoK() {
		return "Hora";
	}

	@Override
	public Object getEncabezadoL() {
		return "Hora";
	}

	@Override
	public Object getEncabezadoM() {
		return "Hora";
	}

	@Override
	public Object getEncabezadoN() {
		return "Hora";
	}

	@Override
	public Object getEncabezadoO() {
		return "Hora";
	}

	@Override
	public Object getEncabezadoP() {
		return "Rnd";
	}

	@Override
	public Object getEncabezadoQ() {
		return "A0";
	}

	@Override
	public Object getEncabezadoR() {
		return "Tiempo";
	}

	@Override
	public Object getEncabezadoS() {
		return "Hora";
	}

	@Override
	public Object getEncabezadoT() {
		return "Estado";
	}

	@Override
	public Object getEncabezadoU() {
		return "Estado";
	}

	@Override
	public Object getEncabezadoV() {
		return "Revisada";
	}

	@Override
	public Object getEncabezadoW() {
		return "Inscripciones";
	}

	@Override
	public Object getEncabezadoX() {
		return "Estado";
	}

	@Override
	public Object getEncabezadoY() {
		return "Revisada";
	}

	@Override
	public Object getEncabezadoZ() {
		return "Inscripciones";
	}

	@Override
	public Object getEncabezadoAA() {
		return "Estado";
	}

	@Override
	public Object getEncabezadoAB() {
		return "Revisada";
	}

	@Override
	public Object getEncabezadoAC() {
		return "Inscripciones";
	}

	@Override
	public Object getEncabezadoAD() {
		return "Estado";
	}

	@Override
	public Object getEncabezadoAE() {
		return "Revisada";
	}

	@Override
	public Object getEncabezadoAF() {
		return "Inscripciones";
	}

	@Override
	public Object getEncabezadoAG() {
		return "Estado";
	}

	@Override
	public Object getEncabezadoAH() {
		return "Revisada";
	}

	@Override
	public Object getEncabezadoAI() {
		return "Inscripciones";
	}

	@Override
	public Object getEncabezadoAJ() {
		return "Cola";
	}

	@Override
	public Object getEncabezadoAK() {
		return "Alumnos que se fueron";
	}

	@Override
	public Object getEncabezadoAL() {
		return "Alumnos que llegaron";
	}

	@Override
	public ListModelList<TablaVectoresEstadoDetalleMTP6EjercicioD> getDetalles() {
		doLog("trace", "getDetalles()", "[{]", null, null);
		ListModelList<TablaVectoresEstadoDetalleMTP6EjercicioD> modelo = new ListModelList<>();
		for (DetalleTabla d : tabla.getDetalles()) {
			TablaVectoresEstadoDetalleMTP6EjercicioD dM = (TablaVectoresEstadoDetalleMTP6EjercicioD) applicationContext.getBean("tablaVectoresEstadoDetalleMTP6EjercicioD");
			dM.getMaquinas().addAll((Collection<? extends Maquina>) tabla.obtenerPermanentes(MaquinaImpl.class));
			Collections.sort(dM.getMaquinas(), MaquinaImpl.comparador());
			dM.getTecnicos().addAll((Collection<? extends Tecnico>) tabla.obtenerPermanentes(TecnicoImpl.class));
			dM.setDetalleTabla(d);
			modelo.add(dM);
		}
		doLog("trace", "getDetalles()", "[}]", null, null);
		return modelo;
	}

	@Override
	public void clear() {
		tabla.getDetalles().clear();
	}

	private void doLog(String level, String signature, String corchetes, String nombreVariable, Loggable loggable) {
		switch (level) {
		case "trace":
			if (LOG.isTraceEnabled())
				LOG.trace(toLogMsg(signature, corchetes, nombreVariable, loggable));
			break;
		case "debug":
			if (LOG.isDebugEnabled())
				LOG.debug(toLogMsg(signature, corchetes, nombreVariable, loggable));
			break;
		case "info":
			if (LOG.isInfoEnabled())
				LOG.info(toLogMsg(signature, corchetes, nombreVariable, loggable));
			break;
		case "warn":
			if (LOG.isWarnEnabled())
				LOG.warn(toLogMsg(signature, corchetes, nombreVariable, loggable));
			break;
		case "error":
			if (LOG.isErrorEnabled())
				LOG.error(toLogMsg(signature, corchetes, nombreVariable, loggable));
			break;
		}
	}

	private String toLogMsg(String signature, String corchetes, String nombreVariable, Loggable loggable) {
		StringBuffer s = new StringBuffer(signature);
		if (corchetes != null)
			s.append(" " + corchetes);
		if (nombreVariable != null)
			s.append(" " + "{ l = " + nombreVariable + " }");
		if (nombreVariable != null && loggable == null)
			s.append(nombreVariable + ": " + loggable);
		else if (nombreVariable != null && loggable != null)
			s.append(loggable.toLogString("l"));
		return s.toString();
	}

}
