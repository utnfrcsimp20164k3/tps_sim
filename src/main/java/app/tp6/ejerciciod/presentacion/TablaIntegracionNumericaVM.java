package app.tp6.ejerciciod.presentacion;

import org.zkoss.bind.annotation.ExecutionArgParam;
import org.zkoss.bind.annotation.GlobalCommand;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.SmartNotifyChange;
import org.zkoss.zk.ui.select.annotation.VariableResolver;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.ListModelList;

import app.Loggable;
import app.tp6.ejerciciod.TP6EjercicioD;

import java.math.BigDecimal;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@VariableResolver(org.zkoss.zkplus.spring.DelegatingVariableResolver.class)
public class TablaIntegracionNumericaVM {

	static final Logger LOG = LoggerFactory.getLogger(TablaIntegracionNumericaVM.class);
	private TP6EjercicioD tP6EjercicioD;
	@WireVariable
	TablaIntegracionNumericaM tablaIntegracionNumericaM;

	@Init
	public void initSetup(@ExecutionArgParam("tP6EjercicioD") TP6EjercicioD tP6EjercicioD, @ExecutionArgParam("key") String key) {
		doLog("trace", "initSetup(@ExecutionArgParam(\"key\") String key)", "[{]", null, null);
		this.tP6EjercicioD = tP6EjercicioD;
		tablaIntegracionNumericaM.setTabla(tP6EjercicioD.getTablaIntegracionNumerica(new BigDecimal(key)));
		doLog("trace", "initSetup(@ExecutionArgParam(\"key\") String key)", "[}]", null, null);
	}

	@GlobalCommand
	@SmartNotifyChange("tablaVectoresEstadoM")
	public void limpiarTablaVectoresEstado() {
		tablaIntegracionNumericaM.clear();
	}

	public ListModelList<TablaIntegracionNumericaDetalleM> getDetalles() {
		return tablaIntegracionNumericaM.getDetalles();
	}

	public TablaIntegracionNumericaM getTablaIntegracionNumericaM() {
		return tablaIntegracionNumericaM;
	}

	private void doLog(String level, String signature, String corchetes, String nombreVariable, Loggable loggable) {
		switch (level) {
		case "trace":
			if (LOG.isTraceEnabled())
				LOG.trace(toLogMsg(signature, corchetes, nombreVariable, loggable));
			break;
		case "debug":
			if (LOG.isDebugEnabled())
				LOG.debug(toLogMsg(signature, corchetes, nombreVariable, loggable));
			break;
		case "info":
			if (LOG.isInfoEnabled())
				LOG.info(toLogMsg(signature, corchetes, nombreVariable, loggable));
			break;
		case "warn":
			if (LOG.isWarnEnabled())
				LOG.warn(toLogMsg(signature, corchetes, nombreVariable, loggable));
			break;
		case "error":
			if (LOG.isErrorEnabled())
				LOG.error(toLogMsg(signature, corchetes, nombreVariable, loggable));
			break;
		}
	}

	private String toLogMsg(String signature, String corchetes, String nombreVariable, Loggable loggable) {
		StringBuffer s = new StringBuffer(signature);
		if (corchetes != null)
			s.append(" " + corchetes);
		if (nombreVariable != null)
			s.append(" " + "{ l = " + nombreVariable + " }");
		if (nombreVariable != null && loggable == null)
			s.append(nombreVariable + ": " + loggable);
		else if (nombreVariable != null && loggable != null)
			s.append(loggable.toLogString("l"));
		return s.toString();
	}

}
