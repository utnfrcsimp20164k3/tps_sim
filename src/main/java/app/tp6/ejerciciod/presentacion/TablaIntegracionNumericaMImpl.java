package app.tp6.ejerciciod.presentacion;

import java.math.BigDecimal;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.zkoss.zul.ListModelList;

import app.Loggable;
import app.utils.DetalleTabla;
import app.utils.TablaIntegracionNumerica;

@Component
public class TablaIntegracionNumericaMImpl implements TablaIntegracionNumericaM {

	static final Logger LOG = LoggerFactory.getLogger(TablaIntegracionNumericaMImpl.class);
	private TablaIntegracionNumerica tabla;
	@Autowired
	private ApplicationContext applicationContext;
	
	public TablaIntegracionNumericaMImpl() {
		super();
	}

	@Bean
	@Scope("prototype")
	static public TablaIntegracionNumericaM tablaIntegracionNumericaM() {
		return new TablaIntegracionNumericaMImpl();
	}

	@Override
	public void setTabla(TablaIntegracionNumerica tabla) {
		doLog("trace", "setTabla(TablaIntegracionNumerica tabla)", "[{]", null, null);
		LOG.debug("setTabla(TablaIntegracionNumerica tabla) [0] tabla: " + tabla);
		this.tabla = tabla;
		doLog("trace", "setTabla(TablaIntegracionNumerica tabla)", "[}]", null, null);
	}

	@Override
	public BigDecimal getH() {
		return tabla.getH();
	}

	@Override
	public Object getEncabezadoA() {
		return "t";
	}

	@Override
	public Object getEncabezadoB() {
		return "A";
	}

	@Override
	public Object getEncabezadoC() {
		return "A/dt";
	}

	@Override
	public Object getEncabezadoD() {
		return "Ai+1";
	}

	@Override
	public ListModelList<TablaIntegracionNumericaDetalleM> getDetalles() {
		doLog("trace", "getDetalles()", "[{]", null, null);
		ListModelList<TablaIntegracionNumericaDetalleM> modelo = new ListModelList<>();
		for (DetalleTabla d : tabla.getDetalles()) {
			TablaIntegracionNumericaDetalleM dM = (TablaIntegracionNumericaDetalleM) applicationContext.getBean("tablaIntegracionNumericaDetalleM");
			dM.setDetalleTabla(d);
			modelo.add(dM);
		}
		doLog("trace", "getDetalles()", "[}]", null, null);
		return modelo;
	}

	@Override
	public void clear() {
		tabla.getDetalles().clear();
	}

	private void doLog(String level, String signature, String corchetes, String nombreVariable, Loggable loggable) {
		switch (level) {
		case "trace":
			if (LOG.isTraceEnabled())
				LOG.trace(toLogMsg(signature, corchetes, nombreVariable, loggable));
			break;
		case "debug":
			if (LOG.isDebugEnabled())
				LOG.debug(toLogMsg(signature, corchetes, nombreVariable, loggable));
			break;
		case "info":
			if (LOG.isInfoEnabled())
				LOG.info(toLogMsg(signature, corchetes, nombreVariable, loggable));
			break;
		case "warn":
			if (LOG.isWarnEnabled())
				LOG.warn(toLogMsg(signature, corchetes, nombreVariable, loggable));
			break;
		case "error":
			if (LOG.isErrorEnabled())
				LOG.error(toLogMsg(signature, corchetes, nombreVariable, loggable));
			break;
		}
	}

	private String toLogMsg(String signature, String corchetes, String nombreVariable, Loggable loggable) {
		StringBuffer s = new StringBuffer(signature);
		if (corchetes != null)
			s.append(" " + corchetes);
		if (nombreVariable != null)
			s.append(" " + "{ l = " + nombreVariable + " }");
		if (nombreVariable != null && loggable == null)
			s.append(nombreVariable + ": " + loggable);
		else if (nombreVariable != null && loggable != null)
			s.append(loggable.toLogString("l"));
		return s.toString();
	}

}
