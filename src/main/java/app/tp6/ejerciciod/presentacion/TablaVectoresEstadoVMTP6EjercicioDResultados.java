package app.tp6.ejerciciod.presentacion;

import org.zkoss.bind.Converter;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.GlobalCommand;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.SmartNotifyChange;
import org.zkoss.zk.ui.select.annotation.VariableResolver;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.ListModelList;

import app.presentacion.ConversorDeDouble;
import app.presentacion.ConversorDeInt;
import app.presentacion.ConversorDeLong;
import app.tp6.ejerciciod.TP6EjercicioDResultados;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@VariableResolver(org.zkoss.zkplus.spring.DelegatingVariableResolver.class)
public class TablaVectoresEstadoVMTP6EjercicioDResultados {

	static final Logger LOG = LoggerFactory.getLogger(TablaVectoresEstadoVMTP6EjercicioDResultados.class);
	@WireVariable
	TablaVectoresEstadoMTP6EjercicioDResultados tablaVectoresEstadoMTP6EjercicioDResultados;
	@WireVariable
	ConversorDeInt conversorDeInt;
	@WireVariable
	ConversorDeDouble conversorDeDouble;
	@WireVariable
	ConversorDeLong conversorDeLong;

	@Init
	public void initSetup(@BindingParam("tP6EjercicioDResultados") TP6EjercicioDResultados tP6EjercicioDResultados) {
		tablaVectoresEstadoMTP6EjercicioDResultados.setTabla(tP6EjercicioDResultados.getTabla());
	}
	
	@GlobalCommand
	@SmartNotifyChange("tablaVectoresEstadoM")
	public void limpiarTablaVectoresEstado() {
		tablaVectoresEstadoMTP6EjercicioDResultados.clear();
	}

	public ListModelList<TablaVectoresEstadoDetalleMTP6EjercicioDResultados> getDetalles() {
		return tablaVectoresEstadoMTP6EjercicioDResultados.getDetalles();
	}

	public Converter<?, ?, ?> getConversorDeInt() {
		return conversorDeInt;
	}

	public Converter<?, ?, ?> getConversorDeLong() {
		return conversorDeLong;
	}

	public Converter<?, ?, ?> getConversorDeDouble() {
		return conversorDeDouble;
	}
	
	public TablaVectoresEstadoMTP6EjercicioDResultados getTablaVectoresEstadoM() {
		return tablaVectoresEstadoMTP6EjercicioDResultados;
	}

}
