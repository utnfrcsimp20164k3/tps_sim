package app.tp6.ejerciciod.presentacion;

import app.utils.DetalleTabla;

public interface TablaIntegracionNumericaDetalleM {
	
	void setDetalleTabla(DetalleTabla d);

	Object getValorA();

	Object getValorB();

	Object getValorC();

	Object getValorD();

}
