package app.tp5.ejerciciod.presentacion;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import app.Loggable;
import app.tp5ytp6.Maquina;
import app.utils.DetalleTabla;

@Component
public class TablaVectoresEstadoDetalleMTP5EjercicioDResultadosImpl implements TablaVectoresEstadoDetalleMTP5EjercicioDResultados {

	static final Logger LOG = LoggerFactory.getLogger(TablaVectoresEstadoDetalleMTP5EjercicioDResultadosImpl.class);
	private DetalleTabla detalleTabla;
	private List<Maquina> maquinas;

	public TablaVectoresEstadoDetalleMTP5EjercicioDResultadosImpl() {
		super();
		maquinas = new ArrayList<>();
	}

	@Bean
	@Scope("prototype")
	static public TablaVectoresEstadoDetalleMTP5EjercicioDResultados tablaVectoresEstadoDetalleMTP5EjercicioDResultados() {
		return new TablaVectoresEstadoDetalleMTP5EjercicioDResultadosImpl();
	}

	public void setDetalleTabla(DetalleTabla detalleTabla) {
		this.detalleTabla = detalleTabla;
	}

	@Override
	public Object getValorA() {
		return detalleTabla.getValores().get("Contador de alumnos que llegaron");
	}

	@Override
	public Object getValorB() {
		return detalleTabla.getValores().get("Contador de alumnos que se fueron");
	}

	@Override
	public Object getValorC() {
		return detalleTabla.getValores().get("Porcentaje de alumnos que llegaron");
	}

	@Override
	public Object getValorD() {
		return detalleTabla.getValores().get("Inscriptos / h");
	}

	@Override
	public Object getValorE() {
		Set<Object> claves = new HashSet<>();
		claves.add(maquinas.get(0));
		claves.add("Inscriptos");
		return detalleTabla.getValores().get(claves);
	}

	@Override
	public Object getValorF() {
		Set<Object> claves = new HashSet<>();
		claves.add(maquinas.get(0));
		claves.add("Tiempo [']");
		return detalleTabla.getValores().get(claves);
	}

	@Override
	public Object getValorG() {
		Set<Object> claves = new HashSet<>();
		claves.add(maquinas.get(0));
		claves.add("Tiempo [h]");
		return detalleTabla.getValores().get(claves);
	}

	@Override
	public Object getValorH() {
		Set<Object> claves = new HashSet<>();
		claves.add(maquinas.get(0));
		claves.add("Inscriptos / h");
		return detalleTabla.getValores().get(claves);
	}

	@Override
	public Object getValorI() {
		Set<Object> claves = new HashSet<>();
		claves.add(maquinas.get(1));
		claves.add("Inscriptos");
		return detalleTabla.getValores().get(claves);
	}

	@Override
	public Object getValorJ() {
		Set<Object> claves = new HashSet<>();
		claves.add(maquinas.get(1));
		claves.add("Tiempo [']");
		return detalleTabla.getValores().get(claves);
	}

	@Override
	public Object getValorK() {
		Set<Object> claves = new HashSet<>();
		claves.add(maquinas.get(1));
		claves.add("Tiempo [h]");
		return detalleTabla.getValores().get(claves);
	}

	@Override
	public Object getValorL() {
		Set<Object> claves = new HashSet<>();
		claves.add(maquinas.get(1));
		claves.add("Inscriptos / h");
		return detalleTabla.getValores().get(claves);
	}

	@Override
	public Object getValorM() {
		Set<Object> claves = new HashSet<>();
		claves.add(maquinas.get(2));
		claves.add("Inscriptos");
		return detalleTabla.getValores().get(claves);
	}

	@Override
	public Object getValorN() {
		Set<Object> claves = new HashSet<>();
		claves.add(maquinas.get(2));
		claves.add("Tiempo [']");
		return detalleTabla.getValores().get(claves);
	}

	@Override
	public Object getValorO() {
		Set<Object> claves = new HashSet<>();
		claves.add(maquinas.get(2));
		claves.add("Tiempo [h]");
		return detalleTabla.getValores().get(claves);
	}

	@Override
	public Object getValorP() {
		Set<Object> claves = new HashSet<>();
		claves.add(maquinas.get(2));
		claves.add("Inscriptos / h");
		return detalleTabla.getValores().get(claves);
	}

	@Override
	public Object getValorQ() {
		Set<Object> claves = new HashSet<>();
		claves.add(maquinas.get(3));
		claves.add("Inscriptos");
		return detalleTabla.getValores().get(claves);
	}

	@Override
	public Object getValorR() {
		Set<Object> claves = new HashSet<>();
		claves.add(maquinas.get(3));
		claves.add("Tiempo [']");
		return detalleTabla.getValores().get(claves);
	}

	@Override
	public Object getValorS() {
		Set<Object> claves = new HashSet<>();
		claves.add(maquinas.get(3));
		claves.add("Tiempo [h]");
		return detalleTabla.getValores().get(claves);
	}

	@Override
	public Object getValorT() {
		Set<Object> claves = new HashSet<>();
		claves.add(maquinas.get(3));
		claves.add("Inscriptos / h");
		return detalleTabla.getValores().get(claves);
	}

	@Override
	public Object getValorU() {
		Set<Object> claves = new HashSet<>();
		claves.add(maquinas.get(4));
		claves.add("Inscriptos");
		return detalleTabla.getValores().get(claves);
	}

	@Override
	public Object getValorV() {
		Set<Object> claves = new HashSet<>();
		claves.add(maquinas.get(4));
		claves.add("Tiempo [']");
		return detalleTabla.getValores().get(claves);
	}

	@Override
	public Object getValorW() {
		Set<Object> claves = new HashSet<>();
		claves.add(maquinas.get(4));
		claves.add("Tiempo [h]");
		return detalleTabla.getValores().get(claves);
	}

	@Override
	public Object getValorX() {
		Set<Object> claves = new HashSet<>();
		claves.add(maquinas.get(4));
		claves.add("Inscriptos / h");
		return detalleTabla.getValores().get(claves);
	}

	@Override
	public List<Maquina> getMaquinas() {
		return maquinas;
	}

	private void doLog(String level, String signature, String corchetes, String nombreVariable, Loggable loggable) {
		switch (level) {
		case "trace":
			if (LOG.isTraceEnabled())
				LOG.trace(toLogMsg(signature, corchetes, nombreVariable, loggable));
			break;
		case "debug":
			if (LOG.isDebugEnabled())
				LOG.debug(toLogMsg(signature, corchetes, nombreVariable, loggable));
			break;
		case "info":
			if (LOG.isInfoEnabled())
				LOG.info(toLogMsg(signature, corchetes, nombreVariable, loggable));
			break;
		case "warn":
			if (LOG.isWarnEnabled())
				LOG.warn(toLogMsg(signature, corchetes, nombreVariable, loggable));
			break;
		case "error":
			if (LOG.isErrorEnabled())
				LOG.error(toLogMsg(signature, corchetes, nombreVariable, loggable));
			break;
		}
	}

	private String toLogMsg(String signature, String corchetes, String nombreVariable, Loggable loggable) {
		StringBuffer s = new StringBuffer(signature);
		if (corchetes != null)
			s.append(" " + corchetes);
		if (nombreVariable != null)
			s.append(" " + "{ l = " + nombreVariable + " }");
		if (nombreVariable != null && loggable == null)
			s.append(nombreVariable + ": " + loggable);
		else if (nombreVariable != null && loggable != null)
			s.append(loggable.toLogString("l"));
		return s.toString();
	}

}
