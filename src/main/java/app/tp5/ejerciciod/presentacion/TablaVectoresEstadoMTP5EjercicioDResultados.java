package app.tp5.ejerciciod.presentacion;

import org.zkoss.zul.ListModelList;

import app.utils.TablaVectorEstados;

public interface TablaVectoresEstadoMTP5EjercicioDResultados {

	void setTabla(TablaVectorEstados tablaVectorEstados);
	
	Object getEncabezadoA();

	Object getEncabezadoB();

	Object getEncabezadoC();

	Object getEncabezadoD();

	Object getEncabezadoE();

	Object getEncabezadoF();

	Object getEncabezadoG();

	Object getEncabezadoH();

	Object getEncabezadoI();

	Object getEncabezadoJ();

	Object getEncabezadoK();

	Object getEncabezadoL();

	Object getEncabezadoM();

	Object getEncabezadoN();

	Object getEncabezadoO();

	Object getEncabezadoP();

	Object getEncabezadoQ();

	Object getEncabezadoR();

	Object getEncabezadoS();

	Object getEncabezadoT();

	Object getEncabezadoU();

	Object getEncabezadoV();

	Object getEncabezadoW();

	Object getEncabezadoX();

	ListModelList<TablaVectoresEstadoDetalleMTP5EjercicioDResultados> getDetalles();

	void clear();

}
