package app.tp5.ejerciciod.presentacion;

import org.zkoss.bind.Converter;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.GlobalCommand;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.SmartNotifyChange;
import org.zkoss.zk.ui.select.annotation.VariableResolver;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.ListModelList;

import app.presentacion.ConversorDeDouble;
import app.presentacion.ConversorDeInt;
import app.presentacion.ConversorDeLong;
import app.tp5.ejerciciod.TP5EjercicioD;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@VariableResolver(org.zkoss.zkplus.spring.DelegatingVariableResolver.class)
public class TablaVectoresEstadoVMTP5EjercicioD {

	static final Logger LOG = LoggerFactory.getLogger(TablaVectoresEstadoVMTP5EjercicioD.class);
	@WireVariable
	TablaVectoresEstadoMTP5EjercicioD tablaVectoresEstadoMTP5EjercicioD;
	@WireVariable
	ConversorDeInt conversorDeInt;
	@WireVariable
	ConversorDeDouble conversorDeDouble;
	@WireVariable
	ConversorDeLong conversorDeLong;

	@Init
	public void initSetup(@BindingParam("tP5EjercicioD") TP5EjercicioD tP5EjercicioD) {
		tablaVectoresEstadoMTP5EjercicioD.setTabla(tP5EjercicioD.getTabla());
	}
	
	@GlobalCommand
	@SmartNotifyChange("tablaVectoresEstadoM")
	public void limpiarTablaVectoresEstado() {
		tablaVectoresEstadoMTP5EjercicioD.clear();
	}

	public ListModelList<TablaVectoresEstadoDetalleMTP5EjercicioD> getDetalles() {
		return tablaVectoresEstadoMTP5EjercicioD.getDetalles();
	}

	public Converter<?, ?, ?> getConversorDeInt() {
		return conversorDeInt;
	}

	public Converter<?, ?, ?> getConversorDeLong() {
		return conversorDeLong;
	}

	public Converter<?, ?, ?> getConversorDeDouble() {
		return conversorDeDouble;
	}
	
	public TablaVectoresEstadoMTP5EjercicioD getTablaVectoresEstadoM() {
		return tablaVectoresEstadoMTP5EjercicioD;
	}

}
