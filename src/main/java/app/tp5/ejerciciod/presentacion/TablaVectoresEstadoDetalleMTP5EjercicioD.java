package app.tp5.ejerciciod.presentacion;

import java.util.List;

import app.tp5ytp6.Maquina;
import app.tp5ytp6.Tecnico;
import app.utils.DetalleTabla;

public interface TablaVectoresEstadoDetalleMTP5EjercicioD {
	
	void setDetalleTabla(DetalleTabla d);

	Object getValorA();

	Object getValorB();

	Object getValorC();

	Object getValorD();

	Object getValorE();

	Object getValorF();

	Object getValorG();

	Object getValorH();

	Object getValorI();

	Object getValorJ();

	Object getValorK();

	Object getValorL();

	Object getValorM();

	Object getValorN();

	Object getValorO();

	Object getValorP();

	Object getValorQ();

	Object getValorR();

	Object getValorS();

	Object getValorT();

	Object getValorU();

	Object getValorV();

	Object getValorW();

	Object getValorX();

	Object getValorY();

	Object getValorZ();

	Object getValorAA();

	Object getValorAB();

	Object getValorAC();

	Object getValorAD();

	Object getValorAE();

	Object getValorAF();

	Object getValorAG();

	Object getValorAH();

	Object getValorAI();

	Object getValorAJ();

	Object getValorAK();

	List<Maquina> getMaquinas();

	List<Tecnico> getTecnicos();

}
