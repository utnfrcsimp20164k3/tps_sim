package app.tp5.ejerciciod.presentacion;

import org.zkoss.bind.Converter;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.GlobalCommand;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.SmartNotifyChange;
import org.zkoss.zk.ui.select.annotation.VariableResolver;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.ListModelList;

import app.presentacion.ConversorDeDouble;
import app.presentacion.ConversorDeInt;
import app.presentacion.ConversorDeLong;
import app.tp5.ejerciciod.TP5EjercicioDResultados;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@VariableResolver(org.zkoss.zkplus.spring.DelegatingVariableResolver.class)
public class TablaVectoresEstadoVMTP5EjercicioDResultados {

	static final Logger LOG = LoggerFactory.getLogger(TablaVectoresEstadoVMTP5EjercicioDResultados.class);
	@WireVariable
	TablaVectoresEstadoMTP5EjercicioDResultados tablaVectoresEstadoMTP5EjercicioDResultados;
	@WireVariable
	ConversorDeInt conversorDeInt;
	@WireVariable
	ConversorDeDouble conversorDeDouble;
	@WireVariable
	ConversorDeLong conversorDeLong;

	@Init
	public void initSetup(@BindingParam("tP5EjercicioDResultados") TP5EjercicioDResultados tP5EjercicioDResultados) {
		tablaVectoresEstadoMTP5EjercicioDResultados.setTabla(tP5EjercicioDResultados.getTabla());
	}
	
	@GlobalCommand
	@SmartNotifyChange("tablaVectoresEstadoM")
	public void limpiarTablaVectoresEstado() {
		tablaVectoresEstadoMTP5EjercicioDResultados.clear();
	}

	public ListModelList<TablaVectoresEstadoDetalleMTP5EjercicioDResultados> getDetalles() {
		return tablaVectoresEstadoMTP5EjercicioDResultados.getDetalles();
	}

	public Converter<?, ?, ?> getConversorDeInt() {
		return conversorDeInt;
	}

	public Converter<?, ?, ?> getConversorDeLong() {
		return conversorDeLong;
	}

	public Converter<?, ?, ?> getConversorDeDouble() {
		return conversorDeDouble;
	}
	
	public TablaVectoresEstadoMTP5EjercicioDResultados getTablaVectoresEstadoM() {
		return tablaVectoresEstadoMTP5EjercicioDResultados;
	}

}
