package app.tp5.ejerciciod.presentacion;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import app.Loggable;
import app.tp5ytp6.Maquina;
import app.tp5ytp6.Tecnico;
import app.tp5ytp6.VectorEstadosFinInscripcionFactory;
import app.tp5ytp6.VectorEstadosFinMantenimientoFactory;
import app.tp5ytp6.VectorEstadosLlegadaAlumnoFactory;
import app.tp5ytp6.VectorEstadosLlegadaTecnicoFactory;
import app.tp5ytp6.VectorEstadosMaquina;
import app.tp5ytp6.VectorEstadosTecnico;
import app.utils.DetalleTabla;

@Component
public class TablaVectoresEstadoDetalleMTP5EjercicioDImpl implements TablaVectoresEstadoDetalleMTP5EjercicioD {

	static final Logger LOG = LoggerFactory.getLogger(TablaVectoresEstadoDetalleMTP5EjercicioDImpl.class);
	private DetalleTabla detalleTabla;
	private List<Maquina> maquinas;
	private List<Tecnico> tecnicos;
	@Autowired
	private VectorEstadosLlegadaAlumnoFactory vectorEstadosLlegadaAlumnoFactory;
	@Autowired
	private VectorEstadosLlegadaTecnicoFactory vectorEstadosLlegadaTecnicoFactory;
	@Autowired
	private VectorEstadosFinInscripcionFactory vectorEstadosFinInscripcionFactory;
	@Autowired
	private VectorEstadosFinMantenimientoFactory vectorEstadosFinMantenimientoFactoryTP5EjercicioD;
	@Autowired
	private VectorEstadosTecnico vectorEstadosTecnico;
	@Autowired
	private VectorEstadosMaquina vectorEstadosMaquina;

	public TablaVectoresEstadoDetalleMTP5EjercicioDImpl() {
		super();
		doLog("trace", "TablaVectoresEstadoDetalleMTP5EjercicioDImpl()", "[{]", null, null);
		tecnicos = new ArrayList<>();
		maquinas = new ArrayList<>();
		doLog("trace", "TablaVectoresEstadoDetalleMTP5EjercicioDImpl()", "[}]", null, null);
	}

	@Bean
	@Scope("prototype")
	static public TablaVectoresEstadoDetalleMTP5EjercicioD tablaVectoresEstadoDetalleMTP5EjercicioD() {
		return new TablaVectoresEstadoDetalleMTP5EjercicioDImpl();
	}

	public void setDetalleTabla(DetalleTabla detalleTabla) {
		this.detalleTabla = detalleTabla;
	}

	@Override
	public Object getValorA() {
		return detalleTabla.getValores().get("Evento");
	}

	@Override
	public Object getValorB() {
		return detalleTabla.getValores().get("Reloj");
	}

	@Override
	public Object getValorC() {
		return detalleTabla.getValores().get(vectorEstadosLlegadaAlumnoFactory.getClaveRnd());
	}

	@Override
	public Object getValorD() {
		return detalleTabla.getValores().get(vectorEstadosLlegadaAlumnoFactory.getClaveTiempoProximo());
	}

	@Override
	public Object getValorE() {
		return detalleTabla.getValores().get(vectorEstadosLlegadaAlumnoFactory.getClaveHoraProximo());
	}

	@Override
	public Object getValorF() {
		return detalleTabla.getValores().get(vectorEstadosLlegadaTecnicoFactory.getClaveRnd());
	}

	@Override
	public Object getValorG() {
		return detalleTabla.getValores().get(vectorEstadosLlegadaTecnicoFactory.getClaveTiempoProximo());
	}

	@Override
	public Object getValorH() {
		Set<Object> claves = new HashSet<>();
		claves.add(vectorEstadosLlegadaTecnicoFactory.getClaveHoraProximo());
		claves.add(tecnicos.get(0));
		return detalleTabla.getValores().get(claves);
	}

	@Override
	public Object getValorI() {
		return detalleTabla.getValores().get(vectorEstadosFinInscripcionFactory.getClaveRnd());
	}

	@Override
	public Object getValorJ() {
		return detalleTabla.getValores().get(vectorEstadosFinInscripcionFactory.getClaveTiempoProximo());
	}

	@Override
	public Object getValorK() {
		Set<Object> claves = new HashSet<>();
		claves.add(vectorEstadosFinInscripcionFactory.getClaveHoraProximo());
		claves.add(maquinas.get(0));
		return detalleTabla.getValores().get(claves);
	}

	@Override
	public Object getValorL() {
		Set<Object> claves = new HashSet<>();
		claves.add(vectorEstadosFinInscripcionFactory.getClaveHoraProximo());
		claves.add(maquinas.get(1));
		return detalleTabla.getValores().get(claves);
	}

	@Override
	public Object getValorM() {
		Set<Object> claves = new HashSet<>();
		claves.add(vectorEstadosFinInscripcionFactory.getClaveHoraProximo());
		claves.add(maquinas.get(2));
		return detalleTabla.getValores().get(claves);
	}

	@Override
	public Object getValorN() {
		Set<Object> claves = new HashSet<>();
		claves.add(vectorEstadosFinInscripcionFactory.getClaveHoraProximo());
		claves.add(maquinas.get(3));
		return detalleTabla.getValores().get(claves);
	}

	@Override
	public Object getValorO() {
		Set<Object> claves = new HashSet<>();
		claves.add(vectorEstadosFinInscripcionFactory.getClaveHoraProximo());
		claves.add(maquinas.get(4));
		return detalleTabla.getValores().get(claves);
	}

	@Override
	public Object getValorP() {
		return detalleTabla.getValores().get(vectorEstadosFinMantenimientoFactoryTP5EjercicioD.getClaveRnd());
	}

	@Override
	public Object getValorQ() {
		return detalleTabla.getValores().get(vectorEstadosFinMantenimientoFactoryTP5EjercicioD.getClaveTiempoProximo());
	}

	@Override
	public Object getValorR() {
		Set<Object> claves = new HashSet<>();
		claves.add(vectorEstadosFinMantenimientoFactoryTP5EjercicioD.getClaveHoraProximo());
		claves.add(tecnicos.get(0));
		return detalleTabla.getValores().get(claves);
	}

	@Override
	public Object getValorS() {
		Set<Object> claves = new HashSet<>();
		claves.add(vectorEstadosTecnico.getClaveEstado());
		claves.add(tecnicos.get(0));
		return detalleTabla.getValores().get(claves);
	}

	@Override
	public Object getValorT() {
		Set<Object> claves = new HashSet<>();
		claves.add(vectorEstadosMaquina.getClaveEstado());
		claves.add(maquinas.get(0));
		return detalleTabla.getValores().get(claves);
	}

	@Override
	public Object getValorU() {
		Set<Object> claves = new HashSet<>();
		claves.add(vectorEstadosMaquina.getClaveRevisada());
		claves.add(maquinas.get(0));
		return detalleTabla.getValores().get(claves);
	}

	@Override
	public Object getValorV() {
		Set<Object> claves = new HashSet<>();
		claves.add(vectorEstadosMaquina.getClaveContadorInscripciones());
		claves.add(maquinas.get(0));
		return detalleTabla.getValores().get(claves);
	}

	@Override
	public Object getValorW() {
		Set<Object> claves = new HashSet<>();
		claves.add(vectorEstadosMaquina.getClaveEstado());
		claves.add(maquinas.get(1));
		return detalleTabla.getValores().get(claves);
	}

	@Override
	public Object getValorX() {
		Set<Object> claves = new HashSet<>();
		claves.add(vectorEstadosMaquina.getClaveRevisada());
		claves.add(maquinas.get(1));
		return detalleTabla.getValores().get(claves);
	}

	@Override
	public Object getValorY() {
		Set<Object> claves = new HashSet<>();
		claves.add(vectorEstadosMaquina.getClaveContadorInscripciones());
		claves.add(maquinas.get(1));
		return detalleTabla.getValores().get(claves);
	}

	@Override
	public Object getValorZ() {
		Set<Object> claves = new HashSet<>();
		claves.add(vectorEstadosMaquina.getClaveEstado());
		claves.add(maquinas.get(2));
		return detalleTabla.getValores().get(claves);
	}

	@Override
	public Object getValorAA() {
		Set<Object> claves = new HashSet<>();
		claves.add(vectorEstadosMaquina.getClaveRevisada());
		claves.add(maquinas.get(2));
		return detalleTabla.getValores().get(claves);
	}

	@Override
	public Object getValorAB() {
		Set<Object> claves = new HashSet<>();
		claves.add(vectorEstadosMaquina.getClaveContadorInscripciones());
		claves.add(maquinas.get(2));
		return detalleTabla.getValores().get(claves);
	}

	@Override
	public Object getValorAC() {
		Set<Object> claves = new HashSet<>();
		claves.add(vectorEstadosMaquina.getClaveEstado());
		claves.add(maquinas.get(3));
		return detalleTabla.getValores().get(claves);
	}

	@Override
	public Object getValorAD() {
		Set<Object> claves = new HashSet<>();
		claves.add(vectorEstadosMaquina.getClaveRevisada());
		claves.add(maquinas.get(3));
		return detalleTabla.getValores().get(claves);
	}

	@Override
	public Object getValorAE() {
		Set<Object> claves = new HashSet<>();
		claves.add(vectorEstadosMaquina.getClaveContadorInscripciones());
		claves.add(maquinas.get(3));
		return detalleTabla.getValores().get(claves);
	}

	@Override
	public Object getValorAF() {
		Set<Object> claves = new HashSet<>();
		claves.add(vectorEstadosMaquina.getClaveEstado());
		claves.add(maquinas.get(4));
		return detalleTabla.getValores().get(claves);
	}

	@Override
	public Object getValorAG() {
		Set<Object> claves = new HashSet<>();
		claves.add(vectorEstadosMaquina.getClaveRevisada());
		claves.add(maquinas.get(4));
		return detalleTabla.getValores().get(claves);
	}

	@Override
	public Object getValorAH() {
		Set<Object> claves = new HashSet<>();
		claves.add(vectorEstadosMaquina.getClaveContadorInscripciones());
		claves.add(maquinas.get(4));
		return detalleTabla.getValores().get(claves);
	}

	@Override
	public Object getValorAI() {
		return detalleTabla.getValores().get("Cola de alumnos");
	}

	@Override
	public Object getValorAJ() {
		return detalleTabla.getValores().get("Contador de alumnos que se fueron");
	}

	@Override
	public Object getValorAK() {
		return detalleTabla.getValores().get("Contador de alumnos que llegaron");
	}

	@Override
	public List<Maquina> getMaquinas() {
		return maquinas;
	}

	@Override
	public List<Tecnico> getTecnicos() {
		return tecnicos;
	}

	private void doLog(String level, String signature, String corchetes, String nombreVariable, Loggable loggable) {
		switch (level) {
		case "trace":
			if (LOG.isTraceEnabled())
				LOG.trace(toLogMsg(signature, corchetes, nombreVariable, loggable));
			break;
		case "debug":
			if (LOG.isDebugEnabled())
				LOG.debug(toLogMsg(signature, corchetes, nombreVariable, loggable));
			break;
		case "info":
			if (LOG.isInfoEnabled())
				LOG.info(toLogMsg(signature, corchetes, nombreVariable, loggable));
			break;
		case "warn":
			if (LOG.isWarnEnabled())
				LOG.warn(toLogMsg(signature, corchetes, nombreVariable, loggable));
			break;
		case "error":
			if (LOG.isErrorEnabled())
				LOG.error(toLogMsg(signature, corchetes, nombreVariable, loggable));
			break;
		}
	}

	private String toLogMsg(String signature, String corchetes, String nombreVariable, Loggable loggable) {
		StringBuffer s = new StringBuffer(signature);
		if (corchetes != null)
			s.append(" " + corchetes);
		if (nombreVariable != null)
			s.append(" " + "{ l = " + nombreVariable + " }");
		if (nombreVariable != null && loggable == null)
			s.append(nombreVariable + ": " + loggable);
		else if (nombreVariable != null && loggable != null)
			s.append(loggable.toLogString("l"));
		return s.toString();
	}

}
