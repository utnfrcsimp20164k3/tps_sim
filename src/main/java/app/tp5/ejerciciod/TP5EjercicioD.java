package app.tp5.ejerciciod;

import java.math.BigDecimal;

import app.Ejercicio;
import app.Loggable;
import app.utils.Observable;
import app.utils.Observer;
import app.utils.TablaVectorEstados;

public interface TP5EjercicioD extends Ejercicio, Observer, Observable, Loggable {

	void reiniciar();

	TP5EjercicioDResultados getResultadosSimulacion();

	TablaVectorEstados getTabla();

	BigDecimal getHoraDesdeAMostrar();

	void setHoraDesdeAMostrar(BigDecimal horaDesdeAMostrar);

	BigDecimal getCantidadDeTiempoASimular();

	void setCantidadDeTiempoASimular(BigDecimal cantidadDeTiempoASimular);

	Long getNumeroIteracionesAMostrar();

	void setNumeroIteracionesAMostrar(Long numeroIteracionesAMostrar);

	Integer getCantidadMaquinas();

	void setCantidadMaquinas(Integer cantidadMaquinas);

	Integer getCantidadTecnicos();

	void setCantidadTecnicos(Integer cantidadTecnicos);

	boolean hasResultadosSimulacion();
	
	void setDemoraInscripcionMinima(BigDecimal demoraInscripcionMinima);

	BigDecimal getDemoraInscripcionMinima();

	void setDemoraInscripcionMaxima(BigDecimal demoraInscripcionMaxima);

	BigDecimal getDemoraInscripcionMaxima();

	void setLlegadaAlumnoMedia(BigDecimal demoraInscripcionMinima);

	BigDecimal getLlegadaAlumnoMedia();

	void setLlegadaTecnicoMedia(BigDecimal demoraInscripcionMinima);

	BigDecimal getLlegadaTecnicoMedia();

	void setLlegadaTecnicoError(BigDecimal demoraInscripcionMinima);

	BigDecimal getLlegadaTecnicoError();

	void setDemoraMantenimientoMedia(BigDecimal demoraMantenimientoMedia);

	BigDecimal getDemoraMantenimientoMedia();

	void setDemoraMantenimientoDesviacion(BigDecimal demoraMantenimientoDesviacion);

	BigDecimal getDemoraMantenimientoDesviacion();

}
