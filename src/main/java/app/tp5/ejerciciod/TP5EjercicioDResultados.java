package app.tp5.ejerciciod;

import java.util.Set;

import app.tp5ytp6.Maquina;
import app.utils.DetalleTabla;
import app.utils.TablaVectorEstados;

public interface TP5EjercicioDResultados {

	boolean hasDetalleTablaFinal();

	void calcular(DetalleTabla detalleTabla);

	void setMaquinas(Set<Maquina> maquinas);

	TablaVectorEstados getTabla();

}
