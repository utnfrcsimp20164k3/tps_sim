package app.tp1.puntoa;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import app.utils.generador.GeneradorPseudoaleatorio;
import app.utils.generador.GeneradorUniformeTransformadaInversaAB;

@Component
public class TP1PuntoAImpl implements TP1PuntoA {

	static final Logger LOG = LoggerFactory.getLogger(TP1PuntoAImpl.class);
	List<Double> lista;
	GeneradorPseudoaleatorio generador;
	@Autowired
	GeneradorUniformeTransformadaInversaAB generadorUniformeTransformadaInversaAB;

	public TP1PuntoAImpl() {
		super();
		lista = new ArrayList<>();
	}

	@Bean
	@Scope("prototype")
	static public TP1PuntoA tP1PuntoA() {
		return new TP1PuntoAImpl();
	}

	@Override
	public void generarLista(GeneradorPseudoaleatorio generador) {
		this.generador = generador;
		reiniciar();
		generadorUniformeTransformadaInversaAB.setExtremoInferior(0d);
		generadorUniformeTransformadaInversaAB.setExtremoSuperior(9999d);
		for (int i = 0; i < 20; i++)
			lista.add((Double) generadorUniformeTransformadaInversaAB.next((Double) generador.next()));
	}

	@Override
	public void reiniciar() {
		lista.clear();
		if (generador != null)
			generador.clear();
	}

	@Override
	public void next() {
		lista.add((Double) generadorUniformeTransformadaInversaAB.next((double) generador.next()));
	}

	public List<Double> getLista() {
		return lista;
	}

	@Override
	public GeneradorPseudoaleatorio getGenerador() {
		return generador;
	}

	@Override
	public void setGenerador(GeneradorPseudoaleatorio generador) {
		this.generador = generador;
	}

}
