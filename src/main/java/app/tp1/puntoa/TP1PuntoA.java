package app.tp1.puntoa;

import java.util.List;

import app.utils.generador.GeneradorPseudoaleatorio;

public interface TP1PuntoA {

	void generarLista(GeneradorPseudoaleatorio generador);

	List<Double> getLista();
	
	void next();

	void reiniciar();
	
	GeneradorPseudoaleatorio getGenerador();

	void setGenerador(GeneradorPseudoaleatorio generador);

}
