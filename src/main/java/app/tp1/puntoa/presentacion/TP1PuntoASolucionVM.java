package app.tp1.puntoa.presentacion;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.zkoss.bind.BindUtils;
import org.zkoss.bind.Converter;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.DependsOn;
import org.zkoss.bind.annotation.GlobalCommand;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.SmartNotifyChange;
import org.zkoss.spring.SpringUtil;
import org.zkoss.zk.ui.select.annotation.VariableResolver;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.ListModelList;

import app.presentacion.ConversorDeDouble;
import app.presentacion.ConversorDeLong;
import app.presentacion.FormateadorDeNumerosDecimales;
import app.presentacion.IngresarConstanteAditivaValidator;
import app.presentacion.IngresarConstanteMultiplicativaValidator;
import app.presentacion.IngresarModuloValidator;
import app.presentacion.IngresarSemillaValidator;
import app.presentacion.InputValidator;
import app.presentacion.Observable;
import app.presentacion.Observer;
import app.presentacion.ResourceBundle;
import app.presentacion.ResourceIndex;
import app.presentacion.SidebarTreeItem;
import app.tp1.puntoa.TP1PuntoA;
import app.utils.generador.GeneradorCongruencialLineal;
import app.utils.generador.GeneradorCongruencialMultiplicativo;

@VariableResolver(org.zkoss.zkplus.spring.DelegatingVariableResolver.class)
public class TP1PuntoASolucionVM implements Observer {

	private static final String TILDE_SRC = "./images/check_mark.png";
	private static final String ADVERTENCIA_SRC = "./images/warning.png";
	private static final String ASTERISCO_SRC = "./images/asterisk_red.png";
	static final Logger LOG = LoggerFactory.getLogger(TP1PuntoASolucionVM.class);
	@WireVariable
	private ResourceIndex resourceIndex;
	@WireVariable
	private InputValidator ingresarSemillaValidator;
	@WireVariable
	private InputValidator ingresarConstanteAditivaValidator;
	@WireVariable
	private InputValidator ingresarConstanteMultiplicativaValidator;
	@WireVariable
	private InputValidator ingresarModuloValidator;
	private ResourceBundle rB;
	private String ingresarSemillaIconSrc, ingresarModuloIconSrc, ingresarConstanteMultiplicativaIconSrc,
			ingresarConstanteAditivaIconSrc;
	private boolean semillaValidada, moduloValidada, constanteMultiplicativaValidada, constanteAditivaValidada;
	private boolean nextHabilitado;
	@WireVariable
	private GeneradorCongruencialLineal generadorCongruencialLineal;
	@WireVariable
	private GeneradorCongruencialMultiplicativo generadorCongruencialMultiplicativo;
	private String tipoGenerador;
	@WireVariable
	private TP1PuntoA tP1PuntoA;
	ListModelList<Double> lista;
	@WireVariable
	FormateadorDeNumerosDecimales formateadorDeNumerosDecimales;
	@WireVariable
	ConversorDeLong conversorDeLong;
	@WireVariable
	ConversorDeDouble conversorDeDouble;
	private boolean mostrandoResultados;

	@Init
	public void initSetup() {
		rB = resourceIndex.getResourceBundle("kjebvm");
		lista = new ListModelList<>();
		ingresarSemillaValidator.addObserver(this);
		ingresarConstanteMultiplicativaValidator.addObserver(this);
		ingresarConstanteAditivaValidator.addObserver(this);
		ingresarModuloValidator.addObserver(this);
		inicializarIconosValidaciones();
		inicializarValidaciones();
	}

	private void inicializarValidaciones() {
	}

	private void inicializarIconosValidaciones() {
		setIngresarSemillaIconSrc(ASTERISCO_SRC);
		setIngresarConstanteMultiplicativaIconSrc(ASTERISCO_SRC);
		setIngresarConstanteAditivaIconSrc(ASTERISCO_SRC);
		setIngresarModuloIconSrc(ASTERISCO_SRC);
	}

	@GlobalCommand
	@SmartNotifyChange({ "lista", "semillaValidada", "moduloValidada", "constanteMultiplicativaValidada",
			"constanteAditivaValidada", "ingresarSemillaIconSrc", "ingresarConstanteMultiplicativaIconSrc",
			"ingresarConstanteAditivaIconSrc" })
	public void showContent(@BindingParam("page") SidebarTreeItem page) {
		if (page == null || page.getUri() == null || !page.getUri().equals(rB.getZul()))
			clear();
	}

	@Command
	@SmartNotifyChange({ "lista", "semillaValidada", "moduloValidada", "constanteMultiplicativaValidada",
			"constanteAditivaValidada", "ingresarSemillaIconSrc", "ingresarModuloIconSrc",
			"ingresarConstanteMultiplicativaIconSrc", "ingresarConstanteAditivaIconSrc", "mostrandoResultados" })
	public void clear() {
		if (tipoGenerador != null && tipoGenerador.equals("Lineal"))
			tP1PuntoA.setGenerador(generadorCongruencialLineal);
		else if (tipoGenerador != null && tipoGenerador.equals("Multiplicativo"))
			tP1PuntoA.setGenerador(generadorCongruencialMultiplicativo);
		tP1PuntoA.reiniciar();
		setSemillaValidada(false);
		setConstanteMultiplicativaValidada(false);
		setConstanteAditivaValidada(false);
		setModuloValidada(false);
		setMostrandoResultados(false);
		inicializarIconosValidaciones();
		inicializarValidaciones();
	}

	@Command
	@SmartNotifyChange({ "tP1PuntoA", "nextHabilitado", "mostrandoResultados" })
	public void correr() {
		if (generadorCongruencialLineal != null && tP1PuntoA != null)
			tP1PuntoA.generarLista(generadorCongruencialLineal);
		else if (generadorCongruencialMultiplicativo != null && tP1PuntoA != null)
			tP1PuntoA.generarLista(generadorCongruencialMultiplicativo);
		setMostrandoResultados(true);
		setNextHabilitado(true);
	}

	@Command
	@SmartNotifyChange("lista")
	public void next() {
		tP1PuntoA.next();
	}

	// "Validación"
	@Override
	public void update(Observable o, Object arg) {
		if (o instanceof IngresarSemillaValidator)
			updateValidacion((IngresarSemillaValidator) o);
		if (o instanceof IngresarConstanteMultiplicativaValidator)
			updateValidacion((IngresarConstanteMultiplicativaValidator) o);
		if (o instanceof IngresarConstanteAditivaValidator)
			updateValidacion((IngresarConstanteAditivaValidator) o);
		if (o instanceof IngresarModuloValidator)
			updateValidacion((IngresarModuloValidator) o);
	}

	private void updateValidacion(IngresarSemillaValidator val) {
		String iconoSrc = registrarResultadoValidacion((InputValidator) val);
		ingresarSemillaIconSrc = iconoSrc;
		if (iconoSrc != null && iconoSrc.equals(TILDE_SRC) || iconoSrc == null)
			setSemillaValidada(true);
		else
			setSemillaValidada(false);
		BindUtils.postNotifyChange(null, null, this, "ingresarSemillaIconSrc");
		BindUtils.postNotifyChange(null, null, this, "semillaValidada");
	}

	private void updateValidacion(IngresarConstanteMultiplicativaValidator val) {
		String iconoSrc = registrarResultadoValidacion((InputValidator) val);
		ingresarConstanteMultiplicativaIconSrc = iconoSrc;
		if (iconoSrc != null && iconoSrc.equals(TILDE_SRC) || iconoSrc == null)
			setConstanteMultiplicativaValidada(true);
		else
			setConstanteMultiplicativaValidada(false);
		BindUtils.postNotifyChange(null, null, this, "ingresarConstanteMultiplicativaIconSrc");
		BindUtils.postNotifyChange(null, null, this, "constanteMultiplicativaValidada");
	}

	private void updateValidacion(IngresarConstanteAditivaValidator val) {
		String iconoSrc = registrarResultadoValidacion((InputValidator) val);
		ingresarConstanteAditivaIconSrc = iconoSrc;
		if (iconoSrc != null && iconoSrc.equals(TILDE_SRC) || iconoSrc == null)
			setConstanteAditivaValidada(true);
		else
			setConstanteAditivaValidada(false);
		BindUtils.postNotifyChange(null, null, this, "ingresarConstanteAditivaIconSrc");
		BindUtils.postNotifyChange(null, null, this, "constanteAditivaValidada");
	}

	private void updateValidacion(IngresarModuloValidator val) {
		String iconoSrc = registrarResultadoValidacion((InputValidator) val);
		ingresarModuloIconSrc = iconoSrc;
		if (iconoSrc != null && iconoSrc.equals(TILDE_SRC) || iconoSrc == null)
			setModuloValidada(true);
		else
			setModuloValidada(false);
		BindUtils.postNotifyChange(null, null, this, "ingresarModuloIconSrc");
		BindUtils.postNotifyChange(null, null, this, "moduloValidada");
	}

	private String registrarResultadoValidacion(InputValidator val) {
		String res = null;
		if (!val.isIncorrecta() && !val.isNula())
			res = TILDE_SRC;
		else if (val.isIncorrecta())
			res = ADVERTENCIA_SRC;
		else if (val.isNula() && val.isObligatoria())
			res = ASTERISCO_SRC;
		else if (val.isNula() && !val.isObligatoria())
			res = null;
		return res;
	}

	@DependsOn({ "semillaValidada", "moduloValidada", "constanteMultiplicativaValidada", "constanteAditivaValidada" })
	public boolean isValidacionOk() {
		return (tipoGenerador != null && tipoGenerador.equals("Lineal")
				&& (semillaValidada
						|| (!getIngresarSemillaValidator().isObligatoria() && getIngresarSemillaIconSrc() == null))
				&& (constanteMultiplicativaValidada || (!getIngresarConstanteMultiplicativaValidator().isObligatoria()
						&& getIngresarConstanteMultiplicativaIconSrc() == null))
				&& (constanteAditivaValidada || (!getIngresarConstanteAditivaValidator().isObligatoria()
						&& getIngresarConstanteAditivaIconSrc() == null))
				&& (moduloValidada
						|| (!getIngresarModuloValidator().isObligatoria() && getIngresarModuloIconSrc() == null)))
				|| (tipoGenerador != null && tipoGenerador.equals("Multiplicativo")
						&& (semillaValidada || (!getIngresarSemillaValidator().isObligatoria()
								&& getIngresarSemillaIconSrc() == null))
						&& (moduloValidada || (!getIngresarModuloValidator().isObligatoria()
								&& getIngresarModuloIconSrc() == null))
						&& (constanteMultiplicativaValidada
								|| (!getIngresarConstanteMultiplicativaValidator().isObligatoria()
										&& getIngresarConstanteMultiplicativaIconSrc() == null)));
	}

	public InputValidator getIngresarSemillaValidator() {
		return ingresarSemillaValidator;
	}

	public InputValidator getIngresarConstanteMultiplicativaValidator() {
		return ingresarConstanteMultiplicativaValidator;
	}

	public InputValidator getIngresarConstanteAditivaValidator() {
		return ingresarConstanteAditivaValidator;
	}

	public InputValidator getIngresarModuloValidator() {
		return ingresarModuloValidator;
	}

	@DependsOn("semillaValidada")
	public String getDefaultTooltiptextIngresarSemilla() {
		String res = null;
		if (getIngresarSemillaValidator().isObligatoria() && !semillaValidada)
			res = "Este campo es obligatorio.";
		else if (semillaValidada)
			res = "Semilla válida.";
		return res;
	}

	@DependsOn("constanteMultiplicativaValidada")
	public String getDefaultTooltiptextIngresarConstanteMultiplicativa() {
		String res = null;
		if (getIngresarConstanteMultiplicativaValidator().isObligatoria() && !constanteMultiplicativaValidada)
			res = "Este campo es obligatorio.";
		else if (constanteMultiplicativaValidada)
			res = "Constante multiplicativa válida.";
		return res;
	}

	@DependsOn("constanteAditivaValidada")
	public String getDefaultTooltiptextIngresarConstanteAditiva() {
		String res = null;
		if (getIngresarConstanteAditivaValidator().isObligatoria() && !constanteAditivaValidada)
			res = "Este campo es obligatorio.";
		else if (constanteAditivaValidada)
			res = "Constante aditiva válida.";
		return res;
	}

	@DependsOn("moduloValidada")
	public String getDefaultTooltiptextIngresarModulo() {
		String res = null;
		if (getIngresarModuloValidator().isObligatoria() && !moduloValidada)
			res = "Este campo es obligatorio.";
		else if (moduloValidada)
			res = "Constante aditiva válida.";
		return res;
	}

	public String getIngresarSemillaIconSrc() {
		return ingresarSemillaIconSrc;
	}

	public String getIngresarConstanteMultiplicativaIconSrc() {
		return ingresarConstanteMultiplicativaIconSrc;
	}

	public String getIngresarConstanteAditivaIconSrc() {
		return ingresarConstanteAditivaIconSrc;
	}

	public String getIngresarModuloIconSrc() {
		return ingresarModuloIconSrc;
	}

	public boolean isSemillaValidada() {
		return semillaValidada;
	}

	public boolean isConstanteMultiplicativaValidada() {
		return constanteMultiplicativaValidada;
	}

	public boolean isConstanteAditivaValidada() {
		return constanteAditivaValidada;
	}

	public boolean isModuloValidada() {
		return moduloValidada;
	}

	public void setIngresarSemillaIconSrc(String ingresarSemillaIconSrc) {
		this.ingresarSemillaIconSrc = ingresarSemillaIconSrc;
	}

	public void setIngresarConstanteMultiplicativaIconSrc(String ingresarConstanteMultiplicativaIconSrc) {
		this.ingresarConstanteMultiplicativaIconSrc = ingresarConstanteMultiplicativaIconSrc;
	}

	public void setIngresarConstanteAditivaIconSrc(String ingresarConstanteAditivaIconSrc) {
		this.ingresarConstanteAditivaIconSrc = ingresarConstanteAditivaIconSrc;
	}

	public void setIngresarModuloIconSrc(String ingresarModuloIconSrc) {
		this.ingresarModuloIconSrc = ingresarModuloIconSrc;
	}

	public void setSemillaValidada(boolean semillaValidada) {
		this.semillaValidada = semillaValidada;
	}

	public void setConstanteMultiplicativaValidada(boolean constanteMultiplicativaValidada) {
		this.constanteMultiplicativaValidada = constanteMultiplicativaValidada;
	}

	public void setConstanteAditivaValidada(boolean constanteAditivaValidada) {
		this.constanteAditivaValidada = constanteAditivaValidada;
	}

	public void setModuloValidada(boolean moduloValidada) {
		this.moduloValidada = moduloValidada;
	}
	// Fin "Validación"

	public boolean isNextHabilitado() {
		return nextHabilitado;
	}

	private void setNextHabilitado(boolean b) {
		nextHabilitado = b;
	}

	public Converter<?, ?, ?> getConversorDeLong() {
		return conversorDeLong;
	}

	public Converter<?, ?, ?> getConversorDeDouble() {
		return conversorDeDouble;
	}

	public Converter<?, ?, ?> getTruncador() {
		formateadorDeNumerosDecimales.setPattern("#.####");
		return formateadorDeNumerosDecimales;
	}

	@DependsOn("mostrandoResultados")
	public String getResultadosSrc() {
		return mostrandoResultados ? rB.getPagePathname("resultados") : null;
	}

	@DependsOn("tipoGenerador")
	public String getInputSrc() {
		String src = null, substring = null;
		if (tipoGenerador != null && tipoGenerador.equals("Lineal"))
			substring = "input-lineal";
		else if (tipoGenerador != null && tipoGenerador.equals("Multiplicativo"))
			substring = "input-multiplicativo";
		if (tipoGenerador != null)
			src = rB.getPagePathname(substring);
		return src;
	}

	public void setInputSrc(String inputSrc) {
	}

	public String getTipoGenerador() {
		return tipoGenerador;
	}

	public void setTipoGenerador(String tipoGenerador) {
		this.tipoGenerador = tipoGenerador;
	}

	public TP1PuntoA gettP1PuntoA() {
		if (tP1PuntoA == null)
			tP1PuntoA = (TP1PuntoA) SpringUtil.getBean("tP1PuntoA");
		return tP1PuntoA;
	}

	public void settP1PuntoA(TP1PuntoA tP1PuntoA) {
		this.tP1PuntoA = tP1PuntoA;
	}

	@DependsOn("tP1PuntoA")
	public ListModelList<Double> getLista() {
		lista.clear();
		if (tP1PuntoA != null && !tP1PuntoA.getLista().isEmpty()) {
			lista.addAll(tP1PuntoA.getLista());
			lista.addToSelection(tP1PuntoA.getLista().get(tP1PuntoA.getLista().size() - 1));
		}
		return lista;
	}

	public boolean isMostrandoResultados() {
		return mostrandoResultados;
	}

	public void setMostrandoResultados(boolean mostrandoResultados) {
		this.mostrandoResultados = mostrandoResultados;
	}

}
