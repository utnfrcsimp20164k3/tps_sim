package app.tp1.puntob;

import app.Ejercicio;
import app.utils.Serie;
import app.utils.SerieFactory;

public interface TP1PuntoB extends Ejercicio {

	void reiniciar();

	Serie getSerie();
	
	int getCantidadNumeros();

	void setCantidadNumeros(int cantidadNumeros);
	
	int getCantidadIntervalos();
	
	void setCantidadIntervalos(int cantidadIntervalos);

	void crearSerie();

	SerieFactory getSerieFactory();

}
