package app.tp1.puntob;

import java.util.Random;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import app.utils.Hipotesis;
import app.utils.Serie;
import app.utils.SerieFactory;
import app.utils.distribucion.TipoDistribucionVariableContinua;

@Component
public class TP1PuntoBImpl implements TP1PuntoB {

	static final Logger LOG = LoggerFactory.getLogger(TP1PuntoBImpl.class);
	@Autowired
	ApplicationContext context;
	int cantidadNumeros, cantidadIntervalos;
	Serie serie;
	@Autowired
	SerieFactory serieFactory;

	public TP1PuntoBImpl() {
		super();
	}
	
	@Bean
	@Scope(value = "prototype")
	static public TP1PuntoB tP1PuntoB() {
		return new TP1PuntoBImpl();
	}
	
	@Override
	public void solucionar() {
		serie.setArray(generarArray());
		Hipotesis hipotesis = (Hipotesis) context.getBean("hipotesis");
		hipotesis.setTipoDistribucion((TipoDistribucionVariableContinua) context.getBean("distribucionUniforme"));
		serie.postularHipotesis(hipotesis);
		serie.generarGraficas();
	}
	
	@Override
	public void reiniciar() {
		cantidadNumeros = 0;
		if (serie != null)
			serie.clear();
	}

	private double[] generarArray() {
		double[] serie = new double[cantidadNumeros];
		Random r = new Random();
		for (int i = 0; i < cantidadNumeros; i++)
			serie[i] = r.nextDouble();
		return serie;
	}

	@Override
	public void crearSerie() {
		serie = serieFactory.crearSerie();
	}

	@Override
	public Serie getSerie() {
		return serie;
	}

	@Override
	public int getCantidadNumeros() {
		return cantidadNumeros;
	}

	@Override
	public void setCantidadNumeros(int cantidadNumeros) {
		this.cantidadNumeros = cantidadNumeros;
	}

	@Override
	public int getCantidadIntervalos() {
		return cantidadIntervalos;
	}

	@Override
	public void setCantidadIntervalos(int cantidadIntervalos) {
		this.cantidadIntervalos = cantidadIntervalos;
	}

	@Override
	public SerieFactory getSerieFactory() {
		return serieFactory;
	}

}
