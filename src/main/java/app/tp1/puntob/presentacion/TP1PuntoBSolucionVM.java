package app.tp1.puntob.presentacion;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.zkoss.bind.BindUtils;
import org.zkoss.bind.Converter;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.DependsOn;
import org.zkoss.bind.annotation.GlobalCommand;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.SmartNotifyChange;
import org.zkoss.spring.SpringUtil;
import org.zkoss.zk.ui.select.annotation.VariableResolver;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.ListModelList;

import app.presentacion.ConversorDeInt;
import app.presentacion.FormateadorDeNumerosDecimales;
import app.presentacion.IngresarCantidadNumerosValidator;
import app.presentacion.IngresarCantidadIntervalosValidator;
import app.presentacion.InputValidator;
import app.presentacion.Observable;
import app.presentacion.Observer;
import app.presentacion.ResourceBundle;
import app.presentacion.ResourceIndex;
import app.presentacion.SidebarTreeItem;
import app.tp1.puntob.TP1PuntoB;

@VariableResolver(org.zkoss.zkplus.spring.DelegatingVariableResolver.class)
public class TP1PuntoBSolucionVM implements Observer {

	private static final String TILDE_SRC = "./images/check_mark.png";
	private static final String ADVERTENCIA_SRC = "./images/warning.png";
	private static final String ASTERISCO_SRC = "./images/asterisk_red.png";
	static final Logger LOG = LoggerFactory.getLogger(TP1PuntoBSolucionVM.class);
	@WireVariable
	private ResourceIndex resourceIndex;
	@WireVariable
	private InputValidator ingresarCantidadNumerosValidator;
	@WireVariable
	private InputValidator ingresarCantidadIntervalosValidator;
	private ResourceBundle rB;
	private String ingresarCantidadNumerosIconSrc, ingresarCantidadIntervalosIconSrc;
	private boolean cantidadNumerosValidada, cantidadIntervalosValidada;
	@WireVariable
	private TP1PuntoB tP1PuntoB;
	ListModelList<Double> lista;
	@WireVariable
	FormateadorDeNumerosDecimales formateadorDeNumerosDecimales;
	@WireVariable
	ConversorDeInt conversorDeInt;
	private boolean mostrandoResultados;

	@Init
	public void initSetup() {
		tP1PuntoB.getSerieFactory().setTipoVariable("Continua");
		tP1PuntoB.crearSerie();
		rB = resourceIndex.getResourceBundle("lypmvw");
		lista = new ListModelList<>();
		ingresarCantidadNumerosValidator.addObserver(this);
		ingresarCantidadIntervalosValidator.addObserver(this);
		inicializarIconosValidaciones();
		inicializarValidaciones();
	}

	private void inicializarValidaciones() {
	}

	private void inicializarIconosValidaciones() {
		setIngresarCantidadNumerosIconSrc(ASTERISCO_SRC);
		setIngresarCantidadIntervalosIconSrc(ASTERISCO_SRC);
	}

	@GlobalCommand
	@SmartNotifyChange({ "lista", "cantidadNumerosValidada", "cantidadIntervalosValidada", "ingresarCantidadNumerosIconSrc",
			"ingresarCantidadIntervalosIconSrc" })
	public void showContent(@BindingParam("page") SidebarTreeItem page) {
		if (page == null || page.getUri() == null || !page.getUri().equals(rB.getZul()))
			clear();
	}

	@Command
	@SmartNotifyChange({ "lista", "cantidadNumerosValidada", "cantidadIntervalosValidada", "ingresarCantidadNumerosIconSrc",
			"ingresarCantidadIntervalosIconSrc", "mostrandoResultados" })
	public void clear() {
		tP1PuntoB.reiniciar();
		setCantidadNumerosValidada(false);
		setCantidadIntervalosValidada(false);
		setMostrandoResultados(false);
		inicializarIconosValidaciones();
		inicializarValidaciones();
	}

	@Command
	@SmartNotifyChange({ "tP1PuntoB", "graficaSrc", "chiCuadradoSrc", "tablaFrecuenciasSrc", "mostrandoResultados" })
	public void correr() throws Exception {
		tP1PuntoB.solucionar();
		setMostrandoResultados(true);
	}

	@Command
	@SmartNotifyChange("mostrandoResultados")
	public void limpiarResultados() {
		setMostrandoResultados(false);
		BindUtils.postGlobalCommand(null, null, "limpiarTablaFrecuencias", null);
		BindUtils.postGlobalCommand(null, null, "limpiarTestChiCuadrado", null);
	}

	// "Validación"
	@Override
	public void update(Observable o, Object arg) {
		if (o instanceof IngresarCantidadNumerosValidator)
			updateValidacion((IngresarCantidadNumerosValidator) o);
		if (o instanceof IngresarCantidadIntervalosValidator)
			updateValidacion((IngresarCantidadIntervalosValidator) o);
	}

	private void updateValidacion(IngresarCantidadNumerosValidator val) {
		String iconoSrc = registrarResultadoValidacion((InputValidator) val);
		ingresarCantidadNumerosIconSrc = iconoSrc;
		if (iconoSrc != null && iconoSrc.equals(TILDE_SRC) || iconoSrc == null)
			setCantidadNumerosValidada(true);
		else
			setCantidadNumerosValidada(false);
		BindUtils.postNotifyChange(null, null, this, "ingresarCantidadNumerosIconSrc");
		BindUtils.postNotifyChange(null, null, this, "cantidadNumerosValidada");
	}

	private void updateValidacion(IngresarCantidadIntervalosValidator val) {
		String iconoSrc = registrarResultadoValidacion((InputValidator) val);
		ingresarCantidadIntervalosIconSrc = iconoSrc;
		if (iconoSrc != null && iconoSrc.equals(TILDE_SRC) || iconoSrc == null)
			setCantidadIntervalosValidada(true);
		else
			setCantidadIntervalosValidada(false);
		BindUtils.postNotifyChange(null, null, this, "ingresarCantidadIntervalosIconSrc");
		BindUtils.postNotifyChange(null, null, this, "cantidadIntervalosValidada");
	}

	private String registrarResultadoValidacion(InputValidator val) {
		String res = null;
		if (!val.isIncorrecta() && !val.isNula())
			res = TILDE_SRC;
		else if (val.isIncorrecta())
			res = ADVERTENCIA_SRC;
		else if (val.isNula() && val.isObligatoria())
			res = ASTERISCO_SRC;
		else if (val.isNula() && !val.isObligatoria())
			res = null;
		return res;
	}

	@DependsOn({ "cantidadNumerosValidada", "cantidadIntervalosValidada" })
	public boolean isValidacionOk() {
		return (cantidadNumerosValidada
				|| (!getIngresarCantidadNumerosValidator().isObligatoria() && getIngresarCantidadNumerosIconSrc() == null))
				&& (cantidadIntervalosValidada || (!getIngresarCantidadIntervalosValidator().isObligatoria()
						&& getIngresarCantidadIntervalosIconSrc() == null));
	}

	public InputValidator getIngresarCantidadNumerosValidator() {
		return ingresarCantidadNumerosValidator;
	}

	public InputValidator getIngresarCantidadIntervalosValidator() {
		return ingresarCantidadIntervalosValidator;
	}

	@DependsOn("cantidadNumerosValidada")
	public String getDefaultTooltiptextIngresarCantidadNumeros() {
		String res = null;
		if (getIngresarCantidadNumerosValidator().isObligatoria() && !cantidadNumerosValidada)
			res = "Este campo es obligatorio.";
		else if (cantidadNumerosValidada)
			res = "Cantidad válida.";
		return res;
	}

	@DependsOn("cantidadIntervalosValidada")
	public String getDefaultTooltiptextIngresarCantidadIntervalos() {
		String res = null;
		if (getIngresarCantidadIntervalosValidator().isObligatoria() && !cantidadIntervalosValidada)
			res = "Este campo es obligatorio.";
		else if (cantidadIntervalosValidada)
			res = "Constante multiplicativa válida.";
		return res;
	}

	public String getIngresarCantidadNumerosIconSrc() {
		return ingresarCantidadNumerosIconSrc;
	}

	public String getIngresarCantidadIntervalosIconSrc() {
		return ingresarCantidadIntervalosIconSrc;
	}

	public boolean isCantidadNumerosValidada() {
		return cantidadNumerosValidada;
	}

	public boolean isCantidadIntervalosValidada() {
		return cantidadIntervalosValidada;
	}

	public void setIngresarCantidadNumerosIconSrc(String ingresarCantidadNumerosIconSrc) {
		this.ingresarCantidadNumerosIconSrc = ingresarCantidadNumerosIconSrc;
	}

	public void setIngresarCantidadIntervalosIconSrc(String ingresarCantidadIntervalosIconSrc) {
		this.ingresarCantidadIntervalosIconSrc = ingresarCantidadIntervalosIconSrc;
	}

	public void setCantidadNumerosValidada(boolean cantidadNumerosValidada) {
		this.cantidadNumerosValidada = cantidadNumerosValidada;
	}

	public void setCantidadIntervalosValidada(boolean cantidadIntervalosValidada) {
		this.cantidadIntervalosValidada = cantidadIntervalosValidada;
	}
	// Fin "Validación"

	public Converter<?, ?, ?> getConversorDeInt() {
		return conversorDeInt;
	}

	public Converter<?, ?, ?> getTruncador() {
		formateadorDeNumerosDecimales.setPattern("#.####");
		return formateadorDeNumerosDecimales;
	}

	@DependsOn("mostrandoResultados")
	public String getResultadosSrc() {
		return mostrandoResultados ? rB.getPagePathname("resultados") : null;
	}

	public String getInputSrc() {
		return rB.getPagePathname("input");
	}

	@DependsOn("resultadosSrc")
	public String getGraficaSrc() {
		String src = null;
		if (tP1PuntoB != null && tP1PuntoB.getSerie() != null && tP1PuntoB.getSerie().hasGrafica())
			src = rB.getPagePathname("grafica");
		return src;
	}

	@DependsOn("resultadosSrc")
	public String getTablaFrecuenciasSrc() {
		String src = null;
		if (tP1PuntoB != null && tP1PuntoB.getSerie() != null && tP1PuntoB.getSerie().hasGrafica())
			src = rB.getPagePathname("tablafrecuencias");
		return src;
	}

	@DependsOn("resultadosSrc")
	public String getChiCuadradoSrc() {
		String src = null;
		if (tP1PuntoB != null && tP1PuntoB.getSerie() != null && tP1PuntoB.getSerie().hasGrafica())
			src = rB.getPagePathname("chicuadrado.");
		return src;
	}

	@DependsOn("resultadosSrc")
	public String getChiCuadradoTablaSrc() {
		String src = null;
		if (tP1PuntoB != null && tP1PuntoB.getSerie() != null && tP1PuntoB.getSerie().hasGrafica())
			src = rB.getPagePathname("chicuadrado-tabla");
		return src;
	}

	public void setInputSrc(String inputSrc) {
	}

	public TP1PuntoB gettP1PuntoB() {
		if (tP1PuntoB == null)
			tP1PuntoB = (TP1PuntoB) SpringUtil.getBean("tP1PuntoB");
		return tP1PuntoB;
	}

	public void settP1PuntoB(TP1PuntoB tP1PuntoB) {
		this.tP1PuntoB = tP1PuntoB;
	}

	@DependsOn("tP1PuntoB")
	public ListModelList<Double> getLista() {
		lista.clear();
		List<Double> numeros = new ArrayList<>();
		if (tP1PuntoB != null && tP1PuntoB.getSerie() != null && tP1PuntoB.getSerie().getArray() != null) {
			for (Double d : tP1PuntoB.getSerie().getArray())
				numeros.add(d);
			lista.addAll(numeros);
			lista.addToSelection(tP1PuntoB.getSerie().getArray()[tP1PuntoB.getSerie().getArray().length - 1]);
		}
		return lista;
	}

	public String getTituloChiCuadrado() {
		return "Prueba de \u03C7\u2072";
	}

	public boolean isMostrandoResultados() {
		return mostrandoResultados;
	}

	public void setMostrandoResultados(boolean mostrandoResultados) {
		this.mostrandoResultados = mostrandoResultados;
	}

	@DependsOn({ "cantidadNumerosValidada", "cantidadIntervalosValidada" })
	public boolean isCorrerHabilitado() {
		return !mostrandoResultados && isValidacionOk();
	}

}
