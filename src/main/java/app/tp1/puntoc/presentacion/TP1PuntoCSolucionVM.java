package app.tp1.puntoc.presentacion;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.zkoss.bind.BindUtils;
import org.zkoss.bind.Converter;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.DependsOn;
import org.zkoss.bind.annotation.GlobalCommand;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.SmartNotifyChange;
import org.zkoss.spring.SpringUtil;
import org.zkoss.zk.ui.select.annotation.VariableResolver;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.ListModelList;

import app.presentacion.ConversorDeInt;
import app.presentacion.ConversorDeLong;
import app.presentacion.FormateadorDeNumerosDecimales;
import app.presentacion.IngresarCantidadNumerosValidator;
import app.presentacion.IngresarConstanteAditivaValidator;
import app.presentacion.IngresarConstanteMultiplicativaValidator;
import app.presentacion.IngresarModuloValidator;
import app.presentacion.IngresarSemillaValidator;
import app.presentacion.IngresarCantidadIntervalosValidator;
import app.presentacion.InputValidator;
import app.presentacion.Observable;
import app.presentacion.Observer;
import app.presentacion.ResourceBundle;
import app.presentacion.ResourceIndex;
import app.presentacion.SidebarTreeItem;
import app.tp1.puntoc.TP1PuntoC;

@VariableResolver(org.zkoss.zkplus.spring.DelegatingVariableResolver.class)
public class TP1PuntoCSolucionVM implements Observer {

	private static final String TILDE_SRC = "./images/check_mark.png";
	private static final String ADVERTENCIA_SRC = "./images/warning.png";
	private static final String ASTERISCO_SRC = "./images/asterisk_red.png";
	static final Logger LOG = LoggerFactory.getLogger(TP1PuntoCSolucionVM.class);
	@WireVariable
	private ResourceIndex resourceIndex;
	@WireVariable
	private InputValidator ingresarCantidadNumerosValidator;
	@WireVariable
	private InputValidator ingresarCantidadIntervalosValidator;
	@WireVariable
	private InputValidator ingresarSemillaValidator;
	@WireVariable
	private InputValidator ingresarConstanteAditivaValidator;
	@WireVariable
	private InputValidator ingresarConstanteMultiplicativaValidator;
	@WireVariable
	private InputValidator ingresarModuloValidator;
	private ResourceBundle rB;
	private String ingresarCantidadNumerosIconSrc, ingresarCantidadIntervalosIconSrc, ingresarSemillaIconSrc,
			ingresarConstanteMultiplicativaIconSrc, ingresarConstanteAditivaIconSrc, ingresarModuloIconSrc;
	private boolean cantidadNumerosValidada, cantidadIntervalosValidada, semillaValidada,
			constanteMultiplicativaValidada, constanteAditivaValidada, moduloValidada;
	@WireVariable
	private TP1PuntoC tP1PuntoC;
	ListModelList<Double> lista;
	@WireVariable
	FormateadorDeNumerosDecimales formateadorDeNumerosDecimales;
	@WireVariable
	ConversorDeInt conversorDeInt;
	@WireVariable
	ConversorDeLong conversorDeLong;
	private boolean mostrandoResultados;

	@Init
	public void initSetup() {
		tP1PuntoC.getSerieFactory().setTipoVariable("Continua");
		tP1PuntoC.crearSerie();
		rB = resourceIndex.getResourceBundle("hhutad");
		lista = new ListModelList<>();
		ingresarCantidadNumerosValidator.addObserver(this);
		ingresarCantidadIntervalosValidator.addObserver(this);
		ingresarSemillaValidator.addObserver(this);
		ingresarConstanteMultiplicativaValidator.addObserver(this);
		ingresarConstanteAditivaValidator.addObserver(this);
		ingresarModuloValidator.addObserver(this);
		inicializarIconosValidaciones();
		inicializarValidaciones();
	}

	private void inicializarValidaciones() {
	}

	private void inicializarIconosValidaciones() {
		setIngresarCantidadNumerosIconSrc(ASTERISCO_SRC);
		setIngresarCantidadIntervalosIconSrc(ASTERISCO_SRC);
		setIngresarSemillaIconSrc(ASTERISCO_SRC);
		setIngresarConstanteMultiplicativaIconSrc(ASTERISCO_SRC);
		setIngresarConstanteAditivaIconSrc(ASTERISCO_SRC);
		setIngresarModuloIconSrc(ASTERISCO_SRC);
	}

	@GlobalCommand
	@SmartNotifyChange({ "lista", "semillaValidada", "constanteMultiplicativaValidada", "constanteAditivaValidada",
			"ingresarSemillaIconSrc", "ingresarConstanteMultiplicativaIconSrc", "ingresarConstanteAditivaIconSrc",
			"cantidadNumerosValidada", "cantidadIntervalosValidada", "ingresarCantidadNumerosIconSrc",
			"ingresarCantidadIntervalosIconSrc", "moduloValidada", "ingresarModuloIconSrc" })
	public void showContent(@BindingParam("page") SidebarTreeItem page) {
		if (page == null || page.getUri() == null || !page.getUri().equals(rB.getZul()))
			clear();
	}

	@Command
	@SmartNotifyChange({ "lista", "moduloValidada", "semillaValidada", "constanteMultiplicativaValidada",
			"constanteAditivaValidada", "ingresarModuloIconSrc", "ingresarSemillaIconSrc",
			"ingresarConstanteMultiplicativaIconSrc", "ingresarConstanteAditivaIconSrc", "cantidadNumerosValidada",
			"cantidadIntervalosValidada", "ingresarCantidadNumerosIconSrc", "ingresarCantidadIntervalosIconSrc",
			"mostrandoResultados" })
	public void clear() {
		tP1PuntoC.reiniciar();
		setCantidadNumerosValidada(false);
		setCantidadIntervalosValidada(false);
		setSemillaValidada(false);
		setConstanteMultiplicativaValidada(false);
		setConstanteAditivaValidada(false);
		setModuloValidada(false);
		setMostrandoResultados(false);
		inicializarIconosValidaciones();
		inicializarValidaciones();
	}

	@Command
	@SmartNotifyChange({ "tP1PuntoC", "graficaSrc", "chiCuadradoSrc", "tablaFrecuenciasSrc", "mostrandoResultados" })
	public void correr() throws Exception {
		tP1PuntoC.solucionar();
		setMostrandoResultados(true);
	}

	@Command
	@SmartNotifyChange("mostrandoResultados")
	public void limpiarResultados() {
		setMostrandoResultados(false);
		BindUtils.postGlobalCommand(null, null, "limpiarTablaFrecuencias", null);
		BindUtils.postGlobalCommand(null, null, "limpiarTestChiCuadrado", null);
	}

	// "Validación"
	@Override
	public void update(Observable o, Object arg) {
		if (o instanceof IngresarCantidadNumerosValidator)
			updateValidacion((IngresarCantidadNumerosValidator) o);
		if (o instanceof IngresarCantidadIntervalosValidator)
			updateValidacion((IngresarCantidadIntervalosValidator) o);
		if (o instanceof IngresarSemillaValidator)
			updateValidacion((IngresarSemillaValidator) o);
		if (o instanceof IngresarConstanteMultiplicativaValidator)
			updateValidacion((IngresarConstanteMultiplicativaValidator) o);
		if (o instanceof IngresarConstanteAditivaValidator)
			updateValidacion((IngresarConstanteAditivaValidator) o);
		if (o instanceof IngresarModuloValidator)
			updateValidacion((IngresarModuloValidator) o);
	}

	private void updateValidacion(IngresarCantidadNumerosValidator val) {
		String iconoSrc = registrarResultadoValidacion((InputValidator) val);
		ingresarCantidadNumerosIconSrc = iconoSrc;
		if (iconoSrc != null && iconoSrc.equals(TILDE_SRC) || iconoSrc == null)
			setCantidadNumerosValidada(true);
		else
			setCantidadNumerosValidada(false);
		BindUtils.postNotifyChange(null, null, this, "ingresarCantidadNumerosIconSrc");
		BindUtils.postNotifyChange(null, null, this, "cantidadNumerosValidada");
	}

	private void updateValidacion(IngresarCantidadIntervalosValidator val) {
		String iconoSrc = registrarResultadoValidacion((InputValidator) val);
		ingresarCantidadIntervalosIconSrc = iconoSrc;
		if (iconoSrc != null && iconoSrc.equals(TILDE_SRC) || iconoSrc == null)
			setCantidadIntervalosValidada(true);
		else
			setCantidadIntervalosValidada(false);
		BindUtils.postNotifyChange(null, null, this, "ingresarCantidadIntervalosIconSrc");
		BindUtils.postNotifyChange(null, null, this, "cantidadIntervalosValidada");
	}

	private void updateValidacion(IngresarSemillaValidator val) {
		String iconoSrc = registrarResultadoValidacion((InputValidator) val);
		ingresarSemillaIconSrc = iconoSrc;
		if (iconoSrc != null && iconoSrc.equals(TILDE_SRC) || iconoSrc == null)
			setSemillaValidada(true);
		else
			setSemillaValidada(false);
		BindUtils.postNotifyChange(null, null, this, "ingresarSemillaIconSrc");
		BindUtils.postNotifyChange(null, null, this, "semillaValidada");
	}

	private void updateValidacion(IngresarConstanteMultiplicativaValidator val) {
		String iconoSrc = registrarResultadoValidacion((InputValidator) val);
		ingresarConstanteMultiplicativaIconSrc = iconoSrc;
		if (iconoSrc != null && iconoSrc.equals(TILDE_SRC) || iconoSrc == null)
			setConstanteMultiplicativaValidada(true);
		else
			setConstanteMultiplicativaValidada(false);
		BindUtils.postNotifyChange(null, null, this, "ingresarConstanteMultiplicativaIconSrc");
		BindUtils.postNotifyChange(null, null, this, "constanteMultiplicativaValidada");
	}

	private void updateValidacion(IngresarConstanteAditivaValidator val) {
		String iconoSrc = registrarResultadoValidacion((InputValidator) val);
		ingresarConstanteAditivaIconSrc = iconoSrc;
		if (iconoSrc != null && iconoSrc.equals(TILDE_SRC) || iconoSrc == null)
			setConstanteAditivaValidada(true);
		else
			setConstanteAditivaValidada(false);
		BindUtils.postNotifyChange(null, null, this, "ingresarConstanteAditivaIconSrc");
		BindUtils.postNotifyChange(null, null, this, "constanteAditivaValidada");
	}

	private void updateValidacion(IngresarModuloValidator val) {
		String iconoSrc = registrarResultadoValidacion((InputValidator) val);
		ingresarModuloIconSrc = iconoSrc;
		if (iconoSrc != null && iconoSrc.equals(TILDE_SRC) || iconoSrc == null)
			setModuloValidada(true);
		else
			setModuloValidada(false);
		BindUtils.postNotifyChange(null, null, this, "ingresarModuloIconSrc");
		BindUtils.postNotifyChange(null, null, this, "moduloValidada");
	}

	private String registrarResultadoValidacion(InputValidator val) {
		String res = null;
		if (!val.isIncorrecta() && !val.isNula())
			res = TILDE_SRC;
		else if (val.isIncorrecta())
			res = ADVERTENCIA_SRC;
		else if (val.isNula() && val.isObligatoria())
			res = ASTERISCO_SRC;
		else if (val.isNula() && !val.isObligatoria())
			res = null;
		return res;
	}

	@DependsOn({ "cantidadNumerosValidada", "cantidadIntervalosValidada", "semillaValidada",
			"constanteMultiplicativaValidada", "constanteAditivaValidada", "moduloValidada" })
	public boolean isValidacionOk() {
		return (cantidadNumerosValidada || (!getIngresarCantidadNumerosValidator().isObligatoria()
				&& getIngresarCantidadNumerosIconSrc() == null))
				&& (cantidadIntervalosValidada || (!getIngresarCantidadIntervalosValidator().isObligatoria()
						&& getIngresarCantidadIntervalosIconSrc() == null))
				&& (semillaValidada
						|| (!getIngresarSemillaValidator().isObligatoria() && getIngresarSemillaIconSrc() == null))
				&& (constanteMultiplicativaValidada || (!getIngresarConstanteMultiplicativaValidator().isObligatoria()
						&& getIngresarConstanteMultiplicativaIconSrc() == null))
				&& (constanteAditivaValidada || (!getIngresarConstanteAditivaValidator().isObligatoria()
						&& getIngresarConstanteAditivaIconSrc() == null))
				&& (moduloValidada
						|| (!getIngresarModuloValidator().isObligatoria() && getIngresarModuloIconSrc() == null));
	}

	public InputValidator getIngresarCantidadNumerosValidator() {
		return ingresarCantidadNumerosValidator;
	}

	public InputValidator getIngresarCantidadIntervalosValidator() {
		return ingresarCantidadIntervalosValidator;
	}

	public InputValidator getIngresarSemillaValidator() {
		return ingresarSemillaValidator;
	}

	public InputValidator getIngresarConstanteMultiplicativaValidator() {
		return ingresarConstanteMultiplicativaValidator;
	}

	public InputValidator getIngresarConstanteAditivaValidator() {
		return ingresarConstanteAditivaValidator;
	}

	public InputValidator getIngresarModuloValidator() {
		return ingresarModuloValidator;
	}

	@DependsOn("cantidadNumerosValidada")
	public String getDefaultTooltiptextIngresarCantidadNumeros() {
		String res = null;
		if (getIngresarCantidadNumerosValidator().isObligatoria() && !cantidadNumerosValidada)
			res = "Este campo es obligatorio.";
		else if (cantidadNumerosValidada)
			res = "Cantidad válida.";
		return res;
	}

	@DependsOn("cantidadIntervalosValidada")
	public String getDefaultTooltiptextIngresarCantidadIntervalos() {
		String res = null;
		if (getIngresarCantidadIntervalosValidator().isObligatoria() && !cantidadIntervalosValidada)
			res = "Este campo es obligatorio.";
		else if (cantidadIntervalosValidada)
			res = "Constante multiplicativa válida.";
		return res;
	}

	@DependsOn("semillaValidada")
	public String getDefaultTooltiptextIngresarSemilla() {
		String res = null;
		if (getIngresarSemillaValidator().isObligatoria() && !semillaValidada)
			res = "Este campo es obligatorio.";
		else if (semillaValidada)
			res = "Semilla válida.";
		return res;
	}

	@DependsOn("constanteMultiplicativaValidada")
	public String getDefaultTooltiptextIngresarConstanteMultiplicativa() {
		String res = null;
		if (getIngresarConstanteMultiplicativaValidator().isObligatoria() && !constanteMultiplicativaValidada)
			res = "Este campo es obligatorio.";
		else if (constanteMultiplicativaValidada)
			res = "Constante multiplicativa válida.";
		return res;
	}

	@DependsOn("constanteAditivaValidada")
	public String getDefaultTooltiptextIngresarConstanteAditiva() {
		String res = null;
		if (getIngresarConstanteAditivaValidator().isObligatoria() && !constanteAditivaValidada)
			res = "Este campo es obligatorio.";
		else if (constanteAditivaValidada)
			res = "Constante aditiva válida.";
		return res;
	}

	@DependsOn("moduloValidada")
	public String getDefaultTooltiptextIngresarModulo() {
		String res = null;
		if (getIngresarModuloValidator().isObligatoria() && !moduloValidada)
			res = "Este campo es obligatorio.";
		else if (moduloValidada)
			res = "Módulo válido.";
		return res;
	}

	public String getIngresarCantidadNumerosIconSrc() {
		return ingresarCantidadNumerosIconSrc;
	}

	public String getIngresarCantidadIntervalosIconSrc() {
		return ingresarCantidadIntervalosIconSrc;
	}

	public String getIngresarSemillaIconSrc() {
		return ingresarSemillaIconSrc;
	}

	public String getIngresarConstanteMultiplicativaIconSrc() {
		return ingresarConstanteMultiplicativaIconSrc;
	}

	public String getIngresarConstanteAditivaIconSrc() {
		return ingresarConstanteAditivaIconSrc;
	}

	public String getIngresarModuloIconSrc() {
		return ingresarModuloIconSrc;
	}

	public boolean isCantidadNumerosValidada() {
		return cantidadNumerosValidada;
	}

	public boolean isCantidadIntervalosValidada() {
		return cantidadIntervalosValidada;
	}

	public boolean isSemillaValidada() {
		return semillaValidada;
	}

	public boolean isConstanteMultiplicativaValidada() {
		return constanteMultiplicativaValidada;
	}

	public boolean isConstanteAditivaValidada() {
		return constanteAditivaValidada;
	}

	public boolean isModuloValidada() {
		return moduloValidada;
	}

	public void setIngresarCantidadNumerosIconSrc(String ingresarCantidadNumerosIconSrc) {
		this.ingresarCantidadNumerosIconSrc = ingresarCantidadNumerosIconSrc;
	}

	public void setIngresarCantidadIntervalosIconSrc(String ingresarCantidadIntervalosIconSrc) {
		this.ingresarCantidadIntervalosIconSrc = ingresarCantidadIntervalosIconSrc;
	}

	public void setIngresarSemillaIconSrc(String ingresarSemillaIconSrc) {
		this.ingresarSemillaIconSrc = ingresarSemillaIconSrc;
	}

	public void setIngresarConstanteMultiplicativaIconSrc(String ingresarConstanteMultiplicativaIconSrc) {
		this.ingresarConstanteMultiplicativaIconSrc = ingresarConstanteMultiplicativaIconSrc;
	}

	public void setIngresarConstanteAditivaIconSrc(String ingresarConstanteAditivaIconSrc) {
		this.ingresarConstanteAditivaIconSrc = ingresarConstanteAditivaIconSrc;
	}

	public void setIngresarModuloIconSrc(String ingresarModuloIconSrc) {
		this.ingresarModuloIconSrc = ingresarModuloIconSrc;
	}

	public void setCantidadNumerosValidada(boolean cantidadNumerosValidada) {
		this.cantidadNumerosValidada = cantidadNumerosValidada;
	}

	public void setCantidadIntervalosValidada(boolean cantidadIntervalosValidada) {
		this.cantidadIntervalosValidada = cantidadIntervalosValidada;
	}

	public void setSemillaValidada(boolean semillaValidada) {
		this.semillaValidada = semillaValidada;
	}

	public void setConstanteMultiplicativaValidada(boolean constanteMultiplicativaValidada) {
		this.constanteMultiplicativaValidada = constanteMultiplicativaValidada;
	}

	public void setConstanteAditivaValidada(boolean constanteAditivaValidada) {
		this.constanteAditivaValidada = constanteAditivaValidada;
	}

	public void setModuloValidada(boolean moduloValidada) {
		this.moduloValidada = moduloValidada;
	}
	// Fin "Validación"

	public Converter<?, ?, ?> getConversorDeInt() {
		return conversorDeInt;
	}

	public Converter<?, ?, ?> getConversorDeLong() {
		return conversorDeLong;
	}

	public Converter<?, ?, ?> getTruncador() {
		formateadorDeNumerosDecimales.setPattern("#.####");
		return formateadorDeNumerosDecimales;
	}

	@DependsOn("mostrandoResultados")
	public String getResultadosSrc() {
		return mostrandoResultados ? rB.getPagePathname("resultados") : null;
	}

	public String getInputSrc() {
		return rB.getPagePathname("input");
	}

	@DependsOn("resultadosSrc")
	public String getGraficaSrc() {
		String src = null;
		if (tP1PuntoC != null && tP1PuntoC.getSerie() != null && tP1PuntoC.getSerie().hasGrafica())
			src = rB.getPagePathname("grafica");
		return src;
	}

	@DependsOn("resultadosSrc")
	public String getTablaFrecuenciasSrc() {
		String src = null;
		if (tP1PuntoC != null && tP1PuntoC.getSerie() != null && tP1PuntoC.getSerie().hasGrafica())
			src = rB.getPagePathname("tablafrecuencias");
		return src;
	}

	@DependsOn("resultadosSrc")
	public String getChiCuadradoSrc() {
		String src = null;
		if (tP1PuntoC != null && tP1PuntoC.getSerie() != null && tP1PuntoC.getSerie().hasGrafica())
			src = rB.getPagePathname("chicuadrado.");
		return src;
	}

	@DependsOn("resultadosSrc")
	public String getChiCuadradoTablaSrc() {
		String src = null;
		if (tP1PuntoC != null && tP1PuntoC.getSerie() != null && tP1PuntoC.getSerie().hasGrafica())
			src = rB.getPagePathname("chicuadrado-tabla");
		return src;
	}

	public void setInputSrc(String inputSrc) {
	}

	public TP1PuntoC gettP1PuntoC() {
		if (tP1PuntoC == null)
			tP1PuntoC = (TP1PuntoC) SpringUtil.getBean("tP1PuntoC");
		return tP1PuntoC;
	}

	public void settP1PuntoC(TP1PuntoC tP1PuntoC) {
		this.tP1PuntoC = tP1PuntoC;
	}

	@DependsOn("tP1PuntoC")
	public ListModelList<Double> getLista() {
		lista.clear();
		List<Double> numeros = new ArrayList<>();
		if (tP1PuntoC != null && tP1PuntoC.getSerie() != null && tP1PuntoC.getSerie().getArray() != null) {
			for (Double d : tP1PuntoC.getSerie().getArray())
				numeros.add(d);
			lista.addAll(numeros);
			lista.addToSelection(tP1PuntoC.getSerie().getArray()[tP1PuntoC.getSerie().getArray().length - 1]);
		}
		return lista;
	}

	public boolean isMostrandoResultados() {
		return mostrandoResultados;
	}

	public void setMostrandoResultados(boolean mostrandoResultados) {
		this.mostrandoResultados = mostrandoResultados;
	}

	public String getTituloChiCuadrado() {
		return "Prueba de \u03C7\u2072";
	}

	@DependsOn({ "cantidadNumerosValidada", "cantidadIntervalosValidada", "semillaValidada",
			"constanteMultiplicativaValidada", "constanteAditivaValidada", "moduloValidada" })
	public boolean isCorrerHabilitado() {
		return !mostrandoResultados && isValidacionOk();
	}

}
