package app.tp1.puntoc;

import app.Ejercicio;
import app.utils.Serie;
import app.utils.SerieFactory;
import app.utils.generador.GeneradorCongruencialLineal;

public interface TP1PuntoC extends Ejercicio {

	void reiniciar();

	Serie getSerie();
	
	int getCantidadNumeros();

	void setCantidadNumeros(int cantidadNumeros);
	
	int getCantidadIntervalos();
	
	void setCantidadIntervalos(int cantidadIntervalos);
	
	GeneradorCongruencialLineal getGeneradorCongruencialLineal();

	void crearSerie();

	SerieFactory getSerieFactory();

}
