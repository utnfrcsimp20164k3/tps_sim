package app.tp4.problema6;

import java.math.BigDecimal;
import java.math.RoundingMode;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.zkoss.bind.annotation.Immutable;

import app.utils.ParRandomEvento;
import app.utils.Porcentaje;
import app.utils.SimuladorDeEventos;
import app.utils.distribucion.TipoDistribucion;

@Component
public class PuertaFactoryImpl implements PuertaFactory {

	static final Logger LOG = LoggerFactory.getLogger(PuertaFactoryImpl.class);
	@Autowired
	private ApplicationContext applicationContext;
	private SimuladorDeEventos simuladorAbre;
	private TipoDistribucion tipoDistribucion;
	private PersonaFactory personaFactory;
	private Puerta instance;
	private Double rndAbre;
	private BigDecimal probabilidadSeAbra;
	boolean abre;

	public PuertaFactoryImpl() {
		super();
	}

	@Bean
	@Scope(value = "singleton")
	static public PuertaFactory puertaFactory() {
		return new PuertaFactoryImpl();
	}

	@PostConstruct
	public void init() {
		personaFactory = (PersonaFactory) applicationContext.getBean("personaFactory");
		simuladorAbre = (SimuladorDeEventos) applicationContext.getBean("simuladorDeEventos");
		tipoDistribucion = (TipoDistribucion) applicationContext.getBean("distribucionUniforme");
		BigDecimal d = new BigDecimal("70").setScale(Porcentaje.getDefaultScale(), RoundingMode.HALF_UP);
		setProbabilidadSeAbra(d);
	}

	@Override
	public Puerta getObject() throws Exception {
		clear();
		instance = new PuertaImpl();
		simularSiAbre();
		instance.setRndAbre(rndAbre);
		instance.setAbierta(abre);
		if (abre)
			instance.setPersona(personaFactory.getObject());
		return instance;
	}

	private void clear() {
		instance = null;
		rndAbre = null;
		abre = false;
		simuladorAbre.clear();
	}

	private void simularSiAbre() {
		ParRandomEvento par = simuladorAbre.next();
		rndAbre = par.getLeft();
		abre = (Boolean) par.getRight();
	}

	@Override
	public Class<?> getObjectType() {
		return Puerta.class;
	}

	@Override
	public boolean isSingleton() {
		return true;
	}

	@Override
	@Immutable
	public BigDecimal getProbabilidadSeAbra() {
		return probabilidadSeAbra;
	}

	@Override
	public void setProbabilidadSeAbra(BigDecimal probabilidadSeAbra) {
		this.probabilidadSeAbra = probabilidadSeAbra;
	}

	@Override
	public void setup() {
		simuladorAbre.asociar(probabilidadSeAbra.divide(new BigDecimal("100")), Boolean.TRUE);
		simuladorAbre.asociar(new BigDecimal("1").subtract(probabilidadSeAbra.divide(new BigDecimal("100"))),
				Boolean.FALSE);
		simuladorAbre.setTipoDistribucion(tipoDistribucion);
		simuladorAbre.setup();
		personaFactory.setup();
	}

}
