package app.tp4.problema6;

public interface Visita {
	
	void setPuerta(Puerta puerta);
	
	Puerta getPuerta();

}
