package app.tp4.problema6;

import java.math.BigDecimal;
import java.math.RoundingMode;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.zkoss.bind.annotation.Immutable;

@Component
public class SuscripcionFactoryImpl implements SuscripcionFactory {
	
	static final Logger LOG = LoggerFactory.getLogger(SuscripcionFactoryImpl.class);
	private Suscripcion instance;
	private BigDecimal utilidadPorSuscripcion;

	public SuscripcionFactoryImpl() {
		super();
	}

	@Bean
	@Scope(value = "singleton")
	static public SuscripcionFactory suscripcionFactory() {
		return new SuscripcionFactoryImpl();
	}

	@PostConstruct
	public void init() {
		BigDecimal d = new BigDecimal("2").setScale(2, RoundingMode.HALF_UP);
		setUtilidadPorSuscripcion(d);
	}

	@Override
	public Suscripcion getObject() throws Exception {
		clear();
		instance = new SuscripcionImpl();
		instance.setUtilidad(utilidadPorSuscripcion);
		return instance;
	}

	private void clear() {
		instance = null;
	}

	@Override
	public Class<?> getObjectType() {
		return Suscripcion.class;
	}

	@Override
	public boolean isSingleton() {
		return true;
	}

	@Override
	public void setUtilidadPorSuscripcion(BigDecimal utilidadPorSuscripcion) {
		this.utilidadPorSuscripcion = utilidadPorSuscripcion;
	}

	@Override
	@Immutable
	public BigDecimal getUtilidadPorSuscripcion() {
		return utilidadPorSuscripcion;
	}

	@Override
	public void setup() { }
	
}
