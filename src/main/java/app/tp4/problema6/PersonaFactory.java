package app.tp4.problema6;

import java.math.BigDecimal;

public interface PersonaFactory {
	
	void setProporcionMujeres(BigDecimal proporcionMujeres);
	
	BigDecimal getProporcionMujeres();

	void setProporcionHombres(BigDecimal proporcionHombres);
	
	BigDecimal getProporcionHombres();

	Persona getObject() throws Exception;

	Class<?> getObjectType();

	boolean isSingleton();
	
	void setup();

}
