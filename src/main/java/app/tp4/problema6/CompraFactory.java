package app.tp4.problema6;

public interface CompraFactory {

	Compra getObject() throws Exception;

	Class<?> getObjectType();

	boolean isSingleton();

	void setup();

}
