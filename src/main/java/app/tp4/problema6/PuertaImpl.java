package app.tp4.problema6;

public class PuertaImpl implements Puerta {

	Persona persona;
	Boolean abierta;
	Double rndAbre;
	
	@Override
	public void setPersona(Persona persona) {
		this.persona = persona;
	}

	@Override
	public Persona getPersona() {
		return persona;
	}

	@Override
	public void setAbierta(Boolean abierta) {
		this.abierta = abierta;
	}

	@Override
	public Boolean isAbierta() {
		return abierta;
	}

	@Override
	public void setRndAbre(Double rndAbre) {
		this.rndAbre = rndAbre;
	}

	@Override
	public Double getRndAbre() {
		return rndAbre;
	}

}
