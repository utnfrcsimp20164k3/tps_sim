package app.tp4.problema6;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
public class CompraFactoryImpl implements CompraFactory {

	static final Logger LOG = LoggerFactory.getLogger(CompraFactoryImpl.class);
	private Compra instance;

	public CompraFactoryImpl() {
		super();
	}

	@Bean
	@Scope(value = "singleton")
	static public CompraFactory compraFactory() {
		return new CompraFactoryImpl();
	}

	@Override
	public Compra getObject() throws Exception {
		clear();
		instance = new CompraImpl();
		return instance;
	}

	private void clear() {
		instance = null;
	}

	@Override
	public Class<?> getObjectType() {
		return Compra.class;
	}

	@Override
	public boolean isSingleton() {
		return true;
	}

	@Override
	public void setup() {
	}

}
