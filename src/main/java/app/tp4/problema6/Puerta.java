package app.tp4.problema6;

public interface Puerta {

	void setPersona(Persona persona);
	
	Persona getPersona();
	
	void setAbierta(Boolean abierta);
	
	Boolean isAbierta();
	
	void setRndAbre(Double rndAbre);
	
	Double getRndAbre();

}
