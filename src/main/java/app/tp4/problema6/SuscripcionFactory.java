package app.tp4.problema6;

import java.math.BigDecimal;

public interface SuscripcionFactory {
	
	void setUtilidadPorSuscripcion(BigDecimal utilidadPorSuscripcion);
	
	BigDecimal getUtilidadPorSuscripcion();

	Suscripcion getObject() throws Exception;

	Class<?> getObjectType();

	boolean isSingleton();

	void setup();

}
