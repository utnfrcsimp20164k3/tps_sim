package app.tp4.problema6;

import java.math.BigDecimal;

public interface Suscripcion {
	
	void setUtilidad(BigDecimal utilidad);
	
	BigDecimal getUtilidad();

}
