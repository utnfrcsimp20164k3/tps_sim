package app.tp4.problema6;

public interface Persona {
	
	void setRndGenero(Double rndGenero);
	
	Double getRndGenero();
	
	String mostrarGenero();
	
	void setRndCompra(Double rndCompra);
	
	Double getRndCompra();
	
	void setRndCantidadSuscripciones(Double rndCantidadSuscripciones);
	
	Double getRndCantidadSuscripciones();
	
	void setCompra(Compra compra);
	
	Compra getCompra();
	
	void setComprador(Boolean comprador);
	
	Boolean isComprador();

}
