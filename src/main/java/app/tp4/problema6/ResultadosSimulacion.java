package app.tp4.problema6;

import app.utils.DetalleTabla;

public interface ResultadosSimulacion extends ResumenFinal {

	void setDetalleTablaFinal(DetalleTabla detalleTablaFinal);
	
	boolean hasDetalleTablaFinal();

	Long getMujeres();

	Long getHombres();

	Long getAbiertas();

	Long getVentasAMujeres();

	Long getMujeresUnaSuscripcion();

	Long getMujeresDosSuscripciones();

	Long getMujeresTresSuscripciones();

	Long getVentasAHombres();

	Long getHombresUnaSuscripcion();

	Long getHombresDosSuscripciones();

	Long getHombresTresSuscripciones();

	Long getHombresCuatroSuscripciones();
}
