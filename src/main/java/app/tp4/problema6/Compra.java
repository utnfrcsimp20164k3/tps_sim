package app.tp4.problema6;

public interface Compra {
	
	Suscripcion[] getSuscripciones();
	
	void setSuscripciones(Suscripcion[] suscripciones);
	
	void setRndCantidadSuscripciones(Double rndCantidadSuscripciones);
	
	Double getRndCantidadSuscripciones();
	
	Long getCantidadSuscripciones();
	
	boolean isUnaSuscripcion();
	
	boolean isDosSuscripciones();
	
	boolean isTresSuscripciones();
	
	boolean isCuatroSuscripciones();

}
