package app.tp4.problema6;

import java.math.BigDecimal;
import java.math.RoundingMode;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import app.utils.DetalleTabla;
import app.utils.Observable;
import app.utils.Observer;
import app.utils.VectorEstados;

@Component
public class VectorEstadosCompraImpl implements VectorEstados<Compra> {
	
	@Autowired
	ApplicationContext applicationContext;

	public VectorEstadosCompraImpl() {
		super();
	}

	@Bean
	@Scope("prototype")
	static public VectorEstados<Compra> vectorEstadosCompra() {
		return new VectorEstadosCompraImpl();
	}

	@Override
	public DetalleTabla parse(Compra compra) {
		DetalleTabla actual = (DetalleTabla) applicationContext.getBean("detalleTabla");
		actual.getValores().put("Rnd cantidad de suscripciones", compra.getRndCantidadSuscripciones());
		actual.getValores().put("Cantidad de suscripciones", compra.getSuscripciones().length);
		actual.getValores().put("Una suscripcion", compra.isUnaSuscripcion() ? "Sí" : null);
		actual.getValores().put("Dos suscripciones", compra.isDosSuscripciones() ? "Sí" : null);
		actual.getValores().put("Tres suscripciones", compra.isTresSuscripciones() ? "Sí" : null);
		actual.getValores().put("Cuatro suscripciones", compra.isCuatroSuscripciones() ? "Sí" : null);
		BigDecimal utilidad = new BigDecimal("0");
		utilidad = utilidad.setScale(2, RoundingMode.HALF_UP);
		for (Suscripcion s : compra.getSuscripciones())
			utilidad = utilidad.add(s.getUtilidad());
		actual.getValores().put("Utilidad", utilidad);
		return actual;
	}

	@Override
	public Object[] getClaves() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void update(Observable o, Object arg) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void addObserver(Observer o) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public int countObservers() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void deleteObserver(Observer o) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void deleteObservers() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean hasChanged() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void notifyObservers() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void notifyObservers(Object arg) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void removeObserver(Observer o) {
		// TODO Auto-generated method stub
		
	}
	
}
