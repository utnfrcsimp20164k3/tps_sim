package app.tp4.problema6;

import app.Ejercicio;
import app.utils.TablaSimple;

public interface TP4Problema6 extends Ejercicio {

	void reiniciar();

	TablaSimple getTabla();

	Long getNumeroVisitasASimular();

	void setNumeroVisitasASimular(Long numeroVisitasASimular);

	Long getPrimeraVisitaAMostrar();

	void setPrimeraVisitaAMostrar(Long primeraVisitaAMostrar);

	Long getNumeroVisitasAMostrar();

	void setNumeroVisitasAMostrar(Long numeroVisitasAMostrar);

	TP4Problema6Resultados getResultadosSimulacion();

	boolean hasResultadosSimulacion();
	
}
