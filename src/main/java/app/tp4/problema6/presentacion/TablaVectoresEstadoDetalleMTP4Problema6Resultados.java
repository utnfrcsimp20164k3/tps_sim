package app.tp4.problema6.presentacion;

import app.utils.DetalleTabla;

public interface TablaVectoresEstadoDetalleMTP4Problema6Resultados {
	
	void setDetalleTabla(DetalleTabla d);

	Object getValorA();

	Object getValorB();

	Object getValorC();

}
