package app.tp4.problema6.presentacion;

import java.util.HashSet;
import java.util.Set;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.zkoss.bind.ValidationContext;
import org.zkoss.bind.validator.AbstractValidator;
import org.zkoss.zel.ELException;

import app.presentacion.InputValidator;
import app.presentacion.Observable;
import app.presentacion.Observer;
import app.tp4.problema6.TP4Problema6;

@Component
public class IngresarNumeroVisitasASimularValidatorImpl extends AbstractValidator
		implements IngresarNumeroVisitasASimularValidator, InputValidator, Observable {

	private Set<Observer> observers;
	private boolean changed;
	private final boolean obligatoria = true;
	private boolean incorrecta;
	private boolean nula;

	IngresarNumeroVisitasASimularValidatorImpl() {
		super();
		observers = new HashSet<>();
	}

	@Bean
	@Scope("prototype")
	static public IngresarNumeroVisitasASimularValidator ingresarNumeroVisitasASimularValidator() {
		return new IngresarNumeroVisitasASimularValidatorImpl();
	}

	@Override
	public void validate(ValidationContext ctx) {
		Long cantidad = (Long) ctx.getProperties("numeroVisitasASimular")[0].getValue();
		try {
			if (cantidad == null || cantidad == 0l) {
				addInvalidMessage(ctx, "ingresarNumeroVisitasASimular",
						"Debe ingresar un número de visitas a similar.");
				nula = true;
			} else if (cantidad < 0l) {
				addInvalidMessage(ctx, "ingresarNumeroVisitasASimular", "Debe ingresar un número entero positivo.");
				incorrecta = true;
			}
		} catch (NumberFormatException ex) {
			addInvalidMessage(ctx, "ingresarNumeroVisitasASimular", "Debe ingresar un número entero positivo.");
			incorrecta = true;
		} catch (ELException ex) {
			addInvalidMessage(ctx, "ingresarNumeroVisitasASimular", "Debe ingresar un número entero positivo.");
			incorrecta = true;
		} finally {
			changed = true;
			notifyObservers();
		}
	}

	@Override
	public void validate(TP4Problema6 tP) {
		Long simular = tP.getNumeroVisitasASimular();
		if (simular == null || simular == 0l)
			nula = true;
		else if (simular < 0l)
			incorrecta = true;
	}

	@Override
	public boolean isObligatoria() {
		return obligatoria;
	}

	@Override
	public boolean isIncorrecta() {
		return incorrecta;
	}

	@Override
	public boolean isNula() {
		return nula;
	}

	@Override
	public void addObserver(Observer o) {
		observers.add(o);
	}

	@Override
	public int countObservers() {
		return observers.size();
	}

	@Override
	public void deleteObserver(Observer o) {
		observers.remove(o);
	}

	@Override
	public void deleteObservers() {
		observers.clear();
	}

	@Override
	public boolean hasChanged() {
		return changed;
	}

	@Override
	public void notifyObservers() {
		if (hasChanged())
			for (Observer obs : observers)
				obs.update(this, null);
		clearChanged();
	}

	@Override
	public void notifyObservers(Object arg) {
		if (hasChanged())
			for (Observer obs : observers)
				obs.update(this, arg);
		clearChanged();
	}

	protected void clearChanged() {
		changed = false;
		incorrecta = false;
		nula = false;
	}

}
