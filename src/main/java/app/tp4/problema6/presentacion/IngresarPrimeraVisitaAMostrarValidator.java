package app.tp4.problema6.presentacion;

import app.presentacion.InputValidator;
import app.tp4.problema6.TP4Problema6;

public interface IngresarPrimeraVisitaAMostrarValidator extends InputValidator {

	void validate(TP4Problema6 tp);

}
