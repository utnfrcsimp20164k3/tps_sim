package app.tp4.problema6.presentacion;

import java.math.BigDecimal;

import app.presentacion.InputValidator;

public interface IngresarProporcionMujeresValidator extends InputValidator {

	void validate(BigDecimal proporcion);

}
