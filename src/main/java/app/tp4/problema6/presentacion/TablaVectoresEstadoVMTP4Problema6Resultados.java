package app.tp4.problema6.presentacion;

import org.zkoss.bind.Converter;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.GlobalCommand;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.SmartNotifyChange;
import org.zkoss.zk.ui.select.annotation.VariableResolver;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.ListModelList;

import app.presentacion.ConversorDeDouble;
import app.presentacion.ConversorDeInt;
import app.presentacion.ConversorDeLong;
import app.tp4.problema6.TP4Problema6Resultados;
import app.tp5.ejerciciod.TP5EjercicioDResultados;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@VariableResolver(org.zkoss.zkplus.spring.DelegatingVariableResolver.class)
public class TablaVectoresEstadoVMTP4Problema6Resultados {

	static final Logger LOG = LoggerFactory.getLogger(TablaVectoresEstadoVMTP4Problema6Resultados.class);
	@WireVariable
	TablaVectoresEstadoMTP4Problema6Resultados tablaVectoresEstadoMTP4Problema6Resultados;
	@WireVariable
	ConversorDeInt conversorDeInt;
	@WireVariable
	ConversorDeDouble conversorDeDouble;
	@WireVariable
	ConversorDeLong conversorDeLong;

	@Init
	public void initSetup(@BindingParam("tP4Problema6Resultados") TP4Problema6Resultados tP4Problema6Resultados) {
		tablaVectoresEstadoMTP4Problema6Resultados.setTabla(tP4Problema6Resultados.getTabla());
	}
	
	@GlobalCommand
	@SmartNotifyChange("tablaVectoresEstadoM")
	public void limpiarTablaVectoresEstado() {
		tablaVectoresEstadoMTP4Problema6Resultados.clear();
	}

	public ListModelList<TablaVectoresEstadoDetalleMTP4Problema6Resultados> getDetalles() {
		return tablaVectoresEstadoMTP4Problema6Resultados.getDetalles();
	}

	public Converter<?, ?, ?> getConversorDeInt() {
		return conversorDeInt;
	}

	public Converter<?, ?, ?> getConversorDeLong() {
		return conversorDeLong;
	}

	public Converter<?, ?, ?> getConversorDeDouble() {
		return conversorDeDouble;
	}
	
	public TablaVectoresEstadoMTP4Problema6Resultados getTablaVectoresEstadoM() {
		return tablaVectoresEstadoMTP4Problema6Resultados;
	}

}
