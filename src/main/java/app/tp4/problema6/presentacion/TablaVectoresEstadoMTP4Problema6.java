package app.tp4.problema6.presentacion;

import org.zkoss.zul.ListModelList;

import app.utils.TablaSimple;

public interface TablaVectoresEstadoMTP4Problema6 {

	void setTabla(TablaSimple tablaSimple);
	
	Object getEncabezadoA();

	Object getEncabezadoB();

	Object getEncabezadoC();

	Object getEncabezadoD();

	Object getEncabezadoE();

	Object getEncabezadoF();

	Object getEncabezadoG();

	Object getEncabezadoH();

	Object getEncabezadoI();

	Object getEncabezadoJ();

	Object getEncabezadoK();

	Object getEncabezadoL();

	Object getEncabezadoM();

	Object getEncabezadoN();

	Object getEncabezadoO();

	Object getEncabezadoP();

	Object getEncabezadoQ();

	Object getEncabezadoR();

	Object getEncabezadoS();

	Object getEncabezadoT();

	Object getEncabezadoU();

	Object getEncabezadoV();

	Object getEncabezadoW();

	Object getEncabezadoX();

	Object getEncabezadoY();

	Object getEncabezadoZ();

	Object getEncabezadoAA();

	Object getEncabezadoAB();

	Object getEncabezadoAC();

	ListModelList<TablaVectoresEstadoDetalleMTP4Problema6> getDetalles();

	void clear();

}
