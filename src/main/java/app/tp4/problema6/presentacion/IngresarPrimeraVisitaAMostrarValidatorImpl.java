package app.tp4.problema6.presentacion;

import java.util.HashSet;
import java.util.Set;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.zkoss.bind.ValidationContext;
import org.zkoss.bind.validator.AbstractValidator;
import org.zkoss.zel.ELException;

import app.presentacion.InputValidator;
import app.presentacion.Observable;
import app.presentacion.Observer;
import app.tp4.problema6.TP4Problema6;

@Component
public class IngresarPrimeraVisitaAMostrarValidatorImpl extends AbstractValidator
		implements IngresarPrimeraVisitaAMostrarValidator, InputValidator, Observable {

	private Set<Observer> observers;
	private boolean changed;
	private final boolean obligatoria = true;
	private boolean incorrecta;
	private boolean nula;

	IngresarPrimeraVisitaAMostrarValidatorImpl() {
		super();
		observers = new HashSet<>();
	}

	@Bean
	@Scope("prototype")
	static public IngresarPrimeraVisitaAMostrarValidator ingresarPrimeraVisitaAMostrarValidator() {
		return new IngresarPrimeraVisitaAMostrarValidatorImpl();
	}

	@Override
	public void validate(ValidationContext ctx) {
		Long primera = (Long) ctx.getProperty().getValue();
		Long numero = (Long) ctx.getProperties("numeroVisitasASimular")[0].getValue();
		try {
			if (primera == null || primera == 0l) {
				addInvalidMessage(ctx, "ingresarPrimeraVisitaAMostrar",
						"Debe especificar la primera visita a mostrar.");
				nula = true;
			} else if (primera < 0l) {
				addInvalidMessage(ctx, "ingresarPrimeraVisitaAMostrar", "Debe ingresar un número entero positivo.");
				incorrecta = true;
			} else if (numero != null && primera > numero) {
				addInvalidMessage(ctx, "ingresarPrimeraVisitaAMostrar",
						"Debe ingresar un número menor o igual al total de visitas a simular.");
				incorrecta = true;
			}
		} catch (NumberFormatException ex) {
			addInvalidMessage(ctx, "ingresarPrimeraVisitaAMostrar", "Debe ingresar un número entero positivo.");
			incorrecta = true;
		} catch (ELException ex) {
			addInvalidMessage(ctx, "ingresarPrimeraVisitaAMostrar", "Debe ingresar un número entero positivo.");
			incorrecta = true;
		} finally {
			changed = true;
			notifyObservers();
		}
	}

	@Override
	public void validate(TP4Problema6 tP) {
		Long primera = tP.getPrimeraVisitaAMostrar();
		Long numero = tP.getNumeroVisitasASimular();
		if (primera == null || primera == 0)
			nula = true;
		else if (primera < 1)
			incorrecta = true;
		else if (numero != null && primera > numero)
			incorrecta = true;
	}

	@Override
	public boolean isObligatoria() {
		return obligatoria;
	}

	@Override
	public boolean isIncorrecta() {
		return incorrecta;
	}

	@Override
	public boolean isNula() {
		return nula;
	}

	@Override
	public void addObserver(Observer o) {
		observers.add(o);
	}

	@Override
	public int countObservers() {
		return observers.size();
	}

	@Override
	public void deleteObserver(Observer o) {
		observers.remove(o);
	}

	@Override
	public void deleteObservers() {
		observers.clear();
	}

	@Override
	public boolean hasChanged() {
		return changed;
	}

	@Override
	public void notifyObservers() {
		if (hasChanged())
			for (Observer obs : observers)
				obs.update(this, null);
		clearChanged();
	}

	@Override
	public void notifyObservers(Object arg) {
		if (hasChanged())
			for (Observer obs : observers)
				obs.update(this, arg);
		clearChanged();
	}

	protected void clearChanged() {
		changed = false;
		incorrecta = false;
		nula = false;
	}

}
