package app.tp4.problema6.presentacion;

import java.math.BigDecimal;
import java.math.RoundingMode;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.zkoss.bind.BindUtils;
import org.zkoss.bind.Converter;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.DependsOn;
import org.zkoss.bind.annotation.GlobalCommand;
import org.zkoss.bind.annotation.Immutable;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.bind.annotation.SmartNotifyChange;
import org.zkoss.spring.SpringUtil;
import org.zkoss.zk.ui.select.annotation.VariableResolver;
import org.zkoss.zk.ui.select.annotation.WireVariable;

import app.presentacion.ConversorDeDouble;
import app.presentacion.ConversorDeFrecuenciaRelativa;
import app.presentacion.ConversorDeInt;
import app.presentacion.ConversorDeLong;
import app.presentacion.ConversorDePorcentaje;
import app.presentacion.ConversorDeSumaDeDinero;
import app.presentacion.FormateadorDeNumerosDecimales;
import app.presentacion.InputValidator;
import app.presentacion.Observable;
import app.presentacion.Observer;
import app.presentacion.ResourceBundle;
import app.presentacion.ResourceIndex;
import app.presentacion.SidebarTreeItem;
import app.tp4.problema6.HombreFactory;
import app.tp4.problema6.MujerFactory;
import app.tp4.problema6.PersonaFactory;
import app.tp4.problema6.PuertaFactory;
import app.tp4.problema6.SuscripcionFactory;
import app.tp4.problema6.TP4Problema6;
import app.utils.FrecuenciaRelativa;
import app.utils.Porcentaje;

@VariableResolver(org.zkoss.zkplus.spring.DelegatingVariableResolver.class)
public class TP4Problema6SolucionVM implements Observer {

	private static final String TILDE_SRC = "./images/check_mark.png";
	private static final String ADVERTENCIA_SRC = "./images/warning.png";
	private static final String ASTERISCO_SRC = "./images/asterisk_red.png";
	static final Logger LOG = LoggerFactory.getLogger(TP4Problema6SolucionVM.class);
	@WireVariable
	private ResourceIndex resourceIndex;
	private ResourceBundle rB;
	@WireVariable
	private InputValidator ingresarProbabilidadAbraPuertaValidator;
	@WireVariable
	private IngresarProporcionMujeresValidator ingresarProporcionMujeresValidator;
	@WireVariable
	private IngresarProporcionHombresValidator ingresarProporcionHombresValidator;
	@WireVariable
	private InputValidator ingresarProbabilidadCompraSenoraValidator;
	@WireVariable
	private InputValidator ingresarProbabilidadCompraSenorValidator;
	@WireVariable
	private InputValidator ingresarUtilidadPorSuscripcionValidator;
	@WireVariable
	private InputValidator ingresarFrecuenciaRelativaUnaSuscripcionSenoraValidator;
	@WireVariable
	private InputValidator ingresarFrecuenciaRelativaDosSuscripcionesSenoraValidator;
	@WireVariable
	private InputValidator ingresarFrecuenciaRelativaTresSuscripcionesSenoraValidator;
	@WireVariable
	private InputValidator ingresarFrecuenciaRelativaUnaSuscripcionSenorValidator;
	@WireVariable
	private InputValidator ingresarFrecuenciaRelativaDosSuscripcionesSenorValidator;
	@WireVariable
	private InputValidator ingresarFrecuenciaRelativaTresSuscripcionesSenorValidator;
	@WireVariable
	private InputValidator ingresarFrecuenciaRelativaCuatroSuscripcionesSenorValidator;
	@WireVariable
	private IngresarNumeroVisitasASimularValidator ingresarNumeroVisitasASimularValidator;
	@WireVariable
	private IngresarNumeroVisitasAMostrarValidator ingresarNumeroVisitasAMostrarValidator;
	@WireVariable
	private IngresarPrimeraVisitaAMostrarValidator ingresarPrimeraVisitaAMostrarValidator;
	@WireVariable
	private SumatoriaFrecuenciaRelativaSenoraValidator sumatoriaFrecuenciaRelativaSenoraValidator;
	@WireVariable
	private SumatoriaFrecuenciaRelativaSenorValidator sumatoriaFrecuenciaRelativaSenorValidator;
	@WireVariable
	private PuertaFactory puertaFactory;
	@WireVariable
	private PersonaFactory personaFactory;
	@WireVariable
	private MujerFactory mujerFactory;
	@WireVariable
	private HombreFactory hombreFactory;
	@WireVariable
	private SuscripcionFactory suscripcionFactory;
	private String ingresarProbabilidadAbraPuertaIconSrc, ingresarProporcionMujeresIconSrc,
			ingresarProporcionHombresIconSrc, ingresarProbabilidadCompraSenoraIconSrc,
			ingresarProbabilidadCompraSenorIconSrc, ingresarUtilidadPorSuscripcionIconSrc,
			ingresarFrecuenciaRelativaUnaSuscripcionSenoraIconSrc,
			ingresarFrecuenciaRelativaDosSuscripcionesSenoraIconSrc,
			ingresarFrecuenciaRelativaTresSuscripcionesSenoraIconSrc,
			ingresarFrecuenciaRelativaUnaSuscripcionSenorIconSrc,
			ingresarFrecuenciaRelativaDosSuscripcionesSenorIconSrc,
			ingresarFrecuenciaRelativaTresSuscripcionesSenorIconSrc,
			ingresarFrecuenciaRelativaCuatroSuscripcionesSenorIconSrc, ingresarNumeroVisitasASimularIconSrc,
			ingresarNumeroVisitasAMostrarIconSrc, ingresarPrimeraVisitaAMostrarIconSrc,
			sumatoriaFrecuenciaRelativaSenoraIconSrc, sumatoriaFrecuenciaRelativaSenorIconSrc;
	private boolean probabilidadAbraPuertaValidada, proporcionMujeresValidada, proporcionHombresValidada,
			probabilidadCompraSenoraValidada, probabilidadCompraSenorValidada, utilidadPorSuscripcionValidada,
			frecuenciaRelativaUnaSuscripcionSenoraValidada, frecuenciaRelativaDosSuscripcionesSenoraValidada,
			frecuenciaRelativaTresSuscripcionesSenoraValidada, frecuenciaRelativaUnaSuscripcionSenorValidada,
			frecuenciaRelativaDosSuscripcionesSenorValidada, frecuenciaRelativaTresSuscripcionesSenorValidada,
			frecuenciaRelativaCuatroSuscripcionesSenorValidada, numeroVisitasASimularValidada,
			numeroVisitasAMostrarValidada, primeraVisitaAMostrarValidada, sumatoriaFrecuenciaRelativaSenoraValidada,
			sumatoriaFrecuenciaRelativaSenorValidada;
	@WireVariable
	private TP4Problema6 tP4Problema6;
	@WireVariable
	private FormateadorDeNumerosDecimales formateadorDeNumerosDecimales;
	@WireVariable
	private ConversorDeInt conversorDeInt;
	@WireVariable
	private ConversorDeLong conversorDeLong;
	@WireVariable
	private ConversorDeDouble conversorDeDouble;
	@WireVariable
	private ConversorDeSumaDeDinero conversorDeSumaDeDinero;
	@WireVariable
	private ConversorDePorcentaje conversorDePorcentaje;
	@WireVariable
	private ConversorDeFrecuenciaRelativa conversorDeFrecuenciaRelativa;
	private boolean mostrandoResultados;
	private BigDecimal proporcionHombres, proporcionMujeres, sumatoriaFrecuenciaRelativaSenora,
			sumatoriaFrecuenciaRelativaSenor;

	@Init
	public void initSetup() {
		rB = resourceIndex.getResourceBundle("lpoigt");
		ingresarProbabilidadAbraPuertaValidator.addObserver(this);
		ingresarProporcionMujeresValidator.addObserver(this);
		ingresarProporcionHombresValidator.addObserver(this);
		ingresarProbabilidadCompraSenoraValidator.addObserver(this);
		ingresarProbabilidadCompraSenorValidator.addObserver(this);
		ingresarUtilidadPorSuscripcionValidator.addObserver(this);
		ingresarFrecuenciaRelativaUnaSuscripcionSenoraValidator.addObserver(this);
		ingresarFrecuenciaRelativaDosSuscripcionesSenoraValidator.addObserver(this);
		ingresarFrecuenciaRelativaTresSuscripcionesSenoraValidator.addObserver(this);
		ingresarFrecuenciaRelativaUnaSuscripcionSenorValidator.addObserver(this);
		ingresarFrecuenciaRelativaDosSuscripcionesSenorValidator.addObserver(this);
		ingresarFrecuenciaRelativaTresSuscripcionesSenorValidator.addObserver(this);
		ingresarFrecuenciaRelativaCuatroSuscripcionesSenorValidator.addObserver(this);
		ingresarNumeroVisitasASimularValidator.addObserver(this);
		ingresarNumeroVisitasAMostrarValidator.addObserver(this);
		ingresarPrimeraVisitaAMostrarValidator.addObserver(this);
		sumatoriaFrecuenciaRelativaSenoraValidator.addObserver(this);
		sumatoriaFrecuenciaRelativaSenorValidator.addObserver(this);
		inicializarIconosValidaciones();
		inicializarValidaciones();
		proporcionMujeres = new BigDecimal("80").setScale(Porcentaje.getDefaultScale(), RoundingMode.HALF_UP);
		proporcionHombres = new BigDecimal("20").setScale(Porcentaje.getDefaultScale(), RoundingMode.HALF_UP);
		sumatoriaFrecuenciaRelativaSenora = new BigDecimal("1").setScale(FrecuenciaRelativa.getDefaultScale(), RoundingMode.HALF_UP);
		sumatoriaFrecuenciaRelativaSenor = new BigDecimal("1").setScale(FrecuenciaRelativa.getDefaultScale(), RoundingMode.HALF_UP);
	}

	private void inicializarValidaciones() {
		setProbabilidadAbraPuertaValidada(true);
		setProporcionMujeresValidada(true);
		setProporcionHombresValidada(true);
		setProbabilidadCompraSenoraValidada(true);
		setProbabilidadCompraSenorValidada(true);
		setUtilidadPorSuscripcionValidada(true);
		setFrecuenciaRelativaUnaSuscripcionSenoraValidada(true);
		setFrecuenciaRelativaDosSuscripcionesSenoraValidada(true);
		setFrecuenciaRelativaTresSuscripcionesSenoraValidada(true);
		setFrecuenciaRelativaUnaSuscripcionSenorValidada(true);
		setFrecuenciaRelativaDosSuscripcionesSenorValidada(true);
		setFrecuenciaRelativaTresSuscripcionesSenorValidada(true);
		setFrecuenciaRelativaCuatroSuscripcionesSenorValidada(true);
		setSumatoriaFrecuenciaRelativaSenoraValidada(true);
		setSumatoriaFrecuenciaRelativaSenorValidada(true);
	}

	private void inicializarIconosValidaciones() {
		setIngresarProbabilidadAbraPuertaIconSrc(TILDE_SRC);
		setIngresarProporcionMujeresIconSrc(TILDE_SRC);
		setIngresarProporcionHombresIconSrc(TILDE_SRC);
		setIngresarProbabilidadCompraSenoraIconSrc(TILDE_SRC);
		setIngresarProbabilidadCompraSenorIconSrc(TILDE_SRC);
		setIngresarUtilidadPorSuscripcionIconSrc(TILDE_SRC);
		setIngresarFrecuenciaRelativaUnaSuscripcionSenoraIconSrc(TILDE_SRC);
		setIngresarFrecuenciaRelativaDosSuscripcionesSenoraIconSrc(TILDE_SRC);
		setIngresarFrecuenciaRelativaTresSuscripcionesSenoraIconSrc(TILDE_SRC);
		setIngresarFrecuenciaRelativaUnaSuscripcionSenorIconSrc(TILDE_SRC);
		setIngresarFrecuenciaRelativaDosSuscripcionesSenorIconSrc(TILDE_SRC);
		setIngresarFrecuenciaRelativaTresSuscripcionesSenorIconSrc(TILDE_SRC);
		setIngresarFrecuenciaRelativaCuatroSuscripcionesSenorIconSrc(TILDE_SRC);
		setSumatoriaFrecuenciaRelativaSenoraIconSrc(TILDE_SRC);
		setSumatoriaFrecuenciaRelativaSenorIconSrc(TILDE_SRC);
		setIngresarNumeroVisitasASimularIconSrc(ASTERISCO_SRC);
		setIngresarNumeroVisitasAMostrarIconSrc(ASTERISCO_SRC);
		setIngresarPrimeraVisitaAMostrarIconSrc(ASTERISCO_SRC);
	}

	@GlobalCommand
	@SmartNotifyChange({ "probabilidadAbraPuertaValidada", "proporcionMujeresValidada", "proporcionHombresValidada",
			"probabilidadCompraSenoraValidada", "probabilidadCompraSenorValidada", "utilidadPorSuscripcionValidada",
			"frecuenciaRelativaUnaSuscripcionSenoraValidada", "frecuenciaRelativaDosSuscripcionesSenoraValidada",
			"frecuenciaRelativaTresSuscripcionesSenoraValidada", "frecuenciaRelativaUnaSuscripcionSenorValidada",
			"frecuenciaRelativaDosSuscripcionesSenorValidada", "frecuenciaRelativaTresSuscripcionesSenorValidada",
			"frecuenciaRelativaCuatroSuscripcionesSenorValidada", "numeroVisitasASimularValidada",
			"numeroVisitasAMostrarValidada", "primeraVisitaAMostrarValidada",
			"sumatoriaFrecuenciaRelativaSenoraValidada", "sumatoriaFrecuenciaRelativaSenorValidada", "ingresarProbabilidadAbraPuertaIconSrc",
			"ingresarProporcionMujeresIconSrc", "ingresarProporcionHombresIconSrc",
			"ingresarProbabilidadCompraSenoraIconSrc", "ingresarProbabilidadCompraSenorIconSrc",
			"ingresarUtilidadPorSuscripcionIconSrc", "ingresarFrecuenciaRelativaUnaSuscripcionSenorIconSrc",
			"ingresarFrecuenciaRelativaDosSuscripcionesSenorIconSrc",
			"ingresarFrecuenciaRelativaTresSuscripcionesSenorIconSrc",
			"ingresarFrecuenciaRelativaCuatroSuscripcionesSenorIconSrc", "ingresarNumeroVisitasASimularIconSrc",
			"ingresarNumeroVisitasAMostrarIconSrc", "ingresarPrimeraVisitaAMostrarIconSrc",
			"ingresarSumatoriaFrecuenciaRelativaSenoraIconSrc", "ingresarSumatoriaFrecuenciaRelativaSenorIconSrc" })
	public void showContent(@BindingParam("page") SidebarTreeItem page) {
		if (page == null || page.getUri() == null || !page.getUri().equals(rB.getZul()))
			clear();
	}

	@Command
	@SmartNotifyChange({ "probabilidadAbraPuertaValidada", "proporcionMujeresValidada", "proporcionHombresValidada",
			"probabilidadCompraSenoraValidada", "probabilidadCompraSenorValidada", "utilidadPorSuscripcionValidada",
			"frecuenciaRelativaUnaSuscripcionSenoraValidada", "frecuenciaRelativaDosSuscripcionesSenoraValidada",
			"frecuenciaRelativaTresSuscripcionesSenoraValidada", "frecuenciaRelativaUnaSuscripcionSenorValidada",
			"frecuenciaRelativaDosSuscripcionesSenorValidada", "frecuenciaRelativaTresSuscripcionesSenorValidada",
			"frecuenciaRelativaCuatroSuscripcionesSenorValidada", "numeroVisitasASimularValidada",
			"numeroVisitasAMostrarValidada", "primeraVisitaAMostrarValidada",
			"sumatoriaFrecuenciaRelativaSenoraValidada", "sumatoriaFrecuenciaRelativaSenorValidada", "ingresarProbabilidadAbraPuertaIconSrc",
			"ingresarProporcionMujeresIconSrc", "ingresarProporcionHombresIconSrc",
			"ingresarProbabilidadCompraSenoraIconSrc", "ingresarProbabilidadCompraSenorIconSrc",
			"ingresarUtilidadPorSuscripcionIconSrc", "ingresarFrecuenciaRelativaUnaSuscripcionSenorIconSrc",
			"ingresarFrecuenciaRelativaDosSuscripcionesSenorIconSrc",
			"ingresarFrecuenciaRelativaTresSuscripcionesSenorIconSrc",
			"ingresarFrecuenciaRelativaCuatroSuscripcionesSenorIconSrc", "ingresarNumeroVisitasASimularIconSrc",
			"ingresarNumeroVisitasAMostrarIconSrc", "ingresarPrimeraVisitaAMostrarIconSrc",
			"ingresarSumatoriaFrecuenciaRelativaSenoraIconSrc", "ingresarSumatoriaFrecuenciaRelativaSenorIconSrc", "mostrandoResultados" })
	public void clear() {
		tP4Problema6.reiniciar();
		setProbabilidadAbraPuertaValidada(false);
		setProporcionMujeresValidada(false);
		setProporcionHombresValidada(false);
		setProbabilidadCompraSenoraValidada(false);
		setProbabilidadCompraSenorValidada(false);
		setUtilidadPorSuscripcionValidada(false);
		setFrecuenciaRelativaUnaSuscripcionSenoraValidada(false);
		setFrecuenciaRelativaDosSuscripcionesSenoraValidada(false);
		setFrecuenciaRelativaTresSuscripcionesSenoraValidada(false);
		setFrecuenciaRelativaUnaSuscripcionSenorValidada(false);
		setFrecuenciaRelativaDosSuscripcionesSenorValidada(false);
		setFrecuenciaRelativaTresSuscripcionesSenorValidada(false);
		setFrecuenciaRelativaCuatroSuscripcionesSenorValidada(false);
		setNumeroVisitasASimularValidada(false);
		setNumeroVisitasAMostrarValidada(false);
		setPrimeraVisitaAMostrarValidada(false);
		setSumatoriaFrecuenciaRelativaSenoraValidada(false);
		setSumatoriaFrecuenciaRelativaSenorValidada(false);
		setMostrandoResultados(false);
		inicializarIconosValidaciones();
		inicializarValidaciones();
	}

	@Command
	@NotifyChange({ "tP4Problema6", "resultadosSrc", "mostrandoResultados" })
	public void correr() throws Exception {
		tP4Problema6.solucionar();
		setMostrandoResultados(true);
	}

	@Command
	@SmartNotifyChange("mostrandoResultados")
	public void limpiarResultados() {
		setMostrandoResultados(false);
		BindUtils.postGlobalCommand(null, null, "limpiarTabla", null);
		BindUtils.postGlobalCommand(null, null, "limpiarResumenFinal", null);
	}

	@Command
	public void ajustarProporcionHombres(@BindingParam("form") PersonaFactory form) {
		if (proporcionMujeresValidada) {
			proporcionHombres = new BigDecimal("100").setScale(Porcentaje.getDefaultScale(), RoundingMode.HALF_UP).subtract((BigDecimal) ((PersonaFactory) form).getProporcionMujeres());
			form.setProporcionHombres(proporcionHombres);
			ingresarProporcionHombresValidator.validate(proporcionHombres);
			BindUtils.postNotifyChange(null, null, this, "proporcionHombres");
		}
	}

	@Command
	public void ajustarProporcionMujeres(@BindingParam("form") PersonaFactory form) {
		if (proporcionHombresValidada) {
			proporcionHombres = new BigDecimal("100").setScale(Porcentaje.getDefaultScale(), RoundingMode.HALF_UP).subtract((BigDecimal) ((PersonaFactory) form).getProporcionHombres());
			form.setProporcionMujeres(proporcionMujeres);
			ingresarProporcionMujeresValidator.validate(proporcionMujeres);
			BindUtils.postNotifyChange(null, null, this, "proporcionMujeres");
		}
	}

	@Command
	public void ajustarSumatoriaFrecuenciaRelativaSenora(@BindingParam("form") MujerFactory form) {
		sumatoriaFrecuenciaRelativaSenora = new BigDecimal("0").setScale(FrecuenciaRelativa.getDefaultScale(), RoundingMode.HALF_UP);
		if (frecuenciaRelativaUnaSuscripcionSenoraValidada)
			sumatoriaFrecuenciaRelativaSenora = sumatoriaFrecuenciaRelativaSenora.add(form.getFrecuenciaRelativaUnaSuscripcion());
		if (frecuenciaRelativaDosSuscripcionesSenoraValidada)
			sumatoriaFrecuenciaRelativaSenora = sumatoriaFrecuenciaRelativaSenora.add(form.getFrecuenciaRelativaDosSuscripciones());
		if (frecuenciaRelativaTresSuscripcionesSenoraValidada)
			sumatoriaFrecuenciaRelativaSenora = sumatoriaFrecuenciaRelativaSenora.add(form.getFrecuenciaRelativaTresSuscripciones());
		sumatoriaFrecuenciaRelativaSenoraValidator.validate(sumatoriaFrecuenciaRelativaSenora);
		BindUtils.postNotifyChange(null, null, this, "sumatoriaFrecuenciaRelativaSenora");
	}

	@Command
	public void ajustarSumatoriaFrecuenciaRelativaSenor(@BindingParam("form") HombreFactory form) {
		sumatoriaFrecuenciaRelativaSenor = new BigDecimal("0").setScale(FrecuenciaRelativa.getDefaultScale(), RoundingMode.HALF_UP);
		if (frecuenciaRelativaUnaSuscripcionSenorValidada)
			sumatoriaFrecuenciaRelativaSenor = sumatoriaFrecuenciaRelativaSenor.add(form.getFrecuenciaRelativaUnaSuscripcion());
		if (frecuenciaRelativaDosSuscripcionesSenorValidada)
			sumatoriaFrecuenciaRelativaSenor = sumatoriaFrecuenciaRelativaSenor.add(form.getFrecuenciaRelativaDosSuscripciones());
		if (frecuenciaRelativaTresSuscripcionesSenorValidada)
			sumatoriaFrecuenciaRelativaSenor = sumatoriaFrecuenciaRelativaSenor.add(form.getFrecuenciaRelativaTresSuscripciones());
		if (frecuenciaRelativaCuatroSuscripcionesSenorValidada)
			sumatoriaFrecuenciaRelativaSenor = sumatoriaFrecuenciaRelativaSenor.add(form.getFrecuenciaRelativaCuatroSuscripciones());
		sumatoriaFrecuenciaRelativaSenorValidator.validate(sumatoriaFrecuenciaRelativaSenor);
		BindUtils.postNotifyChange(null, null, this, "sumatoriaFrecuenciaRelativaSenor");
	}

	@Command
	public void validarParametrosDeLaSalida(@BindingParam("form") TP4Problema6 form) {
	}

	@Command
	public void notificarCambioParametroDeLaSalida() {}

	// "Validación"
	@Override
	public void update(Observable o, Object arg) {
		if (o instanceof IngresarProbabilidadAbraPuertaValidator)
			updateValidacion((IngresarProbabilidadAbraPuertaValidator) o);
		if (o instanceof IngresarProporcionMujeresValidator)
			updateValidacion((IngresarProporcionMujeresValidator) o);
		if (o instanceof IngresarProporcionHombresValidator)
			updateValidacion((IngresarProporcionHombresValidator) o);
		if (o instanceof IngresarProbabilidadCompraSenoraValidator)
			updateValidacion((IngresarProbabilidadCompraSenoraValidator) o);
		if (o instanceof IngresarProbabilidadCompraSenorValidator)
			updateValidacion((IngresarProbabilidadCompraSenorValidator) o);
		if (o instanceof IngresarUtilidadPorSuscripcionValidator)
			updateValidacion((IngresarUtilidadPorSuscripcionValidator) o);
		if (o instanceof IngresarFrecuenciaRelativaUnaSuscripcionSenoraValidator)
			updateValidacion((IngresarFrecuenciaRelativaUnaSuscripcionSenoraValidator) o);
		if (o instanceof IngresarFrecuenciaRelativaDosSuscripcionesSenoraValidator)
			updateValidacion((IngresarFrecuenciaRelativaDosSuscripcionesSenoraValidator) o);
		if (o instanceof IngresarFrecuenciaRelativaTresSuscripcionesSenoraValidator)
			updateValidacion((IngresarFrecuenciaRelativaTresSuscripcionesSenoraValidator) o);
		if (o instanceof IngresarFrecuenciaRelativaUnaSuscripcionSenorValidator)
			updateValidacion((IngresarFrecuenciaRelativaUnaSuscripcionSenorValidator) o);
		if (o instanceof IngresarFrecuenciaRelativaDosSuscripcionesSenorValidator)
			updateValidacion((IngresarFrecuenciaRelativaDosSuscripcionesSenorValidator) o);
		if (o instanceof IngresarFrecuenciaRelativaTresSuscripcionesSenorValidator)
			updateValidacion((IngresarFrecuenciaRelativaTresSuscripcionesSenorValidator) o);
		if (o instanceof IngresarFrecuenciaRelativaCuatroSuscripcionesSenorValidator)
			updateValidacion((IngresarFrecuenciaRelativaCuatroSuscripcionesSenorValidator) o);
		if (o instanceof IngresarNumeroVisitasASimularValidator)
			updateValidacion((IngresarNumeroVisitasASimularValidator) o);
		if (o instanceof IngresarNumeroVisitasAMostrarValidator)
			updateValidacion((IngresarNumeroVisitasAMostrarValidator) o);
		if (o instanceof IngresarPrimeraVisitaAMostrarValidator)
			updateValidacion((IngresarPrimeraVisitaAMostrarValidator) o);
		if (o instanceof SumatoriaFrecuenciaRelativaSenoraValidator)
			updateValidacion((SumatoriaFrecuenciaRelativaSenoraValidator) o);
		if (o instanceof SumatoriaFrecuenciaRelativaSenorValidator)
			updateValidacion((SumatoriaFrecuenciaRelativaSenorValidator) o);
	}

	private void updateValidacion(IngresarProbabilidadAbraPuertaValidator val) {
		String iconoSrc = registrarResultadoValidacion((InputValidator) val);
		ingresarProbabilidadAbraPuertaIconSrc = iconoSrc;
		if (iconoSrc != null && iconoSrc.equals(TILDE_SRC) || iconoSrc == null)
			setProbabilidadAbraPuertaValidada(true);
		else
			setProbabilidadAbraPuertaValidada(false);
		BindUtils.postNotifyChange(null, null, this, "ingresarProbabilidadAbraPuertaIconSrc");
		BindUtils.postNotifyChange(null, null, this, "probabilidadAbraPuertaValidada");
	}

	private void updateValidacion(IngresarProporcionMujeresValidator val) {
		String iconoSrc = registrarResultadoValidacion((InputValidator) val);
		ingresarProporcionMujeresIconSrc = iconoSrc;
		if (iconoSrc != null && iconoSrc.equals(TILDE_SRC) || iconoSrc == null)
			setProporcionMujeresValidada(true);
		else
			setProporcionMujeresValidada(false);
		BindUtils.postNotifyChange(null, null, this, "ingresarProporcionMujeresIconSrc");
		BindUtils.postNotifyChange(null, null, this, "proporcionMujeresValidada");
	}

	private void updateValidacion(IngresarProporcionHombresValidator val) {
		String iconoSrc = registrarResultadoValidacion((InputValidator) val);
		ingresarProporcionHombresIconSrc = iconoSrc;
		if (iconoSrc != null && iconoSrc.equals(TILDE_SRC) || iconoSrc == null)
			setProporcionHombresValidada(true);
		else
			setProporcionHombresValidada(false);
		BindUtils.postNotifyChange(null, null, this, "ingresarProporcionHombresIconSrc");
		BindUtils.postNotifyChange(null, null, this, "proporcionHombresValidada");
	}

	private void updateValidacion(IngresarProbabilidadCompraSenoraValidator val) {
		String iconoSrc = registrarResultadoValidacion((InputValidator) val);
		ingresarProbabilidadCompraSenoraIconSrc = iconoSrc;
		if (iconoSrc != null && iconoSrc.equals(TILDE_SRC) || iconoSrc == null)
			setProbabilidadCompraSenoraValidada(true);
		else
			setProbabilidadCompraSenoraValidada(false);
		BindUtils.postNotifyChange(null, null, this, "ingresarProbabilidadCompraSenoraIconSrc");
		BindUtils.postNotifyChange(null, null, this, "probabilidadCompraSenoraValidada");
	}

	private void updateValidacion(IngresarProbabilidadCompraSenorValidator val) {
		String iconoSrc = registrarResultadoValidacion((InputValidator) val);
		ingresarProbabilidadCompraSenorIconSrc = iconoSrc;
		if (iconoSrc != null && iconoSrc.equals(TILDE_SRC) || iconoSrc == null)
			setProbabilidadCompraSenorValidada(true);
		else
			setProbabilidadCompraSenorValidada(false);
		BindUtils.postNotifyChange(null, null, this, "ingresarProbabilidadCompraSenorIconSrc");
		BindUtils.postNotifyChange(null, null, this, "probabilidadCompraSenorValidada");
	}

	private void updateValidacion(IngresarUtilidadPorSuscripcionValidator val) {
		String iconoSrc = registrarResultadoValidacion((InputValidator) val);
		ingresarUtilidadPorSuscripcionIconSrc = iconoSrc;
		if (iconoSrc != null && iconoSrc.equals(TILDE_SRC) || iconoSrc == null)
			setUtilidadPorSuscripcionValidada(true);
		else
			setUtilidadPorSuscripcionValidada(false);
		BindUtils.postNotifyChange(null, null, this, "ingresarUtilidadPorSuscripcionIconSrc");
		BindUtils.postNotifyChange(null, null, this, "utilidadPorSuscripcionValidada");
	}

	private void updateValidacion(IngresarFrecuenciaRelativaUnaSuscripcionSenoraValidator val) {
		String iconoSrc = registrarResultadoValidacion((InputValidator) val);
		ingresarFrecuenciaRelativaUnaSuscripcionSenoraIconSrc = iconoSrc;
		if (iconoSrc != null && iconoSrc.equals(TILDE_SRC) || iconoSrc == null)
			setFrecuenciaRelativaUnaSuscripcionSenoraValidada(true);
		else
			setFrecuenciaRelativaUnaSuscripcionSenoraValidada(false);
		BindUtils.postNotifyChange(null, null, this, "ingresarFrecuenciaRelativaUnaSuscripcionSenoraIconSrc");
		BindUtils.postNotifyChange(null, null, this, "frecuenciaRelativaUnaSuscripcionSenoraValidada");
	}

	private void updateValidacion(IngresarFrecuenciaRelativaDosSuscripcionesSenoraValidator val) {
		String iconoSrc = registrarResultadoValidacion((InputValidator) val);
		ingresarFrecuenciaRelativaDosSuscripcionesSenoraIconSrc = iconoSrc;
		if (iconoSrc != null && iconoSrc.equals(TILDE_SRC) || iconoSrc == null)
			setFrecuenciaRelativaDosSuscripcionesSenoraValidada(true);
		else
			setFrecuenciaRelativaDosSuscripcionesSenoraValidada(false);
		BindUtils.postNotifyChange(null, null, this, "ingresarFrecuenciaRelativaDosSuscripcionesSenoraIconSrc");
		BindUtils.postNotifyChange(null, null, this, "frecuenciaRelativaDosSuscripcionesSenoraValidada");
	}

	private void updateValidacion(IngresarFrecuenciaRelativaTresSuscripcionesSenoraValidator val) {
		String iconoSrc = registrarResultadoValidacion((InputValidator) val);
		ingresarFrecuenciaRelativaTresSuscripcionesSenoraIconSrc = iconoSrc;
		if (iconoSrc != null && iconoSrc.equals(TILDE_SRC) || iconoSrc == null)
			setFrecuenciaRelativaTresSuscripcionesSenoraValidada(true);
		else
			setFrecuenciaRelativaTresSuscripcionesSenoraValidada(false);
		BindUtils.postNotifyChange(null, null, this, "ingresarFrecuenciaRelativaTresSuscripcionesSenoraIconSrc");
		BindUtils.postNotifyChange(null, null, this, "frecuenciaRelativaTresSuscripcionesSenoraValidada");
	}

	private void updateValidacion(IngresarFrecuenciaRelativaUnaSuscripcionSenorValidator val) {
		String iconoSrc = registrarResultadoValidacion((InputValidator) val);
		ingresarFrecuenciaRelativaUnaSuscripcionSenorIconSrc = iconoSrc;
		if (iconoSrc != null && iconoSrc.equals(TILDE_SRC) || iconoSrc == null)
			setFrecuenciaRelativaUnaSuscripcionSenorValidada(true);
		else
			setFrecuenciaRelativaUnaSuscripcionSenorValidada(false);
		BindUtils.postNotifyChange(null, null, this, "ingresarFrecuenciaRelativaUnaSuscripcionSenorIconSrc");
		BindUtils.postNotifyChange(null, null, this, "frecuenciaRelativaUnaSuscripcionSenorValidada");
	}

	private void updateValidacion(IngresarFrecuenciaRelativaDosSuscripcionesSenorValidator val) {
		String iconoSrc = registrarResultadoValidacion((InputValidator) val);
		ingresarFrecuenciaRelativaDosSuscripcionesSenorIconSrc = iconoSrc;
		if (iconoSrc != null && iconoSrc.equals(TILDE_SRC) || iconoSrc == null)
			setFrecuenciaRelativaDosSuscripcionesSenorValidada(true);
		else
			setFrecuenciaRelativaDosSuscripcionesSenorValidada(false);
		BindUtils.postNotifyChange(null, null, this, "ingresarFrecuenciaRelativaDosSuscripcionesSenorIconSrc");
		BindUtils.postNotifyChange(null, null, this, "frecuenciaRelativaDosSuscripcionesSenorValidada");
	}

	private void updateValidacion(IngresarFrecuenciaRelativaTresSuscripcionesSenorValidator val) {
		String iconoSrc = registrarResultadoValidacion((InputValidator) val);
		ingresarFrecuenciaRelativaTresSuscripcionesSenorIconSrc = iconoSrc;
		if (iconoSrc != null && iconoSrc.equals(TILDE_SRC) || iconoSrc == null)
			setFrecuenciaRelativaTresSuscripcionesSenorValidada(true);
		else
			setFrecuenciaRelativaTresSuscripcionesSenorValidada(false);
		BindUtils.postNotifyChange(null, null, this, "ingresarFrecuenciaRelativaTresSuscripcionesSenorIconSrc");
		BindUtils.postNotifyChange(null, null, this, "frecuenciaRelativaTresSuscripcionesSenorValidada");
	}

	private void updateValidacion(IngresarFrecuenciaRelativaCuatroSuscripcionesSenorValidator val) {
		String iconoSrc = registrarResultadoValidacion((InputValidator) val);
		ingresarFrecuenciaRelativaCuatroSuscripcionesSenorIconSrc = iconoSrc;
		if (iconoSrc != null && iconoSrc.equals(TILDE_SRC) || iconoSrc == null)
			setFrecuenciaRelativaCuatroSuscripcionesSenorValidada(true);
		else
			setFrecuenciaRelativaCuatroSuscripcionesSenorValidada(false);
		BindUtils.postNotifyChange(null, null, this, "ingresarFrecuenciaRelativaCuatroSuscripcionesSenorIconSrc");
		BindUtils.postNotifyChange(null, null, this, "frecuenciaRelativaCuatroSuscripcionesSenorValidada");
	}

	private void updateValidacion(IngresarNumeroVisitasASimularValidator val) {
		String iconoSrc = registrarResultadoValidacion((InputValidator) val);
		ingresarNumeroVisitasASimularIconSrc = iconoSrc;
		if (iconoSrc != null && iconoSrc.equals(TILDE_SRC) || iconoSrc == null)
			setNumeroVisitasASimularValidada(true);
		else
			setNumeroVisitasASimularValidada(false);
		BindUtils.postNotifyChange(null, null, this, "ingresarNumeroVisitasASimularIconSrc");
		BindUtils.postNotifyChange(null, null, this, "numeroVisitasASimularValidada");
	}

	private void updateValidacion(IngresarNumeroVisitasAMostrarValidator val) {
		String iconoSrc = registrarResultadoValidacion((InputValidator) val);
		ingresarNumeroVisitasAMostrarIconSrc = iconoSrc;
		if (iconoSrc != null && iconoSrc.equals(TILDE_SRC) || iconoSrc == null)
			setNumeroVisitasAMostrarValidada(true);
		else
			setNumeroVisitasAMostrarValidada(false);
		BindUtils.postNotifyChange(null, null, this, "ingresarNumeroVisitasAMostrarIconSrc");
		BindUtils.postNotifyChange(null, null, this, "numeroVisitasAMostrarValidada");
	}

	private void updateValidacion(IngresarPrimeraVisitaAMostrarValidator val) {
		String iconoSrc = registrarResultadoValidacion((InputValidator) val);
		ingresarPrimeraVisitaAMostrarIconSrc = iconoSrc;
		if (iconoSrc != null && iconoSrc.equals(TILDE_SRC) || iconoSrc == null)
			setPrimeraVisitaAMostrarValidada(true);
		else
			setPrimeraVisitaAMostrarValidada(false);
		BindUtils.postNotifyChange(null, null, this, "ingresarPrimeraVisitaAMostrarIconSrc");
		BindUtils.postNotifyChange(null, null, this, "primeraVisitaAMostrarValidada");
	}

	private void updateValidacion(SumatoriaFrecuenciaRelativaSenoraValidator val) {
		String iconoSrc = registrarResultadoValidacion((InputValidator) val);
		sumatoriaFrecuenciaRelativaSenoraIconSrc = iconoSrc;
		if (iconoSrc != null && iconoSrc.equals(TILDE_SRC) || iconoSrc == null)
			setSumatoriaFrecuenciaRelativaSenoraValidada(true);
		else
			setSumatoriaFrecuenciaRelativaSenoraValidada(false);
		BindUtils.postNotifyChange(null, null, this, "sumatoriaFrecuenciaRelativaSenoraIconSrc");
		BindUtils.postNotifyChange(null, null, this, "sumatoriaFrecuenciaRelativaSenoraValidada");
	}

	private void updateValidacion(SumatoriaFrecuenciaRelativaSenorValidator val) {
		String iconoSrc = registrarResultadoValidacion((InputValidator) val);
		sumatoriaFrecuenciaRelativaSenorIconSrc = iconoSrc;
		if (iconoSrc != null && iconoSrc.equals(TILDE_SRC) || iconoSrc == null)
			setSumatoriaFrecuenciaRelativaSenorValidada(true);
		else
			setSumatoriaFrecuenciaRelativaSenorValidada(false);
		BindUtils.postNotifyChange(null, null, this, "sumatoriaFrecuenciaRelativaSenorIconSrc");
		BindUtils.postNotifyChange(null, null, this, "sumatoriaFrecuenciaRelativaSenorValidada");
	}

	private String registrarResultadoValidacion(InputValidator val) {
		String res = null;
		if (!val.isIncorrecta() && !val.isNula())
			res = TILDE_SRC;
		else if (val.isIncorrecta())
			res = ADVERTENCIA_SRC;
		else if (val.isNula() && val.isObligatoria())
			res = ASTERISCO_SRC;
		else if (val.isNula() && !val.isObligatoria())
			res = null;
		return res;
	}

	@DependsOn({ "probabilidadAbraPuertaValidada", "proporcionMujeresValidada", "proporcionHombresValidada",
			"probabilidadCompraSenoraValidada", "probabilidadCompraSenorValidada", "utilidadPorSuscripcionValidada",
			"frecuenciaRelativaUnaSuscripcionSenoraValidada", "frecuenciaRelativaDosSuscripcionesSenoraValidada",
			"frecuenciaRelativaTresSuscripcionesSenoraValidada", "frecuenciaRelativaUnaSuscripcionSenorValidada",
			"frecuenciaRelativaDosSuscripcionesSenorValidada", "frecuenciaRelativaTresSuscripcionesSenorValidada",
			"frecuenciaRelativaCuatroSuscripcionesSenorValidada", "numeroVisitasASimularValidada",
			"numeroVisitasAMostrarValidada", "primeraVisitaAMostrarValidada",
			"sumatoriaFrecuenciaRelativaSenoraValidada", "sumatoriaFrecuenciaRelativaSenorValidada" })
	public boolean isValidacionOk() {
		return (probabilidadAbraPuertaValidada || (!getIngresarProbabilidadAbraPuertaValidator().isObligatoria()
				&& getIngresarProbabilidadAbraPuertaIconSrc() == null))
				&& (proporcionMujeresValidada || (!getIngresarProporcionMujeresValidator().isObligatoria()
						&& getIngresarProporcionMujeresIconSrc() == null))
				&& (proporcionHombresValidada || (!getIngresarProporcionHombresValidator().isObligatoria()
						&& getIngresarProporcionHombresIconSrc() == null))
				&& (probabilidadCompraSenoraValidada || (!getIngresarProbabilidadCompraSenoraValidator().isObligatoria()
						&& getIngresarProbabilidadCompraSenoraIconSrc() == null))
				&& (probabilidadCompraSenorValidada || (!getIngresarProbabilidadCompraSenorValidator().isObligatoria()
						&& getIngresarProbabilidadCompraSenorIconSrc() == null))
				&& (utilidadPorSuscripcionValidada || (!getIngresarUtilidadPorSuscripcionValidator().isObligatoria()
						&& getIngresarUtilidadPorSuscripcionIconSrc() == null))
				&& (frecuenciaRelativaUnaSuscripcionSenoraValidada
						|| (!getIngresarFrecuenciaRelativaUnaSuscripcionSenoraValidator().isObligatoria()
								&& getIngresarFrecuenciaRelativaUnaSuscripcionSenoraIconSrc() == null))
				&& (frecuenciaRelativaDosSuscripcionesSenoraValidada
						|| (!getIngresarFrecuenciaRelativaDosSuscripcionesSenoraValidator().isObligatoria()
								&& getIngresarFrecuenciaRelativaDosSuscripcionesSenoraIconSrc() == null))
				&& (frecuenciaRelativaTresSuscripcionesSenoraValidada
						|| (!getIngresarFrecuenciaRelativaTresSuscripcionesSenoraValidator().isObligatoria()
								&& getIngresarFrecuenciaRelativaTresSuscripcionesSenoraIconSrc() == null))
				&& (frecuenciaRelativaUnaSuscripcionSenorValidada
						|| (!getIngresarFrecuenciaRelativaUnaSuscripcionSenorValidator().isObligatoria()
								&& getIngresarFrecuenciaRelativaUnaSuscripcionSenorIconSrc() == null))
				&& (frecuenciaRelativaDosSuscripcionesSenorValidada
						|| (!getIngresarFrecuenciaRelativaDosSuscripcionesSenorValidator().isObligatoria()
								&& getIngresarFrecuenciaRelativaDosSuscripcionesSenorIconSrc() == null))
				&& (frecuenciaRelativaTresSuscripcionesSenorValidada
						|| (!getIngresarFrecuenciaRelativaTresSuscripcionesSenorValidator().isObligatoria()
								&& getIngresarFrecuenciaRelativaTresSuscripcionesSenorIconSrc() == null))
				&& (frecuenciaRelativaCuatroSuscripcionesSenorValidada
						|| (!getIngresarFrecuenciaRelativaCuatroSuscripcionesSenorValidator().isObligatoria()
								&& getIngresarFrecuenciaRelativaCuatroSuscripcionesSenorIconSrc() == null))
				&& (numeroVisitasASimularValidada || (!getIngresarNumeroVisitasASimularValidator().isObligatoria()
						&& getIngresarNumeroVisitasASimularIconSrc() == null))
				&& (numeroVisitasAMostrarValidada || (!getIngresarNumeroVisitasAMostrarValidator().isObligatoria()
						&& getIngresarNumeroVisitasAMostrarIconSrc() == null))
				&& (primeraVisitaAMostrarValidada || (!getIngresarPrimeraVisitaAMostrarValidator().isObligatoria()
						&& getIngresarPrimeraVisitaAMostrarIconSrc() == null))
				&& (sumatoriaFrecuenciaRelativaSenoraValidada
						|| (!getSumatoriaFrecuenciaRelativaSenoraValidator().isObligatoria()
								&& getSumatoriaFrecuenciaRelativaSenoraIconSrc() == null))
				&& (sumatoriaFrecuenciaRelativaSenorValidada
						|| (!getSumatoriaFrecuenciaRelativaSenorValidator().isObligatoria()
								&& getSumatoriaFrecuenciaRelativaSenorIconSrc() == null));
	}

	public InputValidator getIngresarProbabilidadAbraPuertaValidator() {
		return ingresarProbabilidadAbraPuertaValidator;
	}

	public InputValidator getIngresarProporcionMujeresValidator() {
		return ingresarProporcionMujeresValidator;
	}

	public InputValidator getIngresarProporcionHombresValidator() {
		return ingresarProporcionHombresValidator;
	}

	public InputValidator getIngresarProbabilidadCompraSenoraValidator() {
		return ingresarProbabilidadCompraSenoraValidator;
	}

	public InputValidator getIngresarProbabilidadCompraSenorValidator() {
		return ingresarProbabilidadCompraSenorValidator;
	}

	public InputValidator getIngresarUtilidadPorSuscripcionValidator() {
		return ingresarUtilidadPorSuscripcionValidator;
	}

	public InputValidator getIngresarFrecuenciaRelativaUnaSuscripcionSenoraValidator() {
		return ingresarFrecuenciaRelativaUnaSuscripcionSenoraValidator;
	}

	public InputValidator getIngresarFrecuenciaRelativaDosSuscripcionesSenoraValidator() {
		return ingresarFrecuenciaRelativaDosSuscripcionesSenoraValidator;
	}

	public InputValidator getIngresarFrecuenciaRelativaTresSuscripcionesSenoraValidator() {
		return ingresarFrecuenciaRelativaTresSuscripcionesSenoraValidator;
	}

	public InputValidator getIngresarFrecuenciaRelativaUnaSuscripcionSenorValidator() {
		return ingresarFrecuenciaRelativaUnaSuscripcionSenorValidator;
	}

	public InputValidator getIngresarFrecuenciaRelativaDosSuscripcionesSenorValidator() {
		return ingresarFrecuenciaRelativaDosSuscripcionesSenorValidator;
	}

	public InputValidator getIngresarFrecuenciaRelativaTresSuscripcionesSenorValidator() {
		return ingresarFrecuenciaRelativaTresSuscripcionesSenorValidator;
	}

	public InputValidator getIngresarFrecuenciaRelativaCuatroSuscripcionesSenorValidator() {
		return ingresarFrecuenciaRelativaCuatroSuscripcionesSenorValidator;
	}

	public InputValidator getIngresarNumeroVisitasASimularValidator() {
		return ingresarNumeroVisitasASimularValidator;
	}

	public InputValidator getIngresarNumeroVisitasAMostrarValidator() {
		return ingresarNumeroVisitasAMostrarValidator;
	}

	public InputValidator getIngresarPrimeraVisitaAMostrarValidator() {
		return ingresarPrimeraVisitaAMostrarValidator;
	}

	public InputValidator getSumatoriaFrecuenciaRelativaSenoraValidator() {
		return sumatoriaFrecuenciaRelativaSenoraValidator;
	}

	public InputValidator getSumatoriaFrecuenciaRelativaSenorValidator() {
		return sumatoriaFrecuenciaRelativaSenorValidator;
	}

	@DependsOn("probabilidadAbraPuertaValidada")
	public String getDefaultTooltiptextIngresarProbabilidadAbraPuerta() {
		String res = null;
		if (getIngresarProbabilidadAbraPuertaValidator().isObligatoria() && !probabilidadAbraPuertaValidada)
			res = "Este campo es obligatorio.";
		else if (probabilidadAbraPuertaValidada)
			res = "Probabilidad válida.";
		return res;
	}

	@DependsOn("proporcionMujeresValidada")
	public String getDefaultTooltiptextIngresarProporcionMujeres() {
		String res = null;
		if (getIngresarProporcionMujeresValidator().isObligatoria() && !proporcionMujeresValidada)
			res = "Este campo es obligatorio.";
		else if (proporcionMujeresValidada)
			res = "Proporción válida.";
		return res;
	}

	@DependsOn("proporcionHombresValidada")
	public String getDefaultTooltiptextIngresarProporcionHombres() {
		String res = null;
		if (getIngresarProporcionHombresValidator().isObligatoria() && !proporcionHombresValidada)
			res = "Este campo es obligatorio.";
		else if (proporcionHombresValidada)
			res = "Proporción válida.";
		return res;
	}

	@DependsOn("probabilidadCompraSenoraValidada")
	public String getDefaultTooltiptextIngresarProbabilidadCompraSenora() {
		String res = null;
		if (getIngresarProbabilidadCompraSenoraValidator().isObligatoria() && !probabilidadCompraSenoraValidada)
			res = "Este campo es obligatorio.";
		else if (probabilidadCompraSenoraValidada)
			res = "Probabilidad válida.";
		return res;
	}

	@DependsOn("probabilidadCompraSenorValidada")
	public String getDefaultTooltiptextIngresarProbabilidadCompraSenor() {
		String res = null;
		if (getIngresarProbabilidadCompraSenorValidator().isObligatoria() && !probabilidadCompraSenorValidada)
			res = "Este campo es obligatorio.";
		else if (probabilidadCompraSenorValidada)
			res = "Probabilidad válida.";
		return res;
	}

	@DependsOn("utilidadPorSuscripcionValidada")
	public String getDefaultTooltiptextIngresarUtilidadPorSuscripcion() {
		String res = null;
		if (getIngresarUtilidadPorSuscripcionValidator().isObligatoria() && !utilidadPorSuscripcionValidada)
			res = "Este campo es obligatorio.";
		else if (utilidadPorSuscripcionValidada)
			res = "Utilidad válida.";
		return res;
	}

	@DependsOn("frecuenciaRelativaUnaSuscripcionSenoraValidada")
	public String getDefaultTooltiptextIngresarFrecuenciaRelativaUnaSuscripcionSenora() {
		String res = null;
		if (getIngresarFrecuenciaRelativaUnaSuscripcionSenoraValidator().isObligatoria()
				&& !frecuenciaRelativaUnaSuscripcionSenoraValidada)
			res = "Este campo es obligatorio.";
		else if (frecuenciaRelativaUnaSuscripcionSenoraValidada)
			res = "Frecuencia válida.";
		return res;
	}

	@DependsOn("frecuenciaRelativaDosSuscripcionesSenoraValidada")
	public String getDefaultTooltiptextIngresarFrecuenciaRelativaDosSuscripcionesSenora() {
		String res = null;
		if (getIngresarFrecuenciaRelativaDosSuscripcionesSenoraValidator().isObligatoria()
				&& !frecuenciaRelativaDosSuscripcionesSenoraValidada)
			res = "Este campo es obligatorio.";
		else if (frecuenciaRelativaDosSuscripcionesSenoraValidada)
			res = "Frecuencia válida.";
		return res;
	}

	@DependsOn("frecuenciaRelativaTresSuscripcionesSenoraValidada")
	public String getDefaultTooltiptextIngresarFrecuenciaRelativaTresSuscripcionesSenora() {
		String res = null;
		if (getIngresarFrecuenciaRelativaTresSuscripcionesSenoraValidator().isObligatoria()
				&& !frecuenciaRelativaTresSuscripcionesSenoraValidada)
			res = "Este campo es obligatorio.";
		else if (frecuenciaRelativaTresSuscripcionesSenoraValidada)
			res = "Frecuencia válida.";
		return res;
	}

	@DependsOn("frecuenciaRelativaUnaSuscripcionSenorValidada")
	public String getDefaultTooltiptextIngresarFrecuenciaRelativaUnaSuscripcionSenor() {
		String res = null;
		if (getIngresarFrecuenciaRelativaUnaSuscripcionSenorValidator().isObligatoria()
				&& !frecuenciaRelativaUnaSuscripcionSenorValidada)
			res = "Este campo es obligatorio.";
		else if (frecuenciaRelativaUnaSuscripcionSenorValidada)
			res = "Frecuencia válida.";
		return res;
	}

	@DependsOn("frecuenciaRelativaDosSuscripcionesSenorValidada")
	public String getDefaultTooltiptextIngresarFrecuenciaRelativaDosSuscripcionesSenor() {
		String res = null;
		if (getIngresarFrecuenciaRelativaDosSuscripcionesSenorValidator().isObligatoria()
				&& !frecuenciaRelativaDosSuscripcionesSenorValidada)
			res = "Este campo es obligatorio.";
		else if (frecuenciaRelativaDosSuscripcionesSenorValidada)
			res = "Frecuencia válida.";
		return res;
	}

	@DependsOn("frecuenciaRelativaTresSuscripcionesSenorValidada")
	public String getDefaultTooltiptextIngresarFrecuenciaRelativaTresSuscripcionesSenor() {
		String res = null;
		if (getIngresarFrecuenciaRelativaTresSuscripcionesSenorValidator().isObligatoria()
				&& !frecuenciaRelativaTresSuscripcionesSenorValidada)
			res = "Este campo es obligatorio.";
		else if (frecuenciaRelativaTresSuscripcionesSenorValidada)
			res = "Frecuencia válida.";
		return res;
	}

	@DependsOn("frecuenciaRelativaCuatroSuscripcionesSenorValidada")
	public String getDefaultTooltiptextIngresarFrecuenciaRelativaCuatroSuscripcionesSenor() {
		String res = null;
		if (getIngresarFrecuenciaRelativaCuatroSuscripcionesSenorValidator().isObligatoria()
				&& !frecuenciaRelativaCuatroSuscripcionesSenorValidada)
			res = "Este campo es obligatorio.";
		else if (frecuenciaRelativaCuatroSuscripcionesSenorValidada)
			res = "Frecuencia válida.";
		return res;
	}

	@DependsOn("numeroVisitasASimularValidada")
	public String getDefaultTooltiptextIngresarNumeroVisitasASimular() {
		String res = null;
		if (getIngresarNumeroVisitasASimularValidator().isObligatoria() && !numeroVisitasASimularValidada)
			res = "Este campo es obligatorio.";
		else if (numeroVisitasASimularValidada)
			res = "Frecuencia válida.";
		return res;
	}

	@DependsOn("numeroVisitasAMostrarValidada")
	public String getDefaultTooltiptextIngresarNumeroVisitasAMostrar() {
		String res = null;
		if (getIngresarNumeroVisitasAMostrarValidator().isObligatoria() && !numeroVisitasAMostrarValidada)
			res = "Este campo es obligatorio.";
		else if (numeroVisitasAMostrarValidada)
			res = "Frecuencia válida.";
		return res;
	}

	@DependsOn("primeraVisitaAMostrarValidada")
	public String getDefaultTooltiptextIngresarPrimeraVisitaAMostrar() {
		String res = null;
		if (getIngresarPrimeraVisitaAMostrarValidator().isObligatoria() && !primeraVisitaAMostrarValidada)
			res = "Este campo es obligatorio.";
		else if (primeraVisitaAMostrarValidada)
			res = "Frecuencia válida.";
		return res;
	}

	@DependsOn("sumatoriaFrecuenciaRelativaSenoraValidada")
	public String getDefaultTooltiptextSumatoriaFrecuenciaRelativaSenora() {
		String res = null;
		if (getSumatoriaFrecuenciaRelativaSenoraValidator().isObligatoria()
				&& !sumatoriaFrecuenciaRelativaSenoraValidada)
			res = "Este campo es obligatorio.";
		else if (sumatoriaFrecuenciaRelativaSenoraValidada)
			res = "Sumatoria válida.";
		return res;
	}

	@DependsOn("sumatoriaFrecuenciaRelativaSenorValidada")
	public String getDefaultTooltiptextSumatoriaFrecuenciaRelativaSenor() {
		String res = null;
		if (getSumatoriaFrecuenciaRelativaSenorValidator().isObligatoria()
				&& !sumatoriaFrecuenciaRelativaSenorValidada)
			res = "Este campo es obligatorio.";
		else if (sumatoriaFrecuenciaRelativaSenorValidada)
			res = "Sumatoria válida.";
		return res;
	}

	public String getIngresarProbabilidadAbraPuertaIconSrc() {
		return ingresarProbabilidadAbraPuertaIconSrc;
	}

	public String getIngresarProporcionMujeresIconSrc() {
		return ingresarProporcionMujeresIconSrc;
	}

	public String getIngresarProporcionHombresIconSrc() {
		return ingresarProporcionHombresIconSrc;
	}

	public String getIngresarProbabilidadCompraSenoraIconSrc() {
		return ingresarProbabilidadCompraSenoraIconSrc;
	}

	public String getIngresarProbabilidadCompraSenorIconSrc() {
		return ingresarProbabilidadCompraSenorIconSrc;
	}

	public String getIngresarUtilidadPorSuscripcionIconSrc() {
		return ingresarUtilidadPorSuscripcionIconSrc;
	}

	public String getIngresarFrecuenciaRelativaUnaSuscripcionSenoraIconSrc() {
		return ingresarFrecuenciaRelativaUnaSuscripcionSenoraIconSrc;
	}

	public String getIngresarFrecuenciaRelativaDosSuscripcionesSenoraIconSrc() {
		return ingresarFrecuenciaRelativaDosSuscripcionesSenoraIconSrc;
	}

	public String getIngresarFrecuenciaRelativaTresSuscripcionesSenoraIconSrc() {
		return ingresarFrecuenciaRelativaTresSuscripcionesSenoraIconSrc;
	}

	public String getIngresarFrecuenciaRelativaUnaSuscripcionSenorIconSrc() {
		return ingresarFrecuenciaRelativaUnaSuscripcionSenorIconSrc;
	}

	public String getIngresarFrecuenciaRelativaDosSuscripcionesSenorIconSrc() {
		return ingresarFrecuenciaRelativaDosSuscripcionesSenorIconSrc;
	}

	public String getIngresarFrecuenciaRelativaTresSuscripcionesSenorIconSrc() {
		return ingresarFrecuenciaRelativaTresSuscripcionesSenorIconSrc;
	}

	public String getIngresarFrecuenciaRelativaCuatroSuscripcionesSenorIconSrc() {
		return ingresarFrecuenciaRelativaCuatroSuscripcionesSenorIconSrc;
	}

	public String getIngresarNumeroVisitasASimularIconSrc() {
		return ingresarNumeroVisitasASimularIconSrc;
	}

	public String getIngresarNumeroVisitasAMostrarIconSrc() {
		return ingresarNumeroVisitasAMostrarIconSrc;
	}

	public String getIngresarPrimeraVisitaAMostrarIconSrc() {
		return ingresarPrimeraVisitaAMostrarIconSrc;
	}

	public String getSumatoriaFrecuenciaRelativaSenoraIconSrc() {
		return sumatoriaFrecuenciaRelativaSenoraIconSrc;
	}

	public String getSumatoriaFrecuenciaRelativaSenorIconSrc() {
		return sumatoriaFrecuenciaRelativaSenorIconSrc;
	}

	public boolean isProbabilidadAbraPuertaValidada() {
		return probabilidadAbraPuertaValidada;
	}

	public boolean isProporcionMujeresValidada() {
		return proporcionMujeresValidada;
	}

	public boolean isProporcionHombresValidada() {
		return proporcionHombresValidada;
	}

	public boolean isProbabilidadCompraSenoraValidada() {
		return probabilidadCompraSenoraValidada;
	}

	public boolean isProbabilidadCompraSenorValidada() {
		return probabilidadCompraSenorValidada;
	}

	public boolean isUtilidadPorSuscripcionValidada() {
		return utilidadPorSuscripcionValidada;
	}

	public boolean isFrecuenciaRelativaUnaSuscripcionSenoraValidada() {
		return frecuenciaRelativaUnaSuscripcionSenoraValidada;
	}

	public boolean isFrecuenciaRelativaDosSuscripcionesSenoraValidada() {
		return frecuenciaRelativaDosSuscripcionesSenoraValidada;
	}

	public boolean isFrecuenciaRelativaTresSuscripcionesSenoraValidada() {
		return frecuenciaRelativaTresSuscripcionesSenoraValidada;
	}

	public boolean isFrecuenciaRelativaUnaSuscripcionSenorValidada() {
		return frecuenciaRelativaUnaSuscripcionSenorValidada;
	}

	public boolean isFrecuenciaRelativaDosSuscripcionesSenorValidada() {
		return frecuenciaRelativaDosSuscripcionesSenorValidada;
	}

	public boolean isFrecuenciaRelativaTresSuscripcionesSenorValidada() {
		return frecuenciaRelativaTresSuscripcionesSenorValidada;
	}

	public boolean isFrecuenciaRelativaCuatroSuscripcionesSenorValidada() {
		return frecuenciaRelativaCuatroSuscripcionesSenorValidada;
	}

	public boolean isNumeroVisitasASimularValidada() {
		return numeroVisitasASimularValidada;
	}

	public boolean isNumeroVisitasAMostrarValidada() {
		return numeroVisitasAMostrarValidada;
	}

	public boolean isPrimeraVisitaAMostrarValidada() {
		return primeraVisitaAMostrarValidada;
	}

	public boolean isSumatoriaFrecuenciaRelativaSenoraValidada() {
		return sumatoriaFrecuenciaRelativaSenoraValidada;
	}

	public boolean isSumatoriaFrecuenciaRelativaSenorValidada() {
		return sumatoriaFrecuenciaRelativaSenorValidada;
	}

	public void setIngresarProbabilidadAbraPuertaIconSrc(String ingresarProbabilidadAbraPuertaIconSrc) {
		this.ingresarProbabilidadAbraPuertaIconSrc = ingresarProbabilidadAbraPuertaIconSrc;
	}

	public void setIngresarProporcionMujeresIconSrc(String ingresarProporcionMujeresIconSrc) {
		this.ingresarProporcionMujeresIconSrc = ingresarProporcionMujeresIconSrc;
	}

	public void setIngresarProporcionHombresIconSrc(String ingresarProporcionHombresIconSrc) {
		this.ingresarProporcionHombresIconSrc = ingresarProporcionHombresIconSrc;
	}

	public void setIngresarProbabilidadCompraSenoraIconSrc(String ingresarProbabilidadCompraSenoraIconSrc) {
		this.ingresarProbabilidadCompraSenoraIconSrc = ingresarProbabilidadCompraSenoraIconSrc;
	}

	public void setIngresarProbabilidadCompraSenorIconSrc(String ingresarProbabilidadCompraSenoraIconSrc) {
		this.ingresarProbabilidadCompraSenorIconSrc = ingresarProbabilidadCompraSenoraIconSrc;
	}

	public void setIngresarUtilidadPorSuscripcionIconSrc(String ingresarUtilidadPorSuscripcionaIconSrc) {
		this.ingresarUtilidadPorSuscripcionIconSrc = ingresarUtilidadPorSuscripcionaIconSrc;
	}

	public void setIngresarFrecuenciaRelativaUnaSuscripcionSenoraIconSrc(
			String ingresarFrecuenciaRelativaUnaSuscripcionSenoraaIconSrc) {
		this.ingresarFrecuenciaRelativaUnaSuscripcionSenoraIconSrc = ingresarFrecuenciaRelativaUnaSuscripcionSenoraaIconSrc;
	}

	public void setIngresarFrecuenciaRelativaDosSuscripcionesSenoraIconSrc(
			String ingresarFrecuenciaRelativaDosSuscripcionesSenoraaIconSrc) {
		this.ingresarFrecuenciaRelativaDosSuscripcionesSenoraIconSrc = ingresarFrecuenciaRelativaDosSuscripcionesSenoraaIconSrc;
	}

	public void setIngresarFrecuenciaRelativaTresSuscripcionesSenoraIconSrc(
			String ingresarFrecuenciaRelativaTresSuscripcionesSenoraaIconSrc) {
		this.ingresarFrecuenciaRelativaTresSuscripcionesSenoraIconSrc = ingresarFrecuenciaRelativaTresSuscripcionesSenoraaIconSrc;
	}

	public void setIngresarFrecuenciaRelativaUnaSuscripcionSenorIconSrc(
			String ingresarFrecuenciaRelativaUnaSuscripcionSenoraIconSrc) {
		this.ingresarFrecuenciaRelativaUnaSuscripcionSenorIconSrc = ingresarFrecuenciaRelativaUnaSuscripcionSenoraIconSrc;
	}

	public void setIngresarFrecuenciaRelativaDosSuscripcionesSenorIconSrc(
			String ingresarFrecuenciaRelativaDosSuscripcionesSenoraIconSrc) {
		this.ingresarFrecuenciaRelativaDosSuscripcionesSenorIconSrc = ingresarFrecuenciaRelativaDosSuscripcionesSenoraIconSrc;
	}

	public void setIngresarFrecuenciaRelativaTresSuscripcionesSenorIconSrc(
			String ingresarFrecuenciaRelativaTresSuscripcionesSenoraIconSrc) {
		this.ingresarFrecuenciaRelativaTresSuscripcionesSenorIconSrc = ingresarFrecuenciaRelativaTresSuscripcionesSenoraIconSrc;
	}

	public void setIngresarFrecuenciaRelativaCuatroSuscripcionesSenorIconSrc(
			String ingresarFrecuenciaRelativaCuatroSuscripcionesSenoraIconSrc) {
		this.ingresarFrecuenciaRelativaCuatroSuscripcionesSenorIconSrc = ingresarFrecuenciaRelativaCuatroSuscripcionesSenoraIconSrc;
	}

	public void setIngresarNumeroVisitasASimularIconSrc(String ingresarNumeroVisitasASimularaIconSrc) {
		this.ingresarNumeroVisitasASimularIconSrc = ingresarNumeroVisitasASimularaIconSrc;
	}

	public void setIngresarNumeroVisitasAMostrarIconSrc(String ingresarNumeroVisitasAMostraraIconSrc) {
		this.ingresarNumeroVisitasAMostrarIconSrc = ingresarNumeroVisitasAMostraraIconSrc;
	}

	public void setIngresarPrimeraVisitaAMostrarIconSrc(String ingresarPrimeraVisitaAMostraraIconSrc) {
		this.ingresarPrimeraVisitaAMostrarIconSrc = ingresarPrimeraVisitaAMostraraIconSrc;
	}

	public void setSumatoriaFrecuenciaRelativaSenoraIconSrc(String sumatoriaFrecuenciaRelativaSenoraaIconSrc) {
		this.sumatoriaFrecuenciaRelativaSenoraIconSrc = sumatoriaFrecuenciaRelativaSenoraaIconSrc;
	}

	public void setSumatoriaFrecuenciaRelativaSenorIconSrc(String sumatoriaFrecuenciaRelativaSenoraIconSrc) {
		this.sumatoriaFrecuenciaRelativaSenorIconSrc = sumatoriaFrecuenciaRelativaSenoraIconSrc;
	}

	public void setProbabilidadAbraPuertaValidada(boolean probabilidadAbraPuertaValidada) {
		this.probabilidadAbraPuertaValidada = probabilidadAbraPuertaValidada;
	}

	public void setProporcionMujeresValidada(boolean proporcionMujeresValidada) {
		this.proporcionMujeresValidada = proporcionMujeresValidada;
	}

	public void setProporcionHombresValidada(boolean proporcionHombresValidada) {
		this.proporcionHombresValidada = proporcionHombresValidada;
	}

	public void setProbabilidadCompraSenoraValidada(boolean probabilidadCompraSenoraValidada) {
		this.probabilidadCompraSenoraValidada = probabilidadCompraSenoraValidada;
	}

	public void setProbabilidadCompraSenorValidada(boolean probabilidadCompraSenorValidada) {
		this.probabilidadCompraSenorValidada = probabilidadCompraSenorValidada;
	}

	public void setUtilidadPorSuscripcionValidada(boolean utilidadPorSuscripcionValidada) {
		this.utilidadPorSuscripcionValidada = utilidadPorSuscripcionValidada;
	}

	public void setFrecuenciaRelativaUnaSuscripcionSenoraValidada(
			boolean frecuenciaRelativaUnaSuscripcionSenoraValidada) {
		this.frecuenciaRelativaUnaSuscripcionSenoraValidada = frecuenciaRelativaUnaSuscripcionSenoraValidada;
	}

	public void setFrecuenciaRelativaDosSuscripcionesSenoraValidada(
			boolean frecuenciaRelativaDosSuscripcionesSenoraValidada) {
		this.frecuenciaRelativaDosSuscripcionesSenoraValidada = frecuenciaRelativaDosSuscripcionesSenoraValidada;
	}

	public void setFrecuenciaRelativaTresSuscripcionesSenoraValidada(
			boolean frecuenciaRelativaTresSuscripcionesSenoraValidada) {
		this.frecuenciaRelativaTresSuscripcionesSenoraValidada = frecuenciaRelativaTresSuscripcionesSenoraValidada;
	}

	public void setFrecuenciaRelativaUnaSuscripcionSenorValidada(
			boolean frecuenciaRelativaUnaSuscripcionSenorValidada) {
		this.frecuenciaRelativaUnaSuscripcionSenorValidada = frecuenciaRelativaUnaSuscripcionSenorValidada;
	}

	public void setFrecuenciaRelativaDosSuscripcionesSenorValidada(
			boolean frecuenciaRelativaDosSuscripcionesSenorValidada) {
		this.frecuenciaRelativaDosSuscripcionesSenorValidada = frecuenciaRelativaDosSuscripcionesSenorValidada;
	}

	public void setFrecuenciaRelativaTresSuscripcionesSenorValidada(
			boolean frecuenciaRelativaTresSuscripcionesSenorValidada) {
		this.frecuenciaRelativaTresSuscripcionesSenorValidada = frecuenciaRelativaTresSuscripcionesSenorValidada;
	}

	public void setFrecuenciaRelativaCuatroSuscripcionesSenorValidada(
			boolean frecuenciaRelativaCuatroSuscripcionesSenorValidada) {
		this.frecuenciaRelativaCuatroSuscripcionesSenorValidada = frecuenciaRelativaCuatroSuscripcionesSenorValidada;
	}

	public void setNumeroVisitasASimularValidada(boolean numeroVisitasASimularValidada) {
		this.numeroVisitasASimularValidada = numeroVisitasASimularValidada;
	}

	public void setNumeroVisitasAMostrarValidada(boolean numeroVisitasAMostrarValidada) {
		this.numeroVisitasAMostrarValidada = numeroVisitasAMostrarValidada;
	}

	public void setPrimeraVisitaAMostrarValidada(boolean primeraVisitaAMostrarValidada) {
		this.primeraVisitaAMostrarValidada = primeraVisitaAMostrarValidada;
	}

	public void setSumatoriaFrecuenciaRelativaSenoraValidada(boolean sumatoriaFrecuenciaRelativaSenoraValidada) {
		this.sumatoriaFrecuenciaRelativaSenoraValidada = sumatoriaFrecuenciaRelativaSenoraValidada;
	}

	public void setSumatoriaFrecuenciaRelativaSenorValidada(boolean sumatoriaFrecuenciaRelativaSenorValidada) {
		this.sumatoriaFrecuenciaRelativaSenorValidada = sumatoriaFrecuenciaRelativaSenorValidada;
	}

	// Fin "Validación"

	public Converter<?, ?, ?> getConversorDeInt() {
		return conversorDeInt;
	}

	public Converter<?, ?, ?> getConversorDeLong() {
		return conversorDeLong;
	}

	public Converter<?, ?, ?> getConversorDeDouble() {
		return conversorDeDouble;
	}

	public Converter<?, ?, ?> getConversorDeSumaDeDinero() {
		return conversorDeSumaDeDinero;
	}

	public Converter<?, ?, ?> getConversorDePorcentaje() {
		return conversorDePorcentaje;
	}

	public Converter<?, ?, ?> getConversorDeFrecuenciaRelativa() {
		return conversorDeFrecuenciaRelativa;
	}

	public Converter<?, ?, ?> getTruncador() {
		formateadorDeNumerosDecimales.setPattern("#.####");
		return formateadorDeNumerosDecimales;
	}

	public String getResultadosSrc() {
		return mostrandoResultados ? rB.getPagePathname("resultados.") : null;
	}

	public String getInputSrc() {
		return rB.getPagePathname("input");
	}

	@DependsOn("resultadosSrc")
	public String getResultadosSimulacionSrc() {
		String src = null;
		if (tP4Problema6 != null && tP4Problema6.hasResultadosSimulacion())
			src = rB.getPagePathname("resultadossimulacion.");
		return src;
	}

	@DependsOn("resultadosSrc")
	public String getTablaVectoresEstadoSrc() {
		String src = null;
		if (tP4Problema6 != null && tP4Problema6.getTabla() != null)
			src = rB.getPagePathname("tablavectoresestado.");
		return src;
	}

	public void setInputSrc(String inputSrc) {
	}

	public TP4Problema6 gettP4Problema6() {
		if (tP4Problema6 == null)
			tP4Problema6 = (TP4Problema6) SpringUtil.getBean("tP4Problema6");
		return tP4Problema6;
	}

	public void settP4Problema6(TP4Problema6 tP4Problema6) {
		this.tP4Problema6 = tP4Problema6;
	}

	public boolean isMostrandoResultados() {
		return mostrandoResultados;
	}

	public void setMostrandoResultados(boolean mostrandoResultados) {
		this.mostrandoResultados = mostrandoResultados;
	}

	@DependsOn({ "mostrandoResultados", "probabilidadAbraPuertaValidada", "proporcionMujeresValidada", "proporcionHombresValidada",
			"probabilidadCompraSenoraValidada", "probabilidadCompraSenorValidada", "utilidadPorSuscripcionValidada",
			"frecuenciaRelativaUnaSuscripcionSenoraValidada", "frecuenciaRelativaDosSuscripcionesSenoraValidada",
			"frecuenciaRelativaTresSuscripcionesSenoraValidada", "frecuenciaRelativaUnaSuscripcionSenorValidada",
			"frecuenciaRelativaDosSuscripcionesSenorValidada", "frecuenciaRelativaTresSuscripcionesSenorValidada",
			"frecuenciaRelativaCuatroSuscripcionesSenorValidada", "numeroVisitasASimularValidada",
			"numeroVisitasAMostrarValidada", "primeraVisitaAMostrarValidada",
			"sumatoriaFrecuenciaRelativaSenoraValidada",
			"sumatoriaFrecuenciaRelativaSenorValidada" })
	public boolean isCorrerHabilitado() {
		return !mostrandoResultados && isValidacionOk();
	}

	public PuertaFactory getPuertaFactory() {
		return puertaFactory;
	}

	public PersonaFactory getPersonaFactory() {
		return personaFactory;
	}

	public MujerFactory getMujerFactory() {
		return mujerFactory;
	}

	public HombreFactory getHombreFactory() {
		return hombreFactory;
	}

	public SuscripcionFactory getSuscripcionFactory() {
		return suscripcionFactory;
	}

	@Immutable
	public BigDecimal getProporcionHombres() {
		return proporcionHombres;
	}

	@Immutable
	public BigDecimal getProporcionMujeres() {
		return proporcionMujeres;
	}

	@Immutable
	public BigDecimal getSumatoriaFrecuenciaRelativaSenora() {
		return sumatoriaFrecuenciaRelativaSenora;
	}

	@Immutable
	public BigDecimal getSumatoriaFrecuenciaRelativaSenor() {
		return sumatoriaFrecuenciaRelativaSenor;
	}

}
