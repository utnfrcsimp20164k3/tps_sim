package app.tp4.problema6.presentacion;

import org.zkoss.bind.Converter;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.GlobalCommand;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.SmartNotifyChange;
import org.zkoss.zk.ui.select.annotation.VariableResolver;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.ListModelList;

import app.presentacion.ConversorDeDouble;
import app.presentacion.ConversorDeInt;
import app.presentacion.ConversorDeLong;
import app.presentacion.ConversorDeSumaDeDinero;
import app.tp4.problema6.TP4Problema6;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@VariableResolver(org.zkoss.zkplus.spring.DelegatingVariableResolver.class)
public class TablaVectoresEstadoVMTP4Problema6 {

	static final Logger LOG = LoggerFactory.getLogger(TablaVectoresEstadoVMTP4Problema6.class);
	@WireVariable
	TablaVectoresEstadoMTP4Problema6 tablaVectoresEstadoMTP4Problema6;
	@WireVariable
	ConversorDeInt conversorDeInt;
	@WireVariable
	ConversorDeDouble conversorDeDouble;
	@WireVariable
	ConversorDeLong conversorDeLong;
	@WireVariable
	ConversorDeSumaDeDinero conversorDeSumaDeDinero;


	@Init
	public void initSetup(@BindingParam("tP4Problema6") TP4Problema6 tP4Problema6) {
		tablaVectoresEstadoMTP4Problema6.setTabla(tP4Problema6.getTabla());
	}
	
	@GlobalCommand
	@SmartNotifyChange("tablaVectoresEstadoM")
	public void limpiarTablaVectoresEstado() {
		tablaVectoresEstadoMTP4Problema6.clear();
		
	}

	public ListModelList<TablaVectoresEstadoDetalleMTP4Problema6> getDetalles() {
		return tablaVectoresEstadoMTP4Problema6.getDetalles();
	}

	public Converter<?, ?, ?> getConversorDeInt() {
		return conversorDeInt;
	}

	public Converter<?, ?, ?> getConversorDeLong() {
		return conversorDeLong;
	}

	public Converter<?, ?, ?> getConversorDeDouble() {
		return conversorDeDouble;
	}
	
	public Converter<?, ?, ?> getConversorDeSumaDeDinero() {
		return conversorDeSumaDeDinero;
	}
	
	public TablaVectoresEstadoMTP4Problema6 getTablaVectoresEstadoM() {
		return tablaVectoresEstadoMTP4Problema6;
	}

}
