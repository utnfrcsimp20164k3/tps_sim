package app.tp4.problema6.presentacion;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import app.utils.DetalleTabla;

@Component
public class TablaVectoresEstadoDetalleMTP4Problema6Impl implements TablaVectoresEstadoDetalleMTP4Problema6 {
	
	static final Logger LOG = LoggerFactory.getLogger(TablaVectoresEstadoDetalleMTP4Problema6Impl.class);
	DetalleTabla detalleTabla;
	
	public TablaVectoresEstadoDetalleMTP4Problema6Impl() {
		super();
	}

	@Bean
	@Scope(value = "prototype")
	static public TablaVectoresEstadoDetalleMTP4Problema6  tablaVectoresEstadoDetalleMTP4Problema6 () {
		return new TablaVectoresEstadoDetalleMTP4Problema6Impl ();
	}

	public void setDetalleTabla(DetalleTabla detalleTabla) {
		this.detalleTabla = detalleTabla;
	}
	
	@Override
	public Object getValorA() {
		return detalleTabla.getValores().get("Visita");
	}

	@Override
	public Object getValorB() {
		return detalleTabla.getValores().get("Rnd abre");
	}

	@Override
	public Object getValorC() {
		return detalleTabla.getValores().get("Abre");
	}

	@Override
	public Object getValorD() {
		return detalleTabla.getValores().get("Abiertas");
	}

	@Override
	public Object getValorE() {
		return detalleTabla.getValores().get("Rnd genero");
	}

	@Override
	public Object getValorF() {
		return detalleTabla.getValores().get("Mujer");
	}

	@Override
	public Object getValorG() {
		return detalleTabla.getValores().get("Mujeres");
	}

	@Override
	public Object getValorH() {
		return detalleTabla.getValores().get("Hombre");
	}

	@Override
	public Object getValorI() {
		return detalleTabla.getValores().get("Hombres");
	}

	@Override
	public Object getValorJ() {
		return detalleTabla.getValores().get("Mujer / Hombre");
	}

	@Override
	public Object getValorK() {
		return detalleTabla.getValores().get("Rnd venta");
	}

	@Override
	public Object getValorL() {
		return detalleTabla.getValores().get("Venta");
	}

	@Override
	public Object getValorM() {
		return detalleTabla.getValores().get("Ventas");
	}

	@Override
	public Object getValorN() {
		return detalleTabla.getValores().get("Ventas a mujeres");
	}

	@Override
	public Object getValorO() {
		return detalleTabla.getValores().get("Rnd cantidad de suscripciones");
	}

	@Override
	public Object getValorP() {
		return detalleTabla.getValores().get("Cantidad de suscripciones");
	}

	@Override
	public Object getValorQ() {
		return detalleTabla.getValores().get("Una suscripción");
	}

	@Override
	public Object getValorR() {
		return detalleTabla.getValores().get("Dos suscripciones");
	}

	@Override
	public Object getValorS() {
		return detalleTabla.getValores().get("Tres suscripciones");
	}

	@Override
	public Object getValorT() {
		return detalleTabla.getValores().get("Cuatro suscripciones");
	}

	@Override
	public Object getValorU() {
		return detalleTabla.getValores().get("Mujeres que suscribieron una");
	}

	@Override
	public Object getValorV() {
		return detalleTabla.getValores().get("Mujeres que suscribieron dos");
	}

	@Override
	public Object getValorW() {
		return detalleTabla.getValores().get("Mujeres que suscribieron tres");
	}

	@Override
	public Object getValorX() {
		return detalleTabla.getValores().get("Hombres que suscribieron una");
	}

	@Override
	public Object getValorY() {
		return detalleTabla.getValores().get("Hombres que suscribieron dos");
	}

	@Override
	public Object getValorZ() {
		return detalleTabla.getValores().get("Hombres que suscribieron tres");
	}

	@Override
	public Object getValorAA() {
		return detalleTabla.getValores().get("Hombres que suscribieron cuatro");
	}

	@Override
	public Object getValorAB() {
		return detalleTabla.getValores().get("Utilidad");
	}

	@Override
	public Object getValorAC() {
		return detalleTabla.getValores().get("Utilidades");
	}

}
