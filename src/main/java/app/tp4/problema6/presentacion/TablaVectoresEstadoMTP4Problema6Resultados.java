package app.tp4.problema6.presentacion;

import org.zkoss.zul.ListModelList;

import app.utils.TablaVectorEstados;

public interface TablaVectoresEstadoMTP4Problema6Resultados {

	void setTabla(TablaVectorEstados tablaVectorEstados);
	
	Object getEncabezadoA();

	Object getEncabezadoB();

	Object getEncabezadoC();

	ListModelList<TablaVectoresEstadoDetalleMTP4Problema6Resultados> getDetalles();

	void clear();

}
