package app.tp4.problema6.presentacion;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.zkoss.zul.ListModelList;

import app.Loggable;
import app.utils.DetalleTabla;
import app.utils.TablaVectorEstados;

@Component
public class TablaVectoresEstadoMTP4Problema6ResultadosImpl implements TablaVectoresEstadoMTP4Problema6Resultados {

	static final Logger LOG = LoggerFactory.getLogger(TablaVectoresEstadoMTP4Problema6ResultadosImpl.class);
	TablaVectorEstados tabla;
	@Autowired
	ApplicationContext applicationContext;
	
	public TablaVectoresEstadoMTP4Problema6ResultadosImpl() {
		super();
	}

	@Bean
	@Scope("prototype")
	static public TablaVectoresEstadoMTP4Problema6Resultados tablaVectoresEstadoMTP4Problema6Resultados() {
		return new TablaVectoresEstadoMTP4Problema6ResultadosImpl();
	}

	@Override
	public void setTabla(TablaVectorEstados tabla) {
		this.tabla = tabla;
	}

	@Override
	public Object getEncabezadoA() {
		return "Visitas";
	}

	@Override
	public Object getEncabezadoB() {
		return "Ventas";
	}

	@Override
	public Object getEncabezadoC() {
		return "Probabilidad de vender suscripciones";
	}

	@Override
	public ListModelList<TablaVectoresEstadoDetalleMTP4Problema6Resultados> getDetalles() {
		doLog("trace", "getDetalles()", "[{]", null, null);
		ListModelList<TablaVectoresEstadoDetalleMTP4Problema6Resultados> modelo = new ListModelList<>();
		for (DetalleTabla d : tabla.getDetalles()) {
			TablaVectoresEstadoDetalleMTP4Problema6Resultados dM = (TablaVectoresEstadoDetalleMTP4Problema6Resultados) applicationContext.getBean("tablaVectoresEstadoDetalleMTP4Problema6Resultados");
			dM.setDetalleTabla(d);
			modelo.add(dM);
		}
		doLog("trace", "getDetalles()", "[}]", null, null);
		return modelo;
	}

	@Override
	public void clear() {
		tabla.getDetalles().clear();
	}

	private void doLog(String level, String signature, String corchetes, String nombreVariable, Loggable loggable) {
		switch (level) {
		case "trace":
			if (LOG.isTraceEnabled())
				LOG.trace(toLogMsg(signature, corchetes, nombreVariable, loggable));
			break;
		case "debug":
			if (LOG.isDebugEnabled())
				LOG.debug(toLogMsg(signature, corchetes, nombreVariable, loggable));
			break;
		case "info":
			if (LOG.isInfoEnabled())
				LOG.info(toLogMsg(signature, corchetes, nombreVariable, loggable));
			break;
		case "warn":
			if (LOG.isWarnEnabled())
				LOG.warn(toLogMsg(signature, corchetes, nombreVariable, loggable));
			break;
		case "error":
			if (LOG.isErrorEnabled())
				LOG.error(toLogMsg(signature, corchetes, nombreVariable, loggable));
			break;
		}
	}

	private String toLogMsg(String signature, String corchetes, String nombreVariable, Loggable loggable) {
		StringBuffer s = new StringBuffer(signature);
		if (corchetes != null)
			s.append(" " + corchetes);
		if (nombreVariable != null)
			s.append(" " + "{ l = " + nombreVariable + " }");
		if (nombreVariable != null && loggable == null)
			s.append(nombreVariable + ": " + loggable);
		else if (nombreVariable != null && loggable != null)
			s.append(loggable.toLogString("l"));
		return s.toString();
	}

}
