package app.tp4.problema6.presentacion;

import app.utils.DetalleTabla;

public interface TablaVectoresEstadoDetalleMTP4Problema6 {
	
	void setDetalleTabla(DetalleTabla d);

	Object getValorA();

	Object getValorB();

	Object getValorC();

	Object getValorD();

	Object getValorE();

	Object getValorF();

	Object getValorG();

	Object getValorH();

	Object getValorI();

	Object getValorJ();

	Object getValorK();

	Object getValorL();

	Object getValorM();

	Object getValorN();

	Object getValorO();

	Object getValorP();

	Object getValorQ();

	Object getValorR();

	Object getValorS();

	Object getValorT();

	Object getValorU();

	Object getValorV();

	Object getValorW();

	Object getValorX();

	Object getValorY();

	Object getValorZ();

	Object getValorAA();

	Object getValorAB();

	Object getValorAC();

}
