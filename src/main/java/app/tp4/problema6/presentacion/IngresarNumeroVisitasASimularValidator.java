package app.tp4.problema6.presentacion;

import app.presentacion.InputValidator;
import app.tp4.problema6.TP4Problema6;

public interface IngresarNumeroVisitasASimularValidator extends InputValidator {
	
	void validate(TP4Problema6 tp);

}
