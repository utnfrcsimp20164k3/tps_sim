package app.tp4.problema6.presentacion;

import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Set;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.zkoss.bind.ValidationContext;
import org.zkoss.bind.validator.AbstractValidator;
import org.zkoss.zel.ELException;

import app.presentacion.Observable;
import app.presentacion.Observer;

@Component
public class IngresarProbabilidadCompraSenorValidatorImpl extends AbstractValidator
		implements IngresarProbabilidadCompraSenorValidator, Observable {

	private Set<Observer> observers;
	private boolean changed;
	private final boolean obligatoria = true;
	private boolean incorrecta;
	private boolean nula;

	IngresarProbabilidadCompraSenorValidatorImpl() {
		super();
		observers = new HashSet<>();
	}

	@Bean
	@Scope("prototype")
	static public IngresarProbabilidadCompraSenorValidator ingresarProbabilidadCompraSenorValidator() {
		return new IngresarProbabilidadCompraSenorValidatorImpl();
	}

	@Override
	public void validate(ValidationContext ctx) {
		try {
			BigDecimal prob = (BigDecimal) ctx.getProperties("probabilidadCompra")[0].getValue();
			if (prob == null)
				nula = true;
			if (prob != null
					&& (prob.compareTo(new BigDecimal("0")) == -1 || prob.compareTo(new BigDecimal("100")) == 1)) {
				addInvalidMessage(ctx, "probabilidadCompra", "Debe ingresar un número entre 0 y 100.");
				incorrecta = true;
			}
		} catch (NumberFormatException ex) {
			addInvalidMessage(ctx, "probabilidadCompra", "Debe ingresar un número entre 0 y 100.");
			incorrecta = true;
		} catch (ELException ex) {
			addInvalidMessage(ctx, "probabilidadCompra", "Debe ingresar un número entre 0 y 100.");
			incorrecta = true;
		} finally {
			changed = true;
			notifyObservers();
		}
	}

	@Override
	public boolean isObligatoria() {
		return obligatoria;
	}

	@Override
	public boolean isIncorrecta() {
		return incorrecta;
	}

	@Override
	public boolean isNula() {
		return nula;
	}

	@Override
	public void addObserver(Observer o) {
		observers.add(o);
	}

	@Override
	public int countObservers() {
		return observers.size();
	}

	@Override
	public void deleteObserver(Observer o) {
		observers.remove(o);
	}

	@Override
	public void deleteObservers() {
		observers.clear();
	}

	@Override
	public boolean hasChanged() {
		return changed;
	}

	@Override
	public void notifyObservers() {
		if (hasChanged())
			for (Observer obs : observers)
				obs.update(this, null);
		clearChanged();
	}

	@Override
	public void notifyObservers(Object arg) {
		if (hasChanged())
			for (Observer obs : observers)
				obs.update(this, arg);
		clearChanged();
	}

	protected void clearChanged() {
		changed = false;
		incorrecta = false;
		nula = false;
	}

}
