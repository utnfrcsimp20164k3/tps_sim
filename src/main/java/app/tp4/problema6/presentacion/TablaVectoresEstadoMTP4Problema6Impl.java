package app.tp4.problema6.presentacion;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.zkoss.zul.ListModelList;

import app.utils.DetalleTabla;
import app.utils.TablaSimple;

@Component
public class TablaVectoresEstadoMTP4Problema6Impl implements TablaVectoresEstadoMTP4Problema6 {

	static final Logger LOG = LoggerFactory.getLogger(TablaVectoresEstadoMTP4Problema6Impl.class);
	private TablaSimple tablaSimple;
	
	public TablaVectoresEstadoMTP4Problema6Impl() {
		super();
	}

	@Bean
	@Scope("prototype")
	static public TablaVectoresEstadoMTP4Problema6 tablaVectoresEstadoMTP4Problema6() {
		return new TablaVectoresEstadoMTP4Problema6Impl();
	}

	@Override
	public void setTabla(TablaSimple tablaSimple) {
		this.tablaSimple = tablaSimple;
	}

	@Override
	public Object getEncabezadoA() {
		return tablaSimple.getClaves().get(0);
	}

	@Override
	public Object getEncabezadoB() {
		return tablaSimple.getClaves().get(1);
	}

	@Override
	public Object getEncabezadoC() {
		return tablaSimple.getClaves().get(2);
	}

	@Override
	public Object getEncabezadoD() {
		return tablaSimple.getClaves().get(3);
	}

	@Override
	public Object getEncabezadoE() {
		return tablaSimple.getClaves().get(4);
	}

	@Override
	public Object getEncabezadoF() {
		return tablaSimple.getClaves().get(5);
	}

	@Override
	public Object getEncabezadoG() {
		return tablaSimple.getClaves().get(6);
	}

	@Override
	public Object getEncabezadoH() {
		return tablaSimple.getClaves().get(7);
	}

	@Override
	public Object getEncabezadoI() {
		return tablaSimple.getClaves().get(8);
	}

	@Override
	public Object getEncabezadoJ() {
		return tablaSimple.getClaves().get(9);
	}

	@Override
	public Object getEncabezadoK() {
		return tablaSimple.getClaves().get(10);
	}

	@Override
	public Object getEncabezadoL() {
		return tablaSimple.getClaves().get(11);
	}

	@Override
	public Object getEncabezadoM() {
		return tablaSimple.getClaves().get(12);
	}

	@Override
	public Object getEncabezadoN() {
		return tablaSimple.getClaves().get(13);
	}

	@Override
	public Object getEncabezadoO() {
		return tablaSimple.getClaves().get(14);
	}

	@Override
	public Object getEncabezadoP() {
		return tablaSimple.getClaves().get(15);
	}

	@Override
	public Object getEncabezadoQ() {
		return tablaSimple.getClaves().get(16);
	}

	@Override
	public Object getEncabezadoR() {
		return tablaSimple.getClaves().get(17);
	}

	@Override
	public Object getEncabezadoS() {
		return tablaSimple.getClaves().get(18);
	}

	@Override
	public Object getEncabezadoT() {
		return tablaSimple.getClaves().get(19);
	}

	@Override
	public Object getEncabezadoU() {
		return tablaSimple.getClaves().get(20);
	}

	@Override
	public Object getEncabezadoV() {
		return tablaSimple.getClaves().get(21);
	}

	@Override
	public Object getEncabezadoW() {
		return tablaSimple.getClaves().get(22);
	}

	@Override
	public Object getEncabezadoX() {
		return tablaSimple.getClaves().get(23);
	}

	@Override
	public Object getEncabezadoY() {
		return tablaSimple.getClaves().get(24);
	}

	@Override
	public Object getEncabezadoZ() {
		return tablaSimple.getClaves().get(25);
	}

	@Override
	public Object getEncabezadoAA() {
		return tablaSimple.getClaves().get(26);
	}

	@Override
	public Object getEncabezadoAB() {
		return tablaSimple.getClaves().get(27);
	}

	@Override
	public Object getEncabezadoAC() {
		return tablaSimple.getClaves().get(28);
	}

	@Override
	public ListModelList<TablaVectoresEstadoDetalleMTP4Problema6> getDetalles() {
		ListModelList<TablaVectoresEstadoDetalleMTP4Problema6> modelo = new ListModelList<>();
		for (DetalleTabla d : tablaSimple.getDetalles()) {
			TablaVectoresEstadoDetalleMTP4Problema6 dM = new TablaVectoresEstadoDetalleMTP4Problema6Impl();
			dM.setDetalleTabla(d);
			modelo.add(dM);
		}
		return modelo;
	}

	@Override
	public void clear() {
		tablaSimple.getDetalles().clear();
	}

}
