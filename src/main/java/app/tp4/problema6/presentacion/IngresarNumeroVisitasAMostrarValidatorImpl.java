package app.tp4.problema6.presentacion;

import java.util.HashSet;
import java.util.Set;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.zkoss.bind.ValidationContext;
import org.zkoss.bind.validator.AbstractValidator;
import org.zkoss.zel.ELException;

import app.presentacion.InputValidator;
import app.presentacion.Observable;
import app.presentacion.Observer;
import app.tp4.problema6.TP4Problema6;

@Component
public class IngresarNumeroVisitasAMostrarValidatorImpl extends AbstractValidator
		implements IngresarNumeroVisitasAMostrarValidator, InputValidator, Observable {

	private Set<Observer> observers;
	private boolean changed;
	private final boolean obligatoria = true;
	private boolean incorrecta;
	private boolean nula;

	IngresarNumeroVisitasAMostrarValidatorImpl() {
		super();
		observers = new HashSet<>();
	}

	@Bean
	@Scope("prototype")
	static public IngresarNumeroVisitasAMostrarValidator ingresarNumeroVisitasAMostrarValidator() {
		return new IngresarNumeroVisitasAMostrarValidatorImpl();
	}

	@Override
	public void validate(ValidationContext ctx) {
		Long mostrar = (Long) ctx.getProperty().getValue();
		Long simular = (Long) ctx.getProperties("numeroVisitasASimular")[0].getValue();
		Long primera = (Long) ctx.getProperties("primeraVisitaAMostrar")[0].getValue();
		try {
			if (mostrar == null || mostrar == 0l) {
				addInvalidMessage(ctx, "ingresarNumeroVisitasAMostrar",
						"Debe ingresar un número de visitas a mostrar.");
				nula = true;
			} else if (mostrar < 0l) {
				addInvalidMessage(ctx, "ingresarNumeroVisitasAMostrar", "Debe ingresar un número entero positivo.");
				incorrecta = true;
			} else if (simular != null && mostrar > simular) {
				addInvalidMessage(ctx, "ingresarNumeroVisitasAMostrar",
						"Debe ingresar un número menor o igual al número de visitas a simular.");
				incorrecta = true;
			} else if (simular != null && primera != null && mostrar + primera > simular + 1l) {
				addInvalidMessage(ctx, "ingresarNumeroVisitasAMostrar",
						"No es posible mostrar esa cantidad de visitas a partir de la visita especificada.");
				incorrecta = true;
			}
		} catch (NumberFormatException ex) {
			addInvalidMessage(ctx, "ingresarNumeroVisitasAMostrar", "Debe ingresar un número entero positivo.");
			incorrecta = true;
		} catch (ELException ex) {
			addInvalidMessage(ctx, "ingresarNumeroVisitasAMostrar", "Debe ingresar un número entero positivo.");
			incorrecta = true;
		} finally {
			changed = true;
			notifyObservers();
		}
	}

	@Override
	public void validate(TP4Problema6 tP) {
		Long mostrar = tP.getNumeroVisitasAMostrar();
		Long primera = tP.getPrimeraVisitaAMostrar();
		Long simular = tP.getNumeroVisitasASimular();
		if (mostrar == null || mostrar == 0l)
			nula = true;
		else if (mostrar < 0l)
			incorrecta = true;
		else if (simular != null && mostrar > simular)
			incorrecta = true;
		else if (simular != null && primera != null && mostrar + primera > simular)
			incorrecta = true;
	}

	@Override
	public boolean isObligatoria() {
		return obligatoria;
	}

	@Override
	public boolean isIncorrecta() {
		return incorrecta;
	}

	@Override
	public boolean isNula() {
		return nula;
	}

	@Override
	public void addObserver(Observer o) {
		observers.add(o);
	}

	@Override
	public int countObservers() {
		return observers.size();
	}

	@Override
	public void deleteObserver(Observer o) {
		observers.remove(o);
	}

	@Override
	public void deleteObservers() {
		observers.clear();
	}

	@Override
	public boolean hasChanged() {
		return changed;
	}

	@Override
	public void notifyObservers() {
		if (hasChanged())
			for (Observer obs : observers)
				obs.update(this, null);
		clearChanged();
	}

	@Override
	public void notifyObservers(Object arg) {
		if (hasChanged())
			for (Observer obs : observers)
				obs.update(this, arg);
		clearChanged();
	}

	protected void clearChanged() {
		changed = false;
		incorrecta = false;
		nula = false;
	}

}
