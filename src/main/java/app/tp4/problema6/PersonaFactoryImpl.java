package app.tp4.problema6;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.HashSet;
import java.util.Set;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.zkoss.bind.annotation.Immutable;

import app.presentacion.Observable;
import app.presentacion.Observer;
import app.utils.ParRandomEvento;
import app.utils.Porcentaje;
import app.utils.SimuladorDeEventos;
import app.utils.distribucion.TipoDistribucion;

@Component
public class PersonaFactoryImpl implements PersonaFactory, Observable {

	static final Logger LOG = LoggerFactory.getLogger(PersonaFactoryImpl.class);
	@Autowired
	private ApplicationContext applicationContext;
	private Set<Observer> observers;
	private MujerFactory mujerFactory;
	private HombreFactory hombreFactory;
	private SimuladorDeEventos simuladorGenero;
	private TipoDistribucion tipoDistribucion;
	private Persona instance;
	private BigDecimal proporcionMujeres, proporcionHombres;
	boolean mujer, hombre;
	private Double rndGenero;
	private boolean changed;

	public PersonaFactoryImpl() { }
	
	@Bean
	@Scope(value = "singleton")
	static public PersonaFactory personaFactory() {
		return new PersonaFactoryImpl();
	}

	@PostConstruct
	public void init() {
		observers = new HashSet<>();
		mujerFactory = (MujerFactory) applicationContext.getBean("mujerFactory");
		hombreFactory = (HombreFactory) applicationContext.getBean("hombreFactory");
		simuladorGenero = (SimuladorDeEventos) applicationContext.getBean("simuladorDeEventos");
		tipoDistribucion = (TipoDistribucion) applicationContext.getBean("distribucionUniforme");
		setProporcionMujeres(new BigDecimal("80").setScale(Porcentaje.getDefaultScale(), RoundingMode.HALF_UP));
		setProporcionHombres(new BigDecimal("20").setScale(Porcentaje.getDefaultScale(), RoundingMode.HALF_UP));
	}

	@Override
	public Persona getObject() throws Exception {
		clear();
		simularGenero();
		if (mujer)
			instance = mujerFactory.getObject();
		else if (hombre)
			instance = hombreFactory.getObject();
		instance.setRndGenero(rndGenero);
		return instance;
	}

	private void clear() {
		instance = null;
		mujer = false;
		hombre = false;
	}

	private void simularGenero() {
		ParRandomEvento par = simuladorGenero.next();
		rndGenero = par.getLeft();
		if (par.getRight().equals(Mujer.class)) {
			mujer = true;
			hombre = false;
		}
		else if (par.getRight().equals(Hombre.class)) {
			hombre = true; 
			mujer = false;
		}
	}

	@Override
	public Class<?> getObjectType() {
		return Persona.class;
	}

	@Override
	public boolean isSingleton() {
		return true;
	}

	@Override
	public void setProporcionMujeres(BigDecimal proporcionMujeres) {
		this.proporcionMujeres = proporcionMujeres;
		proporcionHombres = new BigDecimal("100").subtract(proporcionMujeres);
		changed = true;
		notifyObservers();
	}

	@Override
	@Immutable
	public BigDecimal getProporcionMujeres() {
		return proporcionMujeres;
	}

	@Override
	public void setProporcionHombres(BigDecimal proporcionHombres) {
		this.proporcionHombres = proporcionHombres;
		proporcionMujeres = new BigDecimal("100").subtract(proporcionHombres);
		changed = true;
		notifyObservers();
	}

	@Override
	@Immutable
	public BigDecimal getProporcionHombres() {
		return proporcionHombres;
	}


	@Override
	public void addObserver(Observer o) {
		observers.add(o);
	}

	@Override
	public int countObservers() {
		return observers.size();
	}

	@Override
	public void deleteObserver(Observer o) {
		observers.remove(o);
	}

	@Override
	public void deleteObservers() {
		observers.clear();
	}

	@Override
	public boolean hasChanged() {
		return changed;
	}

	@Override
	public void notifyObservers() {
		if (hasChanged())
			for (Observer obs : observers)
				obs.update(this, null);
		clearChanged();
	}

	@Override
	public void notifyObservers(Object arg) {
		if (hasChanged())
			for (Observer obs : observers)
				obs.update(this, arg);
		clearChanged();
	}

	protected void clearChanged() {
		changed = false;
	}

	@Override
	public void setup() {
		simuladorGenero.asociar(proporcionMujeres.divide(new BigDecimal("100")), Mujer.class);
		simuladorGenero.asociar(proporcionHombres.divide(new BigDecimal("100")), Hombre.class);
		simuladorGenero.setTipoDistribucion(tipoDistribucion);
		simuladorGenero.setup();
		mujerFactory.setup();
		hombreFactory.setup();
	}
	
}
