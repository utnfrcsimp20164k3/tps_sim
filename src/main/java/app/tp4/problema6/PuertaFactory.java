package app.tp4.problema6;

import java.math.BigDecimal;

public interface PuertaFactory {
	
	BigDecimal getProbabilidadSeAbra();

	void setProbabilidadSeAbra(BigDecimal probabilidadSeAbra);

	Puerta getObject() throws Exception;

	Class<?> getObjectType();

	boolean isSingleton();

	void setup();

}
