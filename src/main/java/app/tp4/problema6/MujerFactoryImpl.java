package app.tp4.problema6;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.HashSet;
import java.util.Set;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.zkoss.bind.annotation.Immutable;

import app.presentacion.Observable;
import app.presentacion.Observer;
import app.utils.FrecuenciaRelativa;
import app.utils.ParRandomEvento;
import app.utils.Porcentaje;
import app.utils.SimuladorDeEventos;
import app.utils.distribucion.TipoDistribucion;

@Component
public class MujerFactoryImpl implements MujerFactory, Observable {

	static final Logger LOG = LoggerFactory.getLogger(MujerFactoryImpl.class);
	@Autowired
	private ApplicationContext applicationContext;
	private Set<Observer> observers;
	private CompraFactory compraFactory;
	private SimuladorDeEventos simuladorCompra;
	private SimuladorDeEventos simuladorCantidadSuscripciones;
	private TipoDistribucion tipoDistribucion;
	private SuscripcionFactory suscripcionFactory;
	private Mujer instance;
	private Double rndCompra, rndCantidadSuscripciones;
	private BigDecimal probabilidadCompra, frecuenciaRelativaUnaSuscripcion, frecuenciaRelativaDosSuscripciones,
			frecuenciaRelativaTresSuscripciones;
	boolean comprador;
	private Compra compra;
	private Long cantidadSuscripciones;
	private boolean changed;

	public MujerFactoryImpl() {
		super();
		observers = new HashSet<>();
	}

	@Bean
	@Scope(value = "singleton")
	static public MujerFactory mujerFactory() {
		return new MujerFactoryImpl();
	}

	@PostConstruct
	public void init() {
		compraFactory = (CompraFactory) applicationContext.getBean("compraFactory");
		suscripcionFactory = (SuscripcionFactory) applicationContext.getBean("suscripcionFactory");
		simuladorCompra = (SimuladorDeEventos) applicationContext.getBean("simuladorDeEventos");
		simuladorCantidadSuscripciones = (SimuladorDeEventos) applicationContext.getBean("simuladorDeEventos");
		tipoDistribucion = (TipoDistribucion) applicationContext.getBean("distribucionUniforme");
		setFrecuenciaRelativaUnaSuscripcion(new BigDecimal("0.60").setScale(FrecuenciaRelativa.getDefaultScale(), RoundingMode.HALF_UP));
		setFrecuenciaRelativaDosSuscripciones(new BigDecimal("0.30").setScale(FrecuenciaRelativa.getDefaultScale(), RoundingMode.HALF_UP));
		setFrecuenciaRelativaTresSuscripciones(new BigDecimal("0.10").setScale(FrecuenciaRelativa.getDefaultScale(), RoundingMode.HALF_UP));
		setProbabilidadCompra(new BigDecimal("15").setScale(Porcentaje.getDefaultScale(), RoundingMode.HALF_UP));
	}

	private void clear() {
		instance = null;
		rndCompra = null;
		compra = null;
		comprador = false;
	}

	@Override
	public Mujer getObject() throws Exception {
		clear();
		instance = new MujerImpl();
		simularSiCompra();
		instance.setRndCompra(rndCompra);
		instance.setComprador(comprador);
		instance.setCompra(compra);
		if (comprador) {
			instance.setCompra(compraFactory.getObject());
			simularCantidadSuscripciones();
			instance.getCompra().setRndCantidadSuscripciones(rndCantidadSuscripciones);
			instance.setRndCantidadSuscripciones(rndCantidadSuscripciones);
			instance.getCompra().setSuscripciones(new Suscripcion[cantidadSuscripciones.intValue()]);
			for (int i = 0; i < instance.getCompra().getSuscripciones().length; i++)
				instance.getCompra().getSuscripciones()[i] = suscripcionFactory.getObject();
		}
		return instance;
	}

	private void simularCantidadSuscripciones() {
		ParRandomEvento par = simuladorCantidadSuscripciones.next();
		rndCantidadSuscripciones = par.getLeft();
		cantidadSuscripciones = (Long) par.getRight();
	}

	private void simularSiCompra() throws Exception {
		ParRandomEvento par = simuladorCompra.next();
		rndCompra = par.getLeft();
		comprador = (Boolean) par.getRight();
		if (comprador)
			compra = compraFactory.getObject();
	}

	@Override
	public Class<?> getObjectType() {
		return Mujer.class;
	}

	@Override
	public boolean isSingleton() {
		return true;
	}

	@Override
	public void setProbabilidadCompra(BigDecimal probabilidadCompra) {
		this.probabilidadCompra = probabilidadCompra;
	}

	@Override
	@Immutable
	public BigDecimal getProbabilidadCompra() {
		return probabilidadCompra;
	}

	@Override
	public void setFrecuenciaRelativaUnaSuscripcion(BigDecimal frecuenciaRelativaUnaSuscripcion) {
		this.frecuenciaRelativaUnaSuscripcion = frecuenciaRelativaUnaSuscripcion;
	}

	@Override
	@Immutable
	public BigDecimal getFrecuenciaRelativaUnaSuscripcion() {
		return frecuenciaRelativaUnaSuscripcion;
	}

	@Override
	public void setFrecuenciaRelativaDosSuscripciones(BigDecimal frecuenciaRelativaDosSuscripciones) {
		this.frecuenciaRelativaDosSuscripciones = frecuenciaRelativaDosSuscripciones;
	}

	@Override
	@Immutable
	public BigDecimal getFrecuenciaRelativaDosSuscripciones() {
		return frecuenciaRelativaDosSuscripciones;
	}

	@Override
	public void setFrecuenciaRelativaTresSuscripciones(BigDecimal frecuenciaRelativaTresSuscripciones) {
		this.frecuenciaRelativaTresSuscripciones = frecuenciaRelativaTresSuscripciones;
	}

	@Override
	@Immutable
	public BigDecimal getFrecuenciaRelativaTresSuscripciones() {
		return frecuenciaRelativaTresSuscripciones;
	}

	@Override
	public void addObserver(Observer o) {
		observers.add(o);
	}

	@Override
	public int countObservers() {
		return observers.size();
	}

	@Override
	public void deleteObserver(Observer o) {
		observers.remove(o);
	}

	@Override
	public void deleteObservers() {
		observers.clear();
	}

	@Override
	public boolean hasChanged() {
		return changed;
	}

	@Override
	public void notifyObservers() {
		if (hasChanged())
			for (Observer obs : observers)
				obs.update(this, null);
		clearChanged();
	}

	@Override
	public void notifyObservers(Object arg) {
		if (hasChanged())
			for (Observer obs : observers)
				obs.update(this, arg);
		clearChanged();
	}

	protected void clearChanged() {
		changed = false;
	}

	@Override
	public void setup() {
		simuladorCompra.asociar(probabilidadCompra.divide(new BigDecimal("100")), Boolean.TRUE);
		simuladorCompra.asociar(new BigDecimal("1").subtract(probabilidadCompra.divide(new BigDecimal("100"))), Boolean.FALSE);
		simuladorCompra.setTipoDistribucion(tipoDistribucion);
		simuladorCompra.setup();
		simuladorCantidadSuscripciones.asociar(frecuenciaRelativaUnaSuscripcion, new Long(1));
		simuladorCantidadSuscripciones.asociar(frecuenciaRelativaDosSuscripciones, new Long(2));
		simuladorCantidadSuscripciones.asociar(frecuenciaRelativaTresSuscripciones, new Long(3));
		simuladorCantidadSuscripciones.setTipoDistribucion(tipoDistribucion);
		simuladorCantidadSuscripciones.setup();
		compraFactory.setup();
	}

}
