package app.tp4.problema6;

public class VisitaImpl implements Visita {
	
	Puerta puerta;

	@Override
	public void setPuerta(Puerta puerta) {
		this.puerta = puerta;
	}

	@Override
	public Puerta getPuerta() {
		return puerta;
	}

}
