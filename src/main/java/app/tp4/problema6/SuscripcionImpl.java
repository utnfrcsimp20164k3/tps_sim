package app.tp4.problema6;

import java.math.BigDecimal;

import org.zkoss.bind.annotation.Immutable;

public class SuscripcionImpl implements Suscripcion {
	
	BigDecimal utilidad;

	@Override
	public void setUtilidad(BigDecimal utilidad) {
		this.utilidad = utilidad;
	}

	@Override
	@Immutable
	public BigDecimal getUtilidad() {
		return utilidad;
	}

}
