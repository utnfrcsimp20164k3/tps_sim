package app.tp4.problema6;

public class CompraImpl implements Compra {

	Suscripcion[] suscripciones;
	Double rndCantidadSuscripciones;

	@Override
	public Suscripcion[] getSuscripciones() {
		return suscripciones;
	}

	@Override
	public void setSuscripciones(Suscripcion[] suscripciones) {
		this.suscripciones = suscripciones;
	}

	@Override
	public void setRndCantidadSuscripciones(Double rndCantidadSuscripciones) {
		this.rndCantidadSuscripciones = rndCantidadSuscripciones;
	}

	@Override
	public Double getRndCantidadSuscripciones() {
		return rndCantidadSuscripciones;
	}

	@Override
	public Long getCantidadSuscripciones() {
		Long c = 0l;
		if (suscripciones != null)
			c = new Long(suscripciones.length);
		return c;
	}

	@Override
	public boolean isUnaSuscripcion() {
		return suscripciones.length == 1;
	}

	@Override
	public boolean isDosSuscripciones() {
		return suscripciones.length == 2;
	}

	@Override
	public boolean isTresSuscripciones() {
		return suscripciones.length == 3;
	}

	@Override
	public boolean isCuatroSuscripciones() {
		return suscripciones.length == 4;
	}

}
