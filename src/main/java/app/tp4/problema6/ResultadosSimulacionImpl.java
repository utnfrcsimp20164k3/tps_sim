package app.tp4.problema6;

import java.math.BigDecimal;

import org.zkoss.bind.annotation.Immutable;

import app.utils.DetalleTabla;

public class ResultadosSimulacionImpl implements ResultadosSimulacion {

	DetalleTabla detalleTablaFinal;

	public ResultadosSimulacionImpl() {
		super();
	}

	@Override
	public void setDetalleTablaFinal(DetalleTabla detalleTablaFinal) {
		this.detalleTablaFinal = detalleTablaFinal;
	}

	@Override
	public Long getCantidadVisitas() {
		return (Long) detalleTablaFinal.getValores().get("Visita");
	}

	@Override
	public String getStringCantidadVisitas() {
		return "N";
	}

	@Override
	public Long getAbiertas() {
		return (Long) detalleTablaFinal.getValores().get("Abiertas");
	}

	@Override
	public String getStringAbiertas() {
		return "Total abiertas";
	}

	@Override
	@Immutable
	public BigDecimal getProbabilidadSeAbra() {
		return new BigDecimal(getAbiertas().doubleValue() / getCantidadVisitas().doubleValue());
	}

	@Override
	public String getFormulaProbabilidadSeAbra() {
		return getStringAbiertas() + " / " + getStringCantidadVisitas();
	}

	@Override
	public String getStringProbabilidadSeAbra() {
		return "Probabilidad se abra";
	}

	@Override
	public Long getMujeres() {
		return (Long) detalleTablaFinal.getValores().get("Mujeres");
	}

	@Override
	public String getStringMujeres() {
		return "Total mujeres";
	}

	@Override
	@Immutable
	public BigDecimal getProporcionMujeres() {
		return new BigDecimal(getMujeres().doubleValue() / getAbiertas().doubleValue()).multiply(new BigDecimal("100"));
	}

	@Override
	public String getStringProporcionMujeres() {
		return "Proporción mujeres";
	}

	@Override
	public String getFormulaProporcionMujeres() {
		return "100 x " + getStringMujeres() + " / " + getStringAbiertas();
	}

	@Override
	public Long getHombres() {
		return (Long) detalleTablaFinal.getValores().get("Hombres");
	}

	@Override
	public String getStringHombres() {
		return "Total hombres";
	}

	@Override
	@Immutable
	public BigDecimal getProporcionHombres() {
		return new BigDecimal(getHombres().doubleValue() / getAbiertas().doubleValue()).multiply(new BigDecimal("100"));
	}

	@Override
	public String getStringProporcionHombres() {
		return "Proporción hombres";
	}

	@Override
	public String getFormulaProporcionHombres() {
		return "100 x " + getStringHombres() + " / " + getStringAbiertas();
	}

	@Override
	public Long getVentasAMujeres() {
		return (Long) detalleTablaFinal.getValores().get("Ventas a mujeres");
	}

	@Override
	public String getStringVentasAMujeres() {
		return "Total ventas a mujeres";
	}

	@Override
	@Immutable
	public BigDecimal getProbabilidadVentaAMujer() {
		return new BigDecimal(getVentasAMujeres().doubleValue() / getMujeres().doubleValue());
	}

	@Override
	public String getStringProbabilidadVentaAMujer() {
		return "Probabilidad venta a mujer";
	}

	@Override
	public String getFormulaProbabilidadVentaAMujer() {
		return getStringVentasAMujeres() + " / " + getMujeres();
	}

	@Override
	public Long getMujeresUnaSuscripcion() {
		return (Long) detalleTablaFinal.getValores().get("Mujeres que suscribieron una");
	}

	@Override
	public String getStringMujeresUnaSuscripcion() {
		return "Total de mujeres con una suscripción";
	}

	@Override
	@Immutable
	public BigDecimal getFrecuenciaRelativaUnaSuscripcionMujer() {
		return new BigDecimal(getMujeresUnaSuscripcion().doubleValue() / getVentasAMujeres().doubleValue());
	}

	@Override
	public String getStringFrecuenciaRelativaUnaSuscripcionMujer() {
		return "Frecuencia relativa mujeres con una suscripción";
	}

	@Override
	public String getFormulaFrecuenciaRelativaUnaSuscripcionMujer() {
		return getStringMujeresUnaSuscripcion() + " / " + getStringVentasAMujeres();
	}

	@Override
	public Long getMujeresDosSuscripciones() {
		return (Long) detalleTablaFinal.getValores().get("Mujeres que suscribieron dos");
	}

	@Override
	public String getStringMujeresDosSuscripciones() {
		return "Total de mujeres con dos suscripciones";
	}

	@Override
	@Immutable
	public BigDecimal getFrecuenciaRelativaDosSuscripcionesMujer() {
		return new BigDecimal(getMujeresDosSuscripciones().doubleValue() / getVentasAMujeres().doubleValue());
	}

	@Override
	public String getStringFrecuenciaRelativaDosSuscripcionesMujer() {
		return "Frecuencia relativa mujeres con dos suscripciones";
	}

	@Override
	public String getFormulaFrecuenciaRelativaDosSuscripcionesMujer() {
		return getStringMujeresDosSuscripciones() + " / " + getStringVentasAMujeres();
	}

	@Override
	public Long getMujeresTresSuscripciones() {
		return (Long) detalleTablaFinal.getValores().get("Mujeres que suscribieron tres");
	}

	@Override
	public String getStringMujeresTresSuscripciones() {
		return "Total de mujeres con tres suscripciones";
	}

	@Override
	@Immutable
	public BigDecimal getFrecuenciaRelativaTresSuscripcionesMujer() {
		return new BigDecimal(getMujeresTresSuscripciones().doubleValue() / getVentasAMujeres().doubleValue());
	}

	@Override
	public String getStringFrecuenciaRelativaTresSuscripcionesMujer() {
		return "Frecuencia relativa mujeres con tres suscripciones";
	}

	@Override
	public String getFormulaFrecuenciaRelativaTresSuscripcionesMujer() {
		return getStringMujeresTresSuscripciones() + " / " + getStringVentasAMujeres();
	}

	@Override
	public Long getVentasAHombres() {
		return (Long) detalleTablaFinal.getValores().get("Ventas")
				- (Long) detalleTablaFinal.getValores().get("Ventas a mujeres");
	}

	@Override
	public String getStringVentasAHombres() {
		return "Total ventas a hombres";
	}

	@Override
	@Immutable
	public BigDecimal getProbabilidadVentaAHombre() {
		return new BigDecimal(getVentasAHombres().doubleValue() / getHombres().doubleValue());
	}

	@Override
	public String getStringProbabilidadVentaAHombre() {
		return "Probabilidad venta a hombre";
	}

	@Override
	public String getFormulaProbabilidadVentaAHombre() {
		return getStringVentasAHombres() + " / " + getHombres();
	}

	@Override
	public Long getHombresUnaSuscripcion() {
		return (Long) detalleTablaFinal.getValores().get("Hombres que suscribieron una");
	}

	@Override
	public String getStringHombresUnaSuscripcion() {
		return "Total de hombres con una suscripción";
	}

	@Override
	@Immutable
	public BigDecimal getFrecuenciaRelativaUnaSuscripcionHombre() {
		return new BigDecimal(getHombresUnaSuscripcion().doubleValue() / getVentasAHombres().doubleValue());
	}

	@Override
	public String getStringFrecuenciaRelativaUnaSuscripcionHombre() {
		return "Frecuencia relativa hombres con una suscripción";
	}

	@Override
	public String getFormulaFrecuenciaRelativaUnaSuscripcionHombre() {
		return getStringHombresUnaSuscripcion() + " / " + getStringVentasAHombres();
	}

	@Override
	public Long getHombresDosSuscripciones() {
		return (Long) detalleTablaFinal.getValores().get("Hombres que suscribieron dos");
	}

	@Override
	public String getStringHombresDosSuscripciones() {
		return "Total de hombres con dos suscripciones";
	}

	@Override
	@Immutable
	public BigDecimal getFrecuenciaRelativaDosSuscripcionesHombre() {
		return new BigDecimal(getHombresDosSuscripciones().doubleValue() / getVentasAHombres().doubleValue());
	}

	@Override
	public String getStringFrecuenciaRelativaDosSuscripcionesHombre() {
		return "Frecuencia relativa hombres con dos suscripciones";
	}

	@Override
	public String getFormulaFrecuenciaRelativaDosSuscripcionesHombre() {
		return getStringHombresDosSuscripciones() + " / " + getStringVentasAHombres();
	}

	@Override
	public Long getHombresTresSuscripciones() {
		return (Long) detalleTablaFinal.getValores().get("Hombres que suscribieron tres");
	}

	@Override
	public String getStringHombresTresSuscripciones() {
		return "Total de hombres con tres suscripciones";
	}

	@Override
	@Immutable
	public BigDecimal getFrecuenciaRelativaTresSuscripcionesHombre() {
		return new BigDecimal(getHombresTresSuscripciones().doubleValue() / getVentasAHombres().doubleValue());
	}

	@Override
	public String getStringFrecuenciaRelativaTresSuscripcionesHombre() {
		return "Frecuencia relativa hombres con tres suscripciones";
	}

	@Override
	public String getFormulaFrecuenciaRelativaTresSuscripcionesHombre() {
		return getStringHombresTresSuscripciones() + " / " + getStringVentasAHombres();
	}

	@Override
	public Long getHombresCuatroSuscripciones() {
		return (Long) detalleTablaFinal.getValores().get("Hombres que suscribieron cuatro");
	}

	@Override
	public String getStringHombresCuatroSuscripciones() {
		return "Total de hombres con cuatro suscripciones";
	}

	@Override
	@Immutable
	public BigDecimal getFrecuenciaRelativaCuatroSuscripcionesHombre() {
		return new BigDecimal(getHombresCuatroSuscripciones().doubleValue() / getVentasAHombres().doubleValue());
	}

	@Override
	public String getStringFrecuenciaRelativaCuatroSuscripcionesHombre() {
		return "Frecuencia relativa hombres con cuatro suscripciones";
	}

	@Override
	public String getFormulaFrecuenciaRelativaCuatroSuscripcionesHombre() {
		return getStringHombresCuatroSuscripciones() + " / " + getStringVentasAHombres();
	}

	@Override
	@Immutable
	public BigDecimal getProbabilidadVentaPorVisita() {
		return getProbabilidadVentaAMujerPorVisita().add(getProbabilidadVentaAHombrePorVisita());
	}

	@Override
	public String getStringProbabilidadVentaPorVisita() {
		return "Probabilidad de venta por visita";
	}

	@Override
	public String getFormulaProbabilidadVentaPorVisita() {
		return getStringProbabilidadVentaAMujerPorVisita() + " + " + getStringProbabilidadVentaAHombrePorVisita();
	}

	@Override
	@Immutable
	public BigDecimal getProbabilidadVentaUnaSuscripcionPorVisita() {
		return getProbabilidadVentaAMujerUnaSuscripcionPorVisita()
				.add(getProbabilidadVentaAHombreUnaSuscripcionPorVisita());
	}

	@Override
	public String getStringProbabilidadVentaUnaSuscripcionPorVisita() {
		return "Probabilidad de venta de una suscripción por visita";
	}

	@Override
	public String getFormulaProbabilidadVentaUnaSuscripcionPorVisita() {
		return getStringProbabilidadVentaAMujerUnaSuscripcionPorVisita() + " + "
				+ getStringProbabilidadVentaAHombreUnaSuscripcionPorVisita();
	}

	@Override
	@Immutable
	public BigDecimal getProbabilidadVentaDosSuscripcionesPorVisita() {
		return getProbabilidadVentaAMujerDosSuscripcionesPorVisita()
				.add(getProbabilidadVentaAHombreDosSuscripcionesPorVisita());
	}

	@Override
	public String getStringProbabilidadVentaDosSuscripcionesPorVisita() {
		return "Probabilidad de venta de dos suscripciones por visita";
	}

	@Override
	public String getFormulaProbabilidadVentaDosSuscripcionesPorVisita() {
		return getStringProbabilidadVentaAMujerDosSuscripcionesPorVisita() + " + "
				+ getStringProbabilidadVentaAHombreDosSuscripcionesPorVisita();
	}

	@Override
	@Immutable
	public BigDecimal getProbabilidadVentaTresSuscripcionesPorVisita() {
		return getProbabilidadVentaAMujerTresSuscripcionesPorVisita()
				.add(getProbabilidadVentaAHombreTresSuscripcionesPorVisita());
	}

	@Override
	public String getStringProbabilidadVentaTresSuscripcionesPorVisita() {
		return "Probabilidad de venta de tres suscripciones por visita";
	}

	@Override
	public String getFormulaProbabilidadVentaTresSuscripcionesPorVisita() {
		return getStringProbabilidadVentaAMujerTresSuscripcionesPorVisita() + " + "
				+ getStringProbabilidadVentaAHombreTresSuscripcionesPorVisita();
	}

	@Override
	@Immutable
	public BigDecimal getProbabilidadVentaCuatroSuscripcionesPorVisita() {
		return getProbabilidadVentaAHombreCuatroSuscripcionesPorVisita();
	}

	@Override
	public String getStringProbabilidadVentaCuatroSuscripcionesPorVisita() {
		return "Probabilidad de venta de cuatro suscripciones por visita";
	}

	@Override
	public String getFormulaProbabilidadVentaCuatroSuscripcionesPorVisita() {
		return getStringProbabilidadVentaAHombreCuatroSuscripcionesPorVisita();
	}

	@Override
	@Immutable
	public BigDecimal getProbabilidadVentaPorLoMenosUnaSuscripcionPorVisita() {
		return getProbabilidadVentaUnaSuscripcionPorVisita().add(
				getProbabilidadVentaDosSuscripcionesPorVisita().add(getProbabilidadVentaTresSuscripcionesPorVisita()
						.add(getProbabilidadVentaAHombreCuatroSuscripcionesPorVisita())));
	}

	@Override
	public String getStringProbabilidadVentaPorLoMenosUnaSuscripcionPorVisita() {
		return "Probabilidad de venta de por lo menos una suscripción por visita";
	}

	@Override
	public String getFormulaProbabilidadVentaPorLoMenosUnaSuscripcionPorVisita() {
		return getStringProbabilidadVentaUnaSuscripcionPorVisita() + " + "
				+ getStringProbabilidadVentaDosSuscripcionesPorVisita() + " + "
				+ getStringProbabilidadVentaTresSuscripcionesPorVisita() + " + "
				+ getStringProbabilidadVentaCuatroSuscripcionesPorVisita();
	}

	@Override
	@Immutable
	public BigDecimal getProbabilidadVentaPorLoMenosDosSuscripcionesPorVisita() {
		return getProbabilidadVentaDosSuscripcionesPorVisita().add(getProbabilidadVentaTresSuscripcionesPorVisita()
				.add(getProbabilidadVentaAHombreCuatroSuscripcionesPorVisita()));
	}

	@Override
	public String getStringProbabilidadVentaPorLoMenosDosSuscripcionesPorVisita() {
		return "Probabilidad de venta de por lo menos dos suscripciones por visita";
	}

	@Override
	public String getFormulaProbabilidadVentaPorLoMenosDosSuscripcionesPorVisita() {
		return getStringProbabilidadVentaDosSuscripcionesPorVisita() + " + "
				+ getStringProbabilidadVentaTresSuscripcionesPorVisita() + " + "
				+ getStringProbabilidadVentaCuatroSuscripcionesPorVisita();
	}

	@Override
	@Immutable
	public BigDecimal getProbabilidadVentaPorLoMenosTresSuscripcionesPorVisita() {
		return getProbabilidadVentaTresSuscripcionesPorVisita()
				.add(getProbabilidadVentaAHombreCuatroSuscripcionesPorVisita());
	}

	@Override
	public String getStringProbabilidadVentaPorLoMenosTresSuscripcionesPorVisita() {
		return "Probabilidad de venta de por lo menos tres suscripciones por visita";
	}

	@Override
	public String getFormulaProbabilidadVentaPorLoMenosTresSuscripcionesPorVisita() {
		return getStringProbabilidadVentaTresSuscripcionesPorVisita() + " + "
				+ getStringProbabilidadVentaCuatroSuscripcionesPorVisita();
	}

	@Override
	@Immutable
	public BigDecimal getProbabilidadVentaPorLoMenosCuatroSuscripcionesPorVisita() {
		return getProbabilidadVentaAHombreCuatroSuscripcionesPorVisita();
	}

	@Override
	public String getStringProbabilidadVentaPorLoMenosCuatroSuscripcionesPorVisita() {
		return "Probabilidad de venta de por lo menos cuatro suscripciones por visita";
	}

	@Override
	public String getFormulaProbabilidadVentaPorLoMenosCuatroSuscripcionesPorVisita() {
		return getStringProbabilidadVentaCuatroSuscripcionesPorVisita();
	}

	@Override
	@Immutable
	public BigDecimal getProbabilidadVentaAMujerPorVisita() {
		return getProbabilidadSeAbra().multiply(getProbabilidadVentaAMujer());
	}

	@Override
	public String getStringProbabilidadVentaAMujerPorVisita() {
		return "Probabilidad de venta a mujer por visita";
	}

	@Override
	public String getFormulaProbabilidadVentaAMujerPorVisita() {
		return getStringProbabilidadSeAbra() + " x " + getStringProbabilidadVentaAMujer();
	}

	@Override
	@Immutable
	public BigDecimal getProbabilidadVentaAMujerUnaSuscripcionPorVisita() {
		return getProbabilidadVentaAMujerPorVisita().multiply(getFrecuenciaRelativaUnaSuscripcionMujer());
	}

	@Override
	public String getStringProbabilidadVentaAMujerUnaSuscripcionPorVisita() {
		return "Probabilidad de venta de una suscripción a mujer por visita";
	}

	@Override
	public String getFormulaProbabilidadVentaAMujerUnaSuscripcionPorVisita() {
		return getStringProbabilidadVentaAMujerPorVisita() + " x " + getStringFrecuenciaRelativaUnaSuscripcionMujer();
	}

	@Override
	@Immutable
	public BigDecimal getProbabilidadVentaAMujerDosSuscripcionesPorVisita() {
		return getProbabilidadVentaAMujerPorVisita().multiply(getFrecuenciaRelativaDosSuscripcionesMujer());
	}

	@Override
	public String getStringProbabilidadVentaAMujerDosSuscripcionesPorVisita() {
		return "Probabilidad de venta de dos suscripciones a mujer por visita";
	}

	@Override
	public String getFormulaProbabilidadVentaAMujerDosSuscripcionesPorVisita() {
		return getStringProbabilidadVentaAMujerPorVisita() + " x " + getStringFrecuenciaRelativaDosSuscripcionesMujer();
	}

	@Override
	@Immutable
	public BigDecimal getProbabilidadVentaAMujerTresSuscripcionesPorVisita() {
		return getProbabilidadVentaAMujerPorVisita().multiply(getFrecuenciaRelativaTresSuscripcionesMujer());
	}

	@Override
	public String getStringProbabilidadVentaAMujerTresSuscripcionesPorVisita() {
		return "Probabilidad de venta de tres suscripciones a mujer por visita";
	}

	@Override
	public String getFormulaProbabilidadVentaAMujerTresSuscripcionesPorVisita() {
		return getStringProbabilidadVentaAMujerPorVisita() + " x "
				+ getStringFrecuenciaRelativaTresSuscripcionesMujer();
	}

	@Override
	@Immutable
	public BigDecimal getProbabilidadVentaAHombrePorVisita() {
		return getProbabilidadSeAbra().multiply(getProbabilidadVentaAHombre());
	}

	@Override
	public String getStringProbabilidadVentaAHombrePorVisita() {
		return "Probabilidad de venta a hombre por visita";
	}

	@Override
	public String getFormulaProbabilidadVentaAHombrePorVisita() {
		return getStringProbabilidadSeAbra() + " x " + getStringProbabilidadVentaAHombre();
	}

	@Override
	@Immutable
	public BigDecimal getProbabilidadVentaAHombreUnaSuscripcionPorVisita() {
		return getProbabilidadVentaAHombrePorVisita().multiply(getFrecuenciaRelativaUnaSuscripcionHombre());
	}

	@Override
	public String getStringProbabilidadVentaAHombreUnaSuscripcionPorVisita() {
		return "Probabilidad de venta de una suscripción a hombre por visita";
	}

	@Override
	public String getFormulaProbabilidadVentaAHombreUnaSuscripcionPorVisita() {
		return getStringProbabilidadVentaAHombrePorVisita() + " x " + getStringFrecuenciaRelativaUnaSuscripcionHombre();
	}

	@Override
	@Immutable
	public BigDecimal getProbabilidadVentaAHombreDosSuscripcionesPorVisita() {
		return getProbabilidadVentaAHombrePorVisita().multiply(getFrecuenciaRelativaDosSuscripcionesHombre());
	}

	@Override
	public String getStringProbabilidadVentaAHombreDosSuscripcionesPorVisita() {
		return "Probabilidad de venta de dos suscripciones a hombre por visita";
	}

	@Override
	public String getFormulaProbabilidadVentaAHombreDosSuscripcionesPorVisita() {
		return getStringProbabilidadVentaAHombrePorVisita() + " x "
				+ getStringFrecuenciaRelativaDosSuscripcionesHombre();
	}

	@Override
	@Immutable
	public BigDecimal getProbabilidadVentaAHombreTresSuscripcionesPorVisita() {
		return getProbabilidadVentaAHombrePorVisita().multiply(getFrecuenciaRelativaTresSuscripcionesHombre());
	}

	@Override
	public String getStringProbabilidadVentaAHombreTresSuscripcionesPorVisita() {
		return "Probabilidad de venta de tres suscripciones a hombre por visita";
	}

	@Override
	public String getFormulaProbabilidadVentaAHombreTresSuscripcionesPorVisita() {
		return getStringProbabilidadVentaAHombrePorVisita() + " x "
				+ getStringFrecuenciaRelativaTresSuscripcionesHombre();
	}

	@Override
	@Immutable
	public BigDecimal getProbabilidadVentaAHombreCuatroSuscripcionesPorVisita() {
		return getProbabilidadVentaAHombrePorVisita().multiply(getFrecuenciaRelativaCuatroSuscripcionesHombre());
	}

	@Override
	public String getStringProbabilidadVentaAHombreCuatroSuscripcionesPorVisita() {
		return "Probabilidad de venta de cuatro suscripciones a hombre por visita";
	}

	@Override
	public String getFormulaProbabilidadVentaAHombreCuatroSuscripcionesPorVisita() {
		return getStringProbabilidadVentaAHombrePorVisita() + " x "
				+ getStringFrecuenciaRelativaCuatroSuscripcionesHombre();
	}

	@Override
	public boolean hasDetalleTablaFinal() {
		return detalleTablaFinal != null;
	}

}
