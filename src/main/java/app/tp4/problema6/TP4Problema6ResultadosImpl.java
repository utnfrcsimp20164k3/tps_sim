package app.tp4.problema6;

import java.math.BigDecimal;
import java.math.RoundingMode;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import app.Loggable;
import app.utils.DetalleTabla;
import app.utils.TablaVectorEstados;

@Component
public class TP4Problema6ResultadosImpl implements TP4Problema6Resultados {

	static final Logger LOG = LoggerFactory.getLogger(TP4Problema6ResultadosImpl.class);
	private DetalleTabla detalleTabla;
	@Autowired
	private TablaVectorEstados tablaVectorEstados;
	@Autowired
	private ApplicationContext applicationContext;

	public TP4Problema6ResultadosImpl() {
		super();
	}

	@Bean
	@Scope(value = "prototype")
	static public TP4Problema6Resultados tP4Problema6Resultados() {
		return new TP4Problema6ResultadosImpl();
	}

	@Override
	public boolean hasDetalleTablaFinal() {
		return detalleTabla != null;
	}

	@Override
	public void calcular(DetalleTabla detalleTabla) {
		this.detalleTabla = detalleTabla;
		construirTabla();
	}

	@Override
	public TablaVectorEstados getTabla() {
		return tablaVectorEstados;
	}

	private void construirTabla() {
		doLog("trace", "construirTabla()", "[{]", null, null);
		DetalleTabla detalle = (DetalleTabla) applicationContext.getBean("detalleTabla");
		tablaVectorEstados.crearColumna("Visitas");
		detalle.getValores().put("Visitas",
				detalleTabla.getValores().get("Visita"));
		tablaVectorEstados.crearColumna("Ventas");
		detalle.getValores().put("Ventas",
				detalleTabla.getValores().get("Ventas"));
		tablaVectorEstados.crearColumna("Probabilidad de vender suscripciones");
		detalle.getValores().put("Probabilidad de vender suscripciones",
				new BigDecimal((Long) detalleTabla.getValores().get("Ventas"))
						.divide(new BigDecimal(
								(Long) detalleTabla.getValores().get("Visita")), 4,
								RoundingMode.HALF_UP));
		tablaVectorEstados.getDetalles().add(detalle);
		doLog("trace", "construirTabla()", "[}]", null, null);
	}

	private void doLog(String level, String signature, String corchetes, String nombreVariable, Loggable loggable) {
		switch (level) {
		case "trace":
			if (LOG.isTraceEnabled())
				LOG.trace(toLogMsg(signature, corchetes, nombreVariable, loggable));
			break;
		case "debug":
			if (LOG.isDebugEnabled())
				LOG.debug(toLogMsg(signature, corchetes, nombreVariable, loggable));
			break;
		case "info":
			if (LOG.isInfoEnabled())
				LOG.info(toLogMsg(signature, corchetes, nombreVariable, loggable));
			break;
		case "warn":
			if (LOG.isWarnEnabled())
				LOG.warn(toLogMsg(signature, corchetes, nombreVariable, loggable));
			break;
		case "error":
			if (LOG.isErrorEnabled())
				LOG.error(toLogMsg(signature, corchetes, nombreVariable, loggable));
			break;
		}
	}

	private String toLogMsg(String signature, String corchetes, String nombreVariable, Loggable loggable) {
		StringBuffer s = new StringBuffer(signature);
		if (corchetes != null)
			s.append(" " + corchetes);
		if (nombreVariable != null)
			s.append(" " + "{ l = " + nombreVariable + " }");
		if (nombreVariable != null && loggable == null)
			s.append(nombreVariable + ": " + loggable);
		else if (nombreVariable != null && loggable != null)
			s.append(loggable.toLogString("l"));
		return s.toString();
	}
}
