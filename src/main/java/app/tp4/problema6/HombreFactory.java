package app.tp4.problema6;

import java.math.BigDecimal;

public interface HombreFactory {

	void setProbabilidadCompra(BigDecimal probabilidadCompra);
	
	BigDecimal getProbabilidadCompra();

	void setFrecuenciaRelativaUnaSuscripcion(BigDecimal frecuenciaRelativaUnaSuscripcion);
	
	BigDecimal getFrecuenciaRelativaUnaSuscripcion();

	void setFrecuenciaRelativaDosSuscripciones(BigDecimal frecuenciaRelativaDosSuscripciones);
	
	BigDecimal getFrecuenciaRelativaDosSuscripciones();

	void setFrecuenciaRelativaTresSuscripciones(BigDecimal frecuenciaRelativaTresSuscripciones);
	
	BigDecimal getFrecuenciaRelativaTresSuscripciones();

	void setFrecuenciaRelativaCuatroSuscripciones(BigDecimal frecuenciaRelativaCuatroSuscripciones);
	
	BigDecimal getFrecuenciaRelativaCuatroSuscripciones();

	Hombre getObject() throws Exception;

	Class<?> getObjectType();

	boolean isSingleton();

	void setup();

}
