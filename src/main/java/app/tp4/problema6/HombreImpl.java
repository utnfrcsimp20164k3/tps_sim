package app.tp4.problema6;

public class HombreImpl implements Hombre {
	
	Double rndGenero, rndCompra, rndCantidadSuscripciones;
	Compra compra;
	Boolean compraSuscripciones;

	@Override
	public void setRndGenero(Double rndGenero) {
		this.rndGenero = rndGenero;
	}

	@Override
	public Double getRndGenero() {
		return rndGenero;
	}

	@Override
	public String mostrarGenero() {
		return "Hombre";
	}

	@Override
	public void setRndCompra(Double rndCompra) {
		this.rndCompra = rndCompra;
	}

	@Override
	public Double getRndCompra() {
		return rndCompra;
	}

	@Override
	public void setCompra(Compra compra) {
		this.compra = compra;
	}

	@Override
	public Compra getCompra() {
		return compra;
	}

	@Override
	public void setComprador(Boolean compraSuscripciones) {
		this.compraSuscripciones = compraSuscripciones;
	}

	@Override
	public Boolean isComprador() {
		return compraSuscripciones;
	}

	@Override
	public void setRndCantidadSuscripciones(Double rndCantidadSuscripciones) {
		this.rndCantidadSuscripciones = rndCantidadSuscripciones;
		
	}

	@Override
	public Double getRndCantidadSuscripciones() {
		return rndCantidadSuscripciones;
	}

	@Override
	public String toString() {
		return "Hombre";
	}

}
