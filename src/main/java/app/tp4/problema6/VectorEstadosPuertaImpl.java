package app.tp4.problema6;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import app.utils.DetalleTabla;
import app.utils.Observable;
import app.utils.Observer;
import app.utils.VectorEstados;

@Component
public class VectorEstadosPuertaImpl implements VectorEstados<Puerta> {

	@Autowired
	ApplicationContext applicationContext;
	@Autowired
	VectorEstados<Persona> vectorEstadosPersona;
	private DetalleTabla actual;

	public VectorEstadosPuertaImpl() {
		super();
	}

	@Bean
	@Scope("prototype")
	static public VectorEstados<Puerta> vectorEstadosPuerta() {
		return new VectorEstadosPuertaImpl();
	}

	@Override
	public DetalleTabla parse(Puerta puerta) {
		actual = (DetalleTabla) applicationContext.getBean("detalleTabla");
		actual.getValores().put("Rnd abre", puerta.getRndAbre());
		actual.getValores().put("Abre", puerta.isAbierta() ? "Sí" : null);
		if (puerta.getPersona() != null)
			parse(vectorEstadosPersona.parse(puerta.getPersona()));
		return actual;
	}

	// Agrega al detalle actual los valores del detalleTabla
	private void parse(DetalleTabla detalleTabla) {
		actual.getValores().putAll(detalleTabla.getValores());
	}

	@Override
	public Object[] getClaves() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void update(Observable o, Object arg) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void addObserver(Observer o) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public int countObservers() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void deleteObserver(Observer o) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void deleteObservers() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean hasChanged() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void notifyObservers() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void notifyObservers(Object arg) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void removeObserver(Observer o) {
		// TODO Auto-generated method stub
		
	}

}
