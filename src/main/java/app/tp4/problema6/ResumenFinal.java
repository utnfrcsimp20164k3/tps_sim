package app.tp4.problema6;

import java.math.BigDecimal;

public interface ResumenFinal {
	
	Long getCantidadVisitas();
	
	String getStringCantidadVisitas();

	String getStringAbiertas();

	BigDecimal getProbabilidadSeAbra();

	String getStringProbabilidadSeAbra();

	String getFormulaProbabilidadSeAbra();

	String getStringMujeres();

	BigDecimal getProporcionMujeres();

	String getStringProporcionMujeres();

	String getFormulaProporcionMujeres();

	String getStringHombres();

	BigDecimal getProporcionHombres();

	String getStringProporcionHombres();

	String getFormulaProporcionHombres();

	String getStringVentasAMujeres();

	BigDecimal getProbabilidadVentaAMujer();
	
	String getStringProbabilidadVentaAMujer();

	String getFormulaProbabilidadVentaAMujer();

	String getStringMujeresUnaSuscripcion();

	BigDecimal getFrecuenciaRelativaUnaSuscripcionMujer();

	String getStringFrecuenciaRelativaUnaSuscripcionMujer();

	String getFormulaFrecuenciaRelativaUnaSuscripcionMujer();

	String getStringMujeresDosSuscripciones();

	BigDecimal getFrecuenciaRelativaDosSuscripcionesMujer();

	String getStringFrecuenciaRelativaDosSuscripcionesMujer();

	String getFormulaFrecuenciaRelativaDosSuscripcionesMujer();

	String getStringMujeresTresSuscripciones();

	BigDecimal getFrecuenciaRelativaTresSuscripcionesMujer();

	String getStringFrecuenciaRelativaTresSuscripcionesMujer();

	String getFormulaFrecuenciaRelativaTresSuscripcionesMujer();

	String getStringVentasAHombres();

	BigDecimal getProbabilidadVentaAHombre();
	
	String getStringProbabilidadVentaAHombre();

	String getFormulaProbabilidadVentaAHombre();

	String getStringHombresUnaSuscripcion();

	BigDecimal getFrecuenciaRelativaUnaSuscripcionHombre();

	String getStringFrecuenciaRelativaUnaSuscripcionHombre();

	String getFormulaFrecuenciaRelativaUnaSuscripcionHombre();

	String getStringHombresDosSuscripciones();

	BigDecimal getFrecuenciaRelativaDosSuscripcionesHombre();

	String getStringFrecuenciaRelativaDosSuscripcionesHombre();

	String getFormulaFrecuenciaRelativaDosSuscripcionesHombre();

	String getStringHombresTresSuscripciones();

	BigDecimal getFrecuenciaRelativaTresSuscripcionesHombre();

	String getStringFrecuenciaRelativaTresSuscripcionesHombre();

	String getFormulaFrecuenciaRelativaTresSuscripcionesHombre();

	String getStringHombresCuatroSuscripciones();

	BigDecimal getFrecuenciaRelativaCuatroSuscripcionesHombre();

	String getStringFrecuenciaRelativaCuatroSuscripcionesHombre();

	String getFormulaFrecuenciaRelativaCuatroSuscripcionesHombre();

	BigDecimal getProbabilidadVentaPorVisita();

	String getStringProbabilidadVentaPorVisita();

	String getFormulaProbabilidadVentaPorVisita();

	BigDecimal getProbabilidadVentaAMujerPorVisita();

	String getStringProbabilidadVentaAMujerPorVisita();

	String getFormulaProbabilidadVentaAMujerPorVisita();

	BigDecimal getProbabilidadVentaAMujerUnaSuscripcionPorVisita();

	String getStringProbabilidadVentaAMujerUnaSuscripcionPorVisita();

	String getFormulaProbabilidadVentaAMujerUnaSuscripcionPorVisita();

	BigDecimal getProbabilidadVentaAMujerDosSuscripcionesPorVisita();

	String getStringProbabilidadVentaAMujerDosSuscripcionesPorVisita();

	String getFormulaProbabilidadVentaAMujerDosSuscripcionesPorVisita();

	BigDecimal getProbabilidadVentaAMujerTresSuscripcionesPorVisita();

	String getStringProbabilidadVentaAMujerTresSuscripcionesPorVisita();

	String getFormulaProbabilidadVentaAMujerTresSuscripcionesPorVisita();

	BigDecimal getProbabilidadVentaAHombrePorVisita();

	String getStringProbabilidadVentaAHombrePorVisita();

	String getFormulaProbabilidadVentaAHombrePorVisita();

	BigDecimal getProbabilidadVentaAHombreUnaSuscripcionPorVisita();

	String getStringProbabilidadVentaAHombreUnaSuscripcionPorVisita();

	String getFormulaProbabilidadVentaAHombreUnaSuscripcionPorVisita();

	BigDecimal getProbabilidadVentaAHombreDosSuscripcionesPorVisita();

	String getStringProbabilidadVentaAHombreDosSuscripcionesPorVisita();

	String getFormulaProbabilidadVentaAHombreDosSuscripcionesPorVisita();

	BigDecimal getProbabilidadVentaAHombreTresSuscripcionesPorVisita();

	String getStringProbabilidadVentaAHombreTresSuscripcionesPorVisita();

	String getFormulaProbabilidadVentaAHombreTresSuscripcionesPorVisita();

	BigDecimal getProbabilidadVentaAHombreCuatroSuscripcionesPorVisita();

	String getStringProbabilidadVentaAHombreCuatroSuscripcionesPorVisita();

	String getFormulaProbabilidadVentaAHombreCuatroSuscripcionesPorVisita();

	BigDecimal getProbabilidadVentaUnaSuscripcionPorVisita();

	String getStringProbabilidadVentaUnaSuscripcionPorVisita();

	String getFormulaProbabilidadVentaUnaSuscripcionPorVisita();

	BigDecimal getProbabilidadVentaDosSuscripcionesPorVisita();

	String getStringProbabilidadVentaDosSuscripcionesPorVisita();

	String getFormulaProbabilidadVentaDosSuscripcionesPorVisita();

	BigDecimal getProbabilidadVentaTresSuscripcionesPorVisita();

	String getStringProbabilidadVentaTresSuscripcionesPorVisita();

	String getFormulaProbabilidadVentaTresSuscripcionesPorVisita();

	BigDecimal getProbabilidadVentaCuatroSuscripcionesPorVisita();

	String getStringProbabilidadVentaCuatroSuscripcionesPorVisita();

	String getFormulaProbabilidadVentaCuatroSuscripcionesPorVisita();

	BigDecimal getProbabilidadVentaPorLoMenosUnaSuscripcionPorVisita();

	String getStringProbabilidadVentaPorLoMenosUnaSuscripcionPorVisita();

	String getFormulaProbabilidadVentaPorLoMenosUnaSuscripcionPorVisita();

	BigDecimal getProbabilidadVentaPorLoMenosDosSuscripcionesPorVisita();

	String getStringProbabilidadVentaPorLoMenosDosSuscripcionesPorVisita();

	String getFormulaProbabilidadVentaPorLoMenosDosSuscripcionesPorVisita();

	BigDecimal getProbabilidadVentaPorLoMenosTresSuscripcionesPorVisita();

	String getStringProbabilidadVentaPorLoMenosTresSuscripcionesPorVisita();

	String getFormulaProbabilidadVentaPorLoMenosTresSuscripcionesPorVisita();

	BigDecimal getProbabilidadVentaPorLoMenosCuatroSuscripcionesPorVisita();

	String getStringProbabilidadVentaPorLoMenosCuatroSuscripcionesPorVisita();

	String getFormulaProbabilidadVentaPorLoMenosCuatroSuscripcionesPorVisita();
	
}
