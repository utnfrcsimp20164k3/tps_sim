package app.tp4.problema6;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import app.utils.DetalleTabla;
import app.utils.Observable;
import app.utils.Observer;
import app.utils.VectorEstados;

@Component
public class VectorEstadosPersonaImpl implements VectorEstados<Persona> {

	@Autowired
	ApplicationContext applicationContext;
	@Autowired
	VectorEstados<Compra> vectorEstadosCompra;
	private DetalleTabla actual;

	public VectorEstadosPersonaImpl() {
		super();
	}

	@Bean
	@Scope("prototype")
	static public VectorEstados<Persona> vectorEstadosPersona() {
		return new VectorEstadosPersonaImpl();
	}

	@Override
	public DetalleTabla parse(Persona persona) {
		actual = (DetalleTabla) applicationContext.getBean("detalleTabla");
		actual.getValores().put("Rnd cantidad de suscripciones", persona.getRndCantidadSuscripciones());
		actual.getValores().put("Rnd genero", persona.getRndGenero());
		actual.getValores().put("Rnd venta", persona.getRndCompra());
		actual.getValores().put("Mujer / Hombre", persona.toString());
		if (persona instanceof Mujer)
			actual.getValores().put("Mujer", "Sí");
		else if (persona instanceof Hombre)
			actual.getValores().put("Hombre", "Sí");
		actual.getValores().put("Venta", persona.isComprador() ? "Sí" : null);
		if (persona.getCompra() != null)
			parse(vectorEstadosCompra.parse(persona.getCompra()));
		return actual;
	}

	// Agrega al detalle actual los valores del detalleTabla
	private void parse(DetalleTabla detalleTabla) {
		actual.getValores().putAll(detalleTabla.getValores());
	}

	@Override
	public Object[] getClaves() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void update(Observable o, Object arg) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void addObserver(Observer o) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public int countObservers() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void deleteObserver(Observer o) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void deleteObservers() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean hasChanged() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void notifyObservers() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void notifyObservers(Object arg) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void removeObserver(Observer o) {
		// TODO Auto-generated method stub
		
	}

}
