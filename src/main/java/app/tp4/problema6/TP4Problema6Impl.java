package app.tp4.problema6;

import java.math.BigDecimal;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import app.utils.DetalleTabla;
import app.utils.TablaSimple;
import app.utils.VectorEstados;

@Component
public class TP4Problema6Impl implements TP4Problema6 {

	@Autowired
	private ApplicationContext applicationContext;
	@Autowired
	private TP4Problema6Resultados tP4Problema6Resultados;
	static final Logger LOG = LoggerFactory.getLogger(TP4Problema6Impl.class);
	@Autowired
	private VisitaFactory visitaFactory;
	@Autowired
	private TablaSimple tablaSimple;
	@Autowired
	private VectorEstados<Visita> vectorEstadosVisita;
	private Long numeroVisitasASimular, primeraVisitaAMostrar, numeroVisitasAMostrar;
	private DetalleTabla anterior;

	public TP4Problema6Impl() {
		super();
	}

	@PostConstruct
	public void init() {
		tablaSimple.crearColumna("Visita");
		tablaSimple.crearColumna("Rnd abre");
		tablaSimple.crearColumna("Abre");
		tablaSimple.acumuladorUnidad("Abre", "Abiertas");
		tablaSimple.crearColumna("Rnd genero");
		tablaSimple.crearColumna("Mujer");
		tablaSimple.acumuladorUnidad("Mujer", "Mujeres");
		tablaSimple.crearColumna("Hombre");
		tablaSimple.acumuladorUnidad("Hombre", "Hombres");
		tablaSimple.crearColumna("Mujer / Hombre");
		tablaSimple.crearColumna("Rnd venta");
		tablaSimple.crearColumna("Venta");
		tablaSimple.acumuladorUnidad("Venta", "Ventas");
		tablaSimple.acumuladorUnidad("Venta", "Mujer", "Ventas a mujeres");
		tablaSimple.crearColumna("Rnd cantidad de suscripciones");
		tablaSimple.crearColumna("Cantidad de suscripciones");
		tablaSimple.crearColumna("Una suscripción");
		tablaSimple.crearColumna("Dos suscripciones");
		tablaSimple.crearColumna("Tres suscripciones");
		tablaSimple.crearColumna("Cuatro suscripciones");
		tablaSimple.acumuladorUnidad("Mujer", "Una suscripcion", "Mujeres que suscribieron una");
		tablaSimple.acumuladorUnidad("Mujer", "Dos suscripciones", "Mujeres que suscribieron dos");
		tablaSimple.acumuladorUnidad("Mujer", "Tres suscripciones", "Mujeres que suscribieron tres");
		tablaSimple.acumuladorUnidad("Hombre", "Una suscripcion", "Hombres que suscribieron una");
		tablaSimple.acumuladorUnidad("Hombre", "Dos suscripciones", "Hombres que suscribieron dos");
		tablaSimple.acumuladorUnidad("Hombre", "Tres suscripciones", "Hombres que suscribieron tres");
		tablaSimple.acumuladorUnidad("Hombre", "Cuatro suscripciones", "Hombres que suscribieron cuatro");
		tablaSimple.crearColumna("Utilidad");
		tablaSimple.acumuladorCantidad("Utilidad", "Utilidades");
		anterior = (DetalleTabla) applicationContext.getBean("detalleTabla");
		anterior.getValores().put("Abiertas", new Long(0));
		anterior.getValores().put("Mujeres", new Long(0));
		anterior.getValores().put("Hombres", new Long(0));
		anterior.getValores().put("Ventas", new Long(0));
		anterior.getValores().put("Ventas a mujeres", new Long(0));
		anterior.getValores().put("Mujeres que suscribieron una", new Long(0));
		anterior.getValores().put("Mujeres que suscribieron dos", new Long(0));
		anterior.getValores().put("Mujeres que suscribieron tres", new Long(0));
		anterior.getValores().put("Hombres que suscribieron una", new Long(0));
		anterior.getValores().put("Hombres que suscribieron dos", new Long(0));
		anterior.getValores().put("Hombres que suscribieron tres", new Long(0));
		anterior.getValores().put("Hombres que suscribieron cuatro", new Long(0));
		anterior.getValores().put("Utilidades", new BigDecimal("0"));
	}

	@Bean
	@Scope(value = "prototype")
	static public TP4Problema6 tP4Problema6() {
		return new TP4Problema6Impl();
	}

	@Override
	public void solucionar() throws Exception {
		visitaFactory.setup();
		for (long i = 0; i < numeroVisitasASimular; i++) {
			anterior = tablaSimple.acumular(anterior, vectorEstadosVisita.parse(visitaFactory.getObject()));
			anterior.getValores().put("Visita", new Long(i + 1));
			if (i + 1 >= primeraVisitaAMostrar && i + 1 < primeraVisitaAMostrar + numeroVisitasAMostrar)
				tablaSimple.getDetalles().add(anterior);
		}
		tP4Problema6Resultados.calcular(anterior);
	}

	@Override
	public TP4Problema6Resultados getResultadosSimulacion() {
		return tP4Problema6Resultados;
	}

	@Override
	public boolean hasResultadosSimulacion() {
		return tP4Problema6Resultados.hasDetalleTablaFinal();
	}

	@Override
	public void reiniciar() {
		// TODO
	}

	@Override
	public TablaSimple getTabla() {
		return tablaSimple;
	}

	@Override
	public Long getNumeroVisitasASimular() {
		return numeroVisitasASimular;
	}

	@Override
	public void setNumeroVisitasASimular(Long numeroVisitasASimular) {
		this.numeroVisitasASimular = numeroVisitasASimular;
	}

	@Override
	public Long getPrimeraVisitaAMostrar() {
		return primeraVisitaAMostrar;
	}

	@Override
	public void setPrimeraVisitaAMostrar(Long primeraVisitaAMostrar) {
		this.primeraVisitaAMostrar = primeraVisitaAMostrar;
	}

	@Override
	public Long getNumeroVisitasAMostrar() {
		return numeroVisitasAMostrar;
	}

	@Override
	public void setNumeroVisitasAMostrar(Long numeroVisitasAMostrar) {
		this.numeroVisitasAMostrar = numeroVisitasAMostrar;
	}

}
