package app.tp4.problema6;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
public class VisitaFactoryImpl implements VisitaFactory {
	
	static final Logger LOG = LoggerFactory.getLogger(VisitaFactoryImpl.class);
	@Autowired
	ApplicationContext applicationContext;
	Visita instance;
	private PuertaFactory puertaFactory;

	public VisitaFactoryImpl() {
		super();
	}
	
	@Bean
	@Scope(value = "singleton")
	static public VisitaFactory visitaFactory() {
		return new VisitaFactoryImpl();
	}

	@PostConstruct
	public void init() {
		puertaFactory = (PuertaFactory) applicationContext.getBean("puertaFactory");
	}

	@Override
	public Visita getObject() throws Exception {
		clear();
		instance = new VisitaImpl();
		instance.setPuerta(puertaFactory.getObject());
		return instance;
	}

	private void clear() {
		instance = null;
	}

	@Override
	public Class<?> getObjectType() {
		return Visita.class;
	}

	@Override
	public boolean isSingleton() {
		return true;
	}

	@Override
	public void setup() {
		puertaFactory.setup();
	}
	
}
