package app.presentacion;

import java.math.BigDecimal;
import java.math.RoundingMode;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.zkoss.bind.BindContext;

import app.utils.FrecuenciaRelativa;

@Component
public class ConversorDeFrecuenciaRelativaImpl implements ConversorDeFrecuenciaRelativa {

	public ConversorDeFrecuenciaRelativaImpl() {
		super();
	}

	@Bean
	@Scope("singleton")
	public ConversorDeFrecuenciaRelativa conversorDeFrecuenciaRelativa() {
		return new ConversorDeFrecuenciaRelativaImpl();
	}

	@Override
	public Object coerceToBean(Object str, org.zkoss.zk.ui.Component arg1, BindContext arg2) {
		BigDecimal d = null;
		try {
			if (str != null && !((String) str).isEmpty()) {
				d = new BigDecimal((String) str);
				d = d.setScale(FrecuenciaRelativa.getDefaultScale(), RoundingMode.HALF_UP);
			}
		} catch (NumberFormatException ex) {

		}
		return d;
	}

	@Override
	public Object coerceToUi(Object dec, org.zkoss.zk.ui.Component comp, BindContext ctx) {
		String s = null;
		if (dec != null) {
			BigDecimal d = ((BigDecimal) dec).setScale(FrecuenciaRelativa.getDefaultScale(), RoundingMode.HALF_UP);
			s = d.toPlainString();
		}
		return s;
	}

}
