package app.presentacion;

import java.util.ArrayList;

public class ResourcePackage {
	
	private String nombre, alias, dir;
	private ArrayList<ResourceBundle> casosUso;
	private ResourcePackage padre;
	
	public ResourcePackage(ResourcePackage padre) {
		this.padre = padre;
		casosUso = new ArrayList<>();
	}

	public int hashcode() {
		return alias.toLowerCase().hashCode();
	}

	@Override
	public boolean equals(Object o) {
		return (o instanceof ResourcePackage) &&
				((ResourcePackage) o).getAlias().toLowerCase().equals(getNombre());
	}
	
	public void add(ResourceBundle cu) {
		casosUso.add(cu);
	}

	public String getNombre() {
		return nombre;
	}

	public String getAlias() {
		return alias;
	}

	public String getDir() {
		return dir;
	}

	public ArrayList<ResourceBundle> getResourceBundles() {
		return casosUso;
	}
	
	public String getPathname() {
		return padre != null ? padre.getPathname() + dir : dir;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public void setAlias(String alias) {
		this.alias = alias;
	}

	public void setDir(String dir) {
		this.dir = dir;
	}

	public void setResourceBundles(ArrayList<ResourceBundle> casoUsos) {
		this.casosUso = casoUsos;
	}
	
	public String toString() {
		return getNombre();
	}

}
