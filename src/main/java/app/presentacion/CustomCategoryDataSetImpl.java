package app.presentacion;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.jfree.data.general.DatasetChangeListener;
import org.jfree.data.general.DatasetGroup;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import app.utils.grafica.BarraGraficoBarras;
import app.utils.grafica.GraficoBarras;

@Component
public class CustomCategoryDataSetImpl implements CustomCategoryDataSet {

	static final Logger LOG = LoggerFactory.getLogger(CustomCategoryDataSetImpl.class);
	List<GraficoBarras> graficosBarras;
	List<DatasetChangeListener> listeners;

	public CustomCategoryDataSetImpl() {
		super();
		graficosBarras = new ArrayList<>();
		listeners = new ArrayList<>();
	}

	@Bean
	@Scope(value = "prototype")
	static public CustomCategoryDataSet customCategoryDataSet() {
		return new CustomCategoryDataSetImpl();
	}

	@Override
	public double getXMinimo() {
		return graficosBarras.get(0).getBarras()[0].getNumero();
	}

	@Override
	public double getXMaximo() {
		return graficosBarras.get(0).getBarras()[graficosBarras.get(0).getBarras().length - 1].getNumero();
	}

	@Override
	public int getColumnIndex(Comparable arg0) {
		int index = 0;
		for (int i = 0; i < graficosBarras.get(0).getNumeros().length; i++) {
			if (new Double(graficosBarras.get(0).getNumeros()[i]).equals(arg0)) {
				index = i;
				break;
			}
		}
		return index;
	}

	@Override
	public Comparable getColumnKey(int arg0) {
		return String.format("%.0f", graficosBarras.get(0).getNumeros()[arg0]);
	}

	@Override
	public List<String> getColumnKeys() {
		List<String> numeros = new ArrayList<>();
		for (Double d : graficosBarras.get(0).getNumeros())
			numeros.add(String.format("%.0f", d));
		return numeros;
	}

	@Override
	public int getRowIndex(Comparable arg0) {
		int index = 0;
		if (arg0.equals("Frecuencias observadas"))
			index = 0;
		else if (arg0.equals("Frecuencias esperadas"))
			index = 1;
		return 0;
	}

	@Override
	public Comparable getRowKey(int arg0) {
		return graficosBarras.get(arg0).getKey();
	}

	@Override
	public List getRowKeys() {
		List lista = new ArrayList();
		lista.add("Frecuencias observadas");
		lista.add("Frecuencias esperadas");
		return lista;
	}

	@Override
	public Number getValue(Comparable arg0, Comparable arg1) {
		Number value = null;
		if (arg0.equals("Frecuencia observadas")) {
			for (BarraGraficoBarras b : graficosBarras.get(0).getBarras())
				if (new Double(b.getNumero()).equals(arg1))
					value = b.getFrecuencia();
		} else if (arg0.equals("Frecuencia esperadas"))
			for (BarraGraficoBarras b : graficosBarras.get(1).getBarras())
				if (new Double(b.getNumero()).equals(arg1))
					value = b.getFrecuencia();
		return value;
	}

	@Override
	public int getColumnCount() {
		return graficosBarras.get(0).getCantidadNumeros();
	}

	@Override
	public int getRowCount() {
		return graficosBarras.size();
	}

	@Override
	public Number getValue(int arg0, int arg1) {
		return graficosBarras.get(arg0).getBarras()[arg1].getFrecuencia();
	}

	@Override
	public void addChangeListener(DatasetChangeListener arg0) {
		listeners.add(arg0);
	}

	@Override
	public DatasetGroup getGroup() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void removeChangeListener(DatasetChangeListener arg0) {
		listeners.remove(arg0);
	}

	@Override
	public void setGroup(DatasetGroup arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void agregarGraficoBarras(GraficoBarras graficoBarras) {
		this.graficosBarras.add(graficoBarras);
	}

}
