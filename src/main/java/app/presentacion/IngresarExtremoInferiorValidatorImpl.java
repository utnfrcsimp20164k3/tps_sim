package app.presentacion;

import java.util.HashSet;
import java.util.Set;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.zkoss.bind.ValidationContext;
import org.zkoss.bind.validator.AbstractValidator;
import org.zkoss.zel.ELException;

@Component
public class IngresarExtremoInferiorValidatorImpl extends AbstractValidator
		implements IngresarExtremoInferiorValidator, Observable {

	private Set<Observer> observers;
	private boolean changed;
	private final boolean obligatoria = true;
	private boolean incorrecta;
	private boolean nula;

	IngresarExtremoInferiorValidatorImpl() {
		super();
		observers = new HashSet<>();
	}

	@Bean
	@Scope("prototype")
	static public IngresarExtremoInferiorValidator ingresarExtremoInferiorValidator() {
		return new IngresarExtremoInferiorValidatorImpl();
	}

	@Override
	public void validate(ValidationContext ctx) {
		try {
			Double extremoInferior = (Double) ctx.getProperties("extremoInferior")[0].getValue();
			if (extremoInferior == null)
				nula = true;
		} catch (NumberFormatException ex) {
			addInvalidMessage(ctx, "extremoInferior", "Debe ingresar un número.");
			incorrecta = true;
		} catch (ELException ex) {
			addInvalidMessage(ctx, "extremoInferior", "Debe ingresar un número.");
			incorrecta = true;
		} finally {
			changed = true;
			notifyObservers();
		}
	}

	@Override
	public boolean isObligatoria() {
		return obligatoria;
	}

	@Override
	public boolean isIncorrecta() {
		return incorrecta;
	}

	@Override
	public boolean isNula() {
		return nula;
	}

	@Override
	public void addObserver(Observer o) {
		observers.add(o);
	}

	@Override
	public int countObservers() {
		return observers.size();
	}

	@Override
	public void deleteObserver(Observer o) {
		observers.remove(o);
	}

	@Override
	public void deleteObservers() {
		observers.clear();
	}

	@Override
	public boolean hasChanged() {
		return changed;
	}

	@Override
	public void notifyObservers() {
		if (hasChanged())
			for (Observer obs : observers)
				obs.update(this, null);
		clearChanged();
	}

	@Override
	public void notifyObservers(Object arg) {
		if (hasChanged())
			for (Observer obs : observers)
				obs.update(this, arg);
		clearChanged();
	}

	protected void clearChanged() {
		changed = false;
		incorrecta = false;
		nula = false;
	}

}
