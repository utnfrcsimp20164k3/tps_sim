package app.presentacion;

import org.jfree.data.category.CategoryDataset;

import app.utils.grafica.GraficoBarras;

public interface CustomCategoryDataSet extends CategoryDataset {
	
	void agregarGraficoBarras(GraficoBarras graficoBarras);
	
	double getXMinimo();
	
	double getXMaximo();

}
