package app.presentacion;

import org.zkoss.bind.Converter;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.GlobalCommand;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.SmartNotifyChange;
import org.zkoss.zk.ui.select.annotation.VariableResolver;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.ListModelList;

import app.utils.SerieVariableContinua;
import app.utils.test.TestChiCuadradoVariableContinua;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@VariableResolver(org.zkoss.zkplus.spring.DelegatingVariableResolver.class)
public class TestChiCuadradoVariableContinuaVM {

	static final Logger LOG = LoggerFactory.getLogger(TestChiCuadradoVariableContinuaVM.class);
	String etiqueta;
	double chiCuadrado;
	@WireVariable
	TestChiCuadradoVariableContinua testChiCuadradoVariableContinua;
	@WireVariable
	TestChiCuadradoTablaM testChiCuadradoVariableContinuaTablaM;
	@WireVariable
	ConversorDeInt conversorDeInt;
	@WireVariable
	ConversorDeDouble conversorDeDouble;

	@Init
	public void initSetup(@BindingParam("serie") SerieVariableContinua serie) {
		etiqueta = "\u03C7\u2072: ";
		chiCuadrado = testChiCuadradoVariableContinua.calcular(serie);
		testChiCuadradoVariableContinuaTablaM.setTabla(testChiCuadradoVariableContinua.getTabla());
	}

	@GlobalCommand
	@SmartNotifyChange("testChiCuadradoTablaM")
	public void limpiarTablaFrecuencias() {
		testChiCuadradoVariableContinuaTablaM.clear();
		
	}

	public String getEtiqueta() {
		return etiqueta;
	}

	public double getChiCuadrado() {
		return chiCuadrado;
	}

	public ListModelList<TestChiCuadradoDetalleM> getDetalles() {
		return testChiCuadradoVariableContinuaTablaM.getDetalles();
	}

	public Converter<?, ?, ?> getConversorDeInt() {
		return conversorDeInt;
	}

	public Converter<?, ?, ?> getConversorDeDouble() {
		return conversorDeDouble;
	}

	public TestChiCuadradoTablaM getTestChiCuadradoTablaM() {
		return testChiCuadradoVariableContinuaTablaM;
	}

}
