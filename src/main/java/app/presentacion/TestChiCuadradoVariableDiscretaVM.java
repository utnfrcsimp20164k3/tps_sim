package app.presentacion;

import org.zkoss.bind.Converter;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Init;
import org.zkoss.zk.ui.select.annotation.VariableResolver;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.ListModelList;

import app.utils.SerieVariableDiscreta;
import app.utils.test.TestChiCuadradoVariableDiscreta;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@VariableResolver(org.zkoss.zkplus.spring.DelegatingVariableResolver.class)
public class TestChiCuadradoVariableDiscretaVM {

	static final Logger LOG = LoggerFactory.getLogger(TestChiCuadradoVariableDiscretaVM.class);
	String etiqueta;
	double chiCuadrado;
	@WireVariable
	TestChiCuadradoVariableDiscreta testChiCuadradoVariableDiscreta;
	@WireVariable
	TestChiCuadradoTablaM testChiCuadradoVariableDiscretaTablaM;
	@WireVariable
	ConversorDeInt conversorDeInt;
	@WireVariable
	ConversorDeDouble conversorDeDouble;

	@Init
	public void initSetup(@BindingParam("serie") SerieVariableDiscreta serie) {
		etiqueta = "\u03C7\u2072: ";
		chiCuadrado = testChiCuadradoVariableDiscreta.calcular(serie);
		testChiCuadradoVariableDiscretaTablaM.setTabla(testChiCuadradoVariableDiscreta.getTabla());
	}

	public String getEtiqueta() {
		return etiqueta;
	}

	public double getChiCuadrado() {
		return chiCuadrado;
	}

	public ListModelList<TestChiCuadradoDetalleM> getDetalles() {
		return testChiCuadradoVariableDiscretaTablaM.getDetalles();
	}

	public Converter<?, ?, ?> getConversorDeInt() {
		return conversorDeInt;
	}

	public Converter<?, ?, ?> getConversorDeDouble() {
		return conversorDeDouble;
	}

	public TestChiCuadradoTablaM getTestChiCuadradoTablaM() {
		return testChiCuadradoVariableDiscretaTablaM;
	}

}
