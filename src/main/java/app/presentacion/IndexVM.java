package app.presentacion;

import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.GlobalCommand;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.SmartNotifyChange;
import org.zkoss.zk.ui.select.annotation.VariableResolver;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@VariableResolver(org.zkoss.zkplus.spring.DelegatingVariableResolver.class)
public class IndexVM {

	private static final String BANNER_SRC = "./pages/main/banner.zul";
	private static final String SIDEBAR_SRC = "./pages/main/sidebar.zul";
	private static final String FOOTNOTE_SRC = "./pages/main/footnote.zul";
	static final Logger LOG = LoggerFactory.getLogger(IndexVM.class);
	private String contentSrc;

	@Init
	public void init() { }

	@GlobalCommand
	@SmartNotifyChange("contentSrc")
	public void showContent(@BindingParam("page") SidebarTreeItem page) {
		contentSrc = page.getUri();
	}

	public String getContentSrc() {
		return contentSrc;
	}

	public String getBannerSrc() {
		return BANNER_SRC;
	}

	public String getSidebarSrc() {
		return SIDEBAR_SRC;
	}

	public String getFootnoteSrc() {
		return FOOTNOTE_SRC;
	}

}
