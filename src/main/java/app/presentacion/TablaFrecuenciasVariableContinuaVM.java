package app.presentacion;

import org.zkoss.bind.Converter;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.GlobalCommand;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.SmartNotifyChange;
import org.zkoss.zk.ui.select.annotation.VariableResolver;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.ListModelList;

import app.utils.grafica.Histograma;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@VariableResolver(org.zkoss.zkplus.spring.DelegatingVariableResolver.class)
public class TablaFrecuenciasVariableContinuaVM {

	static final Logger LOG = LoggerFactory.getLogger(TablaFrecuenciasVariableContinuaVM.class);
	@WireVariable
	TablaFrecuenciasM tablaFrecuenciasVariableContinuaM;
	@WireVariable
	ConversorDeInt conversorDeInt;
	@WireVariable
	ConversorDeDouble conversorDeDouble;


	@Init
	public void initSetup(@BindingParam("histograma") Histograma histograma) {
		tablaFrecuenciasVariableContinuaM.setTabla(histograma.getTabla());
	}
	
	@GlobalCommand
	@SmartNotifyChange("tablaFrecuenciasM")
	public void limpiarTablaFrecuencias() {
		tablaFrecuenciasVariableContinuaM.clear();
		
	}

	public ListModelList<TablaFrecuenciasDetalleM> getDetalles() {
		return tablaFrecuenciasVariableContinuaM.getDetalles();
	}

	public Converter<?, ?, ?> getConversorDeInt() {
		return conversorDeInt;
	}

	public Converter<?, ?, ?> getConversorDeDouble() {
		return conversorDeDouble;
	}
	
	public TablaFrecuenciasM getTablaFrecuenciasM() {
		return tablaFrecuenciasVariableContinuaM;
	}

}
