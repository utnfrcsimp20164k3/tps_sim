package app.presentacion;

import java.awt.image.BufferedImage;
import java.io.IOException;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.encoders.EncoderUtil;
import org.jfree.chart.encoders.ImageFormat;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.StandardXYBarPainter;
import org.jfree.chart.renderer.xy.XYBarRenderer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.SmartNotifyChange;
import org.zkoss.image.AImage;
import org.zkoss.zk.ui.select.annotation.VariableResolver;
import org.zkoss.zk.ui.select.annotation.WireVariable;

import app.utils.SerieVariableContinua;

@VariableResolver(org.zkoss.zkplus.spring.DelegatingVariableResolver.class)
public class HistogramaVM {

	static final Logger LOG = LoggerFactory.getLogger(HistogramaVM.class);
	@WireVariable
	CustomHistogramDataSet customHistogramDataSet;
	AImage imagen;
	String serieAMostrar;

	@Init
	public void initSetup(@BindingParam("serie") SerieVariableContinua serieVariableContinua) {
		customHistogramDataSet.agregarHistograma(serieVariableContinua.getHistogramaObservadas());
		customHistogramDataSet.agregarHistograma(serieVariableContinua.getHistogramaEsperadas());
		setSerieAMostrar("Frecuencias observadas");
		imagen = generarImagen(generarGrafica(customHistogramDataSet));
	}

	@Command
	@SmartNotifyChange("imagen")
	public void cambiarImagen() {
		imagen = generarImagen(generarGrafica(customHistogramDataSet));
	}

	private JFreeChart generarGrafica(CustomHistogramDataSet dataset) {
		JFreeChart grafica = ChartFactory.createHistogram("", "Intervalo", "Frecuencia", dataset,
				PlotOrientation.VERTICAL, true, true, false);
		XYPlot xYPlot = (XYPlot) grafica.getPlot();
		xYPlot.setDomainPannable(true);
		xYPlot.setRangePannable(true);
		xYPlot.setForegroundAlpha(0.85F);
		NumberAxis numberAxis = (NumberAxis) xYPlot.getRangeAxis();
		numberAxis.setStandardTickUnits(NumberAxis.createIntegerTickUnits());
		XYBarRenderer xYBarRenderer = (XYBarRenderer) xYPlot.getRenderer();
		xYBarRenderer.setDrawBarOutline(true);
		xYBarRenderer.setBarPainter(new StandardXYBarPainter());
		xYBarRenderer.setShadowVisible(false);
		if (serieAMostrar.equals("Frecuencias observadas")) {
			xYBarRenderer.setSeriesVisible(0, true);
			xYBarRenderer.setSeriesVisible(1, false);
		} else if (serieAMostrar.equals("Frecuencias esperadas")) {
			xYBarRenderer.setSeriesVisible(0, false);
			xYBarRenderer.setSeriesVisible(1, true);
		} else if (serieAMostrar.equals("Ambas frecuencias")) {
			xYBarRenderer.setSeriesVisible(0, true);
			xYBarRenderer.setSeriesVisible(1, true);
		}
		xYPlot.getRangeAxis().setUpperMargin(xYPlot.getRangeAxis().getUpperMargin() + 0.1);
		return grafica;
	}

	private AImage generarImagen(JFreeChart grafica) {
		BufferedImage bi = grafica.createBufferedImage(500, 300, BufferedImage.TRANSLUCENT, null);
		byte[] bytes = null;
		AImage image = null;
		try {
			bytes = EncoderUtil.encode(bi, ImageFormat.PNG, true);
			image = new AImage(null, bytes);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return image;
	}

	public AImage getImagen() {
		return imagen;
	}

	public void setImagen(AImage imagen) {
		this.imagen = imagen;
	}

	public String getSerieAMostrar() {
		return serieAMostrar;
	}

	public void setSerieAMostrar(String serieAMostrar) {
		this.serieAMostrar = serieAMostrar;
	}

}
