package app.presentacion;

import app.utils.DetalleTabla;

public interface TablaFrecuenciasDetalleM {
	
	Object getValorA();

	Object getValorB();

	Object getValorC();

	Object getValorD();

	void setDetalleTabla(DetalleTabla d);

}
