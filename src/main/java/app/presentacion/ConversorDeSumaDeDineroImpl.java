package app.presentacion;

import java.math.BigDecimal;
import java.math.RoundingMode;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.zkoss.bind.BindContext;

@Component
public class ConversorDeSumaDeDineroImpl implements ConversorDeSumaDeDinero {

	public ConversorDeSumaDeDineroImpl() {
		super();
	}

	@Bean
	@Scope("singleton")
	public ConversorDeSumaDeDinero conversorDeSumaDeDinero() {
		return new ConversorDeSumaDeDineroImpl();
	}

	@Override
	public Object coerceToBean(Object str, org.zkoss.zk.ui.Component arg1, BindContext arg2) {
		BigDecimal d = null;
		try {
			if (str != null && !((String) str).isEmpty()) {
				d = new BigDecimal((String) str);
				d = d.setScale(2, RoundingMode.HALF_UP);
			}
		} catch (NumberFormatException ex) {

		}
		return d;
	}

	@Override
	public Object coerceToUi(Object dec, org.zkoss.zk.ui.Component comp, BindContext ctx) {
		String s = null;
		if (dec instanceof BigDecimal)
			s = ((BigDecimal) dec).toPlainString();
		if (dec instanceof Double) {
			BigDecimal d;
			d = new BigDecimal((Double) dec);
			d = d.setScale(2, RoundingMode.HALF_UP);
			s = d.toPlainString();
		}
		return s;
	}

}
