package app.presentacion;

import org.jfree.data.xy.IntervalXYDataset;

import app.utils.grafica.Histograma;

public interface CustomHistogramDataSet extends IntervalXYDataset {
	
	void agregarHistograma(Histograma histograma);
	
	double getXMinimo();
	
	double getXMaximo();

}
