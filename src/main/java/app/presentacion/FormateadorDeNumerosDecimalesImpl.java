package app.presentacion;

import java.text.DecimalFormat;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.zkoss.bind.BindContext;

@Component
public class FormateadorDeNumerosDecimalesImpl implements FormateadorDeNumerosDecimales {

	String pattern;

	FormateadorDeNumerosDecimalesImpl() {
	}
	
	@Bean
	@Scope("prototype")
	public FormateadorDeNumerosDecimales formateadorDeNumerosDecimales() {
		return new FormateadorDeNumerosDecimalesImpl();
	}

	@Override
	public Object coerceToBean(Object arg0, org.zkoss.zk.ui.Component arg1, BindContext arg2) {
		return arg0;
	}

	@Override
	public Object coerceToUi(Object val, org.zkoss.zk.ui.Component comp, BindContext ctx) {
		Object o = null;
		if (val != null && val instanceof Double) {
			DecimalFormat dF = new DecimalFormat(pattern);
			o = dF.format(val);
		} else
			o = val;
		return o;
	}

	@Override
	public void setPattern(String pattern) {
		this.pattern = pattern;
	}

}
