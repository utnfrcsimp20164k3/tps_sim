package app.presentacion;

import java.util.ArrayList;
import java.util.HashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.PostConstruct;

import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Service;

@Service("resourceIndex")
@Scope(proxyMode = ScopedProxyMode.TARGET_CLASS)
public class ResourceIndex {

	static final Logger LOG = LoggerFactory.getLogger(ResourceIndex.class);

	private HashMap<String, ResourceBundle> cU;
	private HashMap<String, ResourcePackage> paqMap;
	private ArrayList<ResourcePackage> paqList;

	ResourceIndex() { }

	@PostConstruct
	public void init() {

		cU = new HashMap<String, ResourceBundle>(100, 0.5f);
		paqMap = new HashMap<String, ResourcePackage>(10, 0.5f);
		paqList = new ArrayList<ResourcePackage>();
		ResourceBundle c;
		ResourcePackage paq;

		// Inicio de la definición del paquete
		// "Trabajo Práctico 1"
		paq = new ResourcePackage(null);
		paq.setAlias("gagafg");
		paq.setDir("./pages/content/tp1/");
		paq.setNombre("Trabajo Práctico 1");
		paqMap.put(paq.getAlias().toLowerCase(), paq);
		paqList.add(paq);

		c = new ResourceBundle(paq);
		c.setAlias("kjebvm");
		c.setDir("puntoa/");
		c.setNombre("Punto a)");
		c.addZul("tp1-puntoa.zul");
		c.addZul("tp1-puntoa-enunciado.zul");
		c.addZul("tp1-puntoa-solucion.zul");
		c.addZul("tp1-puntoa-input-lineal.zul");
		c.addZul("tp1-puntoa-input-multiplicativo.zul");
		c.addZul("tp1-puntoa-resultados.zul");
		c.setInSidebar(true);

		paq.add(c);
		cU.put(c.getAlias().toLowerCase(), c);

		c = new ResourceBundle(paq);
		c.setAlias("lypmvw");
		c.setDir("puntob/");
		c.setNombre("Punto b)");
		c.addZul("tp1-puntob.zul");
		c.addZul("tp1-puntob-enunciado.zul");
		c.addZul("tp1-puntob-solucion.zul");
		c.addZul("tp1-puntob-input.zul");
		c.addZul("tp1-puntob-resultados.zul");
		c.addZul("tp1-puntob-grafica.zul");
		c.addZul("tp1-puntob-chicuadrado.zul");
		c.addZul("tp1-puntob-chicuadrado-tabla.zul");
		c.addZul("tp1-puntob-tablafrecuencias.zul");
		c.setInSidebar(true);

		paq.add(c);
		cU.put(c.getAlias().toLowerCase(), c);

		c = new ResourceBundle(paq);
		c.setAlias("hhutad");
		c.setDir("puntoc/");
		c.setNombre("Punto c)");
		c.addZul("tp1-puntoc.zul");
		c.addZul("tp1-puntoc-enunciado.zul");
		c.addZul("tp1-puntoc-solucion.zul");
		c.addZul("tp1-puntoc-input.zul");
		c.addZul("tp1-puntoc-resultados.zul");
		c.addZul("tp1-puntoc-grafica.zul");
		c.addZul("tp1-puntoc-chicuadrado.zul");
		c.addZul("tp1-puntoc-chicuadrado-tabla.zul");
		c.addZul("tp1-puntoc-tablafrecuencias.zul");
		c.setInSidebar(true);

		paq.add(c);
		cU.put(c.getAlias().toLowerCase(), c);
		// Fin de la definición del paquete "Trabajo Práctico 1"

		// Inicio de la definición del paquete
		// "Trabajo Práctico 3"
		paq = new ResourcePackage(null);
		paq.setAlias("vbklru");
		paq.setDir("./pages/content/tp3/");
		paq.setNombre("Trabajo Práctico 3");
		paqMap.put(paq.getAlias().toLowerCase(), paq);
		paqList.add(paq);

		c = new ResourceBundle(paq);
		c.setAlias("gthjue");
		c.setDir("puntoayb/");
		c.setNombre("Puntos a) y b)");
		c.addZul("tp3-puntoayb.zul");
		c.addZul("tp3-puntoayb-enunciado.zul");
		c.addZul("tp3-puntoayb-solucion.zul");
		c.addZul("tp3-puntoayb-input-uniforme.zul");
		c.addZul("tp3-puntoayb-input-exponencial.zul");
		c.addZul("tp3-puntoayb-input-poisson.zul");
		c.addZul("tp3-puntoayb-input-normal-convolucion.zul");
		c.addZul("tp3-puntoayb-input-normal-boxmuller.zul");
		c.addZul("tp3-puntoayb-resultados.zul");
		c.addZul("tp3-puntoayb-histograma.zul");
		c.addZul("tp3-puntoayb-graficobarras.zul");
		c.addZul("tp3-puntoayb-chicuadrado-variablecontinua.zul");
		c.addZul("tp3-puntoayb-chicuadrado-variablediscreta.zul");
		c.addZul("tp3-puntoayb-chicuadrado-tabla.zul");
		c.addZul("tp3-puntoayb-tablafrecuencias-variablecontinua.zul");
		c.addZul("tp3-puntoayb-tablafrecuencias-variablediscreta.zul");
		c.setInSidebar(true);

		paq.add(c);
		cU.put(c.getAlias().toLowerCase(), c);

		// Inicio de la definición del paquete
		// "Trabajo Práctico 4"
		paq = new ResourcePackage(null);
		paq.setAlias("ghijsa");
		paq.setDir("./pages/content/tp4/");
		paq.setNombre("Trabajo Práctico 4");
		paqMap.put(paq.getAlias().toLowerCase(), paq);
		paqList.add(paq);

		c = new ResourceBundle(paq);
		c.setAlias("lpoigt");
		c.setDir("problema6/");
		c.setNombre("Problema 6");
		c.addZul("tp4-problema6.zul");
		c.addZul("tp4-problema6-enunciado.zul");
		c.addZul("tp4-problema6-solucion.zul");
		c.addZul("tp4-problema6-input.zul");
		c.addZul("tp4-problema6-resultados.zul");
		c.addZul("tp4-problema6-resultadossimulacion.zul");
		c.addZul("tp4-problema6-solucionanalitica.zul");
		c.addZul("tp4-problema6-tablavectoresestado.zul");
		c.addZul("tp4-problema6-tablavectoresestadocompleta.zul");
		c.setInSidebar(true);

		paq.add(c);
		cU.put(c.getAlias().toLowerCase(), c);

		// Inicio de la definición del paquete
		// "Trabajo Práctico 5"
		paq = new ResourcePackage(null);
		paq.setAlias("ghijsa");
		paq.setDir("./pages/content/tp5/");
		paq.setNombre("Trabajo Práctico 5");
		paqMap.put(paq.getAlias().toLowerCase(), paq);
		paqList.add(paq);

		c = new ResourceBundle(paq);
		c.setAlias("ilelkh");
		c.setDir("ejerciciod/");
		c.setNombre("Ejercicio d");
		c.addZul("tp5-ejerciciod.zul");
		c.addZul("tp5-ejerciciod-enunciado.zul");
		c.addZul("tp5-ejerciciod-solucion.zul");
		c.addZul("tp5-ejerciciod-input.zul");
		c.addZul("tp5-ejerciciod-resultados.zul");
		c.addZul("tp5-ejerciciod-resultadossimulacion.zul");
		c.addZul("tp5-ejerciciod-tablavectoresestado.zul");
		c.setInSidebar(true);

		paq.add(c);
		cU.put(c.getAlias().toLowerCase(), c);

		// Inicio de la definición del paquete
		// "Trabajo Práctico 6"
		paq = new ResourcePackage(null);
		paq.setAlias("bmkyjt");
		paq.setDir("./pages/content/tp6/");
		paq.setNombre("Trabajo Práctico 6");
		paqMap.put(paq.getAlias().toLowerCase(), paq);
		paqList.add(paq);

		c = new ResourceBundle(paq);
		c.setAlias("pqfjyh");
		c.setDir("ejerciciod/");
		c.setNombre("Ejercicio d");
		c.addZul("tp6-ejerciciod.zul");
		c.addZul("tp6-ejerciciod-enunciado.zul");
		c.addZul("tp6-ejerciciod-solucion.zul");
		c.addZul("tp6-ejerciciod-input.zul");
		c.addZul("tp6-ejerciciod-resultados.zul");
		c.addZul("tp6-ejerciciod-resultadossimulacion.zul");
		c.addZul("tp6-ejerciciod-tablavectoresestado.zul");
		c.addZul("tp6-ejerciciod-tablaintegracionnumerica.zul");
		c.setInSidebar(true);

		paq.add(c);
		cU.put(c.getAlias().toLowerCase(), c);
	}

	public ResourcePackage getResourcePackage(String alias) {
		ResourcePackage paquete = null;
		paquete = paqMap.get(alias.toLowerCase());
		return paquete;
	}

	public ResourceBundle getResourceBundle(String alias) {
		ResourceBundle casoUso = null;
		casoUso = cU.get(alias.toLowerCase());
		return casoUso;
	}

	public ArrayList<ResourcePackage> getResourcePackages() {
		return paqList;
	}

}
