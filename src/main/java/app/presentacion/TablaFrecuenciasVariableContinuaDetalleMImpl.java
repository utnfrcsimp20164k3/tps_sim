package app.presentacion;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import app.utils.DetalleTabla;

@Component
public class TablaFrecuenciasVariableContinuaDetalleMImpl implements TablaFrecuenciasDetalleM {
	
	static final Logger LOG = LoggerFactory.getLogger(TablaFrecuenciasVariableContinuaDetalleMImpl.class);
	DetalleTabla detalleTabla;
	
	public TablaFrecuenciasVariableContinuaDetalleMImpl() {
		super();
	}

	@Bean
	@Scope(value = "prototype")
	static public TablaFrecuenciasVariableContinuaDetalleMImpl  tablaFrecuenciasVariableContinuaDetalleM () {
		return new TablaFrecuenciasVariableContinuaDetalleMImpl ();
	}

	public void setDetalleTabla(DetalleTabla detalleTabla) {
		this.detalleTabla = detalleTabla;
	}
	
	@Override
	public Object getValorA() {
		return detalleTabla.getValores().get("k");
	}

	@Override
	public Object getValorB() {
		return detalleTabla.getValores().get("Intervalo");
	}

	@Override
	public Object getValorC() {
		return detalleTabla.getValores().get("fo");
	}

	@Override
	public Object getValorD() {
		return detalleTabla.getValores().get("fe");
	}

}
