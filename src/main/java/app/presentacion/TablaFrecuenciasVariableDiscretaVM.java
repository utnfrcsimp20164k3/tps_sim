package app.presentacion;

import org.zkoss.bind.Converter;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.GlobalCommand;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.SmartNotifyChange;
import org.zkoss.zk.ui.select.annotation.VariableResolver;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.ListModelList;

import app.utils.grafica.GraficoBarras;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@VariableResolver(org.zkoss.zkplus.spring.DelegatingVariableResolver.class)
public class TablaFrecuenciasVariableDiscretaVM {

	static final Logger LOG = LoggerFactory.getLogger(TablaFrecuenciasVariableDiscretaVM.class);
	@WireVariable
	TablaFrecuenciasM tablaFrecuenciasVariableDiscretaM;
	@WireVariable
	ConversorDeInt conversorDeInt;
	@WireVariable
	ConversorDeDouble conversorDeDouble;


	@Init
	public void initSetup(@BindingParam("graficoBarras") GraficoBarras graficoBarras) {
		tablaFrecuenciasVariableDiscretaM.setTabla(graficoBarras.getTabla());
	}
	
	@GlobalCommand
	@SmartNotifyChange("tablaFrecuenciasM")
	public void limpiarTablaFrecuencias() {
		tablaFrecuenciasVariableDiscretaM.clear();
		
	}

	public ListModelList<TablaFrecuenciasDetalleM> getDetalles() {
		return tablaFrecuenciasVariableDiscretaM.getDetalles();
	}

	public Converter<?, ?, ?> getConversorDeInt() {
		return conversorDeInt;
	}

	public Converter<?, ?, ?> getConversorDeDouble() {
		return conversorDeDouble;
	}
	
	public TablaFrecuenciasM getTablaFrecuenciasM() {
		return tablaFrecuenciasVariableDiscretaM;
	}

}
