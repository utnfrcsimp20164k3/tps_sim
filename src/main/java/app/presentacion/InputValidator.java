package app.presentacion;

import org.zkoss.bind.ValidationContext;
import org.zkoss.bind.Validator;

public interface InputValidator extends Validator, Observable {

	void validate(ValidationContext ctx);

	boolean isObligatoria();

	boolean isIncorrecta();

	boolean isNula();

	void addObserver(Observer o);
	
}
