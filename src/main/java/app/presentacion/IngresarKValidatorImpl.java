package app.presentacion;

import java.util.HashSet;
import java.util.Set;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.zkoss.bind.ValidationContext;
import org.zkoss.bind.validator.AbstractValidator;
import org.zkoss.zel.ELException;

@Component
public class IngresarKValidatorImpl extends AbstractValidator
		implements IngresarKValidator, Observable {

	private Set<Observer> observers;
	private boolean changed;
	private final boolean obligatoria = false;
	private boolean incorrecta;
	private boolean nula;

	IngresarKValidatorImpl() {
		super();
		observers = new HashSet<>();
	}

	@Bean
	@Scope("prototype")
	static public IngresarKValidator ingresarKValidator() {
		return new IngresarKValidatorImpl();
	}

	@Override
	public void validate(ValidationContext ctx) {
		Integer cantidad = (Integer) ctx.getProperties("k")[0].getValue();
		try {
			if (cantidad == null || cantidad == 0) {
				addInvalidMessage(ctx, "ingresarK", "Debe ingresar esta constante.");
				nula = true;
			} else if (cantidad < 1) {
				addInvalidMessage(ctx, "ingresarK", "Debe ingresar un número entero positivo.");
				incorrecta = true;
			}
		} catch (NumberFormatException ex) {
			addInvalidMessage(ctx, "ingresarK", "Debe ingresar un número entero positivo.");
			incorrecta = true;
		} catch (ELException ex) {
			addInvalidMessage(ctx, "ingresarK", "Debe ingresar un número entero positivo.");
			incorrecta = true;
		} finally {
			changed = true;
			notifyObservers();
		}
	}

	@Override
	public boolean isObligatoria() {
		return obligatoria;
	}

	@Override
	public boolean isIncorrecta() {
		return incorrecta;
	}

	@Override
	public boolean isNula() {
		return nula;
	}

	@Override
	public void addObserver(Observer o) {
		observers.add(o);
	}

	@Override
	public int countObservers() {
		return observers.size();
	}

	@Override
	public void deleteObserver(Observer o) {
		observers.remove(o);
	}

	@Override
	public void deleteObservers() {
		observers.clear();
	}

	@Override
	public boolean hasChanged() {
		return changed;
	}

	@Override
	public void notifyObservers() {
		if (hasChanged())
			for (Observer obs : observers)
				obs.update(this, null);
		clearChanged();
	}

	@Override
	public void notifyObservers(Object arg) {
		if (hasChanged())
			for (Observer obs : observers)
				obs.update(this, arg);
		clearChanged();
	}

	protected void clearChanged() {
		changed = false;
		incorrecta = false;
		nula = false;
	}

}
