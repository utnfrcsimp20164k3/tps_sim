package app.presentacion;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.zkoss.zul.ListModelList;

import app.utils.DetalleTabla;
import app.utils.TablaSimple;

@Component
public class TestChiCuadradoVariableDiscretaTablaMImpl implements TestChiCuadradoTablaM {

	static final Logger LOG = LoggerFactory.getLogger(TestChiCuadradoVariableDiscretaTablaMImpl.class);
	TablaSimple tablaSimple;
	@Autowired
	ApplicationContext context;
	
	public TestChiCuadradoVariableDiscretaTablaMImpl() {
		super();
	}

	@Bean
	@Scope(value = "prototype")
	static public TestChiCuadradoTablaM testChiCuadradoVariableDiscretaTablaM() {
		return new TestChiCuadradoVariableDiscretaTablaMImpl();
	}

	@Override
	public void setTabla(TablaSimple tablaSimple) {
		this.tablaSimple = tablaSimple;
	}

	@Override
	public Object getEncabezadoA() {
		return tablaSimple.getClaves().get(0);
	}

	@Override
	public Object getEncabezadoB() {
		return tablaSimple.getClaves().get(1);
	}

	@Override
	public Object getEncabezadoC() {
		return tablaSimple.getClaves().get(2);
	}

	@Override
	public Object getEncabezadoD() {
		return tablaSimple.getClaves().get(3);
	}

	@Override
	public Object getEncabezadoE() {
		return tablaSimple.getClaves().get(4);
	}

	@Override
	public Object getEncabezadoF() {
		return tablaSimple.getClaves().get(5);
	}

	@Override
	public Object getEncabezadoG() {
		return tablaSimple.getClaves().get(6);
	}

	@Override
	public Object getEncabezadoH() {
		return tablaSimple.getClaves().get(7);
	}
	
	@Override
	public ListModelList<TestChiCuadradoDetalleM> getDetalles() {
		ListModelList<TestChiCuadradoDetalleM> modelo = new ListModelList<>();
		for (DetalleTabla d : tablaSimple.getDetalles()) {
			TestChiCuadradoDetalleM dM = (TestChiCuadradoDetalleM) context.getBean("testChiCuadradoVariableDiscretaDetalleM");
			dM.setDetalleTabla(d);
			modelo.add(dM);
		}
		return modelo;
	}

	@Override
	public void clear() {
		tablaSimple.getDetalles().clear();
	}

}
