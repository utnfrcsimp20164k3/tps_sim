package app.presentacion;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import app.utils.DetalleTabla;

@Component
public class TestChiCuadradoVariableContinuaDetalleMImpl implements TestChiCuadradoDetalleM {
	
	static final Logger LOG = LoggerFactory.getLogger(TestChiCuadradoVariableContinuaDetalleMImpl.class);
	DetalleTabla detalleTabla;
	
	public TestChiCuadradoVariableContinuaDetalleMImpl() {
		super();
	}

	@Bean
	@Scope(value = "prototype")
	static public TestChiCuadradoDetalleM testChiCuadradoVariableContinuaDetalleM() {
		return new TestChiCuadradoVariableContinuaDetalleMImpl();
	}

	@Override
	public void setDetalleTabla(DetalleTabla detalleTabla) {
		this.detalleTabla = detalleTabla;
	}
	
	@Override
	public Object getValorA() {
		return detalleTabla.getValores().get("k");
	}

	@Override
	public Object getValorB() {
		return detalleTabla.getValores().get("Intervalo");
	}

	@Override
	public Object getValorC() {
		return detalleTabla.getValores().get("fo");
	}

	@Override
	public Object getValorD() {
		return detalleTabla.getValores().get("fe");
	}

	@Override
	public Object getValorE() {
		return detalleTabla.getValores().get("fo - fe");
	}

	@Override
	public Object getValorF() {
		return detalleTabla.getValores().get("(fo - fe)\u2072");
	}

	@Override
	public Object getValorG() {
		return detalleTabla.getValores().get("(fo - fe)\u2072 / fe");
	}

	@Override
	public Object getValorH() {
		return detalleTabla.getValores().get("\u2211((fo - fe)\u2072 / fe)");
	}

}
