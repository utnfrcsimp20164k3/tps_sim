package app.presentacion;

import java.math.BigDecimal;
import java.math.RoundingMode;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.zkoss.bind.BindContext;

import app.utils.Porcentaje;

@Component
public class ConversorDePorcentajeImpl implements ConversorDePorcentaje {

	public ConversorDePorcentajeImpl() {
		super();
	}

	@Bean
	@Scope("singleton")
	public ConversorDePorcentaje conversorDePorcentaje() {
		return new ConversorDePorcentajeImpl();
	}

	@Override
	public Object coerceToBean(Object str, org.zkoss.zk.ui.Component arg1, BindContext arg2) {
		BigDecimal d = null;
		try {
			if (str != null && !((String) str).isEmpty()) {
				d = new BigDecimal((String) str);
				d = d.setScale(Porcentaje.getDefaultScale(), RoundingMode.HALF_UP);
			}
		} catch (NumberFormatException ex) {

		}
		return d;
	}

	@Override
	public Object coerceToUi(Object dec, org.zkoss.zk.ui.Component comp, BindContext ctx) {
		String s = null;
		if (dec != null) {
			BigDecimal d = ((BigDecimal) dec).setScale(Porcentaje.getDefaultScale(), RoundingMode.HALF_UP);
			s = d.toPlainString();
		}
		return s;
	}

}
