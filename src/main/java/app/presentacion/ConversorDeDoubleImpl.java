package app.presentacion;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.zkoss.bind.BindContext;

@Component
public class ConversorDeDoubleImpl implements ConversorDeDouble {

	public ConversorDeDoubleImpl() {
		super();
	}

	@Bean
	@Scope("singleton")
	public ConversorDeDouble conversorDeDouble() {
		return new ConversorDeDoubleImpl();
	}

	@Override
	public Object coerceToBean(Object arg0, org.zkoss.zk.ui.Component arg1, BindContext arg2) {
		Object o = null;
		try {
			if (arg0 != null && !((String) arg0).isEmpty())
				o = Double.parseDouble((String) arg0);
		} catch (NumberFormatException ex) {

		}
		return o;
	}

	@Override
	public Object coerceToUi(Object val, org.zkoss.zk.ui.Component comp, BindContext ctx) {
		Object o = null;
		if (val != null)
			o = String.format("%.4f", val);
		return o;
	}

}
