package app.presentacion;

import org.zkoss.bind.Converter;

public interface FormateadorDeNumerosDecimales extends Converter {
	
	void setPattern(String pattern);

}
