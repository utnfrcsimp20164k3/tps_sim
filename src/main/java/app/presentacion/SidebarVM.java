package app.presentacion;

import java.util.ArrayList;
import java.util.Stack;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.SmartNotifyChange;
import org.zkoss.zk.ui.select.annotation.VariableResolver;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.DefaultTreeModel;
import org.zkoss.zul.DefaultTreeNode;
import org.zkoss.zul.TreeNode;
import org.zkoss.zul.Treeitem;

@VariableResolver(org.zkoss.zkplus.spring.DelegatingVariableResolver.class)
public class SidebarVM {

	static final Logger LOG = LoggerFactory
			.getLogger(SidebarVM.class);

	@WireVariable
	private ResourceIndex resourceIndex;

	DefaultTreeModel<SidebarTreeItem> treeModel;
	private DefaultTreeNode<SidebarTreeItem> itemSeleccionado;
	private DefaultTreeNode<SidebarTreeItem> root;
	private String filtro;
	private String filtroAnterior;

	@Init
	public void init() {
		root = new DefaultTreeNode<SidebarTreeItem>(null,
				new ArrayList<DefaultTreeNode<SidebarTreeItem>>());
		treeModel = genTreeModel(root);
	}

	private DefaultTreeModel<SidebarTreeItem> genTreeModel(DefaultTreeNode<SidebarTreeItem> root) {
		DefaultTreeModel<SidebarTreeItem> result;
		String rPName, rBName, pagePath;
		ArrayList<DefaultTreeNode<SidebarTreeItem>> hijos;
		result = new DefaultTreeModel<SidebarTreeItem>(root);
		for (ResourcePackage rP : resourceIndex.getResourcePackages()) {
			hijos = new ArrayList<DefaultTreeNode<SidebarTreeItem>>();
			rPName = rP.getNombre();
			for (ResourceBundle rB : rP.getResourceBundles()) {
				if (rB.isEnSidebar()) {
					rBName = rB.getNombre();
					pagePath = rB.getPagePathname();
					hijos.add(new DefaultTreeNode<SidebarTreeItem>(
							new SidebarTreeItem(rBName, pagePath)));
				}
			}
			root.add(new DefaultTreeNode<SidebarTreeItem>(new SidebarTreeItem(
					rPName, null), hijos));
		}
		return result;
	}

	public DefaultTreeNode<SidebarTreeItem> getItemSeleccionado() {
		return itemSeleccionado;
	}

	@SmartNotifyChange("itemSeleccionado")
	public void setItemSeleccionado(
			DefaultTreeNode<SidebarTreeItem> itemSeleccionado) {
		this.itemSeleccionado = itemSeleccionado;
	}

	private void cerrarHermanos(Treeitem item) {
		Treeitem prev = (Treeitem) item.getPreviousSibling();
		Treeitem next = (Treeitem) item.getNextSibling();
		while (prev != null) {
			prev.setOpen(false);
			prev = (Treeitem) prev.getPreviousSibling();
		}
		while (next != null) {
			next.setOpen(false);
			next = (Treeitem) next.getNextSibling();
		}
	}

	@Command
	public void cambiarEstado(@BindingParam("item") Treeitem item) {
		cerrarHermanos(item);
	}

	@SmartNotifyChange("treeModel")
	@Command
	public void filtrar() {
		if (filtro == null)
			return;
		Stack<DefaultTreeNode<SidebarTreeItem>> pila;
		DefaultTreeModel<SidebarTreeItem> nuevo;
		pila = new Stack<>();
		if (filtroAnterior != null && filtro.contains(filtroAnterior))
			nuevo = treeModel;
		else {
			root = new DefaultTreeNode<SidebarTreeItem>(null,
					new ArrayList<DefaultTreeNode<SidebarTreeItem>>());
			nuevo = genTreeModel(root);
		}
		apilar(pila, root);
		while (!pila.empty()) {
			DefaultTreeNode<SidebarTreeItem> nodo = pila.pop();
			if (nodo.getData() != null && nodo.getChildCount() == 0
					&& !nodo.getData().getName().contains(filtro))
				nodo.removeFromParent();
			for (TreeNode<SidebarTreeItem> abierto : treeModel.getOpenObjects())
				if (nodo.getData() != null
						&& abierto != null
						&& abierto.getData().getName() == nodo.getData()
								.getName())
					nuevo.addOpenObject(nodo);
		}
		treeModel = nuevo;
		filtroAnterior = filtro;
	}

	private void apilar(Stack<DefaultTreeNode<SidebarTreeItem>> pila,
			DefaultTreeNode<SidebarTreeItem> nodo) {
		pila.push(nodo);
		if (nodo.getChildCount() > 0) {
			for (TreeNode<SidebarTreeItem> n : nodo.getChildren())
				apilar(pila, (DefaultTreeNode<SidebarTreeItem>) n);
		}

	}

	public DefaultTreeModel<SidebarTreeItem> getTreeModel() {
		return treeModel;
	}

	public String getFiltro() {
		return filtro;
	}

	public void setFiltro(String filtro) {
		this.filtro = filtro;
	}

}
