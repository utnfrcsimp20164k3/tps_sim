package app.presentacion;

import java.awt.image.BufferedImage;
import java.io.IOException;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.encoders.EncoderUtil;
import org.jfree.chart.encoders.ImageFormat;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.renderer.category.BarRenderer;
import org.jfree.chart.renderer.category.StandardBarPainter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.SmartNotifyChange;
import org.zkoss.image.AImage;
import org.zkoss.zk.ui.select.annotation.VariableResolver;
import org.zkoss.zk.ui.select.annotation.WireVariable;

import app.utils.SerieVariableDiscreta;

@VariableResolver(org.zkoss.zkplus.spring.DelegatingVariableResolver.class)
public class GraficoBarrasVM {

	static final Logger LOG = LoggerFactory.getLogger(GraficoBarrasVM.class);
	@WireVariable
	CustomCategoryDataSet customCategoryDataSet;
	AImage imagen;
	String serieAMostrar;

	@Init
	public void initSetup(@BindingParam("serie") SerieVariableDiscreta serie) {
		customCategoryDataSet.agregarGraficoBarras(serie.getGraficoBarrasObservadas());
		customCategoryDataSet.agregarGraficoBarras(serie.getGraficoBarrasEsperadas());
		setSerieAMostrar("Frecuencias observadas");
		imagen = generarImagen(generarGrafica(customCategoryDataSet));
	}

	@Command
	@SmartNotifyChange("imagen")
	public void cambiarImagen() {
		imagen = generarImagen(generarGrafica(customCategoryDataSet));
	}

	private JFreeChart generarGrafica(CustomCategoryDataSet dataset) {
		JFreeChart grafica = ChartFactory.createBarChart("", "Intervalo", "Frecuencia", dataset,
				PlotOrientation.VERTICAL, true, true, false);
		CategoryPlot categoryPlot = (CategoryPlot) grafica.getPlot();
		categoryPlot.setRangePannable(true);
		categoryPlot.setForegroundAlpha(0.85F);
		NumberAxis numberAxis = (NumberAxis) categoryPlot.getRangeAxis();
		numberAxis.setStandardTickUnits(NumberAxis.createIntegerTickUnits());
		BarRenderer barRenderer = (BarRenderer) categoryPlot.getRenderer();
		barRenderer.setDrawBarOutline(true);
		barRenderer.setBarPainter(new StandardBarPainter());
		barRenderer.setShadowVisible(false);
		if (serieAMostrar.equals("Frecuencias observadas")) {
			barRenderer.setSeriesVisible(0, true);
			barRenderer.setSeriesVisible(1, false);
		} else if (serieAMostrar.equals("Frecuencias esperadas")) {
			barRenderer.setSeriesVisible(0, false);
			barRenderer.setSeriesVisible(1, true);
		} else if (serieAMostrar.equals("Ambas frecuencias")) {
			barRenderer.setSeriesVisible(0, true);
			barRenderer.setSeriesVisible(1, true);
		}
		categoryPlot.getRangeAxis().setUpperMargin(categoryPlot.getRangeAxis().getUpperMargin() + 0.1);
		return grafica;
	}

	private AImage generarImagen(JFreeChart grafica) {
		BufferedImage bi = grafica.createBufferedImage(500, 300, BufferedImage.TRANSLUCENT, null);
		byte[] bytes = null;
		AImage image = null;
		try {
			bytes = EncoderUtil.encode(bi, ImageFormat.PNG, true);
			image = new AImage(null, bytes);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return image;
	}

	public AImage getImagen() {
		return imagen;
	}

	public void setImagen(AImage imagen) {
		this.imagen = imagen;
	}

	public String getSerieAMostrar() {
		return serieAMostrar;
	}

	public void setSerieAMostrar(String serieAMostrar) {
		this.serieAMostrar = serieAMostrar;
	}

}
