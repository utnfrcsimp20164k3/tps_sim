package app.presentacion;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.jfree.data.DomainOrder;
import org.jfree.data.general.DatasetChangeListener;
import org.jfree.data.general.DatasetGroup;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import app.utils.grafica.Histograma;

@Component
public class CustomHistogramaDataSetImpl implements CustomHistogramDataSet {

	static final Logger LOG = LoggerFactory.getLogger(CustomHistogramaDataSetImpl.class);
	List<Histograma> histogramas;
	List<DatasetChangeListener> listeners;

	public CustomHistogramaDataSetImpl() {
		super();
		histogramas = new ArrayList<>();
		listeners = new ArrayList<>();
	}

	@Bean
	@Scope(value = "prototype")
	static public CustomHistogramDataSet customHistogramDataSet() {
		return new CustomHistogramaDataSetImpl();
	}

	@Override
	public DomainOrder getDomainOrder() {
		return DomainOrder.ASCENDING;
	}

	@Override
	public int getItemCount(int arg0) {
		return histogramas.get(arg0).getBarras().length;
	}

	@Override
	public Number getX(int arg0, int arg1) {
		return histogramas.get(arg0).getBarras()[arg1].getInferior().getValor();
	}

	@Override
	public double getXValue(int arg0, int arg1) {
		return histogramas.get(arg0).getBarras()[arg1].getInferior().getValor();
	}

	@Override
	public Number getY(int arg0, int arg1) {
		return histogramas.get(arg0).getBarras()[arg1].getFrecuencia();
	}

	@Override
	public double getYValue(int arg0, int arg1) {
		return histogramas.get(arg0).getBarras()[arg1].getFrecuencia();
	}

	@Override
	public int getSeriesCount() {
		return histogramas.size();
	}

	@Override
	public Comparable getSeriesKey(int arg0) {
		return histogramas.get(arg0).getKey();
	}

	@Override
	public int indexOf(Comparable arg0) {
		int index = 0;
		for (int i = 0; i < histogramas.size(); i++) {
			Histograma h = histogramas.get(i);
			if (h.getKey().equals(arg0))
				index = i;
		}
		return index;
	}

	@Override
	public void addChangeListener(DatasetChangeListener arg0) {
		listeners.add(arg0);
	}

	@Override
	public DatasetGroup getGroup() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void removeChangeListener(DatasetChangeListener arg0) {
		listeners.remove(arg0);
	}

	@Override
	public void setGroup(DatasetGroup arg0) {
		// TODO Auto-generated method stub
	}

	@Override
	public Number getEndX(int arg0, int arg1) {
		return histogramas.get(arg0).getBarras()[arg1].getSuperior().getValor();
	}

	@Override
	public double getEndXValue(int arg0, int arg1) {
		return histogramas.get(arg0).getBarras()[arg1].getSuperior().getValor();
	}

	@Override
	public Number getEndY(int arg0, int arg1) {
		return histogramas.get(arg0).getBarras()[arg1].getFrecuencia();
	}

	@Override
	public double getEndYValue(int arg0, int arg1) {
		return new Double(histogramas.get(arg0).getBarras()[arg1].getFrecuencia());
	}

	@Override
	public Number getStartX(int arg0, int arg1) {
		return histogramas.get(arg0).getBarras()[arg1].getInferior().getValor();
	}

	@Override
	public double getStartXValue(int arg0, int arg1) {
		return histogramas.get(arg0).getBarras()[arg1].getInferior().getValor();
	}

	@Override
	public Number getStartY(int arg0, int arg1) {
		return histogramas.get(arg0).getBarras()[arg1].getFrecuencia();
	}

	@Override
	public double getStartYValue(int arg0, int arg1) {
		return new Double(histogramas.get(arg0).getBarras()[arg1].getFrecuencia());
	}

	@Override
	public void agregarHistograma(Histograma histograma) {
		histogramas.add(histograma);
	}

	@Override
	public double getXMinimo() {
		double min = histogramas.get(0).getBarras()[0].getInferior().getValor();
		for (Histograma h : histogramas)
			if (h.getBarras()[0].getInferior().getValor() < min)
				min = h.getBarras()[0].getInferior().getValor();
		return min;
	}

	@Override
	public double getXMaximo() {
		double max = histogramas.get(0).getBarras()[histogramas.get(0).getBarras().length - 1].getSuperior()
				.getValor();
		for (Histograma h : histogramas)
			if (h.getBarras()[histogramas.get(0).getBarras().length - 1].getSuperior().getValor() > max)
				max = h.getBarras()[histogramas.get(0).getBarras().length - 1].getSuperior().getValor();
		return max;
	}

}
