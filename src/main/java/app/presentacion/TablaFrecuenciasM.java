package app.presentacion;

import org.zkoss.zul.ListModelList;

import app.utils.TablaSimple;

public interface TablaFrecuenciasM {

	void setTabla(TablaSimple tablaSimple);
	
	Object getEncabezadoA();

	Object getEncabezadoB();

	Object getEncabezadoC();

	Object getEncabezadoD();

	ListModelList<TablaFrecuenciasDetalleM> getDetalles();

	void clear();

}
