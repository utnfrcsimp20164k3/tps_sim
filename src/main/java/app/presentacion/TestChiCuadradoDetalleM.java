package app.presentacion;

import app.utils.DetalleTabla;

public interface TestChiCuadradoDetalleM {
	
	Object getValorA();

	Object getValorB();

	Object getValorC();

	Object getValorD();

	Object getValorE();

	Object getValorF();

	Object getValorG();

	Object getValorH();

	void setDetalleTabla(DetalleTabla d);

}
