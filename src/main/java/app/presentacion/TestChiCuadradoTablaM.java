package app.presentacion;

import org.zkoss.zul.ListModelList;

import app.utils.TablaSimple;

public interface TestChiCuadradoTablaM {

	void setTabla(TablaSimple tablaSimple);
	
	Object getEncabezadoA();

	Object getEncabezadoB();

	Object getEncabezadoC();

	Object getEncabezadoD();

	Object getEncabezadoE();

	Object getEncabezadoF();

	Object getEncabezadoG();

	Object getEncabezadoH();

	ListModelList<TestChiCuadradoDetalleM> getDetalles();

	void clear();

}
