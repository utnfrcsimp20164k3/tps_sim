package app.utils;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
public class EstadoClienteEsperandoServicio implements EstadoCliente {
	
	public EstadoClienteEsperandoServicio() {
		super();
	}

	@Bean
	@Scope(value = "prototype")
	static public EstadoCliente estadoEsperandoServicio() {
		return new EstadoClienteEsperandoServicio();
	}

	@Override
	public String toString() {
		return "Esperando servicio";
	}

	@Override
	public boolean isEnCola() {
		return false;
	}

	@Override
	public boolean isEsperandoServicio() {
		return true;
	}

	@Override
	public boolean isRechazadoPorCola() {
		return false;
	}

	@Override
	public boolean isRechazadoPorGestorColas() {
		return false;
	}

	@Override
	public boolean isSiendoServido() {
		return false;
	}

	@Override
	public boolean isRechazadoPorServidor() {
		return false;
	}

	@Override
	public boolean isServido() {
		return false;
	}

}
