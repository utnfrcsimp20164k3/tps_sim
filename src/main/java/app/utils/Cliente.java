package app.utils;

import app.Loggable;

public interface Cliente extends Observable, Loggable {
	
	TipoServicio getTipoServicio();
	
	void setTipoServicio(TipoServicio tipoServicio);
	
	void declararseEsperandoServicio();

	void servicioDisponible(Servidor servidor);
	
	void servicioTerminado(Servidor servidor);

	boolean isRecibiendoServicio();

	boolean isEsperandoServicio();

	Servidor getServidor();

}
