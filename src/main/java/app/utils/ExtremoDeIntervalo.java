package app.utils;

public interface ExtremoDeIntervalo {

	Double getValor();

	void setValor(Double valor);

	boolean isAbierto();

	void setAbierto(boolean abierto);

}
