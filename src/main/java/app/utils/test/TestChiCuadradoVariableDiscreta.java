package app.utils.test;

import app.utils.SerieVariableDiscreta;
import app.utils.TablaSimple;

public interface TestChiCuadradoVariableDiscreta {
	
	double calcular(SerieVariableDiscreta serie);
	
	TablaSimple getTabla();

}
