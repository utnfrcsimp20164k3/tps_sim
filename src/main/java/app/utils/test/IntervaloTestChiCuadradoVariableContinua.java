package app.utils.test;

import app.utils.ExtremoDeIntervalo;
import app.utils.grafica.BarraHistograma;

public interface IntervaloTestChiCuadradoVariableContinua {

	int getFrecuenciaObservada();

	double getFrecuenciaEsperada();

	ExtremoDeIntervalo getInferior();

	ExtremoDeIntervalo getSuperior();
	
	void agregarBarraHistogramaObservadas(BarraHistograma barraHistograma);

	void agregarBarraHistogramaEsperadas(BarraHistograma barraHistograma);
	
	BarraHistograma getFirstObservadas();

	BarraHistograma getFirstEsperadas();

}
