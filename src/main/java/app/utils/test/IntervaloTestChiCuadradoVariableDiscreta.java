package app.utils.test;

import app.utils.grafica.BarraGraficoBarras;

public interface IntervaloTestChiCuadradoVariableDiscreta {

	int getFrecuenciaObservada();

	double getFrecuenciaEsperada();

	double getInferior();

	double getSuperior();
	
	void agregarBarraGraficoBarrasObservadas(BarraGraficoBarras barraGraficoBarras);

	void agregarBarraGraficoBarrasEsperadas(BarraGraficoBarras barraGraficoBarras);
	
	BarraGraficoBarras getFirstObservadas();

	BarraGraficoBarras getFirstEsperadas();

}
