package app.utils.test;

import java.util.ArrayDeque;
import java.util.Deque;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import app.utils.ExtremoDeIntervalo;
import app.utils.grafica.BarraHistograma;

@Component
public class IntervaloTestChiCuadradoVariableContinuaImpl implements IntervaloTestChiCuadradoVariableContinua {

	Deque<BarraHistograma> intervalosObservadas;
	Deque<BarraHistograma> intervalosEsperadas;

	public IntervaloTestChiCuadradoVariableContinuaImpl() {
		super();
		intervalosObservadas = new ArrayDeque<>();
		intervalosEsperadas = new ArrayDeque<>();
	}

	@Bean
	@Scope(value = "prototype")
	public IntervaloTestChiCuadradoVariableContinua intervaloTestChiCuadradadoVariableContinua() {
		return new IntervaloTestChiCuadradoVariableContinuaImpl();
	}

	@Override
	public int getFrecuenciaObservada() {
		int fo = 0;
		for (BarraHistograma i : intervalosObservadas)
			fo += i.getFrecuencia();
		return fo;
	}

	@Override
	public double getFrecuenciaEsperada() {
		int fe = 0;
		for (BarraHistograma i : intervalosEsperadas)
			fe += i.getFrecuencia();
		return fe;
	}

	@Override
	public ExtremoDeIntervalo getInferior() {
		return intervalosObservadas.getFirst().getInferior();
	}

	@Override
	public ExtremoDeIntervalo getSuperior() {
		return intervalosObservadas.getLast().getSuperior();
	}

	@Override
	public void agregarBarraHistogramaObservadas(BarraHistograma barraHistograma) {
		intervalosObservadas.add(barraHistograma);
	}

	@Override
	public void agregarBarraHistogramaEsperadas(BarraHistograma barraHistograma) {
		intervalosEsperadas.addLast(barraHistograma);
	}

	@Override
	public BarraHistograma getFirstObservadas() {
		return intervalosObservadas.getFirst();
	}

	@Override
	public BarraHistograma getFirstEsperadas() {
		return intervalosEsperadas.getFirst();
	}

	@Override
	public String toString() {
		StringBuffer s = new StringBuffer();
		if (intervalosObservadas.getFirst().getInferior().isAbierto())
			s.append("( ");
		else
			s.append("[ ");
		s.append(String.format("%.4f", intervalosObservadas.getFirst().getInferior().getValor()));
		s.append(" ; ");
		s.append(String.format("%.4f", intervalosObservadas.getLast().getSuperior().getValor()));
		if (intervalosObservadas.getLast().getSuperior().isAbierto())
			s.append(" )");
		else
			s.append("]");
		return s.toString();
	}

}
