package app.utils.test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import app.utils.DetalleTabla;
import app.utils.SerieVariableDiscreta;
import app.utils.TablaSimple;
import app.utils.grafica.BarraGraficoBarras;

@Component
public class TestChiCuadradoVariableDiscretaImpl implements TestChiCuadradoVariableDiscreta {

	static final Logger LOG = LoggerFactory.getLogger(TestChiCuadradoVariableDiscretaImpl.class);
	@Autowired
	TablaSimple tablaSimple;
	@Autowired
	ApplicationContext context;

	public TestChiCuadradoVariableDiscretaImpl() {
		super();
	}

	@Bean
	@Scope(value = "prototype")
	static public TestChiCuadradoVariableDiscreta testChiCuadradoVariableDiscreta() {
		return new TestChiCuadradoVariableDiscretaImpl();
	}

	@PostConstruct
	public void init() {
		tablaSimple.getClaves().add("k");
		tablaSimple.getClaves().add("Clase");
		tablaSimple.getClaves().add("fo");
		tablaSimple.getClaves().add("fe");
		tablaSimple.getClaves().add("fo - fe");
		tablaSimple.getClaves().add("(fo - fe)\u2072");
		tablaSimple.getClaves().add("(fo - fe)\u2072 / fe");
		tablaSimple.getClaves().add("\u2211((fo - fe)\u2072 / fe)");
	}
	

	@Override
	public double calcular(SerieVariableDiscreta serie) {
		double chiCuadrado = 0.0d;
		List<IntervaloTestChiCuadradoVariableDiscreta> intervalosTest = construirIntervalos(serie);
		for (int j = 0; j < intervalosTest.size(); j++) {
			IntervaloTestChiCuadradoVariableDiscreta i = intervalosTest.get(j);
			DetalleTabla d = (DetalleTabla) context.getBean("detalleTabla");
			d.getValores().put("k", j + 1);
			d.getValores().put("Clase", i.toString());
			double fo = i.getFrecuenciaObservada();
			d.getValores().put("fo", fo);
			double fe = i.getFrecuenciaEsperada();
			d.getValores().put("fe", fe);
			double a = fo - fe; // fo - fe
			d.getValores().put("fo - fe", a);
			double b = Math.pow(a, 2d); // Math.pow((fo - fe), 2d)
			d.getValores().put("(fo - fe)\u2072", b);
			double c = b / fe; // Math.pow((fo - fe), 2d) / fe
			d.getValores().put("(fo - fe)\u2072 / fe", c);
			chiCuadrado += c;
			d.getValores().put("\u2211((fo - fe)\u2072 / fe)", chiCuadrado);
			tablaSimple.getDetalles().add(d);
		}
		return chiCuadrado;
	}

	private List<IntervaloTestChiCuadradoVariableDiscreta> construirIntervalos(SerieVariableDiscreta serie) {
		BarraGraficoBarras[] barrasObservadas = serie.getGraficoBarrasObservadas().getBarras();
		BarraGraficoBarras[] barrasEsperadas = serie.getGraficoBarrasEsperadas().getBarras();
		List<IntervaloTestChiCuadradoVariableDiscreta> intervalosTest = new ArrayList<>();
		IntervaloTestChiCuadradoVariableDiscreta anterior = null;
		for (int i = 0; i < barrasObservadas.length; i++) {
			BarraGraficoBarras[] restantesObservadas = null;
			BarraGraficoBarras[] restantesEsperadas = null;
			if (i < serie.getGraficoBarrasObservadas().getBarras().length - 2) {
				restantesObservadas = Arrays.copyOfRange(barrasObservadas, i + 1, barrasObservadas.length - 1);
				restantesEsperadas = Arrays.copyOfRange(barrasEsperadas, i + 1, barrasEsperadas.length - 1);
			}
			IntervaloTestChiCuadradoVariableDiscreta aAgregar = null, intervaloTest = anterior == null
					? (IntervaloTestChiCuadradoVariableDiscreta) context.getBean("intervaloTestChiCuadradadoVariableDiscreta") : anterior;
			if (barrasObservadas[i].getFrecuencia() >= 5 && restantesObservadas != null
					&& sumatoriaFrecuenciasObservadas(restantesObservadas) >= 5) {
				intervaloTest.agregarBarraGraficoBarrasObservadas(barrasObservadas[i]);
				intervaloTest.agregarBarraGraficoBarrasEsperadas(barrasEsperadas[i]);
				aAgregar = intervaloTest;
			} else if (barrasObservadas[i].getFrecuencia() >= 5 && restantesObservadas == null) {
				intervaloTest.agregarBarraGraficoBarrasObservadas(barrasObservadas[i]);
				intervaloTest.agregarBarraGraficoBarrasEsperadas(barrasEsperadas[i]);
				aAgregar = intervaloTest;
			} else if (barrasObservadas[i].getFrecuencia() >= 5 && restantesObservadas != null
					&& sumatoriaFrecuenciasObservadas(restantesObservadas) < 5) {
				intervaloTest.agregarBarraGraficoBarrasObservadas(barrasObservadas[i]);
				intervaloTest.agregarBarraGraficoBarrasEsperadas(barrasEsperadas[i]);
				for (int j = 0; j < restantesObservadas.length; j++) {
					intervaloTest.agregarBarraGraficoBarrasObservadas(restantesObservadas[j]);
					intervaloTest.agregarBarraGraficoBarrasEsperadas(restantesEsperadas[j]);
				}
				i += restantesObservadas.length - 1;
				aAgregar = intervaloTest;
			} else if (intervaloTest.getFrecuenciaObservada() > 0 && intervaloTest.getFrecuenciaObservada() < 5
					&& barrasObservadas[i].getFrecuencia() < 5 && restantesObservadas != null
					&& sumatoriaFrecuenciasObservadas(restantesObservadas) < 5
					&& intervaloTest.getFrecuenciaObservada() + barrasObservadas[i].getFrecuencia()
							+ sumatoriaFrecuenciasObservadas(restantesObservadas) < 5) {
				intervalosTest.get(intervalosTest.size() - 1).agregarBarraGraficoBarrasObservadas(intervaloTest.getFirstObservadas());
				intervalosTest.get(intervalosTest.size() - 1).agregarBarraGraficoBarrasObservadas(barrasObservadas[i]);
				intervalosTest.get(intervalosTest.size() - 1).agregarBarraGraficoBarrasEsperadas(intervaloTest.getFirstEsperadas());
				intervalosTest.get(intervalosTest.size() - 1).agregarBarraGraficoBarrasEsperadas(barrasEsperadas[i]);
				for (int j = 0; j < restantesObservadas.length; j++) {
					intervalosTest.get(intervalosTest.size() - 1).agregarBarraGraficoBarrasObservadas(restantesObservadas[j]);
					intervalosTest.get(intervalosTest.size() - 1).agregarBarraGraficoBarrasEsperadas(restantesEsperadas[j]);
				}
				i += restantesObservadas.length - 1;
			} else if (intervaloTest.getFrecuenciaObservada() == 0
					&& barrasObservadas[i].getFrecuencia() < 5 && restantesObservadas != null
					&& sumatoriaFrecuenciasObservadas(restantesObservadas) < 5
					&& intervaloTest.getFrecuenciaObservada() + barrasObservadas[i].getFrecuencia()
							+ sumatoriaFrecuenciasObservadas(restantesObservadas) < 5) {
				intervalosTest.get(intervalosTest.size() - 1).agregarBarraGraficoBarrasObservadas(barrasObservadas[i]);
				intervalosTest.get(intervalosTest.size() - 1).agregarBarraGraficoBarrasEsperadas(barrasEsperadas[i]);
				for (int j = 0; j < restantesObservadas.length; j++) {
					intervalosTest.get(intervalosTest.size() - 1).agregarBarraGraficoBarrasObservadas(restantesObservadas[j]);
					intervalosTest.get(intervalosTest.size() - 1).agregarBarraGraficoBarrasEsperadas(restantesEsperadas[j]);
				}
				i += restantesObservadas.length - 1;
			} else if (barrasObservadas[i].getFrecuencia() < 5 && i < barrasObservadas.length - 1) {
				intervaloTest.agregarBarraGraficoBarrasObservadas(barrasObservadas[i]);
				intervaloTest.agregarBarraGraficoBarrasEsperadas(barrasEsperadas[i]);
				if (intervaloTest.getFrecuenciaObservada() >= 5)
					aAgregar = intervaloTest;
				else
					anterior = intervaloTest;
			} else if (barrasObservadas[i].getFrecuencia() < 5 && i == barrasObservadas.length - 1) {
				intervalosTest.get(intervalosTest.size() - 1).agregarBarraGraficoBarrasObservadas(barrasObservadas[i]);
				intervalosTest.get(intervalosTest.size() - 1).agregarBarraGraficoBarrasEsperadas(barrasEsperadas[i]);
				anterior = null;
			}
			if (aAgregar != null) {
				intervalosTest.add(aAgregar);
				aAgregar = null;
				anterior = null;
			}
		}
		return intervalosTest;
	}

	private int sumatoriaFrecuenciasObservadas(BarraGraficoBarras[] barrasGraficoBarras) {
		int s = 0;
		for (BarraGraficoBarras i : barrasGraficoBarras)
			s += i.getFrecuencia();
		return s;
	}

	@Override
	public TablaSimple getTabla() {
		return tablaSimple;
	}

}
