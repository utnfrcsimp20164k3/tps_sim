package app.utils.test;

import app.utils.SerieVariableContinua;
import app.utils.TablaSimple;

public interface TestChiCuadradoVariableContinua {
	
	double calcular(SerieVariableContinua serie);
	
	TablaSimple getTabla();

}
