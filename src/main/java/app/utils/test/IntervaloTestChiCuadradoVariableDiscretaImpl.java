package app.utils.test;

import java.util.ArrayDeque;
import java.util.Deque;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import app.utils.grafica.BarraGraficoBarras;

@Component
public class IntervaloTestChiCuadradoVariableDiscretaImpl implements IntervaloTestChiCuadradoVariableDiscreta {

	Deque<BarraGraficoBarras> barrasObservadas;
	Deque<BarraGraficoBarras> barrasEsperadas;

	public IntervaloTestChiCuadradoVariableDiscretaImpl() {
		super();
		barrasObservadas = new ArrayDeque<>();
		barrasEsperadas = new ArrayDeque<>();
	}

	@Bean
	@Scope(value = "prototype")
	public IntervaloTestChiCuadradoVariableDiscreta intervaloTestChiCuadradadoVariableDiscreta() {
		return new IntervaloTestChiCuadradoVariableDiscretaImpl();
	}

	@Override
	public int getFrecuenciaObservada() {
		int fo = 0;
		for (BarraGraficoBarras i : barrasObservadas)
			fo += i.getFrecuencia();
		return fo;
	}

	@Override
	public double getFrecuenciaEsperada() {
		int fe = 0;
		for (BarraGraficoBarras i : barrasEsperadas)
			fe += i.getFrecuencia();
		return fe;
	}

	@Override
	public double getInferior() {
		return barrasObservadas.getFirst().getNumero();
	}

	@Override
	public double getSuperior() {
		return barrasObservadas.getLast().getNumero();
	}

	@Override
	public void agregarBarraGraficoBarrasObservadas(BarraGraficoBarras barraGraficoBarras) {
		barrasObservadas.add(barraGraficoBarras);
	}

	@Override
	public void agregarBarraGraficoBarrasEsperadas(BarraGraficoBarras barraGraficoBarras) {
		barrasEsperadas.addLast(barraGraficoBarras);
	}

	@Override
	public BarraGraficoBarras getFirstObservadas() {
		return barrasObservadas.getFirst();
	}

	@Override
	public BarraGraficoBarras getFirstEsperadas() {
		return barrasEsperadas.getFirst();
	}

	@Override
	public String toString() {
		StringBuffer s = new StringBuffer();
		s.append(String.format("%.0f", new Double(barrasObservadas.getFirst().getNumero())));
		return s.toString();
	}

}
