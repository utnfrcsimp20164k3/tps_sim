package app.utils;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
public class EstadoServidorSirviendo implements EstadoServidor {
	
	public EstadoServidorSirviendo() {
		super();
	}

	@Bean
	@Scope(value = "prototype")
	static public EstadoServidor estadoServidorSirviendo() {
		return new EstadoServidorSirviendo();
	}

	@Override
	public String toString() {
		return "Sirviendo";
	}

	@Override
	public boolean isDisponible() {
		return false;
	}

	@Override
	public boolean isSirviendo() {
		return true;
	}

}
