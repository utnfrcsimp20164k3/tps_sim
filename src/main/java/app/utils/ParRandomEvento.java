package app.utils;

public class ParRandomEvento implements Par<Double, Object> {
	
	Double random;
	Object evento;

	public ParRandomEvento(Double random, Object evento) {
		super();
		this.random = random;
		this.evento = evento;
	}

	@Override
	public Double getLeft() {
		return random;
	}

	@Override
	public void setLeft(Double random) {
		this.random = random;
	}

	@Override
	public Object getRight() {
		return evento;
	}

	@Override
	public void setRight(Object evento) {
		this.evento = evento;
	}

}
