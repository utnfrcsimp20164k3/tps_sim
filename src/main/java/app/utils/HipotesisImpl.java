package app.utils;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import app.utils.distribucion.TipoDistribucion;

@Component
public class HipotesisImpl implements Hipotesis {
	
	boolean aceptada;
	TipoDistribucion tipoDistribucion;

	@Bean
	@Scope("prototype")
	static public Hipotesis hipotesis() {
		return new HipotesisImpl();
	}

	@Override
	public boolean isAceptada() {
		return aceptada;
	}

	@Override
	public TipoDistribucion getTipoDistribucion() {
		return tipoDistribucion;
	}

	@Override
	public void setTipoDistribucion(TipoDistribucion tipoDistribucion) {
		this.tipoDistribucion = tipoDistribucion;

	}

}
