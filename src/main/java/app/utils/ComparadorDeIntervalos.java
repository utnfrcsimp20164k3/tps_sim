package app.utils;

import java.util.Comparator;

public class ComparadorDeIntervalos implements Comparator<Intervalo> {

	@Override
	public int compare(Intervalo arg0, Intervalo arg1) {
		return arg0.getInferior().getValor().compareTo(arg1.getInferior().getValor());
	}

}
