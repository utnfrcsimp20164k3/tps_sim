package app.utils.grafica;

import app.utils.SerieVariableContinua;

public interface HistogramaObservadas extends Histograma {

	void generar(SerieVariableContinua serieVariableContinua, double extremoInferior, double extremoSuperior);
	
}
