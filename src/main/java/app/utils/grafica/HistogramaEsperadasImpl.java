package app.utils.grafica;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import app.utils.DetalleTabla;
import app.utils.Intervalo;
import app.utils.SerieVariableContinua;
import app.utils.TablaSimple;

@Component
public class HistogramaEsperadasImpl implements HistogramaEsperadas {

	static final Logger LOG = LoggerFactory.getLogger(HistogramaEsperadasImpl.class);
	@Autowired
	ApplicationContext context;
	Comparable<String> key;
	SerieVariableContinua serie;
	BarraHistograma[] barras;
	double extremoInferior, extremoSuperior;
	int cantidadIntervalos;
	@Autowired
	TablaSimple tablaSimple;

	public HistogramaEsperadasImpl() {
		super();
	}

	@Bean
	@Scope(value = "prototype")
	static public Histograma histogramaEsperadas() {
		return new HistogramaEsperadasImpl();
	}

	@PostConstruct
	public void init() {
		tablaSimple.getClaves().add("k");
		tablaSimple.getClaves().add("Intervalo");
		tablaSimple.getClaves().add("fe");
	}

	@Override
	public void generar(SerieVariableContinua serie, HistogramaObservadas histogramaObservadas) {
		key = "Frecuencias esperadas";
		this.serie = serie;
		this.cantidadIntervalos = histogramaObservadas.getCantidadIntervalos();
		this.extremoInferior = histogramaObservadas.getExtremoInferior();
		this.extremoSuperior = histogramaObservadas.getExtremoSuperior();
		generarIntervalos(cantidadIntervalos, extremoInferior, extremoSuperior);
		generarTabla();
	}

	@Override
	public void clear() {
		tablaSimple.getDetalles().clear();
	}

	@Override
	public Comparable<String> getKey() {
		return key;
	}

	@Override
	public BarraHistograma[] getBarras() {
		return barras;
	}

	@Override
	public Intervalo[] getIntervalos() {
		Intervalo[] intervalos = new Intervalo[barras.length];
		for (int i = 0; i < barras.length; i++)
			intervalos[i] = barras[i].getIntervalo();
		return intervalos;
	}

	@Override
	public int getCantidadIntervalos() {
		return cantidadIntervalos;
	}

	@Override
	public void setCantidadIntervalos(int cantidadIntervalos) {
		this.cantidadIntervalos = cantidadIntervalos;
	}

	@Override
	public TablaSimple getTabla() {
		return tablaSimple;
	}

	@Override
	public double getExtremoInferior() {
		return extremoInferior;
	}

	@Override
	public double getExtremoSuperior() {
		return extremoSuperior;
	}

	private void generarIntervalos(int cantidad, double min, double max) {
		barras = new BarraHistograma[cantidad];
		double anchoIntervalo = new Double((max - min) / new Double(cantidad));
		BarraHistograma barra;
		for (double i = 0; i < cantidad; i++) {
			barra = (BarraHistograma) context.getBean("barraHistograma");
			barra.setHistograma(this);
			barra.getInferior().setValor(min + i * anchoIntervalo);
			barra.getInferior().setAbierto(false);
			barra.getSuperior().setValor(min + anchoIntervalo + i * anchoIntervalo);
			barra.getSuperior().setAbierto(true);
			barra.setFrecuencia(serie.getTipoDistribucion().getFrecuenciaEsperada(serie, barra.getIntervalo()));
			barras[(int) i] = barra;
		}
	}

	private void generarTabla() {
		for (int i = 0; i < barras.length; i++) {
			DetalleTabla d = (DetalleTabla) context.getBean("detalleTabla");
			d.getValores().put("k", i + 1);
			d.getValores().put("Intervalo", barras[i].toString());
			double fe = serie.getTipoDistribucion().getFrecuenciaEsperada(serie, barras[i].getIntervalo());
			d.getValores().put("fe", fe);
			tablaSimple.getDetalles().add(d);
		}
	}

}
