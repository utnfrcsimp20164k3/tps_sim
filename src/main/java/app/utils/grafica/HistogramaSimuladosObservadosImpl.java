package app.utils.grafica;

import java.util.Map;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import app.utils.DetalleTabla;
import app.utils.Intervalo;
import app.utils.TablaSimple;
import app.utils.distribucion.TipoDistribucion;

@Component
public class HistogramaSimuladosObservadosImpl implements HistogramaSimuladosObservados {

	static final Logger LOG = LoggerFactory.getLogger(HistogramaSimuladosObservadosImpl.class);
	@Autowired
	ApplicationContext context;
	Comparable<String> key;
	BarraHistograma[] barras;
	double extremoInferior, extremoSuperior;
	int cantidadIntervalos;
	@Autowired
	TablaSimple tablaSimple;

	public HistogramaSimuladosObservadosImpl() {
		super();
	}

	@Bean
	@Scope(value = "prototype")
	static public HistogramaSimuladosObservados histogramaObservadas() {
		return new HistogramaSimuladosObservadosImpl();
	}

	@PostConstruct
	public void init() {
		tablaSimple.getClaves().add("k");
		tablaSimple.getClaves().add("Intervalo");
		tablaSimple.getClaves().add("fo");
		tablaSimple.getClaves().add("fe");
	}

	@Override
	public void generar(Intervalo[] intervalos) {
		key = "Frecuencias observadas";
		generarBarras(intervalos);
	}

	@Override
	public void clear() {
		tablaSimple.getDetalles().clear();
	}

	@Override
	public Comparable<String> getKey() {
		return key;
	}

	@Override
	public BarraHistograma[] getBarras() {
		return barras;
	}

	@Override
	public Intervalo[] getIntervalos() {
		Intervalo[] intervalos = new Intervalo[barras.length];
		for (int i = 0; i < barras.length; i++)
			intervalos[i] = barras[i].getIntervalo();
		return intervalos;
	}

	@Override
	public int getCantidadIntervalos() {
		return cantidadIntervalos;
	}

	@Override
	public void setCantidadIntervalos(int cantidadIntervalos) {
	}

	@Override
	public TablaSimple getTabla() {
		return tablaSimple;
	}

	@Override
	public double getExtremoInferior() {
		return extremoInferior;
	}

	@Override
	public double getExtremoSuperior() {
		return extremoSuperior;
	}

	private void generarBarras(Intervalo[] intervalos) {
		barras = new BarraHistograma[intervalos.length];
		BarraHistograma barra;
		for (int i = 0; i < barras.length; i++) {
			barra = (BarraHistograma) context.getBean("barraHistograma");
			barra.setHistograma(this);
			barra.setFrecuencia(0);
			barra.setIntervalo(intervalos[i]);
			barras[i] = barra;
		}
		if (barras.length > 0) {
			extremoInferior = barras[0].getInferior().getValor();
			extremoSuperior = barras[barras.length - 1].getSuperior().getValor();
			cantidadIntervalos = intervalos.length;
		}
	}

	@Override
	public void generarTabla(TipoDistribucion tipoDistribucion, Map<String, Double> parametros) {
		for (int i = 0; i < barras.length; i++) {
			DetalleTabla d = (DetalleTabla) context.getBean("detalleTabla");
			d.getValores().put("k", i + 1);
			d.getValores().put("Intervalo", barras[i].toString());
			double fo = barras[i].getFrecuencia();
			d.getValores().put("fo", fo);
			double fe = tipoDistribucion.getFrecuenciaEsperada(parametros, barras[i].getIntervalo());
			d.getValores().put("fe", fe);
			tablaSimple.getDetalles().add(d);
		}
	}

	@Override
	public void incrementarFrecuencia(Intervalo i) {
		for (BarraHistograma b : barras)
			if (b.getIntervalo().equals(i))
				b.setFrecuencia(b.getFrecuencia() + 1d);
	}

}
