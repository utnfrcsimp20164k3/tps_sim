package app.utils.grafica;

import app.utils.TablaSimple;

public interface GraficoBarras {

	void clear();

	BarraGraficoBarras [] getBarras();
	
	int getCantidadNumeros();

	TablaSimple getTabla();

	double getExtremoSuperior();

	double getExtremoInferior();
	
	Comparable<?> getKey();

	Double[] getNumeros();

}
