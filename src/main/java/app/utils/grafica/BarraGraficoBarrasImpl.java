package app.utils.grafica;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
public class BarraGraficoBarrasImpl implements BarraGraficoBarras {

	GraficoBarras graficoBarras;
	double frecuencia;
	Double numero;

	public BarraGraficoBarrasImpl() {
		super();
	}

	@Bean
	@Scope(value = "prototype")
	public BarraGraficoBarras barraGraficoBarras() {
		return new BarraGraficoBarrasImpl();
	}

	@Override
	public double getFrecuencia() {
		return frecuencia;
	}

	@Override
	public void setFrecuencia(double frecuencia) {
		this.frecuencia = frecuencia;
	}

	@Override
	public GraficoBarras getGraficoBarras() {
		return graficoBarras;
	}

	@Override
	public void setGraficoBarras(GraficoBarras histograma) {
		this.graficoBarras = histograma;
	}

	@Override
	public double getNumero() {
		return numero;
	}

	@Override
	public String toString() {
		return String.format("%.0f", numero);
	}

	@Override
	public void setNumero(double numero) {
		this.numero = numero;
	}

}
