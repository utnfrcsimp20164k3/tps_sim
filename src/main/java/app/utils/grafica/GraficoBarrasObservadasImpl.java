package app.utils.grafica;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import app.utils.DetalleTabla;
import app.utils.SerieVariableDiscreta;
import app.utils.TablaSimple;

@Component
public class GraficoBarrasObservadasImpl implements GraficoBarrasObservadas {

	static final Logger LOG = LoggerFactory.getLogger(GraficoBarrasObservadasImpl.class);
	@Autowired
	ApplicationContext context;
	Comparable<String> key;
	SerieVariableDiscreta serie;
	BarraGraficoBarras[] barras;
	double extremoInferior, extremoSuperior;
	@Autowired
	TablaSimple tablaSimple;

	public GraficoBarrasObservadasImpl() {
		super();
	}

	@Bean
	@Scope(value = "prototype")
	static public GraficoBarras graficoBarrasObservadas() {
		return new GraficoBarrasObservadasImpl();
	}

	@PostConstruct
	public void init() {
		tablaSimple.getClaves().add("k");
		tablaSimple.getClaves().add("Clase");
		tablaSimple.getClaves().add("fo");
		tablaSimple.getClaves().add("fe");
	}

	@Override
	public void generar(SerieVariableDiscreta serie, double extremoInferior, double extremoSuperior) {
		key = "Frecuencias observadas";
		this.serie = serie;
		this.extremoInferior = extremoInferior;
		this.extremoSuperior = extremoSuperior;
		generarBarras();
		determinarFrecuencias();
		generarTabla();
	}

	@Override
	public void clear() {
		serie = null;
		barras = null;
		tablaSimple.getDetalles().clear();
		extremoInferior = 0d;
		extremoSuperior = 0d;
	}

	@Override
	public Comparable<String> getKey() {
		return key;
	}

	@Override
	public BarraGraficoBarras[] getBarras() {
		return barras;
	}

	@Override
	public Double[] getNumeros() {
		Set<Double> aux = new HashSet<>();
		for (int i = 0; i < serie.getArray().length; i++)
			aux.add(serie.getArray()[i]);
		Double[] numeros = new Double[aux.size()];
		int i = 0;
		for (Double d : aux) {
			numeros[i] = d;
			i++;
		}
		Arrays.sort(numeros);
		return numeros;
	}

	private void determinarFrecuencias() {
		for (Double d : serie.getArray()) {
			for (int i = 0; i < barras.length; i++)
				if (barras[i].getNumero() == d) {
					barras[i].setFrecuencia(barras[i].getFrecuencia() + 1d);
					break;
				}
		}
	}

	@Override
	public TablaSimple getTabla() {
		return tablaSimple;
	}

	@Override
	public double getExtremoInferior() {
		return extremoInferior;
	}

	@Override
	public double getExtremoSuperior() {
		return extremoSuperior;
	}

	private void generarBarras() {
		Double[] numeros = getNumeros();
		barras = new BarraGraficoBarras[numeros.length];
		BarraGraficoBarras barra;
		for (int i = 0; i < barras.length; i++) {
			barra = (BarraGraficoBarras) context.getBean("barraGraficoBarras");
			barra.setGraficoBarras(this);
			barra.setFrecuencia(0);
			barra.setNumero(numeros[i]);
			barras[i] = barra;
		}
	}

	private void generarTabla() {
		for (int i = 0; i < barras.length; i++) {
			DetalleTabla d = (DetalleTabla) context.getBean("detalleTabla");
			d.getValores().put("k", i + 1);
			d.getValores().put("Clase", barras[i].toString());
			double fo = barras[i].getFrecuencia();
			d.getValores().put("fo", fo);
			double fe = serie.getTipoDistribucion().getFrecuenciaEsperada(serie, barras[i].getNumero());
			d.getValores().put("fe", fe);
			tablaSimple.getDetalles().add(d);
		}
	}

	@Override
	public int getCantidadNumeros() {
		return barras.length;
	}

}
