package app.utils.grafica;

import app.utils.SerieVariableDiscreta;

public interface GraficoBarrasObservadas extends GraficoBarras {

	void generar(SerieVariableDiscreta serieVariableDiscreta, double extremoInferior, double extremoSuperior);
	
}
