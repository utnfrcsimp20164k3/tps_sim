package app.utils.grafica;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import app.utils.ExtremoDeIntervalo;
import app.utils.Intervalo;

@Component
public class BarraHistogramaImpl implements BarraHistograma {

	Histograma histograma;
	double frecuencia;
	@Autowired
	Intervalo intervalo;

	public BarraHistogramaImpl() {
		super();
	}

	@Bean
	@Scope(value = "prototype")
	public BarraHistograma barraHistograma() {
		return new BarraHistogramaImpl();
	}

	@Override
	public double getFrecuencia() {
		return frecuencia;
	}

	@Override
	public void setFrecuencia(double frecuencia) {
		this.frecuencia = frecuencia;
	}

	@Override
	public ExtremoDeIntervalo getInferior() {
		return intervalo.getInferior();
	}

	@Override
	public void setInferior(ExtremoDeIntervalo inferior) {
		intervalo.setInferior(inferior);;
	}

	@Override
	public ExtremoDeIntervalo getSuperior() {
		return intervalo.getSuperior();
	}

	@Override
	public void setSuperior(ExtremoDeIntervalo superior) {
		intervalo.setSuperior(superior);
	}

	@Override
	public Histograma getHistograma() {
		return histograma;
	}

	@Override
	public void setHistograma(Histograma histograma) {
		this.histograma = histograma;
	}

	@Override
	public Intervalo getIntervalo() {
		return intervalo;
	}

	@Override
	public void setIntervalo(Intervalo intervalo) {
		this.intervalo = intervalo;
	}

	@Override
	public String toString() {
		StringBuffer s = new StringBuffer();
		if (getInferior().isAbierto())
			s.append("( ");
		else
			s.append("[ ");
		s.append(String.format("%.4f", getInferior().getValor()));
		s.append(" ; ");
		s.append(String.format("%.4f", getSuperior().getValor()));
		if (getSuperior().isAbierto())
			s.append(" )");
		else
			s.append("]");
		return s.toString();
	}

}
