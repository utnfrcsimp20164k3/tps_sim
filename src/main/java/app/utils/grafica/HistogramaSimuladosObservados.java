package app.utils.grafica;

import java.util.Map;

import app.utils.Intervalo;
import app.utils.distribucion.TipoDistribucion;

public interface HistogramaSimuladosObservados extends Histograma {
	
	void generar(Intervalo[] intervalos);

	void generarTabla(TipoDistribucion tipoDistribucion, Map<String, Double> parametros);

	void incrementarFrecuencia(Intervalo i);

}
