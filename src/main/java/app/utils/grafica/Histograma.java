package app.utils.grafica;

import app.utils.Intervalo;
import app.utils.TablaSimple;

public interface Histograma {

	void clear();

	BarraHistograma[] getBarras();
	
	int getCantidadIntervalos();
	
	void setCantidadIntervalos(int cantidadIntervalos);
	
	TablaSimple getTabla();

	double getExtremoSuperior();

	double getExtremoInferior();
	
	Comparable<?> getKey();

	Intervalo[] getIntervalos();

}
