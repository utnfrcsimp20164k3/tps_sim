package app.utils.grafica;

public interface BarraGraficoBarras {
	
	GraficoBarras getGraficoBarras();
	
	void setGraficoBarras(GraficoBarras graficoBarras);

	double getFrecuencia();

	void setFrecuencia(double frecuencia);

	double getNumero();
	
	void setNumero(double numero);

}
