package app.utils.grafica;

import app.utils.SerieVariableContinua;

public interface HistogramaEsperadas extends Histograma {

	void generar(SerieVariableContinua serieVariableContinua, HistogramaObservadas histogramaObservadas);

}
