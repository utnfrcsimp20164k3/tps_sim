package app.utils.grafica;

import app.utils.ExtremoDeIntervalo;
import app.utils.Intervalo;

public interface BarraHistograma {
	
	Histograma getHistograma();
	
	void setHistograma(Histograma histograma);

	double getFrecuencia();

	void setFrecuencia(double frecuencia);

	ExtremoDeIntervalo getInferior();

	void setInferior(ExtremoDeIntervalo inferior);

	ExtremoDeIntervalo getSuperior();

	void setSuperior(ExtremoDeIntervalo superior);
	
	Intervalo getIntervalo();
	
	void setIntervalo(Intervalo intervalo);
}
