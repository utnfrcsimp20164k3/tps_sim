package app.utils.grafica;

import app.utils.SerieVariableDiscreta;

public interface GraficoBarrasEsperadas extends GraficoBarras {

	void generar(SerieVariableDiscreta serieVariableDiscreta, GraficoBarrasObservadas graficoBarrasObservadas);

}
