package app.utils.grafica;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import app.utils.DetalleTabla;
import app.utils.SerieVariableDiscreta;
import app.utils.TablaSimple;

@Component
public class GraficoBarrasEsperadasImpl implements GraficoBarrasEsperadas {

	static final Logger LOG = LoggerFactory.getLogger(GraficoBarrasEsperadasImpl.class);
	@Autowired
	ApplicationContext context;
	Comparable<String> key;
	SerieVariableDiscreta serie;
	BarraGraficoBarras[] barras;
	double extremoInferior, extremoSuperior;
	int cantidadNumeros;
	@Autowired
	TablaSimple tablaSimple;

	public GraficoBarrasEsperadasImpl() {
		super();
	}

	@Bean
	@Scope(value = "prototype")
	static public GraficoBarras graficoBarrasEsperadas() {
		return new GraficoBarrasEsperadasImpl();
	}

	@PostConstruct
	public void init() {
		tablaSimple.getClaves().add("k");
		tablaSimple.getClaves().add("Clase");
		tablaSimple.getClaves().add("fe");
	}

	@Override
	public void generar(SerieVariableDiscreta serie, GraficoBarrasObservadas graficoBarrasObservadas) {
		key = "Frecuencias esperadas";
		this.serie = serie;
		this.cantidadNumeros = graficoBarrasObservadas.getCantidadNumeros();
		this.extremoInferior = graficoBarrasObservadas.getExtremoInferior();
		this.extremoSuperior = graficoBarrasObservadas.getExtremoSuperior();
		generarBarras(cantidadNumeros, extremoInferior, extremoSuperior);
		generarTabla();
	}

	@Override
	public void clear() {
		serie = null;
		barras = null;
		cantidadNumeros = 0;
		extremoInferior = 0d;
		extremoSuperior = 0d;
	}

	@Override
	public Comparable<String> getKey() {
		return key;
	}

	@Override
	public BarraGraficoBarras[] getBarras() {
		return barras;
	}

	@Override
	public Double[] getNumeros() {
		Set<Double> aux = new HashSet<>();
		for (int i = 0; i < serie.getArray().length; i++)
			aux.add(serie.getArray()[i]);
		Double[] numeros = new Double[aux.size()];
		int i = 0;
		for (Double d : aux) {
			numeros[i] = d;
			i++;
		}
		Arrays.sort(numeros);
		return numeros;
	}

	@Override
	public TablaSimple getTabla() {
		return tablaSimple;
	}

	@Override
	public double getExtremoInferior() {
		return extremoInferior;
	}

	@Override
	public double getExtremoSuperior() {
		return extremoSuperior;
	}

	private void generarBarras(int cantidad, double min, double max) {
		Double[] numeros = getNumeros();
		barras = new BarraGraficoBarras[numeros.length];
		BarraGraficoBarras barra;
		for (int i = 0; i < barras.length; i++) {
			barra = (BarraGraficoBarras) context.getBean("barraGraficoBarras");
			barra.setGraficoBarras(this);
			barra.setNumero(numeros[i]);
			barra.setFrecuencia(serie.getTipoDistribucion().getFrecuenciaEsperada(serie,
					barra.getNumero()));
			barras[i] = barra;
		}
	}

	private void generarTabla() {
		for (int i = 0; i < barras.length; i++) {
			DetalleTabla d = (DetalleTabla) context.getBean("detalleTabla");
			d.getValores().put("k", i + 1);
			d.getValores().put("Clase", barras[i].toString());
			double fe = serie.getTipoDistribucion().getFrecuenciaEsperada(serie,
					barras[i].getNumero());
			d.getValores().put("fe", fe);
			tablaSimple.getDetalles().add(d);
		}
	}

	@Override
	public int getCantidadNumeros() {
		return barras.length;
	}

}
