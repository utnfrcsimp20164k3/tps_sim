package app.utils;

import app.utils.Observable;
import app.utils.Observer;

public interface VectorEstados<T> extends Observer, Observable {
	
	DetalleTabla parse(T t);

	Object[] getClaves();
	
}
