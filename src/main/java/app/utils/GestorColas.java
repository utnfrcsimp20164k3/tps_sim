package app.utils;

import app.utils.Observable;
import app.utils.Observer;

public interface GestorColas extends Ubicacion, Observer, Observable {
	
	void agregarCola(Cola<?,?> cola);
	
	void removerCola(Cola<?,?> cola);
	
	Object[] getClaves();
	
}
