package app.utils;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
public class TablaSimpleImpl implements TablaSimple {

	List<String> encabezados;
	List<DetalleTabla> detalles;
	List<Acumulador> acumuladores;
	@Autowired
	ApplicationContext applicationContext;

	public TablaSimpleImpl() {
		super();
		encabezados = new ArrayList<>();
		detalles = new ArrayList<>();
		acumuladores = new ArrayList<>();
	}

	@Bean
	@Scope("prototype")
	static public TablaSimple tablaSimple() {
		return new TablaSimpleImpl();
	}

	@Override
	public List<String> getClaves() {
		return encabezados;
	}

	@Override
	public List<DetalleTabla> getDetalles() {
		return detalles;
	}

	@Override
	public void acumuladorCantidad(String encabezado, String encabezadoAcumulador) {
		crearColumna(encabezadoAcumulador);
		Acumulador a = (Acumulador) applicationContext.getBean("acumuladorCantidad");
		a.setKeys(new String[]{ encabezado });
		a.setKeyAcumulador(encabezadoAcumulador);
		acumuladores.add(a);
	}

	@Override
	public void acumuladorUnidad(String encabezado1, String encabezado2, String encabezadoAcumulador) {
		crearColumna(encabezadoAcumulador);
		Acumulador a = (Acumulador) applicationContext.getBean("acumuladorUnidad");
		a.setKeys(new String[]{ encabezado1, encabezado2 });
		a.setKeyAcumulador(encabezadoAcumulador);
		acumuladores.add(a);
	}

	@Override
	public void acumuladorUnidad(String encabezado, String encabezadoAcumulador) {
		crearColumna(encabezadoAcumulador);
		Acumulador a = (Acumulador) applicationContext.getBean("acumuladorUnidad");
		a.setKeys(new String[]{ encabezado });
		a.setKeyAcumulador(encabezadoAcumulador);
		acumuladores.add(a);
	}

	@Override
	public void crearColumna(String encabezado) {
		if (!encabezados.contains(encabezado))
			encabezados.add(encabezado);
	}

	@Override
	public DetalleTabla acumular(DetalleTabla anterior, DetalleTabla actual) {
		for (Acumulador a : acumuladores)
			a.acumular(anterior, actual);
		return actual;
	}

}
