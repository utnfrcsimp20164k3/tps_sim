package app.utils;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
public class EstadoClienteSiendoServido implements EstadoCliente {
	
	public EstadoClienteSiendoServido() {
		super();
	}

	@Bean
	@Scope(value = "prototype")
	static public EstadoCliente estadoClienteSiendoServido() {
		return new EstadoClienteSiendoServido();
	}

	@Override
	public String toString() {
		return "Siendo servido";
	}

	@Override
	public boolean isEnCola() {
		return false;
	}

	@Override
	public boolean isEsperandoServicio() {
		return false;
	}

	@Override
	public boolean isRechazadoPorCola() {
		return false;
	}

	@Override
	public boolean isRechazadoPorGestorColas() {
		return false;
	}

	@Override
	public boolean isSiendoServido() {
		return true;
	}

	@Override
	public boolean isRechazadoPorServidor() {
		return false;
	}

	@Override
	public boolean isServido() {
		return false;
	}

}
