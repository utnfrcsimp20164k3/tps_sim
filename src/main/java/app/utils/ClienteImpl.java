package app.utils;

import java.util.HashSet;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import app.Loggable;

@Component
public class ClienteImpl implements Cliente {

	static final Logger LOG = LoggerFactory.getLogger(ClienteImpl.class);
	private Set<Observer> observers;
	private boolean changed;
	private TipoServicio tipoServicio;
	private boolean esperandoServicio, recibiendoServicio;
	private Servidor servidor;

	public ClienteImpl() {
		super();
		observers = new HashSet<>();
	}

	@Bean
	@Scope(value = "prototype")
	static public Cliente cliente() {
		return new ClienteImpl();
	}

	@Override
	public void declararseEsperandoServicio() {
		doLog("trace", "declararseEsperandoServicio(TipoServicio tipoServicio)", "[{]", null, null);
		changed = !esperandoServicio;
		esperandoServicio = true;
		notifyObservers();
		doLog("trace", "declararseEsperandoServicio(TipoServicio tipoServicio)", "[}]", null, null);
	}

	@Override
	public void servicioDisponible(Servidor servidor) {
		doLog("trace", "servicioDisponible(Servidor servidor)", "[{]", null, null);
		if (esperandoServicio && servidor.getTipoServicio().equals(tipoServicio)) {
			this.servidor = servidor;
			servidor.empezar();
			esperandoServicio = false;
			changed = !recibiendoServicio;
			recibiendoServicio = true;
			notifyObservers();
		}
		doLog("trace", "servicioDisponible(Servidor servidor)", "[}]", null, null);
	}

	@Override
	public void servicioTerminado(Servidor servidor) {
		doLog("trace", "servicioTerminado(Servidor servidor)", "[{]", null, null);
		if (recibiendoServicio && servidor.getTipoServicio().equals(tipoServicio)) {
			this.servidor = null;
			recibiendoServicio = false;
			changed = true;
			notifyObservers();
		}
		doLog("trace", "servicioTerminado(Servidor servidor)", "[{]", null, null);
	}

	@Override
	public boolean isEsperandoServicio() {
		return esperandoServicio;
	}

	@Override
	public boolean isRecibiendoServicio() {
		return recibiendoServicio;
	}

	@Override
	public TipoServicio getTipoServicio() {
		return tipoServicio;
	}

	@Override
	public void setTipoServicio(TipoServicio tipoServicio) {
		this.tipoServicio = tipoServicio;
	}

	@Override
	public Servidor getServidor() {
		return servidor;
	}

	@Override
	public void addObserver(Observer o) {
		observers.add(o);
	}

	@Override
	public void removeObserver(Observer o) {
		observers.remove(o);
	}

	@Override
	public int countObservers() {
		return observers.size();
	}

	@Override
	public void deleteObserver(Observer o) {
		observers.remove(o);
	}

	@Override
	public void deleteObservers() {
		observers.clear();
	}

	@Override
	public boolean hasChanged() {
		return changed;
	}

	@Override
	public void notifyObservers() {
		if (hasChanged())
			for (Observer obs : observers)
				obs.update(this, null);
		clearChanged();
	}

	@Override
	public void notifyObservers(Object o) {
		if (hasChanged())
			for (Observer obs : observers)
				obs.update(this, o);
		clearChanged();
	}

	protected void clearChanged() {
		changed = false;
	}

	private void doLog(String level, String signature, String corchetes, String nombreVariable, Loggable loggable) {
		switch (level) {
		case "trace":
			if (LOG.isTraceEnabled())
				LOG.trace(toLogMsg(signature, corchetes, nombreVariable, loggable));
			break;
		case "debug":
			if (LOG.isDebugEnabled())
				LOG.debug(toLogMsg(signature, corchetes, nombreVariable, loggable));
			break;
		case "info":
			if (LOG.isInfoEnabled())
				LOG.info(toLogMsg(signature, corchetes, nombreVariable, loggable));
			break;
		case "warn":
			if (LOG.isWarnEnabled())
				LOG.warn(toLogMsg(signature, corchetes, nombreVariable, loggable));
			break;
		case "error":
			if (LOG.isErrorEnabled())
				LOG.error(toLogMsg(signature, corchetes, nombreVariable, loggable));
			break;
		}
	}

	private String toLogMsg(String signature, String corchetes, String nombreVariable, Loggable loggable) {
		StringBuffer s = new StringBuffer(signature);
		if (corchetes != null)
			s.append(" " + corchetes);
		if (nombreVariable != null)
			s.append(" " + "{ l = " + nombreVariable + " }");
		if (nombreVariable != null && loggable == null)
			s.append(nombreVariable + ": " + loggable);
		else if (nombreVariable != null && loggable != null)
			s.append(loggable.toLogString("l"));
		return s.toString();
	}

	@Override
	public String toLogString(String nombre) {
		nombre = nombre != null ? nombre : "l";
		StringBuffer s = new StringBuffer();
		s.append(" " + nombre + ": " + this);
		s.append(", " + nombre + ".tipoServicio: " + tipoServicio);
		s.append(", " + nombre + ".esperandoServicio: " + new Boolean(esperandoServicio));
		s.append(", " + nombre + ".recibiendoServicio: " + new Boolean(recibiendoServicio));
		return s.toString();
	}

}
