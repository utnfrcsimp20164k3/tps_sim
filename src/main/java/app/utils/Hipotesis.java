package app.utils;

import app.utils.distribucion.TipoDistribucion;

public interface Hipotesis {
	
	boolean isAceptada();
	
	TipoDistribucion getTipoDistribucion();
	
	void setTipoDistribucion(TipoDistribucion tipo);

}
