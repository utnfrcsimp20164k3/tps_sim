package app.utils;

public interface SerieFactory {
	
	Serie crearSerie();
	
	void setTipoVariable(String tipoVariable);

}
