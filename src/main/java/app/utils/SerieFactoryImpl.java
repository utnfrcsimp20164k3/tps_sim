package app.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
public class SerieFactoryImpl implements SerieFactory {
	
	static final Logger LOG = LoggerFactory.getLogger(SerieFactoryImpl.class);
	String tipoVariable;
	@Autowired
	ApplicationContext applicationContext;

	@Bean
	@Scope(value = "prototype")
	static public SerieFactory serieFactory() {
		return new SerieFactoryImpl();
	}

	@Override
	public Serie crearSerie() {
		Serie serie = null;
		if (tipoVariable.equals("Continua"))
			serie = (Serie) applicationContext.getBean("serieVariableContinua");
		if (tipoVariable.equals("Discreta"))
			serie = (Serie) applicationContext.getBean("serieVariableDiscreta");
		return serie;
	}

	@Override
	public void setTipoVariable(String tipoVariable) {
		this.tipoVariable = tipoVariable;
	}

}
