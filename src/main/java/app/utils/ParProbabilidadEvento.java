package app.utils;

import java.math.BigDecimal;

import org.zkoss.bind.annotation.Immutable;

public class ParProbabilidadEvento implements Par<BigDecimal, Object> {
	
	BigDecimal probabilidad;
	Object evento;

	public ParProbabilidadEvento(BigDecimal probabilidad, Object evento) {
		super();
		this.probabilidad = probabilidad;
		this.evento = evento;
	}

	@Override
	@Immutable
	public BigDecimal getLeft() {
		return probabilidad;
	}

	@Override
	public void setLeft(BigDecimal probabilidad) {
		this.probabilidad = probabilidad;
		
	}

	@Override
	public Object getRight() {
		return evento;
	}

	@Override
	public void setRight(Object evento) {
		this.evento = evento;
	}

}
