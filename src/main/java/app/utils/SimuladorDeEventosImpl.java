package app.utils;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import app.utils.distribucion.TipoDistribucion;
import app.utils.generador.GeneradorPseudoaleatorio;
import app.utils.grafica.HistogramaSimuladosObservados;

@Component
public class SimuladorDeEventosImpl implements SimuladorDeEventos {

	static final Logger LOG = LoggerFactory.getLogger(SimuladorDeEventosImpl.class);
	@Autowired
	ApplicationContext applicationContext;
	@Autowired
	HistogramaSimuladosObservados histogramaSimuladosObservados;
	TipoDistribucion tipoDistribucion;
	GeneradorPseudoaleatorio generadorPseudoaleatorio;
	List<ParProbabilidadEvento> paresProbabilidadEvento;
	Map<Intervalo, Object> mapaIntervaloEvento;
	Intervalo[] intervalos;
	Random random;

	public SimuladorDeEventosImpl() {
		super();
		paresProbabilidadEvento = new ArrayList<>();
		mapaIntervaloEvento = new HashMap<>();
		random = new Random();
	}
	
	@Bean
	@Scope(value = "prototype")
	public SimuladorDeEventos simuladorDeEventos() {
		return new SimuladorDeEventosImpl();
	}

	@Override
	public void asociar(BigDecimal probabilidad, Object evento) {
		paresProbabilidadEvento.add(new ParProbabilidadEvento(probabilidad, evento));
	}

	@Override
	public ParRandomEvento next() {
		Double rnd = random.nextDouble();
		Object evento = null;
		for (Intervalo i : intervalos)
			if (i.incluye((Double) generadorPseudoaleatorio.next(rnd))) {
				evento = mapaIntervaloEvento.get(i);
				histogramaSimuladosObservados.incrementarFrecuencia(i);
			}
		return new ParRandomEvento(rnd, evento);
	}

	@Override
	public void setTipoDistribucion(TipoDistribucion tipoDistribucion) {
		this.tipoDistribucion = tipoDistribucion;
	}

	private void construirIntervalos() {
		intervalos = new Intervalo[paresProbabilidadEvento.size()];
		BigDecimal extremoSuperior = new BigDecimal("0");
		for (ParProbabilidadEvento p : paresProbabilidadEvento)
			extremoSuperior = extremoSuperior.add(p.getLeft());
		BigDecimal i = new BigDecimal("0");
		for (int j = 1; j <= intervalos.length; j++) {
			Intervalo intervalo = (Intervalo) applicationContext.getBean("intervalo");
			ExtremoDeIntervalo inferior = (ExtremoDeIntervalo) applicationContext.getBean("inferior");
			inferior.setAbierto(false);
			inferior.setValor(i.doubleValue());
			ExtremoDeIntervalo superior = (ExtremoDeIntervalo) applicationContext.getBean("superior");
			superior.setAbierto(true);
			superior.setValor(i.add(paresProbabilidadEvento.get(j - 1).getLeft()).doubleValue());
			intervalo.setInferior(inferior);
			intervalo.setSuperior(superior);
			asociar(intervalo, paresProbabilidadEvento.get(j - 1).getRight());
			intervalos[j - 1] = intervalo;
			i = i.add(paresProbabilidadEvento.get(j - 1).getLeft());
		}
		Arrays.sort(intervalos, Intervalo.comparator());
		construirHistograma(intervalos);
	}
	
	private void asociar(Intervalo intervalo, Object evento) {
		mapaIntervaloEvento.put(intervalo, evento);
	}

	private void construirHistograma(Intervalo[] intervalos) {
		histogramaSimuladosObservados.generar(intervalos);
	}

	@Override
	public void clear() {
		paresProbabilidadEvento.clear();
		random = new Random();
	}

	@Override
	public void setup() {
		if (tipoDistribucion == null)
			throw new RuntimeException("No se asoció a este simulador con un tipo de distribución.");
		if (paresProbabilidadEvento.isEmpty())
			throw new RuntimeException("No se asoció a este simulador con una tabla de probabilidades.");
		construirIntervalos();
		generadorPseudoaleatorio = tipoDistribucion.getDefaultGenerator();
		generadorPseudoaleatorio.setExtremoInferior(histogramaSimuladosObservados.getExtremoInferior());
		generadorPseudoaleatorio.setExtremoSuperior(histogramaSimuladosObservados.getExtremoSuperior());
	}

}
