package app.utils;

import java.util.List;
import java.util.Set;

import app.Loggable;

public interface Tabla extends Loggable {
	
	Set<Set<Object>> getClaves();

	List<DetalleTabla> getDetalles();

	void acumuladorCantidad(Object clave, Object claveAcumulador);

	void acumuladorCantidad(Set<Object> clave, Object claveAcumulador);

	void acumuladorUnidad(Object clave, Object claveAcumulador);

	void acumuladorUnidad(Set<Object> claves, Object claveAcumulador);

	void acumuladorUnidad(Object clave1, Object clave2, Object claveAcumulador);

	void acumuladorUnidad(Set<Object> claves1, Set<Object> claves2, Object claveAcumulador);

	void crearColumna(Object clave);

	void crearColumna(Set<Object> claves);

	DetalleTabla acumular(DetalleTabla anterior, DetalleTabla actual);

}
