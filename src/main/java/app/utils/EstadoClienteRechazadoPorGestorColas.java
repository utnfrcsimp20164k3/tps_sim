package app.utils;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
public class EstadoClienteRechazadoPorGestorColas implements EstadoCliente {
	
	public EstadoClienteRechazadoPorGestorColas() {
		super();
	}

	@Bean
	@Scope(value = "prototype")
	static public EstadoCliente estadoRechazadoPorGestorColas() {
		return new EstadoClienteRechazadoPorGestorColas();
	}

	@Override
	public String toString() {
		return "Rechazado";
	}

	@Override
	public boolean isEnCola() {
		return false;
	}

	@Override
	public boolean isEsperandoServicio() {
		return false;
	}

	@Override
	public boolean isRechazadoPorCola() {
		return false;
	}

	@Override
	public boolean isRechazadoPorGestorColas() {
		return true;
	}

	@Override
	public boolean isSiendoServido() {
		return false;
	}

	@Override
	public boolean isRechazadoPorServidor() {
		return false;
	}

	@Override
	public boolean isServido() {
		return false;
	}

}
