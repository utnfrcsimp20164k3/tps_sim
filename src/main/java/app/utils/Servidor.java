package app.utils;

import app.Loggable;

public interface Servidor extends Ubicacion, Observable, Loggable {
	
	void setTipoServicio(TipoServicio tipoServicio);
	
	TipoServicio getTipoServicio();
	
	void ofrecerServicio(Cliente cliente);
	
	void terminar();
	
	boolean isSirviendo();

	void empezar();

}
