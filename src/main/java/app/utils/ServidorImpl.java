package app.utils;

import java.util.HashSet;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import app.Loggable;

@Component
public class ServidorImpl implements Servidor {

	static final Logger LOG = LoggerFactory.getLogger(ServidorImpl.class);
	private Set<Observer> observers;
	private boolean changed;
	private TipoServicio tipoServicio;
	private boolean sirviendo;

	public ServidorImpl() {
		super();
		observers = new HashSet<>();
	}

	@Bean
	@Scope(value = "prototype")
	static public Servidor servidor() {
		return new ServidorImpl();
	}

	@Override
	public TipoServicio getTipoServicio() {
		return tipoServicio;
	}

	@Override
	public void setTipoServicio(TipoServicio tipoServicio) {
		this.tipoServicio = tipoServicio;
	}

	@Override
	public void ofrecerServicio(Cliente cliente) {
		cliente.servicioDisponible(this);
	}

	@Override
	public void empezar() {
		doLog("trace", "empezar()", "[{]", null, null);
		sirviendo = true;
		changed = true;
		notifyObservers();
		doLog("trace", "empezar()", "[}]", null, null);
	}

	@Override
	public void terminar() {
		doLog("trace", "terminar()", "[{]", null, null);
		sirviendo = false;
		changed = true;
		notifyObservers();
		doLog("trace", "terminar()", "[}]", null, null);
	}

	@Override
	public boolean isSirviendo() {
		return sirviendo;
	}

	@Override
	public void addObserver(Observer o) {
		observers.add(o);
	}

	@Override
	public void removeObserver(Observer o) {
		observers.remove(o);
	}

	@Override
	public int countObservers() {
		return observers.size();
	}

	@Override
	public void deleteObserver(Observer o) {
		observers.remove(o);
	}

	@Override
	public void deleteObservers() {
		observers.clear();
	}

	@Override
	public boolean hasChanged() {
		return changed;
	}

	@Override
	public void notifyObservers() {
		if (hasChanged())
			for (Observer obs : observers)
				obs.update(this, null);
		clearChanged();
	}

	@Override
	public void notifyObservers(Object o) {
		if (hasChanged())
			for (Observer obs : observers)
				obs.update(this, o);
		clearChanged();
	}

	protected void clearChanged() {
		changed = false;
	}

	private void doLog(String level, String signature, String corchetes, String nombreVariable, Loggable loggable) {
		switch (level) {
		case "trace":
			if (LOG.isTraceEnabled())
				LOG.trace(toLogMsg(signature, corchetes, nombreVariable, loggable));
			break;
		case "debug":
			if (LOG.isDebugEnabled())
				LOG.debug(toLogMsg(signature, corchetes, nombreVariable, loggable));
			break;
		case "info":
			if (LOG.isInfoEnabled())
				LOG.info(toLogMsg(signature, corchetes, nombreVariable, loggable));
			break;
		case "warn":
			if (LOG.isWarnEnabled())
				LOG.warn(toLogMsg(signature, corchetes, nombreVariable, loggable));
			break;
		case "error":
			if (LOG.isErrorEnabled())
				LOG.error(toLogMsg(signature, corchetes, nombreVariable, loggable));
			break;
		}
	}

	private String toLogMsg(String signature, String corchetes, String nombreVariable, Loggable loggable) {
		StringBuffer s = new StringBuffer(signature);
		if (corchetes != null)
			s.append(" " + corchetes);
		if (nombreVariable != null)
			s.append(" " + "{ l = " + nombreVariable + " }");
		if (nombreVariable != null && loggable == null)
			s.append(nombreVariable + ": " + loggable);
		else if (nombreVariable != null && loggable != null)
			s.append(loggable.toLogString("l"));
		return s.toString();
	}

	@Override
	public String toLogString(String nombre) {
		nombre = nombre != null ? nombre : "l";
		StringBuffer s = new StringBuffer();
		s.append(" " + nombre + ": " + this);
		s.append(", " + nombre + ".tipoServicio: " + tipoServicio);
		s.append(", " + nombre + ".sirviendo: " + new Boolean(sirviendo));
		return s.toString();
	}

}
