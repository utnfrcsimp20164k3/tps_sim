package app.utils;

import java.math.BigDecimal;

import app.utils.distribucion.TipoDistribucion;

public interface SimuladorDeEventos {
	
	void asociar(BigDecimal probabilidad, Object evento);
	
	ParRandomEvento next();
	
	void setTipoDistribucion(TipoDistribucion tipoDistribucion);
	
	void clear();
	
	void setup();

}
