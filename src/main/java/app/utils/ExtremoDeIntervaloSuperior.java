package app.utils;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
public class ExtremoDeIntervaloSuperior implements ExtremoDeIntervalo {

	Double valor;

	boolean abierto;

	public ExtremoDeIntervaloSuperior() {
		super();
	}

	@Bean
	@Scope(value = "prototype")
	static public ExtremoDeIntervalo superior() {
		return new ExtremoDeIntervaloSuperior();
	}

	@Override
	public Double getValor() {
		return valor;
	}

	@Override
	public void setValor(Double valor) {
		this.valor = valor;
	}

	@Override
	public boolean isAbierto() {
		return abierto;
	}

	@Override
	public void setAbierto(boolean abierto) {
		this.abierto = abierto;
	}

	@Override
	public boolean equals(Object extremoDeIntervaloSuperior) {
		return extremoDeIntervaloSuperior != null && extremoDeIntervaloSuperior instanceof ExtremoDeIntervaloSuperior
				&& getValor() != null && ((ExtremoDeIntervaloSuperior) extremoDeIntervaloSuperior).getValor() != null
				&& getValor().equals(((ExtremoDeIntervaloSuperior) extremoDeIntervaloSuperior).getValor())
				&& new Boolean(abierto)
						.equals(new Boolean(((ExtremoDeIntervalo) extremoDeIntervaloSuperior).isAbierto()));
	}

}
