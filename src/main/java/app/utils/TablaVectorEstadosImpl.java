package app.utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import app.Loggable;

@Component
public class TablaVectorEstadosImpl implements TablaVectorEstados {

	static final Logger LOG = LoggerFactory.getLogger(TablaVectorEstadosImpl.class);
	private Set<Set<Object>> claves;
	private List<DetalleTabla> detalles;
	private List<Acumulador> acumuladores;
	private Map<Class<?>, Set<Permanente>> permanentes;
	private Map<Class<?>, Set<Temporal>> temporales;
	private Map<Class<?>, Contador> contadores;
	@Autowired
	private ApplicationContext applicationContext;

	public TablaVectorEstadosImpl() {
		super();
		claves = new HashSet<>();
		detalles = new ArrayList<>();
		acumuladores = new ArrayList<>();
		temporales = new HashMap<>();
		permanentes = new HashMap<>();
		contadores = new HashMap<>();
	}

	@Bean
	@Scope("prototype")
	static public TablaVectorEstados tablaVectorEstados() {
		return new TablaVectorEstadosImpl();
	}

	@Override
	public Set<Set<Object>> getClaves() {
		return claves;
	}

	@Override
	public List<DetalleTabla> getDetalles() {
		return detalles;
	}

	@Override
	public void acumuladorCantidad(Object clave, Object claveAcumulador) {
		Set<Object> aux = new HashSet<>();
		aux.add(clave);
		acumuladorCantidad(aux, claveAcumulador);
	}

	@Override
	public void acumuladorCantidad(Set<Object> clave, Object claveAcumulador) {
		crearColumna(claveAcumulador);
		Acumulador a = (Acumulador) applicationContext.getBean("acumuladorCantidad");
		a.setKeys(new Object[] { clave });
		a.setKeyAcumulador(claveAcumulador);
		acumuladores.add(a);
	}

	@Override
	public void acumuladorUnidad(Object clave, Object claveAcumulador) {
		Set<Object> aux = new HashSet<>();
		aux.add(clave);
		acumuladorUnidad(aux, claveAcumulador);
	}

	@Override
	public void acumuladorUnidad(Set<Object> claves, Object claveAcumulador) {
		crearColumna(claveAcumulador);
		Acumulador a = (Acumulador) applicationContext.getBean("acumuladorUnidad");
		a.setKeys(new Object[] { claves });
		a.setKeyAcumulador(claveAcumulador);
		acumuladores.add(a);
	}

	@Override
	public void acumuladorUnidad(Object clave1, Object clave2, Object claveAcumulador) {
		Set<Object> aux1 = new HashSet<>();
		aux1.add(claves);
		Set<Object> aux2 = new HashSet<>();
		aux2.add(claves);
		acumuladorUnidad(aux1, aux2, claveAcumulador);
	}

	@Override
	public void acumuladorUnidad(Set<Object> claves1, Set<Object> claves2, Object claveAcumulador) {
		crearColumna(claveAcumulador);
		Acumulador a = (Acumulador) applicationContext.getBean("acumuladorUnidad");
		a.setKeys(new Object[] { claves1, claves2 });
		a.setKeyAcumulador(claveAcumulador);
		acumuladores.add(a);
	}

	@Override
	public void crearColumna(Object clave) {
		Set<Object> aux = new HashSet<>();
		aux.add(clave);
		crearColumna(aux);
	}

	@Override
	public void crearColumna(Set<Object> claves) {
		claves.add(claves);
	}

	@Override
	public DetalleTabla acumular(DetalleTabla anterior, DetalleTabla actual) {
		for (Acumulador a : acumuladores)
			a.acumular(anterior, actual);
		return actual;
	}

	@Override
	public boolean isAMostrar(Temporal temporal) {
		return contadores.get(temporal.getClass()).isAMostrar(temporal);
	}

	@Override
	public void mostrar(Class<?> clase, int primero, int cantidad) {
		contadores.put(clase, new Contador(primero, cantidad));
	}

	@Override
	public void registrarTemporal(Temporal temporal) {
		if (!temporales.containsKey(temporal.getClass()))
			temporales.put(temporal.getClass(), new HashSet<>());
		temporales.get(temporal.getClass()).add(temporal);
		contadores.get(temporal.getClass()).contar();
	}

	@Override
	public void registrarPermanente(Permanente permanente) {
		doLog("trace", "registrarPermanente(Permanente permanente)", "[{]", null, null);
		if (!permanentes.containsKey(permanente.getClass()))
			permanentes.put(permanente.getClass(), new HashSet<>());
		permanentes.get(permanente.getClass()).add(permanente);
		doLog("trace", "registrarPermanente(Permanente permanente)", "[}]", null, null);
	}

	class Contador {

		int primero, cantidad, contados;

		public Contador(int primero, int cantidad) {
			super();
			this.primero = primero;
			this.cantidad = cantidad;
		}

		boolean isAMostrar(Numerable numerable) {
			return cantidad - contados > 0 && numerable.getNumero() > primero
					&& numerable.getNumero() < primero + cantidad;
		}

		void contar() {
			contados++;
		}

	}

	@Override
	public Set<Temporal> obtenerTemporales(Class<?> clase) {
		return temporales.get(clase);
	}

	@Override
	public Set<Permanente> obtenerPermanentes(Class<?> clase) {
		doLog("trace", "obtenerPermanentes(Class<?> clase)", "[{]", null, null);
		LOG.debug("obtenerPermanentes(Class<?> clase) [0] clase: " + clase);
		doLog("trace", "obtenerPermanentes(Class<?> clase)", "[}]", null, null);
		return permanentes.get(clase);
	}

	private void doLog(String level, String signature, String corchetes, String nombreVariable, Loggable loggable) {
		switch (level) {
		case "trace":
			if (LOG.isTraceEnabled())
				LOG.trace(toLogMsg(signature, corchetes, nombreVariable, loggable));
			break;
		case "debug":
			if (LOG.isDebugEnabled())
				LOG.debug(toLogMsg(signature, corchetes, nombreVariable, loggable));
			break;
		case "info":
			if (LOG.isInfoEnabled())
				LOG.info(toLogMsg(signature, corchetes, nombreVariable, loggable));
			break;
		case "warn":
			if (LOG.isWarnEnabled())
				LOG.warn(toLogMsg(signature, corchetes, nombreVariable, loggable));
			break;
		case "error":
			if (LOG.isErrorEnabled())
				LOG.error(toLogMsg(signature, corchetes, nombreVariable, loggable));
			break;
		}
	}

	private String toLogMsg(String signature, String corchetes, String nombreVariable, Loggable loggable) {
		StringBuffer s = new StringBuffer(signature);
		if (corchetes != null)
			s.append(" " + corchetes);
		if (nombreVariable != null)
			s.append(" " + "{ l = " + nombreVariable + " }");
		if (nombreVariable != null && loggable == null)
			s.append(nombreVariable + ": " + loggable);
		else if (nombreVariable != null && loggable != null)
			s.append(loggable.toLogString("l"));
		return s.toString();
	}

	@Override
	public String toLogString(String nombre) {
		nombre = nombre != null ? nombre : "l";
		StringBuffer s = new StringBuffer();
		s.append(" " + nombre + ": " + this);
		s.append(", " + nombre + ".detalles: " + detallesToString());
		return s.toString();
	}

	private String detallesToString() {
		StringBuffer s = new StringBuffer();
		s.append("[ ");
		for (DetalleTabla d : detalles) {
			s.append(d.toLogString("d"));
			if (!d.equals(detalles.get(detalles.size() - 1)))
				s.append(d.toLogString(", "));
			else
				s.append(d.toLogString(" ]"));
		}
		return s.toString();
	}

}
