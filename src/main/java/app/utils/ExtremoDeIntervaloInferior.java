package app.utils;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
public class ExtremoDeIntervaloInferior implements ExtremoDeIntervalo {

	Double valor;

	boolean abierto;

	public ExtremoDeIntervaloInferior() {
		super();
	}

	@Bean
	@Scope(value = "prototype")
	static public ExtremoDeIntervalo inferior() {
		return new ExtremoDeIntervaloInferior();
	}

	@Override
	public Double getValor() {
		return valor;
	}

	@Override
	public void setValor(Double valor) {
		this.valor = valor;
	}

	@Override
	public boolean isAbierto() {
		return abierto;
	}

	@Override
	public void setAbierto(boolean abierto) {
		this.abierto = abierto;
	}

	@Override
	public boolean equals(Object extremoDeIntervaloInferior) {
		return extremoDeIntervaloInferior != null && extremoDeIntervaloInferior instanceof ExtremoDeIntervaloInferior
				&& getValor() != null && ((ExtremoDeIntervaloInferior) extremoDeIntervaloInferior).getValor() != null
				&& getValor().equals(((ExtremoDeIntervaloInferior) extremoDeIntervaloInferior).getValor())
				&& new Boolean(abierto)
						.equals(new Boolean(((ExtremoDeIntervalo) extremoDeIntervaloInferior).isAbierto()));
	}

}
