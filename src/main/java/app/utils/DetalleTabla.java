package app.utils;

import java.util.Map;

import app.Loggable;

public interface DetalleTabla extends Loggable {

	Map<Object, Object> getValores();

}
