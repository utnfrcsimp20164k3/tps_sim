package app.utils;

public interface Observable {

	void addObserver(Observer o);
	
	void removeObserver(Observer o);

	int countObservers();

	void deleteObserver(Observer o);

	void deleteObservers();

	boolean	hasChanged();

	void notifyObservers();

	void notifyObservers(Object arg);

}
