package app.utils;

public interface Par<L, R> {
	
	L getLeft();
	
	void setLeft(L left);
	
	R getRight();
	
	void setRight(R right);

}
