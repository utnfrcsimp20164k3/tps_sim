package app.utils;

import java.util.ArrayDeque;
import java.util.Deque;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import app.utils.distribucion.TipoDistribucionVariableDiscreta;
import app.utils.grafica.GraficoBarras;
import app.utils.grafica.GraficoBarrasEsperadas;
import app.utils.grafica.GraficoBarrasObservadas;

@Component
public class SerieVariableDiscretaImpl implements SerieVariableDiscreta {

	static final Logger LOG = LoggerFactory.getLogger(SerieVariableDiscretaImpl.class);
	@Autowired
	private GraficoBarrasObservadas graficoBarrasObservadas;
	@Autowired
	private GraficoBarrasEsperadas graficoBarrasEsperadas;
	private double[] array;
	private Double extremoInferior, extremoSuperior;
	private TipoDistribucionVariableDiscreta tipoDistribucion;
	private Deque<Hipotesis> hipotesis;

	public SerieVariableDiscretaImpl() {
		super();
		hipotesis = new ArrayDeque<>();
	}

	@Bean
	@Scope(value = "prototype")
	static public SerieVariableDiscreta serieVariableDiscreta() {
		return new SerieVariableDiscretaImpl();
	}

	@Override
	public void generarGraficas() {
		Double min = null, max = null;
		if (extremoInferior != null)
			min = extremoInferior;
		if (tipoDistribucion.getExtremoInferiorTeorico() != null)
			min = tipoDistribucion.getExtremoInferiorTeorico();
		if (extremoSuperior != null)
			max = extremoSuperior;
		if (min == null || max == null)
			determinarExtremos();
		if (min == null)
			min = this.extremoInferior;
		if (max == null)
			max = extremoSuperior;
		generarGraficoBarrasObservadas(min, max);
		generarGraficosBarrasEsperadas();
	}

	private void determinarExtremos() {
		extremoInferior = array[0];
		extremoSuperior = array[0];
		for (double d : array) {
			if (extremoSuperior < d)
				extremoSuperior = d;
			if (extremoInferior > d)
				extremoInferior = d;
		}
	}

	private void generarGraficoBarrasObservadas(Double min, Double max) {
		graficoBarrasObservadas.generar(this, min, max);
	}

	@Override
	public GraficoBarras getGraficoBarrasObservadas() {
		return graficoBarrasObservadas;
	}

	private void generarGraficosBarrasEsperadas() {
		graficoBarrasEsperadas.generar(this, graficoBarrasObservadas);
	}

	@Override
	public GraficoBarras getGraficoBarrasEsperadas() {
		return graficoBarrasEsperadas;
	}

	@Override
	public void clear() {
		this.extremoInferior = null;
		this.extremoSuperior = null;
		clearGraficoBarras();
	}

	@Override
	public void clearGraficoBarras() {
		graficoBarrasObservadas.clear();
		graficoBarrasEsperadas.clear();
	}

	@Override
	public double[] getArray() {
		return array;
	}

	@Override
	public void setArray(double[] array) {
		this.array = array;
	}

	@Override
	public Hipotesis exponerHipotesis() {
		return hipotesis.getLast();
	}

	@Override
	public TipoDistribucionVariableDiscreta getTipoDistribucion() {
		return tipoDistribucion;
	}

	@Override
	public void postularHipotesis(Hipotesis hipotesis) {
		if (tipoDistribucion == null)
			tipoDistribucion = (TipoDistribucionVariableDiscreta) hipotesis.getTipoDistribucion();
		this.hipotesis.addLast(hipotesis);
	}

	@Override
	public int getCantidadNumeros() {
		return array != null ? array.length : 0;
	}

	@Override
	public Double getExtremoInferior() {
		return extremoInferior;
	}

	@Override
	public void setExtremoInferior(double extremoInferior) {
		this.extremoInferior = extremoInferior;
	}

	@Override
	public Double getExtremoSuperior() {
		return extremoSuperior;
	}

	@Override
	public void setExtremoSuperior(double extremoSuperior) {
		this.extremoSuperior = extremoSuperior;
	}

	@Override
	public boolean hasGraficoBarras() {
		return graficoBarrasObservadas.getCantidadNumeros() > 0 || graficoBarrasEsperadas.getCantidadNumeros() > 0;
	}

	@Override
	public boolean hasGrafica() {
		return hasGraficoBarras();
	}

}
