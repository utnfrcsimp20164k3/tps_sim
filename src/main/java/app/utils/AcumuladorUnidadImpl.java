package app.utils;

import java.math.BigDecimal;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
public class AcumuladorUnidadImpl implements Acumulador {

	Object[] keys;
	Object keyAcumulador;
	
	@Bean
	@Scope("prototype")
	static public Acumulador acumuladorUnidad() {
		return new AcumuladorUnidadImpl();
	}

	@Override
	public void acumular(DetalleTabla anterior, DetalleTabla actual) {
		if (anterior.getValores().get(keyAcumulador) instanceof BigDecimal)
			acumular(anterior, actual, (BigDecimal) anterior.getValores().get(keyAcumulador));
		else if (anterior.getValores().get(keyAcumulador) instanceof Long)
			acumular(anterior, actual, (Long) anterior.getValores().get(keyAcumulador));
		else if (anterior.getValores().get(keyAcumulador) instanceof Integer)
			acumular(anterior, actual, (Integer) anterior.getValores().get(keyAcumulador));
	}
	
	private void acumular(DetalleTabla anterior, DetalleTabla actual, BigDecimal valor) {
		if (hasNulos(actual))
			actual.getValores().put(keyAcumulador, (BigDecimal) anterior.getValores().get(keyAcumulador));
		else
			actual.getValores().put(keyAcumulador, ((BigDecimal) anterior.getValores().get(keyAcumulador)).add(new BigDecimal("1")));
	}
	
	private void acumular(DetalleTabla anterior, DetalleTabla actual, Long valor) {
		if (hasNulos(actual))
			actual.getValores().put(keyAcumulador, (Long) anterior.getValores().get(keyAcumulador));
		else
			actual.getValores().put(keyAcumulador, ((Long) anterior.getValores().get(keyAcumulador)) + 1l );
	}

	private void acumular(DetalleTabla anterior, DetalleTabla actual, Integer valor) {
		if (hasNulos(actual))
			actual.getValores().put(keyAcumulador, (Integer) anterior.getValores().get(keyAcumulador));
		else
			actual.getValores().put(keyAcumulador, ((Integer) anterior.getValores().get(keyAcumulador)) + 1 );
	}

	private boolean hasNulos(DetalleTabla actual) {
		boolean hasNulos = false;
		for (int i = 0; i < keys.length; i++)
			if (actual.getValores().get(keys[i]) == null) {
				hasNulos = true;
				break;
			}
		return hasNulos;
		
	}

	@Override
	public void setKeys(Object[] keys) {
		this.keys = keys;
	}

	@Override
	public void setKeyAcumulador(Object keyAcumulador) {
		this.keyAcumulador = keyAcumulador;
	}

}
