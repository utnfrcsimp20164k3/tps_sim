package app.utils.distribucion;

import app.utils.SerieVariableDiscreta;

public interface TipoDistribucionVariableDiscreta extends TipoDistribucion {

	double getFrecuenciaEsperada(SerieVariableDiscreta serieVariableDiscreta, double numero);
	
	Double getExtremoInferiorTeorico();
	
}
