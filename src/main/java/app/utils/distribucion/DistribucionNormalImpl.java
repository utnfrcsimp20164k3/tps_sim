package app.utils.distribucion;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import app.utils.Intervalo;
import app.utils.SerieVariableContinua;
import app.utils.generador.GeneradorNormal;

@Component
public class DistribucionNormalImpl implements DistribucionNormal {

	static final Logger LOG = LoggerFactory.getLogger(DistribucionNormalImpl.class);

	@Autowired
	ApplicationContext applicationContext;

	public DistribucionNormalImpl() {
		super();
	}

	@Bean
	@Scope("prototype")
	static public DistribucionNormal distribucionNormal() {
		return new DistribucionNormalImpl();
	}

	@Override
	public double getFrecuenciaEsperada(SerieVariableContinua serieVariableContinua, Intervalo intervalo) {
		return serieVariableContinua.getCantidadNumeros() * getProbabilidadEsperada(serieVariableContinua, calcularMarcaClase(intervalo)) * intervalo.getAncho();
	}

	private double calcularMarcaClase(Intervalo intervalo) {
		return intervalo.getInferior().getValor() + ((intervalo.getSuperior().getValor() - intervalo.getInferior().getValor()) / 2d);
	}

	private double calcularDesviacion(SerieVariableContinua serieVariableContinua) {
		double desviacion = Math.sqrt(
				(1d / (new Double(serieVariableContinua.getArray().length) - 1d)) * calcularSumatoriaDesviacion(serieVariableContinua));
		return desviacion;
	}

	private double calcularSumatoriaDesviacion(SerieVariableContinua serieVariableContinua) {
		double sumatoria = 0d;
		double media = calcularMedia(serieVariableContinua);
		for (double d : serieVariableContinua.getArray())
			sumatoria += Math.pow(d - media, 2d);
		return sumatoria;
	}

	private double calcularMedia(SerieVariableContinua serieVariableContinua) {
		double media = 0d, sumatoria = 0d;
		for (double d : serieVariableContinua.getArray())
			sumatoria += d;
		media = sumatoria / new Double(serieVariableContinua.getArray().length);
		return media;
	}

	private double calcularZ(SerieVariableContinua serieVariableContinua, double marcaClase) {
		return (marcaClase - calcularMedia(serieVariableContinua)) / calcularDesviacion(serieVariableContinua);
	}

	private double getProbabilidadEsperada(SerieVariableContinua serieVariableContinua, double marcaClase) {
		return (1d / (calcularDesviacion(serieVariableContinua) * Math.sqrt(2d * Math.PI)))
				* (Math.exp((-1d / 2d) * Math.pow(calcularZ(serieVariableContinua, marcaClase), 2d)));
	}

	@Override
	public Double getExtremoInferiorTeorico() {
		return null;
	}

	@Override
	public double getFrecuenciaEsperada(Map<String, Double> parametros, Intervalo intervalo) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public GeneradorNormal getDefaultGenerator() {
		return (GeneradorNormal) applicationContext.getBean("generadorNormalConvolucion");
	}

}
