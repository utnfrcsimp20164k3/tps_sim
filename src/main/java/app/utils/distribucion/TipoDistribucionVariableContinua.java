package app.utils.distribucion;

import app.utils.Intervalo;
import app.utils.SerieVariableContinua;

public interface TipoDistribucionVariableContinua extends TipoDistribucion {

	double getFrecuenciaEsperada(SerieVariableContinua serieVariableContinua, Intervalo intervalo);
	
	Double getExtremoInferiorTeorico();
	
}
