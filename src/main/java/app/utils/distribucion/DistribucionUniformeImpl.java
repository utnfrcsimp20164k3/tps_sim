package app.utils.distribucion;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import app.utils.Intervalo;
import app.utils.SerieVariableContinua;
import app.utils.generador.GeneradorPseudoaleatorio;
import app.utils.generador.GeneradorUniformeTransformadaInversaAB;

@Component
public class DistribucionUniformeImpl implements DistribucionUniforme {
	
	@Autowired
	GeneradorUniformeTransformadaInversaAB generadorUniformeTransformadaInversaAB;

	public DistribucionUniformeImpl() {
		super();
	}

	@Bean
	@Scope("prototype")
	static public DistribucionUniforme distribucionUniforme() {
		return new DistribucionUniformeImpl();
	}

	@Override
	public double getFrecuenciaEsperada(SerieVariableContinua serieVariableContinua, Intervalo intervalo) {
		return serieVariableContinua.getCantidadNumeros() / new Double(serieVariableContinua.getHistogramaObservadas().getCantidadIntervalos());
	}

	@Override
	public Double getExtremoInferiorTeorico() {
		return null;
	}

	@Override
	public double getFrecuenciaEsperada(Map<String, Double> parametros, Intervalo intervalo) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public GeneradorPseudoaleatorio getDefaultGenerator() {
		return generadorUniformeTransformadaInversaAB;
	}

}
