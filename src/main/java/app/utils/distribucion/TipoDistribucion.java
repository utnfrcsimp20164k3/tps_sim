package app.utils.distribucion;

import java.util.Map;

import app.utils.Intervalo;
import app.utils.generador.GeneradorAB;
import app.utils.generador.GeneradorPseudoaleatorio;

public interface TipoDistribucion {

	double getFrecuenciaEsperada(Map<String, Double> parametros, Intervalo intervalo);

	GeneradorPseudoaleatorio getDefaultGenerator();

}
