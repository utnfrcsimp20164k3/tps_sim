package app.utils.distribucion;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import app.utils.ExtremoDeIntervalo;
import app.utils.Intervalo;
import app.utils.SerieVariableContinua;
import app.utils.generador.GeneradorExponencial;

@Component
public class DistribucionExponencialImpl implements DistribucionExponencial {
	
	@Autowired
	ApplicationContext applicationContext;
	Double lambda;

	public DistribucionExponencialImpl() {
		super();
	}

	@Bean
	@Scope("prototype")
	static public DistribucionExponencial distribucionExponencial() {
		return new DistribucionExponencialImpl();
	}

	@Override
	public double getFrecuenciaEsperada(SerieVariableContinua serieVariableContinua, Intervalo intervalo) {
		lambda = null;
		return serieVariableContinua.getCantidadNumeros() * calcularAreaIntervalo(serieVariableContinua, intervalo);
	}

	private void calcularLambda(SerieVariableContinua serieVariableContinua) {
		double sumatoria = 0d;
		for (int i = 0; i < serieVariableContinua.getArray().length; i++)
			sumatoria += serieVariableContinua.getArray()[i];
		lambda = 1 / (sumatoria / new Integer(serieVariableContinua.getCantidadNumeros()).doubleValue());
	}

	private double calcularProbabilidadAcumulada(SerieVariableContinua serieVariableContinua, ExtremoDeIntervalo extremo) {
		if (lambda == null)
			calcularLambda(serieVariableContinua);
		return 1d - (Math.exp(-1d * lambda * extremo.getValor()));
	}

	private double calcularAreaIntervalo(SerieVariableContinua serieVariableContinua, Intervalo intervalo) {
		return calcularProbabilidadAcumulada(serieVariableContinua, intervalo.getSuperior())
				- calcularProbabilidadAcumulada(serieVariableContinua, intervalo.getInferior());
	}

	@Override
	public Double getExtremoInferiorTeorico() {
		return 0d;
	}

	@Override
	public double getFrecuenciaEsperada(Map<String, Double> parametros, Intervalo intervalo) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public GeneradorExponencial getDefaultGenerator() {
		return (GeneradorExponencial) applicationContext.getBean("generadorExponencial");
	}

}
