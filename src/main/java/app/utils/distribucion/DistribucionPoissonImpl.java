package app.utils.distribucion;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import app.utils.Intervalo;
import app.utils.SerieVariableDiscreta;
import app.utils.generador.GeneradorPoisson;
import app.utils.grafica.BarraGraficoBarras;

@Component
public class DistribucionPoissonImpl implements DistribucionPoisson {

	static final Logger LOG = LoggerFactory.getLogger(DistribucionPoissonImpl.class);

	@Autowired
	ApplicationContext applicationContext;
	double lambda, sumatoriaFrecuencias;

	public DistribucionPoissonImpl() {
		super();
	}

	@Bean
	@Scope("prototype")
	static public DistribucionPoisson distribucionPoisson() {
		return new DistribucionPoissonImpl();
	}

	@Override
	public double getFrecuenciaEsperada(SerieVariableDiscreta serie, double numero) {
		sumatoriaFrecuencias = 0d;
		calcularLambda(serie);
		return getProbabilidadEsperada(numero) * sumatoriaFrecuencias;
	}

	private double getProbabilidadEsperada(double numero) {
		return (Math.exp((-1) * lambda)) * Math.pow(lambda, numero) / factorial(numero);
	}

	private void calcularLambda(SerieVariableDiscreta serie) {
		BarraGraficoBarras[] barras = serie.getGraficoBarrasObservadas().getBarras();
		double numeradorLambda = 0d;
		for (BarraGraficoBarras b : barras) {
			sumatoriaFrecuencias += b.getFrecuencia();
			numeradorLambda += b.getNumero() * b.getFrecuencia();
		}
		lambda = numeradorLambda / sumatoriaFrecuencias;
	}

	private double factorial(double numero) {
		if (numero == 0d)
			return 1d;
		else
			return numero * factorial(numero - 1d);
	}

	@Override
	public Double getExtremoInferiorTeorico() {
		return null;
	}

	@Override
	public double getFrecuenciaEsperada(Map<String, Double> parametros, Intervalo intervalo) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public GeneradorPoisson getDefaultGenerator() {
		return (GeneradorPoisson) applicationContext.getBean("generadorPoisson");
	}

}
