package app.utils;

import java.util.ArrayDeque;
import java.util.Deque;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import app.utils.distribucion.TipoDistribucionVariableContinua;
import app.utils.grafica.Histograma;
import app.utils.grafica.HistogramaEsperadas;
import app.utils.grafica.HistogramaEsperadasImpl;
import app.utils.grafica.HistogramaObservadas;

@Component
public class SerieVariableContinuaImpl implements SerieVariableContinua {

	static final Logger LOG = LoggerFactory.getLogger(HistogramaEsperadasImpl.class);
	@Autowired
	private HistogramaObservadas histogramaObservadas;
	@Autowired
	private HistogramaEsperadas histogramaEsperadas;
	private double[] array;
	private Double extremoInferior, extremoSuperior;
	private TipoDistribucionVariableContinua tipoDistribucionVariableContinua;
	private Deque<Hipotesis> hipotesis;

	public SerieVariableContinuaImpl() {
		super();
		hipotesis = new ArrayDeque<>();
	}

	@Bean
	@Scope(value = "prototype")
	static public SerieVariableContinua serieVariableContinua() {
		return new SerieVariableContinuaImpl();
	}

	@Override
	public void generarGraficas() {
		Double min = null, max = null;
		if (extremoInferior != null)
			min = extremoInferior;
		if (tipoDistribucionVariableContinua.getExtremoInferiorTeorico() != null)
			min = tipoDistribucionVariableContinua.getExtremoInferiorTeorico();
		if (extremoSuperior != null)
			max = extremoSuperior;
		if (min == null || max == null)
			determinarExtremos();
		if (min == null)
			min = this.extremoInferior;
		if (max == null)
			max = extremoSuperior;
		generarHistogramaObservadas(min, max);
		generarHistogramaEsperadas();
	}

	private void determinarExtremos() {
		extremoInferior = array[0];
		extremoSuperior = array[0];
		for (double d : array) {
			if (extremoSuperior < d)
				extremoSuperior = d;
			if (extremoInferior > d)
				extremoInferior = d;
		}
	}

	private void generarHistogramaObservadas(Double min, Double max) {
		histogramaObservadas.generar(this, min, max);
	}

	@Override
	public Histograma getHistogramaObservadas() {
		return histogramaObservadas;
	}

	private void generarHistogramaEsperadas() {
		histogramaEsperadas.generar(this, histogramaObservadas);
	}

	@Override
	public Histograma getHistogramaEsperadas() {
		return histogramaEsperadas;
	}

	@Override
	public void clear() {
		this.extremoInferior = null;
		this.extremoSuperior = null;
		clearHistogramas();
	}

	@Override
	public void clearHistogramas() {
		histogramaObservadas.clear();
		histogramaEsperadas.clear();
	}

	@Override
	public double[] getArray() {
		return array;
	}

	@Override
	public void setArray(double[] array) {
		this.array = array;
	}

	@Override
	public Hipotesis exponerHipotesis() {
		return hipotesis.getLast();
	}

	@Override
	public TipoDistribucionVariableContinua getTipoDistribucion() {
		return tipoDistribucionVariableContinua;
	}

	@Override
	public void postularHipotesis(Hipotesis hipotesis) {
		if (tipoDistribucionVariableContinua == null)
			tipoDistribucionVariableContinua = (TipoDistribucionVariableContinua) hipotesis.getTipoDistribucion();
		this.hipotesis.addLast(hipotesis);
	}

	@Override
	public int getCantidadNumeros() {
		return array != null ? array.length : 0;
	}

	@Override
	public Double getExtremoInferior() {
		return extremoInferior;
	}

	@Override
	public void setExtremoInferior(double extremoInferior) {
		this.extremoInferior = extremoInferior;
	}

	@Override
	public Double getExtremoSuperior() {
		return extremoSuperior;
	}

	@Override
	public void setExtremoSuperior(double extremoSuperior) {
		this.extremoSuperior = extremoSuperior;
	}

	@Override
	public boolean hasHistogramas() {
		return histogramaObservadas.getCantidadIntervalos() > 0 || histogramaEsperadas.getCantidadIntervalos() > 0;
	}

	@Override
	public boolean hasGrafica() {
		return hasHistogramas();
	}

}
