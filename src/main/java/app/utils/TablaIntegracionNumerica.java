package app.utils;

import java.math.BigDecimal;

public interface TablaIntegracionNumerica extends Tabla {

	void setH(BigDecimal h);

	BigDecimal getH();

}
