package app.utils;

public interface Observer {

	void update(Observable o, Object arg);

}
