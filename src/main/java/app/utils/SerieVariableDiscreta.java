package app.utils;

import app.utils.distribucion.TipoDistribucionVariableDiscreta;
import app.utils.grafica.GraficoBarras;

public interface SerieVariableDiscreta extends Serie {
	
	GraficoBarras getGraficoBarrasObservadas();

	GraficoBarras getGraficoBarrasEsperadas();

	void clearGraficoBarras();

	boolean hasGraficoBarras();
	
	TipoDistribucionVariableDiscreta getTipoDistribucion();

}
