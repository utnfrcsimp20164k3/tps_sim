package app.utils;

public interface Cola<C,S> extends Ubicacion {
	
	void setSize(Integer size);
	
	Integer getOcupacion();
	
	void next();
	
	boolean hayLugar();
	
	void agregarCliente(C cliente);

	void agregarServidor(S servidor);
	
	boolean isEmpty();

	Object getId();

}
