package app.utils;

public interface Serie {

	double[] getArray();
	
	Hipotesis exponerHipotesis();
	
	void postularHipotesis(Hipotesis hipotesis);
	
	int getCantidadNumeros();

	Double getExtremoInferior();

	void setExtremoInferior(double extremoInferior);

	Double getExtremoSuperior();
	
	void setExtremoSuperior(double extremoSuperior);

	void clear();
	
	void generarGraficas();

	void setArray(double[] array);
	
	boolean hasGrafica();

}
