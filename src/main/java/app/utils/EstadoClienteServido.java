package app.utils;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
public class EstadoClienteServido implements EstadoCliente {
	
	public EstadoClienteServido() {
		super();
	}

	@Bean
	@Scope(value = "prototype")
	static public EstadoCliente estadoClienteServido() {
		return new EstadoClienteServido();
	}

	@Override
	public String toString() {
		return "Rechazado";
	}

	@Override
	public boolean isEnCola() {
		return false;
	}

	@Override
	public boolean isEsperandoServicio() {
		return false;
	}

	@Override
	public boolean isRechazadoPorCola() {
		return false;
	}

	@Override
	public boolean isRechazadoPorGestorColas() {
		return false;
	}

	@Override
	public boolean isSiendoServido() {
		return false;
	}

	@Override
	public boolean isRechazadoPorServidor() {
		return false;
	}

	@Override
	public boolean isServido() {
		return true;
	}

}
