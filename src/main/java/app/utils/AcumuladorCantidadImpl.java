package app.utils;

import java.math.BigDecimal;
import java.math.RoundingMode;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
public class AcumuladorCantidadImpl implements Acumulador {

	Object[] keys;
	Object keyAcumulador;

	@Bean
	@Scope("prototype")
	static public Acumulador acumuladorCantidad() {
		return new AcumuladorCantidadImpl();
	}

	@Override
	public void acumular(DetalleTabla anterior, DetalleTabla actual) {
		if (anterior.getValores().get(keyAcumulador) instanceof BigDecimal)
			acumular(anterior, actual, (BigDecimal) anterior.getValores().get(keyAcumulador));
		else if (anterior.getValores().get(keyAcumulador) instanceof Long)
			acumular(anterior, actual, (Long) anterior.getValores().get(keyAcumulador));
	}

	private void acumular(DetalleTabla anterior, DetalleTabla actual, BigDecimal valor) {
		BigDecimal valorActual = new BigDecimal("0");
		valorActual = valorActual.setScale(2, RoundingMode.HALF_UP);
		BigDecimal acumuladoAnterior = (BigDecimal) anterior.getValores().get(keyAcumulador);
		for (int i = 0; i < keys.length; i++)
			valorActual = valorActual.add(actual.getValores().get(keys[i]) != null ? (BigDecimal) actual.getValores().get(keys[i]) : new BigDecimal("0"));
		actual.getValores().put(keyAcumulador, acumuladoAnterior.add(valorActual));
	}

	private void acumular(DetalleTabla anterior, DetalleTabla actual, Long valor) {
		Long valorActual = 0l;
		Long acumuladoAnterior = valor;
		for (int i = 0; i < keys.length; i++)
			valorActual = valorActual + (actual.getValores().get(keys[i]) != null ? (Long) actual.getValores().get(keys[i]) : 0l);
		actual.getValores().put(keyAcumulador, acumuladoAnterior + valorActual);
	}

	@Override
	public void setKeys(Object[] keys) {
		this.keys = keys;
	}

	@Override
	public void setKeyAcumulador(Object keyAcumulador) {
		this.keyAcumulador = keyAcumulador;
	}

}
