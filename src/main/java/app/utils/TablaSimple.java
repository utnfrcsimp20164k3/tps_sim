package app.utils;

import java.util.List;

public interface TablaSimple {
	
	List<String> getClaves();
	
	List<DetalleTabla> getDetalles();

	void acumuladorCantidad(String encabezado, String encabezadoAcumulador);
	
	void acumuladorUnidad(String encabezado, String encabezadoAcumulador);

	void acumuladorUnidad(String encabezado1, String encabezado2, String encabezadoAcumulador);
	
	void crearColumna(String encabezado);
	
	DetalleTabla acumular(DetalleTabla anterior, DetalleTabla actual);

}
