package app.utils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
public class IntervaloImpl implements Intervalo {

	@Autowired
	ExtremoDeIntervalo inferior, superior;

	@Bean
	@Scope(value = "prototype")
	public Intervalo intervalo() {
		return new IntervaloImpl();
	}

	@Override
	public ExtremoDeIntervalo getInferior() {
		return inferior;
	}

	@Override
	public void setInferior(ExtremoDeIntervalo inferior) {
		this.inferior = inferior;
	}

	@Override
	public ExtremoDeIntervalo getSuperior() {
		return superior;
	}

	@Override
	public void setSuperior(ExtremoDeIntervalo superior) {
		this.superior = superior;
	}

	@Override
	public double getAncho() {
		return superior.getValor() - inferior.getValor();
	}

	@Override
	public boolean incluye(double valor) {
		boolean pertenece = true;
		if (getInferior().isAbierto() && valor <= getInferior().getValor())
			pertenece = false;
		if (!getInferior().isAbierto() && valor < getInferior().getValor())
			pertenece = false;
		if (getSuperior().isAbierto() && valor >= getSuperior().getValor())
			pertenece = false;
		if (!getSuperior().isAbierto() && valor > getSuperior().getValor())
			pertenece = false;
		return pertenece;
	}

	@Override
	public boolean equals(Object intervalo) {
		return intervalo != null && intervalo instanceof Intervalo && getInferior() != null
				&& ((Intervalo) intervalo).getInferior() != null
				&& getInferior().equals(((Intervalo) intervalo).getInferior()) && getSuperior() != null
				&& ((Intervalo) intervalo).getSuperior() != null
				&& getSuperior().equals(((Intervalo) intervalo).getSuperior());
	}

}
