package app.utils;

import app.utils.Estado;

public interface EstadoServidor extends Estado {
	
	boolean isDisponible();
	
	boolean isSirviendo();
	
}
