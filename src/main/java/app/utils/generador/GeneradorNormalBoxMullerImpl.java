package app.utils.generador;

import java.util.Random;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import app.utils.distribucion.TipoDistribucionVariableContinua;

@Component
public class GeneradorNormalBoxMullerImpl implements GeneradorNormalBoxMuller {

	static final Logger LOG = LoggerFactory.getLogger(GeneradorNormalBoxMullerImpl.class);
	Random random;
	Long semilla;
	Double media, desviacion;
	@Autowired
	TipoDistribucionVariableContinua distribucionNormal;
	Double z1, z2;

	public GeneradorNormalBoxMullerImpl() {
		super();
		random = new Random();
	}

	@Bean
	@Scope(value = "prototype")
	static public GeneradorNormalBoxMuller generadorNormalBoxMuller() {
		return new GeneradorNormalBoxMullerImpl();
	}

	@Override
	public Number next() {
		Double nuevo = null;
		if (z1 == null && z2 == null) {
			generarNuevos();
			nuevo = new Double(z1);
			z1 = null;
		} else if (z1 == null && z2 != null) {
			nuevo = new Double(z2);
			z2 = null;
		}
		return nuevo;
	}

	@Override
	public Double next(double nextDouble) {
		// TODO Auto-generated method stub
		return null;
	}

	private void generarNuevos() {
		Double ri = random.nextDouble(), rii = random.nextDouble();
		z1 = (Math.sqrt(-2d * (Math.log(ri)))) * Math.sin(2 * Math.PI * rii) * desviacion + media;
		z2 = (Math.sqrt(-1d * 2d * (Math.log(ri)))) * Math.cos(2 * Math.PI * rii) * desviacion + media;
	}

	@Override
	public void clear() {
		semilla = null;
		random = new Random();
		z1 = null;
		z2 = null;
	}

	@Override
	public Long getSemilla() {
		return semilla;
	}

	@Override
	public void setSemilla(Long semilla) {
		this.semilla = semilla;
		if (semilla != null)
			random.setSeed(semilla);
	}

	@Override
	public TipoDistribucionVariableContinua getTipoDistribucion() {
		return distribucionNormal;
	}

	@Override
	public Double getExtremoInferior() {
		return null;
	}

	@Override
	public void setExtremoInferior(Double extremoInferior) {
	}

	@Override
	public Double getExtremoSuperior() {
		return null;
	}

	@Override
	public void setExtremoSuperior(Double extremoSuperior) {
	}

	@Override
	public Double getMedia() {
		return media;
	}

	@Override
	public Double getDesviacion() {
		return desviacion;
	}

	@Override
	public void setDesviacion(Double desviacion) {
		this.desviacion = desviacion;
	}

	@Override
	public void setMedia(Double media) {
		this.media = media;
	}

	@Override
	public Number getLambda() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setLambda(Number lambda) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public int getK() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void setK(int k) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Long getA() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setA(Long a) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Long getC() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setC(Long c) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Long getM() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setM(Long m) {
		// TODO Auto-generated method stub
		
	}

}
