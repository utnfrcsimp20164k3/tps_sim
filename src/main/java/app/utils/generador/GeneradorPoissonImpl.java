package app.utils.generador;

import java.util.Random;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import app.utils.distribucion.TipoDistribucion;

@Component
public class GeneradorPoissonImpl implements GeneradorPoisson {

	static final Logger LOG = LoggerFactory.getLogger(GeneradorPoissonImpl.class);
	Random random = new Random();
	private Long semilla;
	private Long lambda;
	@Autowired
	TipoDistribucion distribucionPoisson;

	public GeneradorPoissonImpl() {
		super();
		random = new Random();
	}

	@Bean
	@Scope(value="prototype")
	static public GeneradorPoisson generadorPoisson() {
		return new GeneradorPoissonImpl();
	}

	@Override
	public Number next() {
		double a = Math.exp(-lambda);
		double b = 1.0d;
		int i = 0;
		do {
			i++;
			b *= random.nextDouble();
		} while (b > a);
		return new Double(i - 1);
	}

	@Override
	public Double next(double nextDouble) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void clear() {
		semilla = null;
		lambda = null;
		random = new Random();
	}

	@Override
	public Long getSemilla() {
		return semilla;
	}

	@Override
	public void setSemilla(Long semilla) {
		this.semilla = semilla;
		random.setSeed(semilla);
	}

	@Override
	public TipoDistribucion getTipoDistribucion() {
		return distribucionPoisson;
	}

	@Override
	public Number getLambda() {
		return lambda;
	}

	@Override
	public void setLambda(Number lambda) {
		this.lambda = lambda.longValue();
	}

	@Override
	public Double getExtremoInferior() {
		return null;
	}

	@Override
	public void setExtremoInferior(Double extremoInferior) {
	}

	@Override
	public Double getExtremoSuperior() {
		return null;
	}

	@Override
	public void setExtremoSuperior(Double extremoSuperior) {
	}

	@Override
	public int getK() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void setK(int k) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Double getMedia() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Double getDesviacion() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setDesviacion(Double desviacion) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setMedia(Double media) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Long getA() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setA(Long a) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Long getC() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setC(Long c) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Long getM() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setM(Long m) {
		// TODO Auto-generated method stub
		
	}

}
