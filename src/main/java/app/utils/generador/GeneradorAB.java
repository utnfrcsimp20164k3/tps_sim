package app.utils.generador;

public interface GeneradorAB {

	Double next(Number rnd);

	void setA(Double a);

	void setB(Double b);

}
