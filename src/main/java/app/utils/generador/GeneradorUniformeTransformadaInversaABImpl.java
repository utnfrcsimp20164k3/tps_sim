package app.utils.generador;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import app.utils.distribucion.TipoDistribucion;

@Component
public class GeneradorUniformeTransformadaInversaABImpl implements GeneradorUniformeTransformadaInversaAB {
	
	Double a,b;
	
	@Bean
	@Scope("prototype")
	static public GeneradorUniformeTransformadaInversaAB generadorUniformeTransformadaInversaAB() {
		return new GeneradorUniformeTransformadaInversaABImpl();
	}

	public Double next(Number rnd) {
		Double n = null;
		n = a + rnd.doubleValue() * (b - a);
		return n;
	}
	
	public void setA(Double a) {
		this.a = a;
	}

	public void setB(Double b) {
		this.b = b;
	}

	@Override
	public Number next() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Double next(double rnd) {
		Double n = null;
		n = a + rnd * (b - a);
		return n;
	}

	@Override
	public void clear() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public TipoDistribucion getTipoDistribucion() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Double getExtremoInferior() {
		return a;
	}

	@Override
	public void setExtremoInferior(Double extremoInferior) {
		setA(extremoInferior);
	}

	@Override
	public Double getExtremoSuperior() {
		return b;
	}

	@Override
	public void setExtremoSuperior(Double extremoSuperior) {
		setB(extremoSuperior);
	}

	@Override
	public Number getLambda() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setLambda(Number lambda) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Long getSemilla() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setSemilla(Long semilla) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public int getK() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void setK(int k) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Double getMedia() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Double getDesviacion() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setDesviacion(Double desviacion) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setMedia(Double media) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Long getA() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setA(Long a) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Long getC() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setC(Long c) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Long getM() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setM(Long m) {
		// TODO Auto-generated method stub
		
	}

}
