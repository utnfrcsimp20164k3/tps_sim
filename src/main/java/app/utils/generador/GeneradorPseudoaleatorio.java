package app.utils.generador;

import app.utils.distribucion.TipoDistribucion;

public interface GeneradorPseudoaleatorio {

	Number getLambda();
	
	void setLambda(Number lambda);

	Long getSemilla();

	void setSemilla(Long semilla);

	Number next();
	
	Number next(double nextDouble);
	
	void clear();

	TipoDistribucion getTipoDistribucion();

	Double getExtremoInferior();

	void setExtremoInferior(Double extremoInferior);
	
	Double getExtremoSuperior();
	
	void setExtremoSuperior(Double extremoSuperior);

	int getK();

	void setK(int k);

	Double getMedia();
	
	Double getDesviacion();
	
	void setDesviacion(Double desviacion);

	void setMedia(Double media);

	Long getA();

	void setA(Long a);

	Long getC();

	void setC(Long c);

	Long getM();

	void setM(Long m);

}
