package app.utils.generador;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import app.utils.distribucion.TipoDistribucionVariableContinua;

@Component
public class GeneradorCongruencialLinealImpl implements GeneradorCongruencialLineal {

	static final Logger LOG = LoggerFactory.getLogger(GeneradorCongruencialLinealImpl.class);
	private Long x0;
	private Long xi;
	private Long a;
	private Long c;
	private Long m;
	@Autowired
	TipoDistribucionVariableContinua distribucionUniforme;

	@Bean
	@Scope(value="prototype")
	static public GeneradorCongruencialLineal generadorCongruencialLineal() {
		return new GeneradorCongruencialLinealImpl();
	}

	public Long getSemilla() {
		return x0;
	}

	@Override
	public void setSemilla(Long semilla) {
		this.x0 = semilla;
	}

	@Override
	public Long getA() {
		return a;
	}

	@Override
	public void setA(Long a) {
		this.a = a;
	}

	@Override
	public Long getC() {
		return c;
	}

	@Override
	public void setC(Long c) {
		this.c = c;
	}

	@Override
	public Long getM() {
		return m;
	}

	@Override
	public void setM(Long m) {
		this.m = m;
	}

	@Override
	public Number next() {
		Double ri;
		x0 = x0 == null ? 0 : x0;
		xi = xi == null ? x0 : xi;
		xi = (a * xi + c) % m;
		ri = new Double(xi) / new Double(m);
		return ri;
	}

	@Override
	public Double next(double nextDouble) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void clear() {
		xi = null;
	}

	@Override
	public TipoDistribucionVariableContinua getTipoDistribucion() {
		return distribucionUniforme;
	}

	@Override
	public Double getExtremoInferior() {
		return null;
	}

	@Override
	public void setExtremoInferior(Double extremoInferior) {
	}

	@Override
	public Double getExtremoSuperior() {
		return null;
	}

	@Override
	public void setExtremoSuperior(Double extremoSuperior) {
	}

	@Override
	public Number getLambda() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setLambda(Number lambda) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public int getK() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void setK(int k) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Double getMedia() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Double getDesviacion() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setDesviacion(Double desviacion) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setMedia(Double media) {
		// TODO Auto-generated method stub
		
	}

}
