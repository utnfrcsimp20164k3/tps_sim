package app.utils.generador;

import java.util.Random;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import app.Loggable;
import app.utils.distribucion.TipoDistribucionVariableContinua;

@Component
public class GeneradorExponencialImpl implements GeneradorExponencial {
	
	static final Logger LOG = LoggerFactory.getLogger(GeneradorExponencialImpl.class);
	private Random random;
	private Long semilla;
	private Double lambda;
	@Autowired
	private TipoDistribucionVariableContinua distribucionExponencial;

	public GeneradorExponencialImpl() {
		super();
		random = new Random();
	}

	@Bean
	@Scope(value="prototype")
	static public GeneradorExponencial generadorExponencial() {
		return new GeneradorExponencialImpl();
	}

	@Override
	public Number next() {
		return (-1d * (Math.log(1 - random.nextDouble())) / lambda);
	}

	@Override
	public Double next(double rnd) {
		doLog("trace", "next(double rnd)", "[{]", null, null);
		LOG.debug("next(double rnd) rnd: " + new Double(rnd));
		LOG.debug("next(double rnd) lambda: " + lambda);
		doLog("trace", "next(double rnd)", "[}]", null, null);
		return (-1d * (Math.log(1 - rnd)) / lambda);
	}

	@Override
	public void clear() {
		semilla = null;
		lambda = null;
		random = new Random();
	}

	@Override
	public Long getSemilla() {
		return semilla;
	}

	@Override
	public void setSemilla(Long semilla) {
		this.semilla = semilla;
		random.setSeed(semilla);
	}

	@Override
	public Number getLambda() {
		return lambda;
	}

	@Override
	public void setLambda(Number lambda) {
		if (lambda instanceof Double)
			this.lambda = (Double) lambda;
	}

	@Override
	public TipoDistribucionVariableContinua getTipoDistribucion() {
		return distribucionExponencial;
	}

	@Override
	public Double getExtremoInferior() {
		return null;
	}

	@Override
	public void setExtremoInferior(Double extremoInferior) {
	}

	@Override
	public Double getExtremoSuperior() {
		return null;
	}

	@Override
	public void setExtremoSuperior(Double extremoSuperior) {
	}

	@Override
	public int getK() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void setK(int k) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Double getMedia() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Double getDesviacion() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setDesviacion(Double desviacion) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setMedia(Double media) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Long getA() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setA(Long a) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Long getC() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setC(Long c) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Long getM() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setM(Long m) {
		// TODO Auto-generated method stub
		
	}

	private void doLog(String level, String signature, String corchetes, String nombreVariable, Loggable loggable) {
		switch (level) {
		case "trace":
			if (LOG.isTraceEnabled())
				LOG.trace(toLogMsg(signature, corchetes, nombreVariable, loggable));
			break;
		case "debug":
			if (LOG.isDebugEnabled())
				LOG.debug(toLogMsg(signature, corchetes, nombreVariable, loggable));
			break;
		case "info":
			if (LOG.isInfoEnabled())
				LOG.info(toLogMsg(signature, corchetes, nombreVariable, loggable));
			break;
		case "warn":
			if (LOG.isWarnEnabled())
				LOG.warn(toLogMsg(signature, corchetes, nombreVariable, loggable));
			break;
		case "error":
			if (LOG.isErrorEnabled())
				LOG.error(toLogMsg(signature, corchetes, nombreVariable, loggable));
			break;
		}
	}

	private String toLogMsg(String signature, String corchetes, String nombreVariable, Loggable loggable) {
		StringBuffer s = new StringBuffer(signature);
		if (corchetes != null)
			s.append(" " + corchetes);
		if (nombreVariable != null)
			s.append(" " + "{ l = " + nombreVariable + " }");
		if (nombreVariable != null && loggable == null)
			s.append(nombreVariable + ": " + loggable);
		else if (nombreVariable != null && loggable != null)
			s.append(loggable.toLogString("l"));
		return s.toString();
	}

}
