package app.utils.generador;

import java.util.Random;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import app.utils.distribucion.TipoDistribucionVariableContinua;

@Component
public class GeneradorNormalConvolucionImpl implements GeneradorNormalConvolucion {

	static final Logger LOG = LoggerFactory.getLogger(GeneradorNormalConvolucionImpl.class);
	Random random;
	Long semilla;
	int k;
	Double media, desviacion;
	@Autowired
	TipoDistribucionVariableContinua distribucionNormal;

	public GeneradorNormalConvolucionImpl() {
		super();
		random = new Random();
	}

	@Bean
	@Scope(value = "prototype")
	static public GeneradorNormalConvolucion generadorNormalConvolucion() {
		return new GeneradorNormalConvolucionImpl();
	}

	@Override
	public Number next() {
		Double kAux = k == 0 ? 12d : new Double(k);
		double a = 0d;
		for (int i = 0; i < k; i++)
			a += random.nextDouble();
		a = a - (kAux / 2d);
		return (a / Math.sqrt(kAux / 12d)) * desviacion + media;
	}

	@Override
	public Double next(double nextDouble) {
		Double kAux = k == 0 ? 12d : new Double(k);
		double a = 0d;
		for (int i = 0; i < k; i++)
			a += random.nextDouble();
		a = a - (kAux / 2d);
		return (a / Math.sqrt(kAux / 12d)) * desviacion + media;
	}

	@Override
	public void clear() {
		random = new Random();
		semilla = null;
		k = 0;
		media = null;
		desviacion = null;
	}

	@Override
	public Long getSemilla() {
		return semilla;
	}

	@Override
	public void setSemilla(Long semilla) {
		this.semilla = semilla;
		if (semilla != null)
			random.setSeed(semilla);
	}

	@Override
	public Double getMedia() {
		return media;
	}

	@Override
	public void setMedia(Double media) {
		this.media = media;
	}

	@Override
	public Double getDesviacion() {
		return desviacion;
	}

	@Override
	public void setDesviacion(Double desviacion) {
		this.desviacion = desviacion;
	}

	@Override
	public TipoDistribucionVariableContinua getTipoDistribucion() {
		return distribucionNormal;
	}

	@Override
	public Double getExtremoInferior() {
		return null;
	}

	@Override
	public void setExtremoInferior(Double extremoInferior) {
	}

	@Override
	public Double getExtremoSuperior() {
		return null;
	}

	@Override
	public void setExtremoSuperior(Double extremoSuperior) {
	}

	@Override
	public int getK() {
		return k;
	}

	@Override
	public void setK(int k) {
		this.k = k;
	}

	@Override
	public Number getLambda() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setLambda(Number lambda) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Long getA() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setA(Long a) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Long getC() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setC(Long c) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Long getM() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setM(Long m) {
		// TODO Auto-generated method stub
		
	}

}
