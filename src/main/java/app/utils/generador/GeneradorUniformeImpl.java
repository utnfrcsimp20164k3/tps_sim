package app.utils.generador;

import java.util.Random;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import app.utils.distribucion.TipoDistribucionVariableContinua;

@Component
public class GeneradorUniformeImpl implements GeneradorUniforme {

	Random random;
	Long semilla;
	double extremoInferior, extremoSuperior;
	@Autowired
	GeneradorUniformeTransformadaInversaAB generadorUniformeTransformadaInversaAB;
	@Autowired
	TipoDistribucionVariableContinua distribucionUniforme;

	public GeneradorUniformeImpl() {
		super();
		random = new Random();
	}

	@Bean
	@Scope(value = "prototype")
	static public GeneradorUniforme generadorUniforme() {
		return new GeneradorUniformeImpl();
	}

	@Override
	public Number next() {
		return generadorUniformeTransformadaInversaAB.next(random.nextDouble());
	}

	@Override
	public Double next(double nextDouble) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void clear() {
		setSemilla(null);
		extremoInferior = 0d;
		extremoSuperior = 0d;
		random = new Random();
		generadorUniformeTransformadaInversaAB.setExtremoInferior(extremoInferior);
		generadorUniformeTransformadaInversaAB.setExtremoSuperior(extremoSuperior);
	}

	@Override
	public Long getSemilla() {
		return semilla;
	}

	@Override
	public void setSemilla(Long semilla) {
		this.semilla = semilla;
		if (semilla != null)
			random.setSeed(semilla);
	}

	@Override
	public Double getExtremoInferior() {
		return extremoInferior;
	}

	@Override
	public void setExtremoInferior(Double extremoInferior) {
		this.extremoInferior = extremoInferior;
		generadorUniformeTransformadaInversaAB.setExtremoInferior(extremoInferior);
	}

	@Override
	public Double getExtremoSuperior() {
		return extremoSuperior;
	}

	@Override
	public void setExtremoSuperior(Double extremoSuperior) {
		this.extremoSuperior = extremoSuperior;
		generadorUniformeTransformadaInversaAB.setExtremoSuperior(extremoSuperior);
	}

	@Override
	public TipoDistribucionVariableContinua getTipoDistribucion() {
		return distribucionUniforme;
	}

	@Override
	public Number getLambda() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setLambda(Number lambda) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public int getK() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void setK(int k) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Double getMedia() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Double getDesviacion() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setDesviacion(Double desviacion) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setMedia(Double media) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Long getA() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setA(Long a) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Long getC() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setC(Long c) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Long getM() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setM(Long m) {
		// TODO Auto-generated method stub
		
	}

}
