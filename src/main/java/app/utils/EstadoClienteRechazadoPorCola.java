package app.utils;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
public class EstadoClienteRechazadoPorCola implements EstadoCliente {
	
	public EstadoClienteRechazadoPorCola() {
		super();
	}

	@Bean
	@Scope(value = "prototype")
	static public EstadoCliente estadoClienteRechazadoPorCola() {
		return new EstadoClienteRechazadoPorCola();
	}

	@Override
	public String toString() {
		return "Rechazado";
	}

	@Override
	public boolean isEnCola() {
		return false;
	}

	@Override
	public boolean isEsperandoServicio() {
		return false;
	}

	@Override
	public boolean isRechazadoPorCola() {
		return true;
	}

	@Override
	public boolean isRechazadoPorGestorColas() {
		return false;
	}

	@Override
	public boolean isSiendoServido() {
		return false;
	}

	@Override
	public boolean isRechazadoPorServidor() {
		return false;
	}

	@Override
	public boolean isServido() {
		return false;
	}

}
