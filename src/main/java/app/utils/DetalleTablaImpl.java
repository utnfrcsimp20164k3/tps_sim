package app.utils;

import java.util.HashMap;
import java.util.Map;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
public class DetalleTablaImpl implements DetalleTabla {

	Map<Object, Object> valores;

	public DetalleTablaImpl() {
		super();
		valores = new HashMap<>();
	}

	@Bean
	@Scope("prototype")
	static public DetalleTabla detalleTabla() {
		return new DetalleTablaImpl();
	}

	@Override
	public Map<Object, Object> getValores() {
		return valores;
	}

	@Override
	public String toLogString(String nombre) {
		nombre = nombre != null ? nombre : "l";
		StringBuffer s = new StringBuffer();
		s.append(" " + nombre + ": " + this);
		s.append(", " + nombre + ".valores: " + valores);
		return s.toString();
	}
	
	@Override
	public boolean equals(Object obj) {
		return obj instanceof DetalleTabla && ((DetalleTabla) obj).getValores() != null && ((DetalleTabla) obj).getValores().equals(valores);
	}

}
