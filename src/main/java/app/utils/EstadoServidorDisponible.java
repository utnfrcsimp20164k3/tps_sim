package app.utils;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
public class EstadoServidorDisponible implements EstadoServidor {
	
	public EstadoServidorDisponible() {
		super();
	}

	@Bean
	@Scope(value = "prototype")
	static public EstadoServidor estadoServidorDisponible() {
		return new EstadoServidorDisponible();
	}

	@Override
	public String toString() {
		return "Disponible";
	}

	@Override
	public boolean isDisponible() {
		return true;
	}

	@Override
	public boolean isSirviendo() {
		return false;
	}

}
