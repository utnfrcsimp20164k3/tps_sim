package app.utils;

public interface Acumulador {
	
	void acumular(DetalleTabla anterior, DetalleTabla actual);
	
	void setKeys(Object[] keys);
	
	void setKeyAcumulador(Object keyAcumulador);

}
