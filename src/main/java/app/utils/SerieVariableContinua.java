package app.utils;

import app.utils.distribucion.TipoDistribucionVariableContinua;
import app.utils.grafica.Histograma;

public interface SerieVariableContinua extends Serie {
	
	Histograma getHistogramaObservadas();

	Histograma getHistogramaEsperadas();

	void clearHistogramas();

	boolean hasHistogramas();
	
	TipoDistribucionVariableContinua getTipoDistribucion();

}
