package app.utils;

import app.utils.Estado;

public interface EstadoCliente extends Estado {
	
	boolean isEnCola();
	
	boolean isEsperandoServicio();
	
	boolean isRechazadoPorCola();
	
	boolean isRechazadoPorGestorColas();

	boolean isSiendoServido();
	
	boolean isRechazadoPorServidor();
	
	boolean isServido();

}
