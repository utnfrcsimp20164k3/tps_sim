package app.utils;

import java.util.Comparator;

public interface Intervalo {

	ExtremoDeIntervalo getInferior();

	void setInferior(ExtremoDeIntervalo inferior);

	ExtremoDeIntervalo getSuperior();

	void setSuperior(ExtremoDeIntervalo superior);
	
	double getAncho();
	
	boolean incluye(double valor);

	static Comparator<Intervalo> comparator() {
		return new ComparadorDeIntervalos();
	}

}
