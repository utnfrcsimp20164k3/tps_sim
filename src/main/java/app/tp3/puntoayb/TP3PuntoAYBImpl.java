package app.tp3.puntoayb;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import app.utils.Hipotesis;
import app.utils.Serie;
import app.utils.SerieFactory;
import app.utils.generador.GeneradorPseudoaleatorio;

@Component
public class TP3PuntoAYBImpl implements TP3PuntoAYB {

	static final Logger LOG = LoggerFactory.getLogger(TP3PuntoAYBImpl.class);
	int cantidadNumeros;
	Serie serie;
	@Autowired
	SerieFactory serieFactory;
	@Autowired
	ApplicationContext context;
	GeneradorPseudoaleatorio generador;

	public TP3PuntoAYBImpl() {
		super();
	}

	@Bean
	@Scope(value = "prototype")
	static public TP3PuntoAYB tP3PuntoAYB() {
		return new TP3PuntoAYBImpl();
	}

	@Override
	public void solucionar() {
		serie.setArray(generarArray());
		Hipotesis hipotesis = (Hipotesis) context.getBean("hipotesis");
		hipotesis.setTipoDistribucion(generador.getTipoDistribucion());
		serie.postularHipotesis(hipotesis);
		serie.generarGraficas();
	}

	@Override
	public void clear() {
		if (serie != null)
			serie.clear();
		if (generador != null)
			generador.clear();
	}

	private double[] generarArray() {
		generador.setExtremoInferior(serie.getExtremoInferior());
		generador.setExtremoSuperior(serie.getExtremoSuperior());
		double[] serie = new double[cantidadNumeros];
		for (int i = 0; i < cantidadNumeros; i++)
			serie[i] = generador.next().doubleValue();
		return serie;
	}

	@Override
	public void crearSerie() {
		serie = serieFactory.crearSerie();
	}

	@Override
	public Serie getSerie() {
		return serie;
	}

	@Override
	public int getCantidadNumeros() {
		return cantidadNumeros;
	}

	@Override
	public void setCantidadNumeros(int cantidadNumeros) {
		this.cantidadNumeros = cantidadNumeros;
	}

	@Override
	public GeneradorPseudoaleatorio getGenerador() {
		return generador;
	}

	@Override
	public void setGenerador(GeneradorPseudoaleatorio generador) {
		this.generador = generador;
	}

	@Override
	public SerieFactory getSerieFactory() {
		return serieFactory;
	}

}
