package app.tp3.puntoayb.presentacion;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.zkoss.bind.BindUtils;
import org.zkoss.bind.Converter;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.DependsOn;
import org.zkoss.bind.annotation.GlobalCommand;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.SmartNotifyChange;
import org.zkoss.zk.ui.select.annotation.VariableResolver;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.ListModelList;

import app.presentacion.ConversorDeDouble;
import app.presentacion.ConversorDeInt;
import app.presentacion.ConversorDeLong;
import app.presentacion.FormateadorDeNumerosDecimales;
import app.presentacion.IngresarCantidadNumerosValidator;
import app.presentacion.IngresarDesviacionValidator;
import app.presentacion.IngresarExtremoInferiorValidator;
import app.presentacion.IngresarExtremoSuperiorValidator;
import app.presentacion.IngresarKValidator;
import app.presentacion.IngresarLambdaDoubleValidator;
import app.presentacion.IngresarLambdaLongValidator;
import app.presentacion.IngresarMediaValidator;
import app.presentacion.IngresarSemillaValidator;
import app.presentacion.IngresarCantidadIntervalosValidator;
import app.presentacion.InputValidator;
import app.presentacion.Observable;
import app.presentacion.Observer;
import app.presentacion.ResourceBundle;
import app.presentacion.ResourceIndex;
import app.presentacion.SidebarTreeItem;
import app.tp3.puntoayb.TP3PuntoAYB;
import app.utils.SerieVariableContinua;
import app.utils.SerieVariableDiscreta;
import app.utils.generador.GeneradorExponencial;
import app.utils.generador.GeneradorNormalBoxMuller;
import app.utils.generador.GeneradorNormalConvolucion;
import app.utils.generador.GeneradorPoisson;
import app.utils.generador.GeneradorUniforme;

@VariableResolver(org.zkoss.zkplus.spring.DelegatingVariableResolver.class)
public class TP3PuntoAYBSolucionVM implements Observer {

	private static final String TILDE_SRC = "./images/check_mark.png";
	private static final String ADVERTENCIA_SRC = "./images/warning.png";
	private static final String ASTERISCO_SRC = "./images/asterisk_red.png";
	static final Logger LOG = LoggerFactory.getLogger(TP3PuntoAYBSolucionVM.class);
	@WireVariable
	private ResourceIndex resourceIndex;
	@WireVariable
	private InputValidator ingresarCantidadNumerosValidator;
	@WireVariable
	private InputValidator ingresarCantidadIntervalosValidator;
	@WireVariable
	private InputValidator ingresarSemillaValidator;
	@WireVariable
	private InputValidator ingresarExtremoInferiorValidator;
	@WireVariable
	private InputValidator ingresarExtremoSuperiorValidator;
	@WireVariable
	private InputValidator ingresarLambdaDoubleValidator;
	@WireVariable
	private InputValidator ingresarLambdaLongValidator;
	@WireVariable
	private InputValidator ingresarKValidator;
	@WireVariable
	private InputValidator ingresarMediaValidator;
	@WireVariable
	private InputValidator ingresarDesviacionValidator;
	private ResourceBundle rB;
	private String ingresarCantidadNumerosIconSrc, ingresarCantidadIntervalosIconSrc, ingresarSemillaIconSrc,
			ingresarExtremoInferiorIconSrc, ingresarExtremoSuperiorIconSrc, ingresarLambdaDoubleIconSrc,
			ingresarLambdaLongIconSrc, ingresarKIconSrc, ingresarMediaIconSrc, ingresarDesviacionIconSrc;
	private boolean cantidadNumerosValidada, cantidadIntervalosValidada, semillaValidada, extremoInferiorValidada,
			extremoSuperiorValidada, lambdaDoubleValidada, lambdaLongValidada, kValidada, mediaValidada,
			desviacionValidada;
	@WireVariable
	GeneradorUniforme generadorUniforme;
	@WireVariable
	GeneradorExponencial generadorExponencial;
	@WireVariable
	GeneradorPoisson generadorPoisson;
	@WireVariable
	GeneradorNormalConvolucion generadorNormalConvolucion;
	@WireVariable
	GeneradorNormalBoxMuller generadorNormalBoxMuller;
	private String tipoGenerador;
	@WireVariable
	private TP3PuntoAYB tP3PuntoAYB;
	ListModelList<Double> lista;
	@WireVariable
	FormateadorDeNumerosDecimales formateadorDeNumerosDecimales;
	@WireVariable
	ConversorDeInt conversorDeInt;
	@WireVariable
	ConversorDeLong conversorDeLong;
	@WireVariable
	ConversorDeDouble conversorDeDouble;
	private boolean mostrandoResultados;

	@Init
	public void initSetup() {
		rB = resourceIndex.getResourceBundle("gthjue");
		lista = new ListModelList<>();
		ingresarCantidadNumerosValidator.addObserver(this);
		ingresarCantidadIntervalosValidator.addObserver(this);
		ingresarSemillaValidator.addObserver(this);
		ingresarExtremoInferiorValidator.addObserver(this);
		ingresarExtremoSuperiorValidator.addObserver(this);
		ingresarLambdaDoubleValidator.addObserver(this);
		ingresarLambdaLongValidator.addObserver(this);
		ingresarKValidator.addObserver(this);
		ingresarMediaValidator.addObserver(this);
		ingresarDesviacionValidator.addObserver(this);
		inicializarIconosValidaciones();
		inicializarValidaciones();
	}

	private void inicializarValidaciones() {
	}

	private void inicializarIconosValidaciones() {
		setIngresarCantidadNumerosIconSrc(ASTERISCO_SRC);
		setIngresarCantidadIntervalosIconSrc(ASTERISCO_SRC);
		setIngresarSemillaIconSrc(ASTERISCO_SRC);
		setIngresarExtremoInferiorIconSrc(ASTERISCO_SRC);
		setIngresarExtremoSuperiorIconSrc(ASTERISCO_SRC);
		setIngresarLambdaDoubleIconSrc(ASTERISCO_SRC);
		setIngresarLambdaLongIconSrc(ASTERISCO_SRC);
	}

	@GlobalCommand
	@SmartNotifyChange({ "lista", "cantidadNumerosValidada", "cantidadIntervalosValidada", "semillaValidada",
			"extremoInferiorValidada", "extremoSuperiorValidada", "lambdaDoubleValidada", "lambdaLongValidada", "kValidada", "mediaValidada",
			"desviacionValidada", "ingresarCantidadNumerosIconSrc", "ingresarCantidadIntervalosIconSrc",
			"ingresarSemillaIconSrc", "ingresarExtremoInferiorIconSrc", "ingresarExtremoSuperiorIconSrc",
			"ingresarLambdaDoubleIconSrc", "ingresarLambdaLongIconSrc", "ingresarKIconSrc", "ingresarMediaIconSrc", "ingresarDesviacionIconSrc" })
	public void showContent(@BindingParam("page") SidebarTreeItem page) {
		if (page == null || page.getUri() == null || !page.getUri().equals(rB.getZul()))
			clear();
	}

	@Command
	@SmartNotifyChange({ "lista", "cantidadNumerosValidada", "cantidadIntervalosValidada", "semillaValidada",
			"extremoInferiorValidada", "extremoSuperiorValidada", "lambdaDoubleValidada", "lambdaLongValidada",
			"ingresarCantidadNumerosIconSrc", "ingresarCantidadIntervalosIconSrc", "ingresarSemillaIconSrc",
			"ingresarExtremoInferiorIconSrc", "ingresarExtremoSuperiorIconSrc", "ingresarLambdaDoubleIconSrc", "ingresarLambdaLongIconSrc",
			"mostrandoResultados" })
	public void clear() {
		if (tipoGenerador != null && tipoGenerador.equals("Uniforme")) {
			tP3PuntoAYB.getSerieFactory().setTipoVariable("Continua");
			tP3PuntoAYB.crearSerie();
			tP3PuntoAYB.setGenerador(generadorUniforme);
		}
		if (tipoGenerador != null && tipoGenerador.equals("Exponencial")) {
			tP3PuntoAYB.getSerieFactory().setTipoVariable("Continua");
			tP3PuntoAYB.crearSerie();
			tP3PuntoAYB.setGenerador(generadorExponencial);
		}
		if (tipoGenerador != null && tipoGenerador.equals("Poisson")) {
			tP3PuntoAYB.getSerieFactory().setTipoVariable("Discreta");
			tP3PuntoAYB.crearSerie();
			tP3PuntoAYB.setGenerador(generadorPoisson);
		}
		if (tipoGenerador != null && tipoGenerador.equals("Normal")) {
			tP3PuntoAYB.getSerieFactory().setTipoVariable("Continua");
			tP3PuntoAYB.crearSerie();
			tP3PuntoAYB.setGenerador(generadorNormalBoxMuller);
		}
		tP3PuntoAYB.clear();
		setCantidadNumerosValidada(false);
		setCantidadIntervalosValidada(false);
		setSemillaValidada(false);
		setExtremoInferiorValidada(false);
		setExtremoSuperiorValidada(false);
		setLambdaDoubleValidada(false);
		setLambdaLongValidada(false);
		setKValidada(false);
		setMediaValidada(false);
		setDesviacionValidada(false);
		setMostrandoResultados(false);
		inicializarIconosValidaciones();
		inicializarValidaciones();
	}

	@Command
	@SmartNotifyChange({ "tP3PuntoAYB", "graficaSrc", "chiCuadradoSrc", "tablaFrecuenciasSrc", "mostrandoResultados" })
	public void correr() throws Exception {
		tP3PuntoAYB.solucionar();
		setMostrandoResultados(true);
	}

	@Command
	@SmartNotifyChange("mostrandoResultados")
	public void limpiarResultados() {
		setMostrandoResultados(false);
		BindUtils.postGlobalCommand(null, null, "limpiarTablaFrecuencias", null);
		BindUtils.postGlobalCommand(null, null, "limpiarTestChiCuadrado", null);
	}

	// "Validación"
	@Override
	public void update(Observable o, Object arg) {
		if (o instanceof IngresarCantidadNumerosValidator)
			updateValidacion((IngresarCantidadNumerosValidator) o);
		if (o instanceof IngresarCantidadIntervalosValidator)
			updateValidacion((IngresarCantidadIntervalosValidator) o);
		if (o instanceof IngresarSemillaValidator)
			updateValidacion((IngresarSemillaValidator) o);
		if (o instanceof IngresarExtremoInferiorValidator)
			updateValidacion((IngresarExtremoInferiorValidator) o);
		if (o instanceof IngresarExtremoSuperiorValidator)
			updateValidacion((IngresarExtremoSuperiorValidator) o);
		if (o instanceof IngresarLambdaDoubleValidator)
			updateValidacion((IngresarLambdaDoubleValidator) o);
		if (o instanceof IngresarLambdaLongValidator)
			updateValidacion((IngresarLambdaLongValidator) o);
		if (o instanceof IngresarKValidator)
			updateValidacion((IngresarKValidator) o);
		if (o instanceof IngresarMediaValidator)
			updateValidacion((IngresarMediaValidator) o);
		if (o instanceof IngresarDesviacionValidator)
			updateValidacion((IngresarDesviacionValidator) o);
	}

	private void updateValidacion(IngresarCantidadNumerosValidator val) {
		String iconoSrc = registrarResultadoValidacion((InputValidator) val);
		ingresarCantidadNumerosIconSrc = iconoSrc;
		if (iconoSrc != null && iconoSrc.equals(TILDE_SRC) || iconoSrc == null)
			setCantidadNumerosValidada(true);
		else
			setCantidadNumerosValidada(false);
		BindUtils.postNotifyChange(null, null, this, "ingresarCantidadNumerosIconSrc");
		BindUtils.postNotifyChange(null, null, this, "cantidadNumerosValidada");
	}

	private void updateValidacion(IngresarCantidadIntervalosValidator val) {
		String iconoSrc = registrarResultadoValidacion((InputValidator) val);
		ingresarCantidadIntervalosIconSrc = iconoSrc;
		if (iconoSrc != null && iconoSrc.equals(TILDE_SRC) || iconoSrc == null)
			setCantidadIntervalosValidada(true);
		else
			setCantidadIntervalosValidada(false);
		BindUtils.postNotifyChange(null, null, this, "ingresarCantidadIntervalosIconSrc");
		BindUtils.postNotifyChange(null, null, this, "cantidadIntervalosValidada");
	}

	private void updateValidacion(IngresarSemillaValidator val) {
		String iconoSrc = registrarResultadoValidacion((InputValidator) val);
		ingresarSemillaIconSrc = iconoSrc;
		if (iconoSrc != null && iconoSrc.equals(TILDE_SRC) || iconoSrc == null)
			setSemillaValidada(true);
		else
			setSemillaValidada(false);
		BindUtils.postNotifyChange(null, null, this, "ingresarSemillaIconSrc");
		BindUtils.postNotifyChange(null, null, this, "semillaValidada");
	}

	private void updateValidacion(IngresarExtremoInferiorValidator val) {
		String iconoSrc = registrarResultadoValidacion((InputValidator) val);
		ingresarExtremoInferiorIconSrc = iconoSrc;
		if (iconoSrc != null && iconoSrc.equals(TILDE_SRC) || iconoSrc == null)
			setExtremoInferiorValidada(true);
		else
			setExtremoInferiorValidada(false);
		BindUtils.postNotifyChange(null, null, this, "ingresarExtremoInferiorIconSrc");
		BindUtils.postNotifyChange(null, null, this, "extremoInferiorValidada");
	}

	private void updateValidacion(IngresarExtremoSuperiorValidator val) {
		String iconoSrc = registrarResultadoValidacion((InputValidator) val);
		ingresarExtremoSuperiorIconSrc = iconoSrc;
		if (iconoSrc != null && iconoSrc.equals(TILDE_SRC) || iconoSrc == null)
			setExtremoSuperiorValidada(true);
		else
			setExtremoSuperiorValidada(false);
		BindUtils.postNotifyChange(null, null, this, "ingresarExtremoSuperiorIconSrc");
		BindUtils.postNotifyChange(null, null, this, "extremoSuperiorValidada");
	}

	private void updateValidacion(IngresarLambdaDoubleValidator val) {
		String iconoSrc = registrarResultadoValidacion((InputValidator) val);
		ingresarLambdaDoubleIconSrc = iconoSrc;
		if (iconoSrc != null && iconoSrc.equals(TILDE_SRC) || iconoSrc == null)
			setLambdaDoubleValidada(true);
		else
			setLambdaDoubleValidada(false);
		BindUtils.postNotifyChange(null, null, this, "ingresarLambdaDoubleIconSrc");
		BindUtils.postNotifyChange(null, null, this, "lambdaDoubleValidada");
	}

	private void updateValidacion(IngresarLambdaLongValidator val) {
		String iconoSrc = registrarResultadoValidacion((InputValidator) val);
		ingresarLambdaLongIconSrc = iconoSrc;
		if (iconoSrc != null && iconoSrc.equals(TILDE_SRC) || iconoSrc == null)
			setLambdaLongValidada(true);
		else
			setLambdaLongValidada(false);
		BindUtils.postNotifyChange(null, null, this, "ingresarLambdaLongIconSrc");
		BindUtils.postNotifyChange(null, null, this, "lambdaLongValidada");
	}

	private void updateValidacion(IngresarKValidator val) {
		String iconoSrc = registrarResultadoValidacion((InputValidator) val);
		ingresarKIconSrc = iconoSrc;
		if (iconoSrc != null && iconoSrc.equals(TILDE_SRC) || iconoSrc == null)
			setKValidada(true);
		else
			setKValidada(false);
		BindUtils.postNotifyChange(null, null, this, "ingresarKIconSrc");
		BindUtils.postNotifyChange(null, null, this, "kValidada");
	}

	private void updateValidacion(IngresarMediaValidator val) {
		String iconoSrc = registrarResultadoValidacion((InputValidator) val);
		ingresarMediaIconSrc = iconoSrc;
		if (iconoSrc != null && iconoSrc.equals(TILDE_SRC) || iconoSrc == null)
			setMediaValidada(true);
		else
			setMediaValidada(false);
		BindUtils.postNotifyChange(null, null, this, "ingresarMediaIconSrc");
		BindUtils.postNotifyChange(null, null, this, "mediaValidada");
	}

	private void updateValidacion(IngresarDesviacionValidator val) {
		String iconoSrc = registrarResultadoValidacion((InputValidator) val);
		ingresarDesviacionIconSrc = iconoSrc;
		if (iconoSrc != null && iconoSrc.equals(TILDE_SRC) || iconoSrc == null)
			setDesviacionValidada(true);
		else
			setDesviacionValidada(false);
		BindUtils.postNotifyChange(null, null, this, "ingresarDesviacionIconSrc");
		BindUtils.postNotifyChange(null, null, this, "desviacionValidada");
	}

	private String registrarResultadoValidacion(InputValidator val) {
		String res = null;
		if (!val.isIncorrecta() && !val.isNula())
			res = TILDE_SRC;
		else if (val.isIncorrecta())
			res = ADVERTENCIA_SRC;
		else if (val.isNula() && val.isObligatoria())
			res = ASTERISCO_SRC;
		else if (val.isNula() && !val.isObligatoria())
			res = null;
		return res;
	}

	@DependsOn({ "cantidadNumerosValidada", "cantidadIntervalosValidada", "semillaValidada", "extremoInferiorValidada",
			"extremoSuperiorValidada", "lambdaDoubleValidada", "lambdaLongValidada", "kValidada", "mediaValidada", "desviacionValidada" })
	public boolean isValidacionOk() {
		return (tipoGenerador.equals("Uniforme") && isValidacionUniformeOk())
				|| (tipoGenerador.equals("Exponencial") && isValidacionExponencialOk())
				|| (tipoGenerador.equals("Poisson") && isValidacionPoissonOk())
				|| (tipoGenerador.equals("Normal") && isValidacionNormalOk());
	}

	private boolean isValidacionUniformeOk() {
		return (cantidadNumerosValidada || (!getIngresarCantidadNumerosValidator().isObligatoria()
				&& getIngresarCantidadNumerosIconSrc() == null))
				&& (cantidadIntervalosValidada || (!getIngresarCantidadIntervalosValidator().isObligatoria()
						&& getIngresarCantidadIntervalosIconSrc() == null))
				&& (semillaValidada
						|| (!getIngresarSemillaValidator().isObligatoria() && getIngresarSemillaIconSrc() == null))
				&& (extremoInferiorValidada || (!getIngresarExtremoInferiorValidator().isObligatoria()
						&& getIngresarExtremoInferiorIconSrc() == null))
				&& (extremoSuperiorValidada || (!getIngresarExtremoSuperiorValidator().isObligatoria()
						&& getIngresarExtremoSuperiorIconSrc() == null));
	}

	private boolean isValidacionExponencialOk() {
		return (cantidadNumerosValidada || (!getIngresarCantidadNumerosValidator().isObligatoria()
				&& getIngresarCantidadNumerosIconSrc() == null))
				&& (cantidadIntervalosValidada || (!getIngresarCantidadIntervalosValidator().isObligatoria()
						&& getIngresarCantidadIntervalosIconSrc() == null))
				&& (semillaValidada
						|| (!getIngresarSemillaValidator().isObligatoria() && getIngresarSemillaIconSrc() == null))
				&& (lambdaDoubleValidada || (!getIngresarLambdaDoubleValidator().isObligatoria()
						&& getIngresarLambdaDoubleIconSrc() == null));
	}

	private boolean isValidacionPoissonOk() {
		return (cantidadNumerosValidada || (!getIngresarCantidadNumerosValidator().isObligatoria()
				&& getIngresarCantidadNumerosIconSrc() == null))
				&& (semillaValidada
						|| (!getIngresarSemillaValidator().isObligatoria() && getIngresarSemillaIconSrc() == null))
				&& (lambdaLongValidada || (!getIngresarLambdaLongValidator().isObligatoria()
						&& getIngresarLambdaLongIconSrc() == null));
	}

	private boolean isValidacionNormalOk() {
		return (cantidadNumerosValidada || (!getIngresarCantidadNumerosValidator().isObligatoria()
				&& getIngresarCantidadNumerosIconSrc() == null))
				&& (cantidadIntervalosValidada || (!getIngresarCantidadIntervalosValidator().isObligatoria()
						&& getIngresarCantidadIntervalosIconSrc() == null))
				&& (semillaValidada
						|| (!getIngresarSemillaValidator().isObligatoria() && getIngresarSemillaIconSrc() == null))
				&& (kValidada || (!getIngresarKValidator().isObligatoria() && getIngresarKIconSrc() == null))
				&& (mediaValidada
						|| (!getIngresarMediaValidator().isObligatoria() && getIngresarMediaIconSrc() == null))
				&& (desviacionValidada || (!getIngresarDesviacionValidator().isObligatoria()
						&& getIngresarDesviacionIconSrc() == null));
	}

	public InputValidator getIngresarCantidadNumerosValidator() {
		return ingresarCantidadNumerosValidator;
	}

	public InputValidator getIngresarCantidadIntervalosValidator() {
		return ingresarCantidadIntervalosValidator;
	}

	public InputValidator getIngresarSemillaValidator() {
		return ingresarSemillaValidator;
	}

	public InputValidator getIngresarExtremoInferiorValidator() {
		return ingresarExtremoInferiorValidator;
	}

	public InputValidator getIngresarExtremoSuperiorValidator() {
		return ingresarExtremoSuperiorValidator;
	}

	public InputValidator getIngresarLambdaDoubleValidator() {
		return ingresarLambdaDoubleValidator;
	}

	public InputValidator getIngresarLambdaLongValidator() {
		return ingresarLambdaLongValidator;
	}

	public InputValidator getIngresarKValidator() {
		return ingresarKValidator;
	}

	public InputValidator getIngresarMediaValidator() {
		return ingresarMediaValidator;
	}

	public InputValidator getIngresarDesviacionValidator() {
		return ingresarDesviacionValidator;
	}

	@DependsOn("cantidadNumerosValidada")
	public String getDefaultTooltiptextIngresarCantidadNumeros() {
		String res = null;
		if (getIngresarCantidadNumerosValidator().isObligatoria() && !cantidadNumerosValidada)
			res = "Este campo es obligatorio.";
		else if (cantidadNumerosValidada)
			res = "Cantidad válida.";
		return res;
	}

	@DependsOn("cantidadIntervalosValidada")
	public String getDefaultTooltiptextIngresarCantidadIntervalos() {
		String res = null;
		if (getIngresarCantidadIntervalosValidator().isObligatoria() && !cantidadIntervalosValidada)
			res = "Este campo es obligatorio.";
		else if (cantidadIntervalosValidada)
			res = "Constante multiplicativa válida.";
		return res;
	}

	@DependsOn("semillaValidada")
	public String getDefaultTooltiptextIngresarSemilla() {
		String res = null;
		if (getIngresarSemillaValidator().isObligatoria() && !semillaValidada)
			res = "Este campo es obligatorio.";
		else if (semillaValidada)
			res = "Semilla válida.";
		return res;
	}

	@DependsOn("extremoInferiorValidada")
	public String getDefaultTooltiptextIngresarExtremoInferior() {
		String res = null;
		if (getIngresarExtremoInferiorValidator().isObligatoria() && !extremoInferiorValidada)
			res = "Este campo es obligatorio.";
		else if (extremoInferiorValidada)
			res = "Extremo inferior válido.";
		return res;
	}

	@DependsOn("extremoSuperiorValidada")
	public String getDefaultTooltiptextIngresarExtremoSuperior() {
		String res = null;
		if (getIngresarExtremoSuperiorValidator().isObligatoria() && !extremoSuperiorValidada)
			res = "Este campo es obligatorio.";
		else if (extremoSuperiorValidada)
			res = "Extremo superior válido.";
		return res;
	}

	@DependsOn("lambdaDoubleValidada")
	public String getDefaultTooltiptextIngresarLambdaDouble() {
		String res = null;
		if (getIngresarLambdaDoubleValidator().isObligatoria() && !lambdaDoubleValidada)
			res = "Este campo es obligatorio.";
		else if (lambdaDoubleValidada)
			res = "Extremo superior válido.";
		return res;
	}

	@DependsOn("lambdaLongValidada")
	public String getDefaultTooltiptextIngresarLambdaLong() {
		String res = null;
		if (getIngresarLambdaLongValidator().isObligatoria() && !lambdaLongValidada)
			res = "Este campo es obligatorio.";
		else if (lambdaLongValidada)
			res = "Extremo superior válido.";
		return res;
	}

	@DependsOn("kValidada")
	public String getDefaultTooltiptextIngresarK() {
		String res = null;
		if (getIngresarKValidator().isObligatoria() && !kValidada)
			res = "Este campo es obligatorio.";
		else if (kValidada)
			res = "Extremo superior válido.";
		return res;
	}

	@DependsOn("mediaValidada")
	public String getDefaultTooltiptextIngresarMedia() {
		String res = null;
		if (getIngresarMediaValidator().isObligatoria() && !mediaValidada)
			res = "Este campo es obligatorio.";
		else if (mediaValidada)
			res = "Extremo superior válido.";
		return res;
	}

	@DependsOn("desviacionValidada")
	public String getDefaultTooltiptextIngresarDesviacion() {
		String res = null;
		if (getIngresarDesviacionValidator().isObligatoria() && !desviacionValidada)
			res = "Este campo es obligatorio.";
		else if (desviacionValidada)
			res = "Extremo superior válido.";
		return res;
	}

	public String getIngresarCantidadNumerosIconSrc() {
		return ingresarCantidadNumerosIconSrc;
	}

	public String getIngresarCantidadIntervalosIconSrc() {
		return ingresarCantidadIntervalosIconSrc;
	}

	public String getIngresarSemillaIconSrc() {
		return ingresarSemillaIconSrc;
	}

	public String getIngresarExtremoInferiorIconSrc() {
		return ingresarExtremoInferiorIconSrc;
	}

	public String getIngresarExtremoSuperiorIconSrc() {
		return ingresarExtremoSuperiorIconSrc;
	}

	public String getIngresarLambdaDoubleIconSrc() {
		return ingresarLambdaDoubleIconSrc;
	}

	public String getIngresarLambdaLongIconSrc() {
		return ingresarLambdaLongIconSrc;
	}

	public String getIngresarKIconSrc() {
		return ingresarKIconSrc;
	}

	public String getIngresarMediaIconSrc() {
		return ingresarMediaIconSrc;
	}

	public String getIngresarDesviacionIconSrc() {
		return ingresarDesviacionIconSrc;
	}

	public boolean isCantidadNumerosValidada() {
		return cantidadNumerosValidada;
	}

	public boolean isCantidadIntervalosValidada() {
		return cantidadIntervalosValidada;
	}

	public boolean isSemillaValidada() {
		return semillaValidada;
	}

	public boolean isExtremoInferiorValidada() {
		return extremoInferiorValidada;
	}

	public boolean isExtremoSuperiorValidada() {
		return extremoSuperiorValidada;
	}

	public boolean isLambdaDoubleValidada() {
		return lambdaDoubleValidada;
	}

	public boolean isLambdaLongValidada() {
		return lambdaLongValidada;
	}

	public boolean iskValidada() {
		return kValidada;
	}

	public boolean isMediaValidada() {
		return mediaValidada;
	}

	public boolean isDesviacionValidada() {
		return desviacionValidada;
	}

	public void setIngresarCantidadNumerosIconSrc(String ingresarCantidadIconSrc) {
		this.ingresarCantidadNumerosIconSrc = ingresarCantidadIconSrc;
	}

	public void setIngresarCantidadIntervalosIconSrc(String ingresarSubintervalosIconSrc) {
		this.ingresarCantidadIntervalosIconSrc = ingresarSubintervalosIconSrc;
	}

	public void setIngresarSemillaIconSrc(String ingresarSemillaIconSrc) {
		this.ingresarSemillaIconSrc = ingresarSemillaIconSrc;
	}

	public void setIngresarExtremoInferiorIconSrc(String ingresarExtremoInferiorIconSrc) {
		this.ingresarExtremoInferiorIconSrc = ingresarExtremoInferiorIconSrc;
	}

	public void setIngresarExtremoSuperiorIconSrc(String ingresarExtremoSuperiorIconSrc) {
		this.ingresarExtremoSuperiorIconSrc = ingresarExtremoSuperiorIconSrc;
	}

	public void setIngresarLambdaDoubleIconSrc(String ingresarLambdaDoubleIconSrc) {
		this.ingresarLambdaDoubleIconSrc = ingresarLambdaDoubleIconSrc;
	}

	public void setIngresarLambdaLongIconSrc(String ingresarLambdaLongIconSrc) {
		this.ingresarLambdaLongIconSrc = ingresarLambdaLongIconSrc;
	}

	public void setIngresarKIconSrc(String ingresarKIconSrc) {
		this.ingresarKIconSrc = ingresarKIconSrc;
	}

	public void setIngresarMediaIconSrc(String ingresarMediaIconSrc) {
		this.ingresarMediaIconSrc = ingresarMediaIconSrc;
	}

	public void setIngresarDesviacionIconSrc(String ingresarDesviacionIconSrc) {
		this.ingresarDesviacionIconSrc = ingresarDesviacionIconSrc;
	}

	public void setCantidadNumerosValidada(boolean cantidadValidada) {
		this.cantidadNumerosValidada = cantidadValidada;
	}

	public void setCantidadIntervalosValidada(boolean subintervalosValidada) {
		this.cantidadIntervalosValidada = subintervalosValidada;
	}

	public void setSemillaValidada(boolean semillaValidada) {
		this.semillaValidada = semillaValidada;
	}

	public void setExtremoInferiorValidada(boolean extremoInferiorValidada) {
		this.extremoInferiorValidada = extremoInferiorValidada;
	}

	public void setExtremoSuperiorValidada(boolean extremoSuperiorValidada) {
		this.extremoSuperiorValidada = extremoSuperiorValidada;
	}

	public void setLambdaDoubleValidada(boolean lambdaDoubleValidada) {
		this.lambdaDoubleValidada = lambdaDoubleValidada;
	}

	public void setLambdaLongValidada(boolean lambdaLongValidada) {
		this.lambdaLongValidada = lambdaLongValidada;
	}

	public void setKValidada(boolean kValidada) {
		this.kValidada = kValidada;
	}

	public void setMediaValidada(boolean mediaValidada) {
		this.mediaValidada = mediaValidada;
	}

	public void setDesviacionValidada(boolean desviacionValidada) {
		this.desviacionValidada = desviacionValidada;
	}
	// Fin "Validación"

	public Converter<?, ?, ?> getConversorDeInt() {
		return conversorDeInt;
	}

	public Converter<?, ?, ?> getConversorDeLong() {
		return conversorDeLong;
	}

	public Converter<?, ?, ?> getConversorDeDouble() {
		return conversorDeDouble;
	}

	public Converter<?, ?, ?> getTruncador() {
		formateadorDeNumerosDecimales.setPattern("#.####");
		return formateadorDeNumerosDecimales;
	}

	@DependsOn("mostrandoResultados")
	public String getResultadosSrc() {
		return mostrandoResultados ? rB.getPagePathname("resultados") : null;
	}

	@DependsOn("tipoGenerador")
	public String getInputSrc() {
		String src = null, substring = null;
		if (tipoGenerador != null && tipoGenerador.equals("Uniforme"))
			substring = "input-uniforme";
		else if (tipoGenerador != null && tipoGenerador.equals("Exponencial"))
			substring = "input-exponencial";
		else if (tipoGenerador != null && tipoGenerador.equals("Poisson"))
			substring = "input-poisson";
		else if (tipoGenerador != null && tipoGenerador.equals("Normal"))
			substring = "input-normal-boxmuller";
		if (tipoGenerador != null)
			src = rB.getPagePathname(substring);
		return src;
	}

	@DependsOn("resultadosSrc")
	public String getGraficaSrc() {
		String src = null;
		if (mostrandoResultados && tP3PuntoAYB != null && tP3PuntoAYB.getSerie() != null
				&& tP3PuntoAYB.getSerie().hasGrafica() && tP3PuntoAYB.getSerie() instanceof SerieVariableContinua)
			src = rB.getPagePathname("histograma");
		else if (mostrandoResultados && tP3PuntoAYB != null && tP3PuntoAYB.getSerie() != null
				&& tP3PuntoAYB.getSerie().hasGrafica() && tP3PuntoAYB.getSerie() instanceof SerieVariableDiscreta)
			src = rB.getPagePathname("graficobarras");
		return src;
	}

	@DependsOn("resultadosSrc")
	public String getTablaFrecuenciasSrc() {
		String src = null;
		if (mostrandoResultados && tP3PuntoAYB != null && tP3PuntoAYB.getSerie() != null
				&& tP3PuntoAYB.getSerie().hasGrafica() && tP3PuntoAYB.getSerie() instanceof SerieVariableContinua)
			src = rB.getPagePathname("tablafrecuencias-variablecontinua");
		else if (mostrandoResultados && tP3PuntoAYB != null && tP3PuntoAYB.getSerie() != null
				&& tP3PuntoAYB.getSerie().hasGrafica() && tP3PuntoAYB.getSerie() instanceof SerieVariableDiscreta)
			src = rB.getPagePathname("tablafrecuencias-variablediscreta");
		return src;
	}

	@DependsOn("resultadosSrc")
	public String getChiCuadradoSrc() {
		String src = null;
		if (mostrandoResultados && tP3PuntoAYB != null && tP3PuntoAYB.getSerie() != null
				&& tP3PuntoAYB.getSerie().hasGrafica() && tP3PuntoAYB.getSerie() instanceof SerieVariableContinua)
			src = rB.getPagePathname("chicuadrado-variablecontinua.");
		else if (mostrandoResultados && tP3PuntoAYB != null && tP3PuntoAYB.getSerie() != null
				&& tP3PuntoAYB.getSerie().hasGrafica() && tP3PuntoAYB.getSerie() instanceof SerieVariableDiscreta)
			src = rB.getPagePathname("chicuadrado-variablediscreta.");
		return src;
	}

	@DependsOn("resultadosSrc")
	public String getChiCuadradoTablaSrc() {
		String src = null;
		if (mostrandoResultados && tP3PuntoAYB != null && tP3PuntoAYB.getSerie() != null
				&& tP3PuntoAYB.getSerie().hasGrafica())
			src = rB.getPagePathname("chicuadrado-tabla");
		return src;
	}

	public void setInputSrc(String inputSrc) {
	}

	public String getTipoGenerador() {
		return tipoGenerador;
	}

	public void setTipoGenerador(String tipoGenerador) {
		this.tipoGenerador = tipoGenerador;
	}

	public TP3PuntoAYB gettP3PuntoAYB() {
		return tP3PuntoAYB;
	}

	public void settP3PuntoAYB(TP3PuntoAYB tP3PuntoAYB) {
		this.tP3PuntoAYB = tP3PuntoAYB;
	}

	@DependsOn("tP3PuntoAYB")
	public ListModelList<Double> getLista() {
		lista.clear();
		List<Double> numeros = new ArrayList<>();
		if (tP3PuntoAYB != null && tP3PuntoAYB.getSerie() != null && tP3PuntoAYB.getSerie().getArray() != null) {
			for (Double d : tP3PuntoAYB.getSerie().getArray())
				numeros.add(d);
			lista.addAll(numeros);
			lista.addToSelection(tP3PuntoAYB.getSerie().getArray()[tP3PuntoAYB.getSerie().getArray().length - 1]);
		}
		return lista;
	}

	public boolean isMostrandoResultados() {
		return mostrandoResultados;
	}

	public void setMostrandoResultados(boolean mostrandoResultados) {
		this.mostrandoResultados = mostrandoResultados;
	}

	public String getTituloChiCuadrado() {
		return "Prueba de \u03C7\u2072";
	}

	public GeneradorUniforme getGeneradorUniforme() {
		return generadorUniforme;
	}

	public void setGeneradorUniforme(GeneradorUniforme generadorUniforme) {
		this.generadorUniforme = generadorUniforme;
	}

	@DependsOn({ "mostrandoResultados", "cantidadNumerosValidada", "cantidadIntervalosValidada", "semillaValidada",
			"extremoInferiorValidada", "extremoSuperiorValidada", "lambdaDoubleValidada", "lambdaLongValidada", "kValidada", "mediaValidada",
			"desviacionValidada" })
	public boolean isCorrerHabilitado() {
		return !mostrandoResultados && isValidacionOk();
	}

}
