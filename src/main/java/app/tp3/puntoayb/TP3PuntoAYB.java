package app.tp3.puntoayb;

import app.Ejercicio;
import app.utils.Serie;
import app.utils.SerieFactory;
import app.utils.generador.GeneradorPseudoaleatorio;

public interface TP3PuntoAYB extends Ejercicio {

	void clear();

	Serie getSerie();

	GeneradorPseudoaleatorio getGenerador();

	void setGenerador(GeneradorPseudoaleatorio generador);

	int getCantidadNumeros();

	void setCantidadNumeros(int cantidadNumeros);
	
	SerieFactory getSerieFactory();

	void crearSerie();

}
