package app.tp5ytp6;

import app.utils.Evento;

public interface LlegadaAlumno extends Evento {

	Alumno getAlumno();

	void setAlumno(Alumno alumno);

}
