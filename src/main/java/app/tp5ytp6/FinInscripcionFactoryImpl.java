package app.tp5ytp6;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Random;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import app.Ejercicio;
import app.Loggable;
import app.tp5.ejerciciod.TP5EjercicioD;
import app.utils.Observable;
import app.utils.Observer;
import app.utils.TablaIntegracionNumerica;
import app.utils.TablaVectorEstados;
import app.utils.distribucion.TipoDistribucion;
import app.utils.generador.GeneradorPseudoaleatorio;

@Component
public class FinInscripcionFactoryImpl implements EventoFactory<FinInscripcion>, Observer, Observable {

	static final Logger LOG = LoggerFactory.getLogger(FinInscripcionFactoryImpl.class);
	private Random random;
	private Double rndProximo;
	private BigDecimal tiempoProximo, horaActual;
	private TipoDistribucion tipoDistribucion;
	private GeneradorPseudoaleatorio generadorPseudoaleatorio;
	private Map<Maquina, BigDecimal> horasProximos;
	private Set<Observer> observers;
	private boolean changed;

	public FinInscripcionFactoryImpl() {
		super();
		random = new Random();
		observers = new HashSet<>();
		horaActual = new BigDecimal("0");
		horasProximos = new HashMap<>();
	}

	@Bean
	@Scope(value = "prototype")
	static public EventoFactory<FinInscripcion> finInscripcionFactory() {
		return new FinInscripcionFactoryImpl();
	}

	@Override
	public Double getRndProximo() {
		return rndProximo;
	}

	@Override
	public BigDecimal getHoraProximo() {
		return null;
	}

	@Override
	public Map<?, BigDecimal> getHorasProximos() {
		return horasProximos;
	}

	@Override
	public BigDecimal getTiempoProximo() {
		return tiempoProximo;
	}

	@Override
	public FinInscripcion getObject() throws Exception {
		return new FinInscripcionImpl();
	}

	@Override
	public Class<?> getObjectType() {
		return FinInscripcion.class;
	}

	@Override
	public boolean isSingleton() {
		return true;
	}

	@Override
	public void setTipoDistribucion(TipoDistribucion tipoDistribucion) {
		this.tipoDistribucion = tipoDistribucion;
		generadorPseudoaleatorio = tipoDistribucion.getDefaultGenerator();
	}

	@Override
	public GeneradorPseudoaleatorio getGeneradorPseudoaleatorio() {
		return generadorPseudoaleatorio;
	}

	@Override
	public void update(Observable o, Object arg) {
		doLog("trace", "update(Observable o, Object arg)", "[{]", null, null);
		if (o instanceof Ejercicio && arg instanceof BigDecimal)
			horaActual = (BigDecimal) arg;
		else if (o instanceof Ejercicio && arg instanceof TablaVectorEstados)
			update((TablaVectorEstados) arg);
		else if (o instanceof Ejercicio && arg instanceof Reloj)
			update((Reloj) arg);
		else if (o instanceof MaquinaFactory && arg instanceof Maquina)
			registrarMaquina((Maquina) arg);
		else if (o instanceof Maquina)
			update((Maquina) o);
		doLog("trace", "update(Observable o, Object arg)", "[}]", null, null);
	}

	private void update(TablaVectorEstados tabla) {
		doLog("trace", "update(TP5EjercicioD tP5EjercicioD)", "[{]", null, null);
		if (tipoDistribucion == null)
			throw new RuntimeException("No se asoció a esta factory con un tipo de distribución.");
		doLog("trace", "update(TP5EjercicioD tP5EjercicioD)", "[}]", null, null);
	}

	private void update(Reloj reloj) {
		doLog("trace", "update(Reloj reloj)", "[{]", null, null);
		for (Maquina maquina : horasProximos.keySet())
			if (horasProximos.get(maquina) != null && horasProximos.get(maquina).equals(horaActual)) {
				generarEvento(maquina);
			} else {
				changed = rndProximo != null || tiempoProximo != null || !horasProximos.values().isEmpty();
				rndProximo = null;
				tiempoProximo = null;
				notifyObservers(horasProximos);
			}
		doLog("trace", "update(Reloj reloj)", "[}]", null, null);
	}

	private void registrarMaquina(Maquina maquina) {
		doLog("trace", "registrarMaquina(Maquina maquina)", "[{]", null, null);
		maquina.addObserver(this);
		horasProximos.put(maquina, null);
		doLog("trace", "registrarMaquina(Maquina maquina)", "[}]", null, null);
	}

	private void update(Maquina maquina) {
		doLog("trace", "update(Maquina maquina)", "[{]", null, null);
		if (maquina.isInscribiendo())
			definirHoraProximo(maquina);
		else if (maquina.isLibre()) {
			horasProximos.put(maquina, null);
			changed = true;
			notifyObservers(horasProximos);
		}
		doLog("trace", "update(Maquina maquina)", "[}]", null, null);
	}

	private void definirHoraProximo(Maquina maquina) {
		doLog("trace", "definirHoraProximo(Maquina maquina)", "[{]", null, null);
		rndProximo = random.nextDouble();
		tiempoProximo = BigDecimal.valueOf((double) generadorPseudoaleatorio.next(rndProximo));
		BigDecimal horaProximo = horaActual.add(tiempoProximo);
		horasProximos.put(maquina, horaProximo);
		changed = true;
		notifyObservers(horaProximo);
		changed = true;
		notifyObservers(horasProximos);
		doLog("trace", "definirHoraProximo(Maquina maquina)", "[}]", null, null);
	}

	private void generarEvento(Maquina maquina) {
		doLog("trace", "generarEvento(Maquina maquina)", "[{]", null, null);
		try {
			FinInscripcion finInscripcion = (FinInscripcion) getObject();
			finInscripcion.setMaquina(maquina);
			changed = true;
			notifyObservers(finInscripcion);
		} catch (Exception e) {
			e.printStackTrace();
		}
		doLog("trace", "generarEvento(Maquina maquina)", "[}]", null, null);
	}

	@Override
	public void addObserver(Observer o) {
		observers.add(o);
	}

	@Override
	public void removeObserver(Observer o) {
		observers.remove(o);
	}

	@Override
	public int countObservers() {
		return observers.size();
	}

	@Override
	public void deleteObserver(Observer o) {
		observers.remove(o);
	}

	@Override
	public void deleteObservers() {
		observers.clear();
	}

	@Override
	public boolean hasChanged() {
		return changed;
	}

	@Override
	public void notifyObservers() {
		if (hasChanged())
			for (Observer obs : observers)
				obs.update(this, null);
		clearChanged();
	}

	@Override
	public void notifyObservers(Object o) {
		if (hasChanged())
			for (Observer obs : observers)
				obs.update(this, o);
		clearChanged();
	}

	protected void clearChanged() {
		changed = false;
	}

	private void doLog(String level, String signature, String corchetes, String nombreVariable, Loggable loggable) {
		switch (level) {
		case "trace":
			if (LOG.isTraceEnabled())
				LOG.trace(toLogMsg(signature, corchetes, nombreVariable, loggable));
			break;
		case "debug":
			if (LOG.isDebugEnabled())
				LOG.debug(toLogMsg(signature, corchetes, nombreVariable, loggable));
			break;
		case "info":
			if (LOG.isInfoEnabled())
				LOG.info(toLogMsg(signature, corchetes, nombreVariable, loggable));
			break;
		case "warn":
			if (LOG.isWarnEnabled())
				LOG.warn(toLogMsg(signature, corchetes, nombreVariable, loggable));
			break;
		case "error":
			if (LOG.isErrorEnabled())
				LOG.error(toLogMsg(signature, corchetes, nombreVariable, loggable));
			break;
		}
	}

	private String toLogMsg(String signature, String corchetes, String nombreVariable, Loggable loggable) {
		StringBuffer s = new StringBuffer(signature);
		if (corchetes != null)
			s.append(" " + corchetes);
		if (nombreVariable != null)
			s.append(" " + "{ l = " + nombreVariable + " }");
		if (nombreVariable != null && loggable == null)
			s.append(nombreVariable + ": " + loggable);
		else if (nombreVariable != null && loggable != null)
			s.append(loggable.toLogString("l"));
		return s.toString();
	}

}
