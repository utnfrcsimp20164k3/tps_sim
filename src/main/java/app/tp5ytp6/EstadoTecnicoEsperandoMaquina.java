package app.tp5ytp6;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
public class EstadoTecnicoEsperandoMaquina implements EstadoTecnico {
	
	public EstadoTecnicoEsperandoMaquina() {
		super();
	}

	@Bean
	@Scope(value = "prototype")
	static public EstadoTecnico estadoTecnicoEsperandoMaquina() {
		return new EstadoTecnicoEsperandoMaquina();
	}

	@Override
	public String toString() {
		return "Esperando máquina";
	}

	@Override
	public boolean isAusente() {
		return false;
	}

	@Override
	public boolean isHaciendoMantenimiento() {
		return false;
	}

	@Override
	public boolean isEsperandoMaquina() {
		return true;
	}

}
