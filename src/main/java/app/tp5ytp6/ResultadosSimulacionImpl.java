package app.tp5ytp6;

import app.utils.DetalleTabla;

public class ResultadosSimulacionImpl implements ResultadosSimulacion {

	DetalleTabla detalleTablaFinal;

	public ResultadosSimulacionImpl() {
		super();
	}

	@Override
	public void setDetalleTablaFinal(DetalleTabla detalleTablaFinal) {
		this.detalleTablaFinal = detalleTablaFinal;
	}

	@Override
	public boolean hasDetalleTablaFinal() {
		return detalleTablaFinal != null;
	}

}
