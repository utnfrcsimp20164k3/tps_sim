package app.tp5ytp6;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Deque;
import java.util.HashSet;
import java.util.Set;

import app.utils.Observable;
import app.utils.Observer;
import app.Loggable;
import app.utils.Cola;

public class ColaAlumnos implements Cola<Alumno, Maquina>, Observable {

	static final Logger LOG = LoggerFactory.getLogger(ColaAlumnos.class);
	private Set<Observer> observers;
	private boolean changed;
	private Deque<Alumno> alumnos;
	private Deque<Maquina> maquinas;
	private Integer size;
	private String id;

	public ColaAlumnos(Deque<Alumno> alumnos, Deque<Maquina> maquinas) {
		super();
		observers = new HashSet<>();
		this.alumnos = alumnos;
		this.maquinas = maquinas;
		id = "Cola de alumnos";
	}

	public ColaAlumnos(Deque<Alumno> alumnos, Deque<Maquina> maquinas, String id) {
		this(alumnos, maquinas);
		this.id = id;
	}

	public ColaAlumnos(Deque<Alumno> alumnos, Deque<Maquina> maquinas, Integer size) {
		this(alumnos, maquinas);
		this.size = size;
	}

	public ColaAlumnos(Deque<Alumno> alumnos, Deque<Maquina> maquinas, String id, Integer size) {
		this(alumnos, maquinas, id);
		this.size = size;
	}

	@Override
	public void setSize(Integer size) {
		this.size = size;
	}

	@Override
	public Integer getOcupacion() {
		return alumnos.size();
	}

	@Override
	public void next() {
		doLog("trace", "next()", "[{]", null, null);
		if (!alumnos.isEmpty())
			for (Maquina maquina : maquinas)
				if ((maquina.isLibre() || maquina.isEsperandoMantenimiento()) && !alumnos.isEmpty()) {
					Alumno alumno = escogerAlumno();
					maquina.ofrecerServicio(alumno);
				}
		doLog("trace", "next()", "[}]", null, null);
	}

	private Alumno escogerAlumno() {
		Alumno alumno = null, primero = alumnos.removeFirst(), aux = primero;
		do {
			alumno = aux.isEsperandoMaquina() ? aux : null;
			if (alumno == null) {
				alumnos.addLast(aux);
				aux = alumnos.removeFirst();
			} else
				aux = null;
		} while (alumno == null || (aux != null && aux.equals(primero)));
		return alumno;
	}

	@Override
	public boolean hayLugar() {
		int ocupacion = getOcupacion();
		return size - ocupacion > 0;
	}

	@Override
	public boolean isEmpty() {
		return alumnos.isEmpty();
	}

	@Override
	public void agregarCliente(Alumno alumno) {
		doLog("trace", "agregarCliente(Alumno alumno)", "[{]", null, null);
		if (!hayLugar())
			alumno.declararseAusente();
		else {
			alumnos.addLast(alumno);
			alumno.declararseEsperandoMaquina();
		}
		doLog("trace", "agregarCliente(Alumno alumno)", "[}]", null, null);
	}

	@Override
	public void agregarServidor(Maquina maquina) {
		if (!maquinas.contains(maquina))
			maquinas.addLast(maquina);
	}

	@Override
	public Object getId() {
		return id;
	}

	@Override
	public String toString() {
		return id;
	}

	@Override
	public void addObserver(Observer o) {
		observers.add(o);
	}

	@Override
	public void removeObserver(Observer o) {
		observers.remove(o);
	}

	@Override
	public int countObservers() {
		return observers.size();
	}

	@Override
	public void deleteObserver(Observer o) {
		observers.remove(o);
	}

	@Override
	public void deleteObservers() {
		observers.clear();
	}

	@Override
	public boolean hasChanged() {
		return changed;
	}

	@Override
	public void notifyObservers() {
		if (hasChanged())
			for (Observer obs : observers)
				obs.update(this, null);
		clearChanged();
	}

	@Override
	public void notifyObservers(Object o) {
		if (hasChanged())
			for (Observer obs : observers)
				obs.update(this, o);
		clearChanged();
	}

	protected void clearChanged() {
		changed = false;
	}

	private void doLog(String level, String signature, String corchetes, String nombreVariable, Loggable loggable) {
		switch (level) {
		case "trace":
			if (LOG.isTraceEnabled())
				LOG.trace(toLogMsg(signature, corchetes, nombreVariable, loggable));
			break;
		case "debug":
			if (LOG.isDebugEnabled())
				LOG.debug(toLogMsg(signature, corchetes, nombreVariable, loggable));
			break;
		case "info":
			if (LOG.isInfoEnabled())
				LOG.info(toLogMsg(signature, corchetes, nombreVariable, loggable));
			break;
		case "warn":
			if (LOG.isWarnEnabled())
				LOG.warn(toLogMsg(signature, corchetes, nombreVariable, loggable));
			break;
		case "error":
			if (LOG.isErrorEnabled())
				LOG.error(toLogMsg(signature, corchetes, nombreVariable, loggable));
			break;
		}
	}

	private String toLogMsg(String signature, String corchetes, String nombreVariable, Loggable loggable) {
		StringBuffer s = new StringBuffer(signature);
		if (corchetes != null)
			s.append(" " + corchetes);
		if (nombreVariable != null)
			s.append(" " + "{ l = " + nombreVariable + " }");
		if (nombreVariable != null && loggable == null)
			s.append(nombreVariable + ": " + loggable);
		else if (nombreVariable != null && loggable != null)
			s.append(loggable.toLogString("l"));
		return s.toString();
	}

}
