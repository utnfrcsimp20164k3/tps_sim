package app.tp5ytp6;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

import app.Ejercicio;
import app.Loggable;
import app.utils.DetalleTabla;
import app.utils.Observable;
import app.utils.Observer;
import app.utils.ParRandomEvento;
import app.utils.SimuladorDeEventos;
import app.utils.TablaIntegracionNumerica;
import app.utils.TablaVectorEstados;
import app.utils.distribucion.TipoDistribucion;
import app.utils.generador.GeneradorPseudoaleatorio;

@Component
public class FinMantenimientoFactoryTP6EjercicioDImpl implements FinMantenimientoFactory, Observer, Observable {

	@Autowired
	private ApplicationContext applicationContext;
	@Autowired
	private SimuladorDeEventos simuladorDeEventos;
	static final Logger LOG = LoggerFactory.getLogger(FinMantenimientoFactoryTP6EjercicioDImpl.class);
	private Double rndProximo;
	private BigDecimal a0Proximo, tiempoProximo, horaActual;
	private GeneradorPseudoaleatorio generadorPseudoaleatorio;
	private Map<Tecnico, BigDecimal> horasProximos;
	private Map<Object, TablaIntegracionNumerica> tablasIntegracionNumerica;
	private Set<Observer> observers;
	private boolean changed;
	private String h;

	public FinMantenimientoFactoryTP6EjercicioDImpl() {
		super();
		observers = new HashSet<>();
		horaActual = new BigDecimal("0");
		horasProximos = new HashMap<>();
	}

	@PostConstruct
	private void initSimuladorDeEventos() {
		simuladorDeEventos.asociar(new BigDecimal("0.33").setScale(2, RoundingMode.HALF_UP), new BigDecimal("1000"));
		simuladorDeEventos.asociar(new BigDecimal("0.33").setScale(2, RoundingMode.HALF_UP), new BigDecimal("1500"));
		simuladorDeEventos.asociar(new BigDecimal("0.34").setScale(2, RoundingMode.HALF_UP), new BigDecimal("2000"));
		simuladorDeEventos.setTipoDistribucion((TipoDistribucion) applicationContext.getBean("distribucionUniforme"));
		simuladorDeEventos.setup();
	}

	@Override
	public Double getRndProximo() {
		return rndProximo;
	}

	@Override
	public BigDecimal getA0Proximo() {
		return a0Proximo;
	}

	@Override
	public BigDecimal getHoraProximo() {
		return null;
	}

	@Override
	public Map<?, BigDecimal> getHorasProximos() {
		return horasProximos;
	}

	@Override
	public BigDecimal getTiempoProximo() {
		return tiempoProximo;
	}

	@Override
	public FinMantenimiento getObject() throws Exception {
		return new FinMantenimientoImpl();
	}

	@Override
	public Class<?> getObjectType() {
		return FinMantenimiento.class;
	}

	@Override
	public boolean isSingleton() {
		return true;
	}

	@Override
	public void setTipoDistribucion(TipoDistribucion tipoDistribucion) {
	}

	@Override
	public void setTablasDeIntegracionNumerica(Map<Object, TablaIntegracionNumerica> tablasDeIntegracionNumerica) {
		this.tablasIntegracionNumerica = tablasDeIntegracionNumerica;
	}

	@Override
	public void setH(String h) {
		this.h = h;
	}

	@Override
	public GeneradorPseudoaleatorio getGeneradorPseudoaleatorio() {
		return generadorPseudoaleatorio;
	}

	@Override
	public void update(Observable o, Object arg) {
		doLog("trace", "update(Observable o, Object arg)", "[{]", null, null);
		if (o instanceof Ejercicio && arg instanceof BigDecimal)
			horaActual = (BigDecimal) arg;
		else if (o instanceof Ejercicio && arg instanceof TablaVectorEstados)
			update((TablaVectorEstados) arg);
		else if (o instanceof Ejercicio && arg instanceof Reloj)
			update((Reloj) arg);
		else if (o instanceof TecnicoFactory && arg instanceof Tecnico)
			registrarTecnico((Tecnico) arg);
		else if (o instanceof Tecnico)
			update((Tecnico) o);
		doLog("trace", "update(Observable o, Object arg)", "[}]", null, null);
	}

	private void update(TablaVectorEstados tabla) {
		doLog("trace", "update(TablaVectorEstados tabla)", "[{]", null, null);
		construirTablas();
		doLog("trace", "update(TablaVectorEstados tabla)", "[}]", null, null);
	}

	private void construirTablas() {
		BigDecimal a0 = null;
		for (Object key : tablasIntegracionNumerica.keySet())
			if (key instanceof BigDecimal) {
				a0 = (BigDecimal) key;
				construirTabla(tablasIntegracionNumerica.get(key), a0);
			}
	}

	private void construirTabla(TablaIntegracionNumerica tabla, BigDecimal a0) {
		doLog("trace", "construirTabla(TablaIntegracionNumerica tabla, BigDecimal a0)", "[{]", null, null);
		LOG.debug("construirTabla(TablaIntegracionNumerica tabla, BigDecimal a0) [0] tabla: " + tabla);
		tabla.setH(new BigDecimal(h).setScale(2, RoundingMode.HALF_UP));
		crearColumnas(tabla);
		generarLineaInicial(tabla, a0);
		generarLineas(tabla, a0);
		doLog("trace", "construirTabla(TablaIntegracionNumerica tabla, BigDecimal a0)", "[}]", null, null);
	}

	private void crearColumnas(TablaIntegracionNumerica tabla) {
		tabla.crearColumna("t");
		tabla.crearColumna("A");
		tabla.crearColumna("A/dt");
		tabla.crearColumna("Ai+1");
	}

	private void generarLineaInicial(TablaIntegracionNumerica tabla, BigDecimal a0) {
		DetalleTabla inicial = (DetalleTabla) applicationContext.getBean("detalleTabla");
		inicial.getValores().put("t", new BigDecimal("0").setScale(2, RoundingMode.HALF_UP));
		inicial.getValores().put("A", a0);
		inicial.getValores().put("A/dt", new BigDecimal("-68").setScale(2, RoundingMode.HALF_UP)
				.subtract(((BigDecimal) inicial.getValores().get("A")).pow(2).divide(a0, 4, RoundingMode.HALF_UP)));
		inicial.getValores().put("Ai+1", ((BigDecimal) inicial.getValores().get("A"))
				.add(((BigDecimal) inicial.getValores().get("A/dt")).multiply(tabla.getH())));
		tabla.getDetalles().add(inicial);
	}

	private void generarLineas(TablaIntegracionNumerica tabla, BigDecimal a0) {
		doLog("trace", "TablaIntegracionNumerica tabla, BigDecimal a0", "[{]", null, null);
		while (((BigDecimal) tabla.getDetalles().get(tabla.getDetalles().size() - 1).getValores().get("A"))
				.compareTo(new BigDecimal("0")) > 0) {
			LOG.debug(
					"TablaIntegracionNumerica tabla, BigDecimal a0 [0] ((BigDecimal) tabla.getDetalles().get(tabla.getDetalles().size() - 1).getValores().get(\"A\")): "
							+ ((BigDecimal) tabla.getDetalles().get(tabla.getDetalles().size() - 1).getValores()
									.get("A")));
			DetalleTabla detalleTabla = (DetalleTabla) applicationContext.getBean("detalleTabla");
			detalleTabla.getValores().put("t",
					((BigDecimal) tabla.getDetalles().get(tabla.getDetalles().size() - 1).getValores().get("t"))
							.add(tabla.getH()));
			detalleTabla.getValores().put("A",
					tabla.getDetalles().get(tabla.getDetalles().size() - 1).getValores().get("Ai+1"));
			detalleTabla.getValores().put("A/dt", new BigDecimal("-68").setScale(2, RoundingMode.HALF_UP).subtract(
					((BigDecimal) detalleTabla.getValores().get("A")).pow(2).divide(a0, 4, RoundingMode.HALF_UP)));
			detalleTabla.getValores().put("Ai+1", ((BigDecimal) detalleTabla.getValores().get("A"))
					.add(((BigDecimal) detalleTabla.getValores().get("A/dt")).multiply(tabla.getH())));
			tabla.getDetalles().add(detalleTabla);

		}
		doLog("trace", "TablaIntegracionNumerica tabla, BigDecimal a0", "[}]", null, null);
	}

	private void update(Reloj reloj) {
		doLog("trace", "update(Reloj reloj)", "[{]", null, null);
		for (Tecnico tecnico : horasProximos.keySet())
			if (horasProximos.get(tecnico) != null && horasProximos.get(tecnico).equals(horaActual)) {
				generarEvento(tecnico);
			} else {
				changed = rndProximo != null || a0Proximo != null || tiempoProximo != null
						|| !horasProximos.values().isEmpty();
				rndProximo = null;
				a0Proximo = null;
				tiempoProximo = null;
				notifyObservers(horasProximos);
			}
		doLog("trace", "update(Reloj reloj)", "[}]", null, null);
	}

	private void registrarTecnico(Tecnico tecnico) {
		doLog("trace", "registrarTecnico(Tecnico tecnico)", "[{]", null, null);
		tecnico.addObserver(this);
		horasProximos.put(tecnico, null);
		doLog("trace", "registrarTecnico(Tecnico tecnico)", "[}]", null, null);
	}

	private void update(Tecnico tecnico) {
		doLog("trace", "update(Tecnico tecnico)", "[{]", null, null);
		if (tecnico.isHaciendoMantenimiento())
			definirHoraProximo(tecnico);
		else if (tecnico.isEsperandoMaquina()) {
			horasProximos.put(tecnico, null);
			changed = true;
			notifyObservers(horasProximos);
		}
		doLog("trace", "update(Tecnico tecnico)", "[}]", null, null);
	}

	private void definirHoraProximo(Tecnico tecnico) {
		doLog("trace", "definirHoraProximo(Tecnico tecnico)", "[{]", null, null);
		ParRandomEvento par = simuladorDeEventos.next();
		rndProximo = par.getLeft();
		a0Proximo = (BigDecimal) par.getRight();
		tiempoProximo = ((BigDecimal) tablasIntegracionNumerica.get(a0Proximo).getDetalles()
				.get(tablasIntegracionNumerica.get(a0Proximo).getDetalles().size() - 1).getValores().get("t"));
		BigDecimal horaProximo = horaActual.add(tiempoProximo);
		horasProximos.put(tecnico, horaProximo);
		changed = true;
		notifyObservers(horaProximo);
		changed = true;
		notifyObservers(horasProximos);
		doLog("trace", "definirHoraProximo(Tecnico tecnico)", "[}]", null, null);
	}

	private void generarEvento(Tecnico tecnico) {
		doLog("trace", "generarEvento(Tecnico tecnico)", "[{]", null, null);
		try {
			FinMantenimiento finMantenimiento = (FinMantenimiento) getObject();
			finMantenimiento.setTecnico(tecnico);
			changed = true;
			notifyObservers(finMantenimiento);
		} catch (Exception e) {
			e.printStackTrace();
		}
		doLog("trace", "generarEvento(Tecnico tecnico)", "[}]", null, null);
	}

	@Override
	public void addObserver(Observer o) {
		observers.add(o);
	}

	@Override
	public void removeObserver(Observer o) {
		observers.remove(o);
	}

	@Override
	public int countObservers() {
		return observers.size();
	}

	@Override
	public void deleteObserver(Observer o) {
		observers.remove(o);
	}

	@Override
	public void deleteObservers() {
		observers.clear();
	}

	@Override
	public boolean hasChanged() {
		return changed;
	}

	@Override
	public void notifyObservers() {
		if (hasChanged())
			for (Observer obs : observers)
				obs.update(this, null);
		clearChanged();
	}

	@Override
	public void notifyObservers(Object instance) {
		if (hasChanged())
			for (Observer obs : observers)
				obs.update(this, instance);
		clearChanged();
	}

	protected void clearChanged() {
		changed = false;
	}

	private void doLog(String level, String signature, String corchetes, String nombreVariable, Loggable loggable) {
		switch (level) {
		case "trace":
			if (LOG.isTraceEnabled())
				LOG.trace(toLogMsg(signature, corchetes, nombreVariable, loggable));
			break;
		case "debug":
			if (LOG.isDebugEnabled())
				LOG.debug(toLogMsg(signature, corchetes, nombreVariable, loggable));
			break;
		case "info":
			if (LOG.isInfoEnabled())
				LOG.info(toLogMsg(signature, corchetes, nombreVariable, loggable));
			break;
		case "warn":
			if (LOG.isWarnEnabled())
				LOG.warn(toLogMsg(signature, corchetes, nombreVariable, loggable));
			break;
		case "error":
			if (LOG.isErrorEnabled())
				LOG.error(toLogMsg(signature, corchetes, nombreVariable, loggable));
			break;
		}
	}

	private String toLogMsg(String signature, String corchetes, String nombreVariable, Loggable loggable) {
		StringBuffer s = new StringBuffer(signature);
		if (corchetes != null)
			s.append(" " + corchetes);
		if (nombreVariable != null)
			s.append(" " + "{ l = " + nombreVariable + " }");
		if (nombreVariable != null && loggable == null)
			s.append(nombreVariable + ": " + loggable);
		else if (nombreVariable != null && loggable != null)
			s.append(loggable.toLogString("l"));
		return s.toString();
	}

}
