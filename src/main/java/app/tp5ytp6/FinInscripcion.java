package app.tp5ytp6;

import app.utils.Evento;

public interface FinInscripcion extends Evento {
	
	Maquina getMaquina();

	void setMaquina(Maquina maquina);

}
