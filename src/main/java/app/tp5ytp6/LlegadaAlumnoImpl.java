package app.tp5ytp6;

public class LlegadaAlumnoImpl implements LlegadaAlumno {
	
	Alumno alumno;

	public LlegadaAlumnoImpl() {
		super();
	}

	@Override
	public Alumno getAlumno() {
		return alumno;
	}
	
	@Override
	public void setAlumno(Alumno alumno) {
		this.alumno = alumno;
	}
	
	@Override
	public String toString() {
		return "Llegada de un alumno";
	}

}
