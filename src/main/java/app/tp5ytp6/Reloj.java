package app.tp5ytp6;

import java.math.BigDecimal;
import java.util.List;
import java.util.Set;

import app.Loggable;
import app.utils.Observable;
import app.utils.Observer;

public interface Reloj extends Observer, Observable, Loggable {
	
	void resetear(Set<EventoFactory<?>> eventoFactories);
	
	void parar();

	BigDecimal getHoraActual();

	void iniciar();

	void next();

	List<BigDecimal> getHorasProximas();
	
}
