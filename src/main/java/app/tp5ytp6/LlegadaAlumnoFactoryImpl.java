package app.tp5ytp6;

import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Map;
import java.util.Random;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import app.Ejercicio;
import app.Loggable;
import app.utils.Observable;
import app.utils.Observer;
import app.utils.TablaVectorEstados;
import app.utils.distribucion.TipoDistribucion;
import app.utils.generador.GeneradorPseudoaleatorio;

@Component
public class LlegadaAlumnoFactoryImpl implements EventoFactory<LlegadaAlumno> {

	@Autowired
	AlumnoFactory alumnoFactory;
	static final Logger LOG = LoggerFactory.getLogger(LlegadaAlumnoFactoryImpl.class);
	private Random random;
	private Double rndProxima;
	private BigDecimal horaProxima, tiempoProxima, horaActual;
	private TipoDistribucion tipoDistribucion;
	private GeneradorPseudoaleatorio generadorPseudoaleatorio;
	private Set<Observer> observers;
	private boolean changed;

	public LlegadaAlumnoFactoryImpl() {
		super();
		random = new Random();
		observers = new HashSet<>();
		horaActual = new BigDecimal("0");
		horaProxima = new BigDecimal("0");
	}

	@Bean
	@Scope(value = "prototype")
	static public EventoFactory<LlegadaAlumno> llegadaAlumnoFactory() {
		return new LlegadaAlumnoFactoryImpl();
	}
	
	@Override
	public Double getRndProximo() {
		return rndProxima;
	}

	@Override
	public BigDecimal getHoraProximo() {
		return horaProxima;
	}

	@Override
	public Map<Object, BigDecimal> getHorasProximos() {
		return null;
	}

	@Override
	public BigDecimal getTiempoProximo() {
		return tiempoProxima;
	}

	@Override
	public LlegadaAlumno getObject() throws Exception {
		LlegadaAlumno llegadaAlumno = new LlegadaAlumnoImpl();
		llegadaAlumno.setAlumno(alumnoFactory.getObject());
		return llegadaAlumno;
	}

	@Override
	public Class<?> getObjectType() {
		return LlegadaAlumno.class;
	}

	@Override
	public boolean isSingleton() {
		return true;
	}

	@Override
	public void setTipoDistribucion(TipoDistribucion tipoDistribucion) {
		this.tipoDistribucion = tipoDistribucion;
		generadorPseudoaleatorio = tipoDistribucion.getDefaultGenerator();
	}

	@Override
	public GeneradorPseudoaleatorio getGeneradorPseudoaleatorio() {
		return generadorPseudoaleatorio;
	}

	@Override
	public void update(Observable o, Object arg) {
		doLog("trace", "update(Observable o, Object arg)", "[{]", null, null);
		LOG.debug("update(Observable o, Object arg) [0] arg: " + arg);
		if (o instanceof Ejercicio && arg instanceof BigDecimal)
			horaActual = (BigDecimal) arg;
		else if (o instanceof Ejercicio && arg instanceof TablaVectorEstados)
			update((TablaVectorEstados) arg);
		else if (o instanceof Ejercicio && arg instanceof Reloj)
			update((Reloj) arg);
		doLog("trace", "update(Observable o, Object arg)", "[}]", null, null);
	}

	private void update(TablaVectorEstados tabla) {
		doLog("trace", "update(TablaVectorEstados tabla)", "[{]", null, null);
		if (tipoDistribucion == null)
			throw new RuntimeException("No se asoció a esta factory con un tipo de distribución.");
		else
			definirHoraProximo();
		doLog("trace", "update(TablaVectorEstados tabla)", "[}]", null, null);
	}

	private void update(Reloj reloj) {
		doLog("trace", "update(Reloj reloj)", "[{]", null, null);
		if (horaActual.equals(horaProxima)) {
			generarEvento();
			definirHoraProximo();
		} else {
			changed = rndProxima != null || tiempoProxima != null;
			rndProxima = null;
			tiempoProxima = null;
			notifyObservers();
		}
		doLog("trace", "update(Reloj reloj)", "[}]", null, null);
	}

	private void definirHoraProximo() {
		doLog("trace", "definirHoraProximo()", "[{]", null, null);
		rndProxima = random.nextDouble();
		tiempoProxima = BigDecimal.valueOf((double) generadorPseudoaleatorio.next(rndProxima));
		horaProxima = horaActual.add(tiempoProxima);
		changed = true;
		notifyObservers(horaProxima);
		changed = true;
		notifyObservers();
		doLog("trace", "definirHoraProximo()", "[}]", null, null);
	}

	private void generarEvento() {
		doLog("trace", "generarEvento()", "[{]", null, null);
		try {
			changed = true;
			notifyObservers((LlegadaAlumno) getObject());
		} catch (Exception e) {
			e.printStackTrace();
		}
		doLog("trace", "generarEvento()", "[}]", null, null);
	}

	@Override
	public void addObserver(Observer o) {
		observers.add(o);
	}

	@Override
	public void removeObserver(Observer o) {
		observers.remove(o);
	}

	@Override
	public int countObservers() {
		return observers.size();
	}

	@Override
	public void deleteObserver(Observer o) {
		observers.remove(o);
	}

	@Override
	public void deleteObservers() {
		observers.clear();
	}

	@Override
	public boolean hasChanged() {
		return changed;
	}

	@Override
	public void notifyObservers() {
		if (hasChanged())
			for (Observer obs : observers)
				obs.update(this, null);
		clearChanged();
	}

	@Override
	public void notifyObservers(Object o) {
		if (hasChanged())
			for (Observer obs : observers)
				obs.update(this, o);
		clearChanged();
	}

	protected void clearChanged() {
		changed = false;
	}

	private void doLog(String level, String signature, String corchetes, String nombreVariable, Loggable loggable) {
		switch (level) {
		case "trace":
			if (LOG.isTraceEnabled())
				LOG.trace(toLogMsg(signature, corchetes, nombreVariable, loggable));
			break;
		case "debug":
			if (LOG.isDebugEnabled())
				LOG.debug(toLogMsg(signature, corchetes, nombreVariable, loggable));
			break;
		case "info":
			if (LOG.isInfoEnabled())
				LOG.info(toLogMsg(signature, corchetes, nombreVariable, loggable));
			break;
		case "warn":
			if (LOG.isWarnEnabled())
				LOG.warn(toLogMsg(signature, corchetes, nombreVariable, loggable));
			break;
		case "error":
			if (LOG.isErrorEnabled())
				LOG.error(toLogMsg(signature, corchetes, nombreVariable, loggable));
			break;
		}
	}

	private String toLogMsg(String signature, String corchetes, String nombreVariable, Loggable loggable) {
		StringBuffer s = new StringBuffer(signature);
		if (corchetes != null)
			s.append(" " + corchetes);
		if (nombreVariable != null)
			s.append(" " + "{ l = " + nombreVariable + " }");
		if (nombreVariable != null && loggable == null)
			s.append(nombreVariable + ": " + loggable);
		else if (nombreVariable != null && loggable != null)
			s.append(loggable.toLogString("l"));
		return s.toString();
	}

}
