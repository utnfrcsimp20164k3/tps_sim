package app.tp5ytp6;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
public class EstadoAlumnoEsperandoMaquina implements EstadoAlumno {
	
	public EstadoAlumnoEsperandoMaquina() {
		super();
	}

	@Bean
	@Scope(value = "prototype")
	static public EstadoAlumno estadoAlumnoEsperandoMaquina() {
		return new EstadoAlumnoEsperandoMaquina();
	}

	@Override
	public String toString() {
		return "Esperando máquina";
	}

	@Override
	public boolean isAusente() {
		return false;
	}

	@Override
	public boolean isEsperandoMaquina() {
		return true;
	}

	@Override
	public boolean isInscribiendose() {
		return false;
	}

	@Override
	public boolean isInscripto() {
		return false;
	}

}
