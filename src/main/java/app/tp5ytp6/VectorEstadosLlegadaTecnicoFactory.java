package app.tp5ytp6;

import app.utils.VectorEstados;

public interface VectorEstadosLlegadaTecnicoFactory extends VectorEstados<EventoFactory<LlegadaTecnico>> {

	Object getClaveRnd();

	Object getClaveTiempoProximo();

	Object getClaveHoraProximo();

}
