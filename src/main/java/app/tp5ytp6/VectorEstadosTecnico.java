package app.tp5ytp6;

import app.utils.VectorEstados;

public interface VectorEstadosTecnico extends VectorEstados<Tecnico> {

	Object getClaveEstado();

}
