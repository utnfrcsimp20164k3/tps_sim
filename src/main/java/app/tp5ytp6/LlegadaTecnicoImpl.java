package app.tp5ytp6;

public class LlegadaTecnicoImpl implements LlegadaTecnico {
	
	Tecnico tecnico;

	public LlegadaTecnicoImpl() {
		super();
	}

	@Override
	public Tecnico getTecnico() {
		return tecnico;
	}

	@Override
	public void setTecnico(Tecnico tecnico) {
		this.tecnico = tecnico;
	}

	@Override
	public String toString() {
		return "Llegada del técnico";
	}

}
