package app.tp5ytp6;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
public class EstadoTecnicoAusente implements EstadoTecnico {
	
	public EstadoTecnicoAusente() {
		super();
	}

	@Bean
	@Scope(value = "prototype")
	static public EstadoTecnico estadoTecnicoAusente() {
		return new EstadoTecnicoAusente();
	}

	@Override
	public String toString() {
		return "Ausente";
	}

	@Override
	public boolean isAusente() {
		return true;
	}

	@Override
	public boolean isHaciendoMantenimiento() {
		return false;
	}

	@Override
	public boolean isEsperandoMaquina() {
		return false;
	}

}
