package app.tp5ytp6;

import app.utils.Observable;

public interface MaquinaFactory extends Observable {
	
	Maquina getObject() throws Exception;

	Class<?> getObjectType();

	boolean isSingleton();

}
