package app.tp5ytp6;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

@Configuration
public class FinMantenimientoConfig {

	@Bean
	@Scope(value = "prototype")
	static public EventoFactory<FinMantenimiento> finMantenimientoFactoryTP5EjercicioD() {
		return new FinMantenimientoFactoryTP5EjercicioDImpl();
	}
	
	@Bean
	@Scope(value = "prototype")
	static public FinMantenimientoFactory finMantenimientoFactoryTP6EjercicioD() {
		return new FinMantenimientoFactoryTP6EjercicioDImpl();
	}
	
}
