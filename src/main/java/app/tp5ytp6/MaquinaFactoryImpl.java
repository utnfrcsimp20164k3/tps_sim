package app.tp5ytp6;

import java.util.HashSet;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import app.Loggable;
import app.utils.Observer;

@Component
public class MaquinaFactoryImpl implements MaquinaFactory {
	
	@Autowired
	private ApplicationContext applicationContext;
	static final Logger LOG = LoggerFactory.getLogger(MaquinaFactoryImpl.class);
	private Set<Observer> observers;
	private boolean changed;

	public MaquinaFactoryImpl() {
		super();
		observers = new HashSet<>();
	}

	@Bean
	@Scope("prototype")
	static public MaquinaFactory maquinaFactory() {
		return new MaquinaFactoryImpl();
	}

	@Override
	public Maquina getObject() throws Exception {
		doLog("trace", "getObject()", "[{]", null, null);
		Maquina maquina = (Maquina) applicationContext.getBean("maquina");
		changed = true;
		notifyObservers(maquina);
		doLog("trace", "getObject()", "[}]", null, null);
		return maquina;
	}

	@Override
	public Class<?> getObjectType() {
		return Maquina.class;
	}

	@Override
	public boolean isSingleton() {
		return true;
	}

	@Override
	public void addObserver(Observer o) {
		doLog("trace", "addObserver(Observer o)", "[{]", null, null);
		LOG.debug("addObserver(Observer o) [1] o: " + o);
		observers.add(o);
		doLog("trace", "addObserver(Observer o)", "[}]", null, null);
	}

	@Override
	public void removeObserver(Observer o) {
		observers.add(o);
	}

	@Override
	public int countObservers() {
		return observers.size();
	}

	@Override
	public void deleteObserver(Observer o) {
		observers.remove(o);
	}

	@Override
	public void deleteObservers() {
		observers.clear();
	}

	@Override
	public boolean hasChanged() {
		return changed;
	}

	@Override
	public void notifyObservers() {
		doLog("trace", "notifyObservers()", "[{]", null, null);
		LOG.debug("notifyObservers() [0] observers: " + observers);
		if (hasChanged())
			for (Observer obs : observers)
				obs.update(this, null);
		clearChanged();
		doLog("trace", "notifyObservers()", "[}]", null, null);
	}

	@Override
	public void notifyObservers(Object o) {
		doLog("trace", "notifyObservers(Object o)", "[{]", null, null);
		LOG.debug("notifyObservers() [0] observers: " + observers);
		if (hasChanged())
			for (Observer obs : observers)
				obs.update(this, o);
		clearChanged();
		doLog("trace", "notifyObservers(Object o)", "[}]", null, null);
	}

	protected void clearChanged() {
		changed = false;
	}

	private void doLog(String level, String signature, String corchetes, String nombreVariable, Loggable loggable) {
		switch (level) {
		case "trace":
			if (LOG.isTraceEnabled())
				LOG.trace(toLogMsg(signature, corchetes, nombreVariable, loggable));
			break;
		case "debug":
			if (LOG.isDebugEnabled())
				LOG.debug(toLogMsg(signature, corchetes, nombreVariable, loggable));
			break;
		case "info":
			if (LOG.isInfoEnabled())
				LOG.info(toLogMsg(signature, corchetes, nombreVariable, loggable));
			break;
		case "warn":
			if (LOG.isWarnEnabled())
				LOG.warn(toLogMsg(signature, corchetes, nombreVariable, loggable));
			break;
		case "error":
			if (LOG.isErrorEnabled())
				LOG.error(toLogMsg(signature, corchetes, nombreVariable, loggable));
			break;
		}
	}

	private String toLogMsg(String signature, String corchetes, String nombreVariable, Loggable loggable) {
		StringBuffer s = new StringBuffer(signature);
		if (corchetes != null)
			s.append(" " + corchetes);
		if (nombreVariable != null)
			s.append(" " + "{ l = " + nombreVariable + " }");
		if (nombreVariable != null && loggable == null)
			s.append(nombreVariable + ": " + loggable);
		else if (nombreVariable != null && loggable != null)
			s.append(loggable.toLogString("l"));
		return s.toString();
	}

}
