package app.tp5ytp6;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import app.utils.Observable;
import app.utils.Observer;
import app.Loggable;
import app.utils.DetalleTabla;
import app.utils.TablaVectorEstados;

@Component
public class VectorEstadosLlegadaTecnicoFactoryImpl implements VectorEstadosLlegadaTecnicoFactory {

	@Autowired
	private ApplicationContext applicationContext;
	static final Logger LOG = LoggerFactory.getLogger(VectorEstadosLlegadaTecnicoFactoryImpl.class);
	TablaVectorEstados tabla;
	Set<Tecnico> tecnicos;
	private Set<Observer> observers;
	private boolean changed;
	private DetalleTabla detalleTabla;

	public VectorEstadosLlegadaTecnicoFactoryImpl() {
		super();
		observers = new HashSet<>();
		tecnicos = new HashSet<>();
	}

	@Bean
	@Scope("prototype")
	static public VectorEstadosLlegadaTecnicoFactory vectorEstadosLlegadaTecnicoFactory() {
		return new VectorEstadosLlegadaTecnicoFactoryImpl();
	}

	@Override
	public Object[] getClaves() {
		Object[] claves = new Object[2 + tecnicos.size()];
		claves[0] = getClaveRnd();
		claves[1] = getClaveTiempoProximo();
		Iterator<Tecnico> iterator = tecnicos.iterator();
		for (int i = 0; i < tecnicos.size(); i++) {
			Set<Object> aux = new HashSet<>();
			aux.add(getClaveHoraProximo());
			aux.add(iterator.next());
			claves[i + 2] = aux;
		}
		return claves;
	}

	@Override
	public Object getClaveRnd() {
		return "Rnd llegada técnico";
	}

	@Override
	public Object getClaveTiempoProximo() {
		return "Tiempo entre llegadas del técnico";
	}

	@Override
	public Object getClaveHoraProximo() {
		return "Hora de la próxima llegada del técnico";
	}

	@Override
	public void update(Observable o, Object arg) {
		doLog("trace", "update(Observable o, Object arg)", "[{]", null, null);
		if (detalleTabla == null)
			detalleTabla = (DetalleTabla) applicationContext.getBean("detalleTabla");
		if (arg != null && arg instanceof TablaVectorEstados)
			update((TablaVectorEstados) arg);
		else if (o instanceof EventoFactory && arg instanceof Map<?,?>)
			update((EventoFactory<?>) o, (Map<?,?>) arg);
		else if (o instanceof TecnicoFactory && arg instanceof Tecnico)
			update((TecnicoFactory) o, (Tecnico) arg);
		doLog("trace", "update(Observable o, Object arg)", "[}]", null, null);
	}

	private void update(TablaVectorEstados tabla) {
		doLog("trace", "update(TablaVectorEstados tabla)", "[{]", null, null);
		this.tabla = tabla;
		crearColumnas();
		doLog("trace", "update(TablaVectorEstados tabla)", "[}]", null, null);
	}

	private void crearColumnas() {
		for (int i = 0; i < getClaves().length; i++)
			tabla.crearColumna(getClaves()[i]);
	}

	private void update(EventoFactory<?> llegadaTecnicoFactory, Map<?, ?> arg) {
		doLog("trace", "update(EventoFactory<?> llegadaTecnicoFactory, Map<?, ?> arg)", "[{]", null, null);
		Map<Maquina, BigDecimal> horasProximos = (Map<Maquina, BigDecimal>) arg;
		if (llegadaTecnicoFactory.getRndProximo() != null)
			detalleTabla.getValores().put(getClaveRnd(),
					new BigDecimal(llegadaTecnicoFactory.getRndProximo()).setScale(2, RoundingMode.HALF_UP));
		if (llegadaTecnicoFactory.getTiempoProximo() != null)
			detalleTabla.getValores().put(getClaveTiempoProximo(),
					llegadaTecnicoFactory.getTiempoProximo().setScale(2, RoundingMode.HALF_UP));
		for (Tecnico tecnico : tecnicos) {
			Set<Object> claves = new HashSet<>();
			claves.add(tecnico);
			claves.add(getClaveHoraProximo());
			if (horasProximos.get(tecnico) != null)
				detalleTabla.getValores().put(claves, horasProximos.get(tecnico).setScale(2, RoundingMode.HALF_UP));
			else
				detalleTabla.getValores().put(claves, null);
		}
		changed = true;
		notifyObservers();
		doLog("trace", "update(EventoFactory<?> llegadaTecnicoFactory, Map<?, ?> arg)", "[}]", null, null);
	}


	private void update(TecnicoFactory tecnicoFactory, Tecnico tecnico) {
		doLog("trace", "update(TecnicoFactory tecnicoFactory, Tecnico tecnico)", "[{]", null, null);
		tecnicos.add(tecnico);
		crearColumna(tecnico);
		doLog("trace", "update(TecnicoFactory tecnicoFactory, Tecnico tecnico)", "[}]", null, null);
	}

	private void crearColumna(Tecnico tecnico) {
		Set<Object> clave = new HashSet<>();
		clave.add(getClaveHoraProximo());
		clave.add(tecnico);
		tabla.crearColumna(clave);
	}

	@Override
	public DetalleTabla parse(EventoFactory<LlegadaTecnico> llegadaTecnicoFactory) {
		throw new RuntimeException();
	}

	@Override
	public void addObserver(Observer o) {
		observers.add(o);
	}

	@Override
	public void removeObserver(Observer o) {
		observers.remove(o);
	}

	@Override
	public int countObservers() {
		return observers.size();
	}

	@Override
	public void deleteObserver(Observer o) {
		observers.remove(o);
	}

	@Override
	public void deleteObservers() {
		observers.clear();
	}

	@Override
	public boolean hasChanged() {
		return changed;
	}

	@Override
	public void notifyObservers() {
		if (hasChanged())
			for (Observer obs : observers)
				obs.update(this, detalleTabla);
		clearChanged();
	}

	@Override
	public void notifyObservers(Object o) {
		if (hasChanged())
			for (Observer obs : observers)
				obs.update(this, o);
		clearChanged();
	}

	protected void clearChanged() {
		changed = false;
		detalleTabla = null;
	}

	private void doLog(String level, String signature, String corchetes, String nombreVariable, Loggable loggable) {
		switch (level) {
		case "trace":
			if (LOG.isTraceEnabled())
				LOG.trace(toLogMsg(signature, corchetes, nombreVariable, loggable));
			break;
		case "debug":
			if (LOG.isDebugEnabled())
				LOG.debug(toLogMsg(signature, corchetes, nombreVariable, loggable));
			break;
		case "info":
			if (LOG.isInfoEnabled())
				LOG.info(toLogMsg(signature, corchetes, nombreVariable, loggable));
			break;
		case "warn":
			if (LOG.isWarnEnabled())
				LOG.warn(toLogMsg(signature, corchetes, nombreVariable, loggable));
			break;
		case "error":
			if (LOG.isErrorEnabled())
				LOG.error(toLogMsg(signature, corchetes, nombreVariable, loggable));
			break;
		}
	}

	private String toLogMsg(String signature, String corchetes, String nombreVariable, Loggable loggable) {
		StringBuffer s = new StringBuffer(signature);
		if (corchetes != null)
			s.append(" " + corchetes);
		if (nombreVariable != null)
			s.append(" " + "{ l = " + nombreVariable + " }");
		if (nombreVariable != null && loggable == null)
			s.append(nombreVariable + ": " + loggable);
		else if (nombreVariable != null && loggable != null)
			s.append(loggable.toLogString("l"));
		return s.toString();
	}

}
