package app.tp5ytp6;

import app.utils.Estado;

public interface EstadoTecnico extends Estado {
	
	boolean isAusente();
	
	boolean isHaciendoMantenimiento();
	
	boolean isEsperandoMaquina();

}
