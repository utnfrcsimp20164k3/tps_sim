package app.tp5ytp6;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
public class EstadoAlumnoInscribiendose implements EstadoAlumno {
	
	public EstadoAlumnoInscribiendose() {
		super();
	}

	@Bean
	@Scope(value = "prototype")
	static public EstadoAlumno estadoAlumnoInscribiendose() {
		return new EstadoAlumnoInscribiendose();
	}

	@Override
	public String toString() {
		return "Inscribiéndose";
	}

	@Override
	public boolean isAusente() {
		return false;
	}

	@Override
	public boolean isEsperandoMaquina() {
		return false;
	}

	@Override
	public boolean isInscribiendose() {
		return true;
	}

	@Override
	public boolean isInscripto() {
		return false;
	}

}
