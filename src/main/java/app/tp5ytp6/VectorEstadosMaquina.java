package app.tp5ytp6;

import app.utils.VectorEstados;

public interface VectorEstadosMaquina extends VectorEstados<Maquina> {

	Object getClaveEstado();

	Object getClaveRevisada();

	Object getClaveContadorInscripciones();

}
