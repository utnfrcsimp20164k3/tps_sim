package app.tp5ytp6;

import java.util.HashSet;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import app.Loggable;
import app.utils.Observer;

@Component
public class AlumnoFactoryImpl implements AlumnoFactory {
	
	@Autowired
	private ApplicationContext applicationContext;
	static final Logger LOG = LoggerFactory.getLogger(AlumnoFactoryImpl.class);
	private Set<Observer> observers;
	private boolean changed;

	public AlumnoFactoryImpl() {
		super();
		observers = new HashSet<>();
	}

	@Bean
	@Scope("prototype")
	static public AlumnoFactory alumnoFactory() {
		return new AlumnoFactoryImpl();
	}

	@Override
	public Alumno getObject() throws Exception {
		doLog("trace", "getObject()", "[{]", null, null);
		Alumno alumno = (Alumno) applicationContext.getBean("alumno");;
		changed = true;
		notifyObservers(alumno);
		doLog("trace", "getObject()", "[}]", null, null);
		return alumno;
	}

	@Override
	public Class<?> getObjectType() {
		return Alumno.class;
	}

	@Override
	public boolean isSingleton() {
		return true;
	}

	@Override
	public void addObserver(Observer o) {
		observers.add(o);
	}

	@Override
	public void removeObserver(Observer o) {
		observers.remove(o);
	}

	@Override
	public int countObservers() {
		return observers.size();
	}

	@Override
	public void deleteObserver(Observer o) {
		observers.remove(o);
	}

	@Override
	public void deleteObservers() {
		observers.clear();
	}

	@Override
	public boolean hasChanged() {
		return changed;
	}

	@Override
	public void notifyObservers() {
		if (hasChanged())
			for (Observer obs : observers)
				obs.update(this, null);
		clearChanged();
	}

	@Override
	public void notifyObservers(Object o) {
		if (hasChanged())
			for (Observer obs : observers)
				obs.update(this, o);
		clearChanged();
	}

	protected void clearChanged() {
		changed = false;
	}

	private void doLog(String level, String signature, String corchetes, String nombreVariable, Loggable loggable) {
		switch (level) {
		case "trace":
			if (LOG.isTraceEnabled())
				LOG.trace(toLogMsg(signature, corchetes, nombreVariable, loggable));
			break;
		case "debug":
			if (LOG.isDebugEnabled())
				LOG.debug(toLogMsg(signature, corchetes, nombreVariable, loggable));
			break;
		case "info":
			if (LOG.isInfoEnabled())
				LOG.info(toLogMsg(signature, corchetes, nombreVariable, loggable));
			break;
		case "warn":
			if (LOG.isWarnEnabled())
				LOG.warn(toLogMsg(signature, corchetes, nombreVariable, loggable));
			break;
		case "error":
			if (LOG.isErrorEnabled())
				LOG.error(toLogMsg(signature, corchetes, nombreVariable, loggable));
			break;
		}
	}

	private String toLogMsg(String signature, String corchetes, String nombreVariable, Loggable loggable) {
		StringBuffer s = new StringBuffer(signature);
		if (corchetes != null)
			s.append(" " + corchetes);
		if (nombreVariable != null)
			s.append(" " + "{ l = " + nombreVariable + " }");
		if (nombreVariable != null && loggable == null)
			s.append(nombreVariable + ": " + loggable);
		else if (nombreVariable != null && loggable != null)
			s.append(loggable.toLogString("l"));
		return s.toString();
	}

}
