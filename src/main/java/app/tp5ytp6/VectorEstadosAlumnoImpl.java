package app.tp5ytp6;

import java.util.HashSet;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import app.utils.Observable;
import app.utils.Observer;
import app.Ejercicio;
import app.Loggable;
import app.utils.DetalleTabla;
import app.utils.GestorColas;
import app.utils.TablaVectorEstados;
import app.utils.Temporal;
import app.utils.VectorEstados;

@Component
public class VectorEstadosAlumnoImpl implements VectorEstados<Alumno> {

	@Autowired
	private ApplicationContext applicationContext;
	static final Logger LOG = LoggerFactory.getLogger(VectorEstadosAlumnoImpl.class);
	private Set<Observer> observers;
	private boolean changed;
	private TablaVectorEstados tabla;
	private DetalleTabla detalleTabla;
	private Long alumnosLlegaron, alumnosSeFueron;

	public VectorEstadosAlumnoImpl() {
		super();
		observers = new HashSet<>();
		alumnosLlegaron = 0l;
		alumnosSeFueron = 0l;
	}

	@Bean
	@Scope("prototype")
	static public VectorEstados<Alumno> vectorEstadosAlumno() {
		return new VectorEstadosAlumnoImpl();
	}

	@Override
	public Object[] getClaves() {
		return new Object[] { getClaveEstado() };
	}

	public Object getClaveEstado() {
		return "Estado";
	}

	@Override
	public void update(Observable o, Object arg) {
		doLog("trace", "update(Observable o, Object arg)", "[{]", null, null);
		if (detalleTabla == null)
			detalleTabla = (DetalleTabla) applicationContext.getBean("detalleTabla");
		if (o instanceof Ejercicio && arg instanceof TablaVectorEstados)
			update((TablaVectorEstados) arg);
		// TODO
		/*
		 * else if (o instanceof Ejercicio && arg instanceof Reloj)
		 * update((Reloj) arg);
		 */
		else if (o instanceof AlumnoFactory && arg instanceof Alumno)
			update((AlumnoFactory) o, (Alumno) arg);
		else if (o instanceof GestorColas && ! (arg instanceof Alumno))
			update((GestorColas) o);
		else if (o instanceof GestorColas && arg instanceof Alumno)
			update((Alumno) arg);
		doLog("trace", "update(Observable o, Object arg)", "[}]", null, null);
	}

	private void update(TablaVectorEstados tabla) {
		doLog("trace", "update(TablaVectorEstados tabla)", "[{]", null, null);
		this.tabla = tabla;
		this.tabla.crearColumna("Contador de alumnos que llegaron");
		this.tabla.crearColumna("Contador de alumnos que se fueron");
		changed = true;
		notifyObservers();
		doLog("trace", "update(TablaVectorEstados tabla)", "[}]", null, null);
	}

	// TODO
	/*
	 * private void update(Reloj arg) { doLog("trace", "update(Reloj arg)",
	 * "[{]", null, null); parseTemporales(); changed = true; notifyObservers();
	 * doLog("trace", "update(Reloj arg)", "[}]", null, null); }
	 */

	private void update(AlumnoFactory alumnoFactory, Alumno alumno) {
		doLog("trace", "update(AlumnoFactory alumnoFactory, Alumno alumno)", "[{]", null, null);
		if (tabla.isAMostrar(alumno)) {
			alumno.addObserver(this);
			registrar(alumno);
		}
		doLog("trace", "update(AlumnoFactory alumnoFactory, Alumno alumno)", "[}]", null, null);
	}

	private void update(GestorColas o) {
		detalleTabla.getValores().put("Contador de alumnos que se fueron", alumnosSeFueron);
		detalleTabla.getValores().put("Contador de alumnos que llegaron", alumnosLlegaron);
		changed = true;
		notifyObservers();
	}

	private void update(Alumno alumno) {
		doLog("trace", "update(Alumno alumno)", "[{]", null, null);
		if (alumno.isAusente()) {
			detalleTabla.getValores().put("Contador de alumnos que se fueron", alumnosSeFueron++);
		} else
			detalleTabla.getValores().put("Contador de alumnos que se fueron", alumnosSeFueron);
		detalleTabla.getValores().put("Contador de alumnos que llegaron", alumnosLlegaron++);
		changed = true;
		notifyObservers();
		doLog("trace", "update(Alumno alumno)", "[}]", null, null);
	}

	private void registrar(Alumno alumno) {
		for (Object clave : getClaves()) {
			Set<Object> claves = new HashSet<>();
			claves.add(alumno);
			claves.add(clave);
			tabla.crearColumna(claves);
		}
	}

	private void parseTemporales() {
		Set<Temporal> temporales = tabla.obtenerTemporales(AlumnoImpl.class);
		for (Temporal temporal : temporales) {
			Set<Object> claves = new HashSet<>();
			claves.add(getClaveEstado());
			claves.add(temporal);
			detalleTabla.getValores().put(claves, ((Alumno) temporal).mostrarEstado());
		}
	}

	@Override
	public DetalleTabla parse(Alumno alumno) {
		throw new RuntimeException();
	}

	@Override
	public void addObserver(Observer o) {
		observers.add(o);
	}

	@Override
	public void removeObserver(Observer o) {
		observers.add(o);
	}

	@Override
	public int countObservers() {
		return observers.size();
	}

	@Override
	public void deleteObserver(Observer o) {
		observers.remove(o);
	}

	@Override
	public void deleteObservers() {
		observers.clear();
	}

	@Override
	public boolean hasChanged() {
		return changed;
	}

	@Override
	public void notifyObservers() {
		if (hasChanged())
			for (Observer obs : observers)
				obs.update(this, detalleTabla);
		clearChanged();
	}

	@Override
	public void notifyObservers(Object o) {
		if (hasChanged())
			for (Observer obs : observers)
				obs.update(this, o);
		clearChanged();
	}

	protected void clearChanged() {
		changed = false;
		detalleTabla = null;
	}

	private void doLog(String level, String signature, String corchetes, String nombreVariable, Loggable loggable) {
		switch (level) {
		case "trace":
			if (LOG.isTraceEnabled())
				LOG.trace(toLogMsg(signature, corchetes, nombreVariable, loggable));
			break;
		case "debug":
			if (LOG.isDebugEnabled())
				LOG.debug(toLogMsg(signature, corchetes, nombreVariable, loggable));
			break;
		case "info":
			if (LOG.isInfoEnabled())
				LOG.info(toLogMsg(signature, corchetes, nombreVariable, loggable));
			break;
		case "warn":
			if (LOG.isWarnEnabled())
				LOG.warn(toLogMsg(signature, corchetes, nombreVariable, loggable));
			break;
		case "error":
			if (LOG.isErrorEnabled())
				LOG.error(toLogMsg(signature, corchetes, nombreVariable, loggable));
			break;
		}
	}

	private String toLogMsg(String signature, String corchetes, String nombreVariable, Loggable loggable) {
		StringBuffer s = new StringBuffer(signature);
		if (corchetes != null)
			s.append(" " + corchetes);
		if (nombreVariable != null)
			s.append(" " + "{ l = " + nombreVariable + " }");
		if (nombreVariable != null && loggable == null)
			s.append(nombreVariable + ": " + loggable);
		else if (nombreVariable != null && loggable != null)
			s.append(loggable.toLogString("l"));
		return s.toString();
	}

}
