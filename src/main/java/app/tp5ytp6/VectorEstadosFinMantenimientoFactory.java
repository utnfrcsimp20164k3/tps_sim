package app.tp5ytp6;

import java.util.Set;

import app.utils.VectorEstados;

public interface VectorEstadosFinMantenimientoFactory extends VectorEstados<EventoFactory<FinMantenimiento>> {
	
	Set<Tecnico> getTecnicos();

	Object getClaveRnd();

	Object getClaveTiempoProximo();

	Object getClaveHoraProximo();

	Object getClaveA0();
}
