package app.tp5ytp6;

import app.utils.Observable;
import app.utils.Observer;
import app.utils.Permanente;
import app.utils.Servidor;

public interface Tecnico extends Permanente, Observer, Observable {
	
	Integer getNumero();

	boolean isAusente();
	
	boolean isHaciendoMantenimiento();
	
	boolean isEsperandoMaquina();

	void ofrecerServicio(Maquina maquina);
	
	String mostrarEstado();

	void declararseEsperandoMaquina();

	void declararseAusente();

	Servidor getServidor();

}
