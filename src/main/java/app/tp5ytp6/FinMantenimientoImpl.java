package app.tp5ytp6;

public class FinMantenimientoImpl implements FinMantenimiento {
	
	Tecnico tecnico;

	public FinMantenimientoImpl() {
		super();
	}

	@Override
	public Tecnico getTecnico() {
		return tecnico;
	}

	@Override
	public void setTecnico(Tecnico tecnico) {
		this.tecnico = tecnico;
	}

	@Override
	public String toString() {
		return "Fin de mantenimiento";
	}

}
