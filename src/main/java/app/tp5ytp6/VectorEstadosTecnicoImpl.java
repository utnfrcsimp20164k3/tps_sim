package app.tp5ytp6;

import java.util.HashSet;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import app.utils.Observable;
import app.utils.Observer;
import app.Ejercicio;
import app.Loggable;
import app.utils.DetalleTabla;
import app.utils.TablaVectorEstados;

@Component
public class VectorEstadosTecnicoImpl implements VectorEstadosTecnico {

	@Autowired
	private ApplicationContext applicationContext;
	static final Logger LOG = LoggerFactory.getLogger(VectorEstadosTecnicoImpl.class);
	private Set<Observer> observers;
	private boolean changed;
	private TablaVectorEstados tabla;
	private DetalleTabla detalleTabla;
	private Set<Tecnico> tecnicos;

	public VectorEstadosTecnicoImpl() {
		super();
		observers = new HashSet<>();
		tecnicos = new HashSet<>();
	}

	@Bean
	@Scope("prototype")
	static public VectorEstadosTecnico vectorEstadosTecnico() {
		return new VectorEstadosTecnicoImpl();
	}

	@Override
	public Object[] getClaves() {
		return new Object[] { getClaveEstado() };
	}

	@Override
	public Object getClaveEstado() {
		return "Estado";
	}

	@Override
	public void update(Observable o, Object arg) {
		doLog("trace", "update(Observable o, Object arg)", "[{]", null, null);
		if (detalleTabla == null)
			detalleTabla = (DetalleTabla) applicationContext.getBean("detalleTabla");
		if (o instanceof Ejercicio && arg instanceof TablaVectorEstados)
			update((TablaVectorEstados) arg);
		else if (o instanceof Ejercicio && arg instanceof Reloj)
			update((Reloj) arg);
		else if (o instanceof TecnicoFactory && arg instanceof Tecnico)
			update((TecnicoFactory) o, (Tecnico) arg);
		else if (o instanceof Tecnico)
			update((Tecnico) o);
		doLog("trace", "update(Observable o, Object arg)", "[}]", null, null);
	}

	private void update(TablaVectorEstados tabla) {
		doLog("trace", "update(TablaVectorEstados tabla)", "[{]", null, null);
		this.tabla = tabla;
		doLog("trace", "update(TablaVectorEstados tabla)", "[}]", null, null);
	}

	private void update(Reloj reloj) {
		doLog("trace", "update(Reloj arg)", "[{]", null, null);
		actualizarTecnicos();
		doLog("trace", "update(Reloj arg)", "[}]", null, null);
	}

	private void actualizarTecnicos() {
		doLog("trace", "actualizarTecnicos()", "[{]", null, null);
		for (Tecnico tecnico : tecnicos) {
			update(tecnico);
			detalleTabla = (DetalleTabla) applicationContext.getBean("detalleTabla");
		}
		doLog("trace", "actualizarTecnicos()", "[}]", null, null);
	}

	private void update(TecnicoFactory tecnicoFactory, Tecnico tecnico) {
		doLog("trace", "update(TecnicoFactory tecnicoFactory, Tecnico tecnico)", "[{]", null, null);
		tecnico.addObserver(this);
		registrar(tecnico);
		update(tecnico);
		doLog("trace", "update(TecnicoFactory tecnicoFactory, Tecnico tecnico)", "[}]", null, null);
	}

	private void registrar(Tecnico tecnico) {
		doLog("trace", "registrar(Tecnico tecnico)", "[{]", null, null);
		tecnicos.add(tecnico);
		for (Object clave : getClaves()) {
			Set<Object> claves = new HashSet<>();
			claves.add(tecnico);
			claves.add(clave);
			tabla.crearColumna(claves);
		}
		tabla.registrarPermanente(tecnico);
		doLog("trace", "registrar(Tecnico tecnico)", "[}]", null, null);
	}

	private void update(Tecnico tecnico) {
		doLog("trace", "update(Tecnico tecnico)", "[{]", null, null);
		Set<Object> claves = new HashSet<>();
		claves.add(getClaveEstado());
		claves.add(tecnico);
		detalleTabla.getValores().put(claves, tecnico.mostrarEstado());
		changed = true;
		notifyObservers();
		doLog("trace", "update(Tecnico tecnico)", "[}]", null, null);
	}

	@Override
	public DetalleTabla parse(Tecnico tecnico) {
		throw new RuntimeException();
	}

	@Override
	public void addObserver(Observer o) {
		observers.add(o);
	}

	@Override
	public void removeObserver(Observer o) {
		observers.remove(o);
	}
	
	@Override
	public int countObservers() {
		return observers.size();
	}

	@Override
	public void deleteObserver(Observer o) {
		observers.remove(o);
	}

	@Override
	public void deleteObservers() {
		observers.clear();
	}

	@Override
	public boolean hasChanged() {
		return changed;
	}

	@Override
	public void notifyObservers() {
		if (hasChanged())
			for (Observer obs : observers)
				obs.update(this, detalleTabla);
		clearChanged();
	}

	@Override
	public void notifyObservers(Object o) {
		if (hasChanged())
			for (Observer obs : observers)
				obs.update(this, o);
		clearChanged();
	}

	protected void clearChanged() {
		changed = false;
		detalleTabla = null;
	}

	private void doLog(String level, String signature, String corchetes, String nombreVariable, Loggable loggable) {
		switch (level) {
		case "trace":
			if (LOG.isTraceEnabled())
				LOG.trace(toLogMsg(signature, corchetes, nombreVariable, loggable));
			break;
		case "debug":
			if (LOG.isDebugEnabled())
				LOG.debug(toLogMsg(signature, corchetes, nombreVariable, loggable));
			break;
		case "info":
			if (LOG.isInfoEnabled())
				LOG.info(toLogMsg(signature, corchetes, nombreVariable, loggable));
			break;
		case "warn":
			if (LOG.isWarnEnabled())
				LOG.warn(toLogMsg(signature, corchetes, nombreVariable, loggable));
			break;
		case "error":
			if (LOG.isErrorEnabled())
				LOG.error(toLogMsg(signature, corchetes, nombreVariable, loggable));
			break;
		}
	}

	private String toLogMsg(String signature, String corchetes, String nombreVariable, Loggable loggable) {
		StringBuffer s = new StringBuffer(signature);
		if (corchetes != null)
			s.append(" " + corchetes);
		if (nombreVariable != null)
			s.append(" " + "{ l = " + nombreVariable + " }");
		if (nombreVariable != null && loggable == null)
			s.append(nombreVariable + ": " + loggable);
		else if (nombreVariable != null && loggable != null)
			s.append(loggable.toLogString("l"));
		return s.toString();
	}

}
