package app.tp5ytp6;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
public class EstadoMaquinaRecibiendoMantenimiento implements EstadoMaquina {
	
	public EstadoMaquinaRecibiendoMantenimiento() {
		super();
	}

	@Bean
	@Scope(value = "prototype")
	static public EstadoMaquina estadoMaquinaRecibiendoMantenimiento() {
		return new EstadoMaquinaRecibiendoMantenimiento();
	}

	@Override
	public String toString() {
		return "Recibiendo mantenimiento";
	}

	@Override
	public boolean isInscribiendo() {
		return false;
	}

	@Override
	public boolean isLibre() {
		return false;
	}

	@Override
	public boolean isRecibiendoMantenimiento() {
		return true;
	}

	@Override
	public boolean isEsperandoMantenimiento() {
		return false;
	}

	@Override
	public boolean equals(Object obj) {
		return obj instanceof EstadoMaquinaRecibiendoMantenimiento;
	}

}
