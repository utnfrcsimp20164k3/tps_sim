package app.tp5ytp6;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

@Configuration
public class VectorEstadosFinMantenimientoConfig {

	@Bean
	@Scope("prototype")
	static public VectorEstadosFinMantenimientoFactory vectorEstadosFinMantenimientoFactoryTP5EjercicioD() {
		return new VectorEstadosFinMantenimientoFactoryTP5EjercicioDImpl();
	}

	@Bean
	@Scope("prototype")
	static public VectorEstadosFinMantenimientoFactory vectorEstadosFinMantenimientoFactoryTP6EjercicioD() {
		return new VectorEstadosFinMantenimientoFactoryTP6EjercicioDImpl();
	}

}
