package app.tp5ytp6;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
public class EstadoMaquinaEsperandoMantenimiento implements EstadoMaquina {
	
	public EstadoMaquinaEsperandoMantenimiento() {
		super();
	}

	@Bean
	@Scope(value = "prototype")
	static public EstadoMaquina estadoMaquinaEsperandoMantenimiento() {
		return new EstadoMaquinaEsperandoMantenimiento();
	}

	@Override
	public String toString() {
		return "Esperando mantenimiento";
	}

	@Override
	public boolean isInscribiendo() {
		return false;
	}

	@Override
	public boolean isLibre() {
		return false;
	}

	@Override
	public boolean isRecibiendoMantenimiento() {
		return false;
	}

	@Override
	public boolean isEsperandoMantenimiento() {
		return true;
	}

	@Override
	public boolean equals(Object obj) {
		return obj instanceof EstadoMaquinaEsperandoMantenimiento;
	}

}
