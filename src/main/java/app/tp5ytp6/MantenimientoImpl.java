package app.tp5ytp6;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import app.utils.TipoServicio;

@Component
public class MantenimientoImpl implements TipoServicio {
	
	private String nombre;
	
	public MantenimientoImpl() {
		super();
		nombre = "Mantenimiento";
	}

	@Bean
	@Scope(value = "prototype")
	static public TipoServicio mantenimiento() {
		return new MantenimientoImpl();
	}

	@Override
	public String getNombre() {
		return nombre;
	}
	
	@Override
	public String toString() {
		return nombre;
	}
	
	@Override
	public boolean equals(Object obj) {
		return obj != null && obj instanceof MantenimientoImpl && ((MantenimientoImpl) obj).getNombre().equals(nombre);
	}

}
