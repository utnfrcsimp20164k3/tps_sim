package app.tp5ytp6;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
public class EstadoTecnicoHaciendoMantenimiento implements EstadoTecnico {
	
	public EstadoTecnicoHaciendoMantenimiento() {
		super();
	}

	@Bean
	@Scope(value = "prototype")
	static public EstadoTecnico estadoTecnicoHaciendoMantenimiento() {
		return new EstadoTecnicoHaciendoMantenimiento();
	}

	@Override
	public String toString() {
		return "Haciendo mantenimiento";
	}

	@Override
	public boolean isAusente() {
		return false;
	}

	@Override
	public boolean isHaciendoMantenimiento() {
		return true;
	}

	@Override
	public boolean isEsperandoMaquina() {
		return false;
	}

}
