package app.tp5ytp6;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Random;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import app.Ejercicio;
import app.Loggable;
import app.utils.Observable;
import app.utils.Observer;
import app.utils.TablaVectorEstados;
import app.utils.distribucion.TipoDistribucion;
import app.utils.generador.GeneradorPseudoaleatorio;

@Component
public class FinMantenimientoFactoryTP5EjercicioDImpl implements EventoFactory<FinMantenimiento>, Observer, Observable {

	static final Logger LOG = LoggerFactory.getLogger(FinMantenimientoFactoryTP5EjercicioDImpl.class);
	private Random random;
	private Double rndProximo;
	private BigDecimal tiempoProximo, horaActual;
	private TipoDistribucion tipoDistribucion;
	private GeneradorPseudoaleatorio generadorPseudoaleatorio;
	private Map<Tecnico, BigDecimal> horasProximos;
	private Set<Observer> observers;
	private boolean changed;

	public FinMantenimientoFactoryTP5EjercicioDImpl() {
		super();
		random = new Random();
		observers = new HashSet<>();
		horaActual = new BigDecimal("0");
		horasProximos = new HashMap<>();
	}

	@Override
	public Double getRndProximo() {
		return rndProximo;
	}

	@Override
	public BigDecimal getHoraProximo() {
		return null;
	}

	@Override
	public Map<?, BigDecimal> getHorasProximos() {
		return horasProximos;
	}

	@Override
	public BigDecimal getTiempoProximo() {
		return tiempoProximo;
	}

	@Override
	public FinMantenimiento getObject() throws Exception {
		return new FinMantenimientoImpl();
	}

	@Override
	public Class<?> getObjectType() {
		return FinMantenimiento.class;
	}

	@Override
	public boolean isSingleton() {
		return true;
	}

	@Override
	public void setTipoDistribucion(TipoDistribucion tipoDistribucion) {
		this.tipoDistribucion = tipoDistribucion;
		generadorPseudoaleatorio = tipoDistribucion.getDefaultGenerator();
	}

	@Override
	public GeneradorPseudoaleatorio getGeneradorPseudoaleatorio() {
		return generadorPseudoaleatorio;
	}

	@Override
	public void update(Observable o, Object arg) {
		doLog("trace", "update(Observable o, Object arg)", "[{]", null, null);
		if (o instanceof Ejercicio && arg instanceof BigDecimal)
			horaActual = (BigDecimal) arg;
		else if (o instanceof Ejercicio && arg instanceof TablaVectorEstados)
			update((TablaVectorEstados) arg);
		else if (o instanceof Ejercicio && arg instanceof Reloj)
			update((Reloj) arg);
		else if (o instanceof TecnicoFactory && arg instanceof Tecnico)
			registrarTecnico((Tecnico) arg);
		else if (o instanceof Tecnico)
			update((Tecnico) o);
		doLog("trace", "update(Observable o, Object arg)", "[}]", null, null);
	}

	private void update(TablaVectorEstados tabla) {
		doLog("trace", "update(TablaVectorEstados tabla)", "[{]", null, null);
		if (tipoDistribucion == null)
			throw new RuntimeException("No se asoció a esta factory con un tipo de distribución.");
		doLog("trace", "update(TablaVectorEstados tabla)", "[}]", null, null);
	}

	private void update(Reloj reloj) {
		doLog("trace", "update(Reloj reloj)", "[{]", null, null);
		for (Tecnico tecnico : horasProximos.keySet())
			if (horasProximos.get(tecnico) != null && horasProximos.get(tecnico).equals(horaActual)) {
				generarEvento(tecnico);
			} else {
				changed = rndProximo != null || tiempoProximo != null || !horasProximos.values().isEmpty();
				rndProximo = null;
				tiempoProximo = null;
				notifyObservers(horasProximos);
			}
		doLog("trace", "update(Reloj reloj)", "[}]", null, null);
	}

	private void registrarTecnico(Tecnico tecnico) {
		doLog("trace", "registrarTecnico(Tecnico tecnico)", "[{]", null, null);
		tecnico.addObserver(this);
		horasProximos.put(tecnico, null);
		doLog("trace", "registrarTecnico(Tecnico tecnico)", "[}]", null, null);
	}

	private void update(Tecnico tecnico) {
		doLog("trace", "update(Tecnico tecnico)", "[{]", null, null);
		if (tecnico.isHaciendoMantenimiento())
			definirHoraProximo(tecnico);
		else if (tecnico.isEsperandoMaquina()) {
			horasProximos.put(tecnico, null);
			changed = true;
			notifyObservers(horasProximos);
		}
		doLog("trace", "update(Tecnico tecnico)", "[}]", null, null);
	}

	private void definirHoraProximo(Tecnico tecnico) {
		doLog("trace", "definirHoraProximo(Tecnico tecnico)", "[{]", null, null);
		rndProximo = random.nextDouble();
		tiempoProximo = BigDecimal.valueOf((double) generadorPseudoaleatorio.next(rndProximo));
		BigDecimal horaProximo = horaActual.add(tiempoProximo);
		horasProximos.put(tecnico, horaProximo);
		changed = true;
		notifyObservers(horaProximo);
		changed = true;
		notifyObservers(horasProximos);
		doLog("trace", "definirHoraProximo(Tecnico tecnico)", "[}]", null, null);
	}
	
	private void generarEvento(Tecnico tecnico) {
		doLog("trace", "generarEvento(Tecnico tecnico)", "[{]", null, null);
		try {
			FinMantenimiento finMantenimiento = (FinMantenimiento) getObject();
			finMantenimiento.setTecnico(tecnico);
			changed = true;
			notifyObservers(finMantenimiento);
		} catch (Exception e) {
			e.printStackTrace();
		}
		doLog("trace", "generarEvento(Tecnico tecnico)", "[}]", null, null);
	}

	@Override
	public void addObserver(Observer o) {
		observers.add(o);
	}

	@Override
	public void removeObserver(Observer o) {
		observers.remove(o);
	}

	@Override
	public int countObservers() {
		return observers.size();
	}

	@Override
	public void deleteObserver(Observer o) {
		observers.remove(o);
	}

	@Override
	public void deleteObservers() {
		observers.clear();
	}

	@Override
	public boolean hasChanged() {
		return changed;
	}

	@Override
	public void notifyObservers() {
		if (hasChanged())
			for (Observer obs : observers)
				obs.update(this, null);
		clearChanged();
	}

	@Override
	public void notifyObservers(Object instance) {
		if (hasChanged())
			for (Observer obs : observers)
				obs.update(this, instance);
		clearChanged();
	}

	protected void clearChanged() {
		changed = false;
	}
	private void doLog(String level, String signature, String corchetes, String nombreVariable, Loggable loggable) {
		switch (level) {
		case "trace":
			if (LOG.isTraceEnabled())
				LOG.trace(toLogMsg(signature, corchetes, nombreVariable, loggable));
			break;
		case "debug":
			if (LOG.isDebugEnabled())
				LOG.debug(toLogMsg(signature, corchetes, nombreVariable, loggable));
			break;
		case "info":
			if (LOG.isInfoEnabled())
				LOG.info(toLogMsg(signature, corchetes, nombreVariable, loggable));
			break;
		case "warn":
			if (LOG.isWarnEnabled())
				LOG.warn(toLogMsg(signature, corchetes, nombreVariable, loggable));
			break;
		case "error":
			if (LOG.isErrorEnabled())
				LOG.error(toLogMsg(signature, corchetes, nombreVariable, loggable));
			break;
		}
	}

	private String toLogMsg(String signature, String corchetes, String nombreVariable, Loggable loggable) {
		StringBuffer s = new StringBuffer(signature);
		if (corchetes != null)
			s.append(" " + corchetes);
		if (nombreVariable != null)
			s.append(" " + "{ l = " + nombreVariable + " }");
		if (nombreVariable != null && loggable == null)
			s.append(nombreVariable + ": " + loggable);
		else if (nombreVariable != null && loggable != null)
			s.append(loggable.toLogString("l"));
		return s.toString();
	}

}
