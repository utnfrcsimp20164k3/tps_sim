package app.tp5ytp6;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import app.utils.Observable;
import app.utils.Observer;
import app.Ejercicio;
import app.Loggable;
import app.utils.DetalleTabla;
import app.utils.TablaVectorEstados;

@Component
public class VectorEstadosFinInscripcionFactoryImpl implements VectorEstadosFinInscripcionFactory {

	static final Logger LOG = LoggerFactory.getLogger(VectorEstadosFinInscripcionFactoryImpl.class);
	@Autowired
	private ApplicationContext applicationContext;
	private TablaVectorEstados tabla;
	private Set<Maquina> maquinas;
	private Set<Observer> observers;
	private boolean changed;
	private DetalleTabla detalleTabla;

	public VectorEstadosFinInscripcionFactoryImpl() {
		super();
		observers = new HashSet<>();
		maquinas = new HashSet<>();
	}

	@Bean
	@Scope("prototype")
	static public VectorEstadosFinInscripcionFactory vectorEstadosFinInscripcionFactory() {
		return new VectorEstadosFinInscripcionFactoryImpl();
	}

	@Override
	public Object[] getClaves() {
		Object[] claves = new Object[2 + maquinas.size()];
		claves[0] = getClaveRnd();
		claves[1] = getClaveTiempoProximo();
		Iterator<Maquina> iterator = maquinas.iterator();
		for (int i = 0; i < maquinas.size(); i++) {
			Set<Object> aux = new HashSet<>();
			aux.add(getClaveHoraProximo());
			aux.add(iterator.next());
			claves[i + 2] = aux;
		}
		return claves;
	}

	@Override
	public Set<Maquina> getMaquinas() {
		return maquinas;
	}

	@Override
	public Object getClaveRnd() {
		return "Rnd fin inscripción";
	}

	@Override
	public Object getClaveTiempoProximo() {
		return "Tiempo entre fines de inscripción";
	}

	@Override
	public Object getClaveHoraProximo() {
		return "Hora del fin de la inscripción actual";
	}

	@Override
	public void update(Observable o, Object arg) {
		doLog("trace", "update(Observable o, Object arg)", "[{]", null, null);
		if (detalleTabla == null)
			detalleTabla = (DetalleTabla) applicationContext.getBean("detalleTabla");
		if (o instanceof Ejercicio && arg instanceof TablaVectorEstados)
			update((TablaVectorEstados) arg);
		else if (o instanceof EventoFactory && arg instanceof Map<?, ?>)
			update((EventoFactory<?>) o, (Map<?, ?>) arg);
		else if (o instanceof MaquinaFactory && arg instanceof Maquina)
			update((MaquinaFactory) o, (Maquina) arg);
		doLog("trace", "update(Observable o, Object arg)", "[}]", null, null);
	}

	private void update(TablaVectorEstados tabla) {
		doLog("trace", "update(TablaVectorEstados tabla)", "[{]", null, null);
		this.tabla = tabla;
		crearColumnas();
		doLog("trace", "update(TablaVectorEstados tabla)", "[}]", null, null);
	}

	private void crearColumnas() {
		for (int i = 0; i < getClaves().length; i++)
			tabla.crearColumna(getClaves()[i]);
	}

	private void update(EventoFactory<?> finInscripcionFactory, Map<?, ?> arg) {
		doLog("trace", "update(EventoFactory<?> finInscripcionFactory, Map<?, ?> arg)", "[{]", null, null);
		Map<Maquina, BigDecimal> horasProximos = (Map<Maquina, BigDecimal>) arg;
		if (finInscripcionFactory.getRndProximo() != null)
			detalleTabla.getValores().put(getClaveRnd(),
					new BigDecimal(finInscripcionFactory.getRndProximo()).setScale(2, RoundingMode.HALF_UP));
		if (finInscripcionFactory.getTiempoProximo() != null)
			detalleTabla.getValores().put(getClaveTiempoProximo(),
					finInscripcionFactory.getTiempoProximo().setScale(2, RoundingMode.HALF_UP));
		for (Maquina maquina : maquinas) {
			Set<Object> claves = new HashSet<>();
			claves.add(maquina);
			claves.add(getClaveHoraProximo());
			if (horasProximos.get(maquina) != null)
				detalleTabla.getValores().put(claves, horasProximos.get(maquina).setScale(2, RoundingMode.HALF_UP));
			else
				detalleTabla.getValores().put(claves, null);
		}
		changed = true;
		notifyObservers();
		doLog("trace", "update(EventoFactory<?> finInscripcionFactory, Map<?, ?> arg)", "[}]", null, null);
	}

	private void update(MaquinaFactory maquinaFactory, Maquina maquina) {
		doLog("trace", "update(MaquinaFactory maquinaFactory, Maquina maquina)", "[{]", null, null);
		maquinas.add(maquina);
		crearColumna(maquina);
		doLog("trace", "update(MaquinaFactory maquinaFactory, Maquina maquina)", "[}]", null, null);
	}

	private void crearColumna(Maquina maquina) {
		Set<Object> clave = new HashSet<>();
		clave.add(getClaveHoraProximo());
		clave.add(maquina);
		tabla.crearColumna(clave);
	}

	@Override
	public DetalleTabla parse(EventoFactory<FinInscripcion> finInscripcionFactory) {
		throw new RuntimeException();
	}

	@Override
	public void addObserver(Observer o) {
		doLog("trace", "addObserver(Observer o)", "[{]", null, null);
		observers.add(o);
		doLog("trace", "addObserver(Observer o)", "[}]", null, null);
	}

	@Override
	public void removeObserver(Observer o) {
		observers.remove(o);
	}

	@Override
	public int countObservers() {
		return observers.size();
	}

	@Override
	public void deleteObserver(Observer o) {
		observers.remove(o);
	}

	@Override
	public void deleteObservers() {
		observers.clear();
	}

	@Override
	public boolean hasChanged() {
		return changed;
	}

	@Override
	public void notifyObservers() {
		if (hasChanged())
			for (Observer obs : observers)
				obs.update(this, detalleTabla);
		clearChanged();
	}

	@Override
	public void notifyObservers(Object o) {
		if (hasChanged())
			for (Observer obs : observers)
				obs.update(this, o);
		clearChanged();
	}

	protected void clearChanged() {
		changed = false;
		detalleTabla = null;
	}

	private void doLog(String level, String signature, String corchetes, String nombreVariable, Loggable loggable) {
		switch (level) {
		case "trace":
			if (LOG.isTraceEnabled())
				LOG.trace(toLogMsg(signature, corchetes, nombreVariable, loggable));
			break;
		case "debug":
			if (LOG.isDebugEnabled())
				LOG.debug(toLogMsg(signature, corchetes, nombreVariable, loggable));
			break;
		case "info":
			if (LOG.isInfoEnabled())
				LOG.info(toLogMsg(signature, corchetes, nombreVariable, loggable));
			break;
		case "warn":
			if (LOG.isWarnEnabled())
				LOG.warn(toLogMsg(signature, corchetes, nombreVariable, loggable));
			break;
		case "error":
			if (LOG.isErrorEnabled())
				LOG.error(toLogMsg(signature, corchetes, nombreVariable, loggable));
			break;
		}
	}

	private String toLogMsg(String signature, String corchetes, String nombreVariable, Loggable loggable) {
		StringBuffer s = new StringBuffer(signature);
		if (corchetes != null)
			s.append(" " + corchetes);
		if (nombreVariable != null)
			s.append(" " + "{ l = " + nombreVariable + " }");
		if (nombreVariable != null && loggable == null)
			s.append(nombreVariable + ": " + loggable);
		else if (nombreVariable != null && loggable != null)
			s.append(loggable.toLogString("l"));
		return s.toString();
	}

}
