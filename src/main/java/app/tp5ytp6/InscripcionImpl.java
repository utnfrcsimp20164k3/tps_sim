package app.tp5ytp6;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import app.utils.TipoServicio;

@Component
public class InscripcionImpl implements TipoServicio {
	
	private String nombre;
	
	public InscripcionImpl() {
		super();
		nombre = "Inscripción";
	}

	@Bean
	@Scope(value = "prototype")
	static public TipoServicio inscripcion() {
		return new InscripcionImpl();
	}

	@Override
	public String getNombre() {
		return nombre;
	}
	
	@Override
	public String toString() {
		return nombre;
	}
	
	@Override
	public boolean equals(Object obj) {
		return obj != null && obj instanceof InscripcionImpl && ((InscripcionImpl) obj).getNombre().equals(nombre);
	}

}
