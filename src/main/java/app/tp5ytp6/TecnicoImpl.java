package app.tp5ytp6;

import java.util.HashSet;
import java.util.Set;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import app.Loggable;
import app.utils.GestorColas;
import app.utils.Observable;
import app.utils.Observer;
import app.utils.Servidor;
import app.utils.TipoServicio;

@Component
public class TecnicoImpl implements Tecnico, Observable {

	static final Logger LOG = LoggerFactory.getLogger(TecnicoImpl.class);
	static int ultimoNumero;
	private int numero;
	private EstadoTecnico estado;
	@Autowired
	private Servidor servidor;
	@Autowired
	private EstadoTecnico estadoTecnicoAusente;
	@Autowired
	private EstadoTecnico estadoTecnicoHaciendoMantenimiento;
	@Autowired
	private EstadoTecnico estadoTecnicoEsperandoMaquina;
	@Autowired
	private TipoServicio mantenimiento;
	private Set<Observer> observers;
	private boolean changed;

	static {
		ultimoNumero = 0;
	}

	public TecnicoImpl() {
		super();
		observers = new HashSet<>();
		numero = ultimoNumero++;
	}

	@Bean
	@Scope(value = "prototype")
	static public Tecnico tecnico() {
		return new TecnicoImpl();
	}

	@PostConstruct
	public void init() {
		servidor.addObserver(this);
		servidor.setTipoServicio(mantenimiento);
		estado = estadoTecnicoAusente;
	}

	@Override
	public Integer getNumero() {
		return numero;
	}

	@Override
	public Servidor getServidor() {
		return servidor;
	}

	@Override
	public void ofrecerServicio(Maquina maquina) {
		maquina.servicioDisponible(servidor);
	}

	@Override
	public void declararseEsperandoMaquina() {
		cambiarEstado(estadoTecnicoEsperandoMaquina);
	}

	@Override
	public void declararseAusente() {
		servidor.terminar();
		cambiarEstado(estadoTecnicoAusente);
	}

	@Override
	public boolean isAusente() {
		return estado != null && estado.isAusente();
	}

	@Override
	public boolean isHaciendoMantenimiento() {
		return estado != null && estado.isHaciendoMantenimiento();
	}

	@Override
	public boolean isEsperandoMaquina() {
		return estado != null && estado.isEsperandoMaquina();
	}

	@Override
	public String mostrarEstado() {
		return estado.toString();
	}

	@Override
	public void addObserver(Observer o) {
		observers.add(o);
	}

	@Override
	public void removeObserver(Observer o) {
		observers.remove(o);
	}

	@Override
	public int countObservers() {
		return observers.size();
	}

	@Override
	public void deleteObserver(Observer o) {
		observers.remove(o);
	}

	@Override
	public void deleteObservers() {
		observers.clear();
	}

	@Override
	public boolean hasChanged() {
		return changed;
	}

	@Override
	public void notifyObservers() {
		if (hasChanged())
			for (Observer obs : observers)
				obs.update(this, null);
		clearChanged();
	}

	@Override
	public void notifyObservers(Object o) {
		if (hasChanged())
			for (Observer obs : observers)
				obs.update(this, o);
		clearChanged();
	}

	protected void clearChanged() {
		changed = false;
	}

	@Override
	public void update(Observable o, Object arg) {
		doLog("trace", "update(Observable o, Object arg)", "[{]", null, null);
		if (o instanceof ColaMaquinas)
			update((ColaMaquinas) o);
		else if (o instanceof Servidor)
			update((Servidor) o);
		else if (o instanceof GestorColas && arg != null && arg instanceof FinMantenimiento)
			update((GestorColas) o, (FinMantenimiento) arg);
		doLog("trace", "update(Observable o, Object arg)", "[}]", null, null);
	}

	private void cambiarEstado(EstadoTecnico estado) {
		changed = this.estado != estado;
		this.estado = estado;
		notifyObservers();
	}

	private void update(ColaMaquinas colaMaquinas) {
		doLog("trace", "update(ColaMaquinas colaMaquinas)", "[{]", null, null);
		if (colaMaquinas.isEmpty())
			cambiarEstado(estadoTecnicoAusente);
		doLog("trace", "update(ColaMaquinas colaMaquinas)", "[}]", null, null);
	}

	private void update(Servidor servidor) {
		doLog("trace", "update(Servidor servidor)", "[{]", null, null);
		if (servidor.isSirviendo())
			cambiarEstado(estadoTecnicoHaciendoMantenimiento);
		else 
			cambiarEstado(estadoTecnicoEsperandoMaquina);
		doLog("trace", "update(Servidor servidor)", "[}]", null, null);
	}

	private void update(GestorColas gestorColas, FinMantenimiento finMantenimiento) {
		doLog("trace", "update(GestorColas gestorColas, FinMantenimiento finMantenimiento)", "[{]", null, null);
		if (finMantenimiento.getTecnico().equals(this))
			servidor.terminar(); 
		doLog("trace", "update(GestorColas gestorColas, FinMantenimiento finMantenimiento)", "[}]", null, null);
	}

	@Override
	public boolean equals(Object obj) {
		return obj != null && obj instanceof Tecnico && ((Tecnico) obj).getNumero().equals(numero);
	}

	private void doLog(String level, String signature, String corchetes, String nombreVariable, Loggable loggable) {
		switch (level) {
		case "trace":
			if (LOG.isTraceEnabled())
				LOG.trace(toLogMsg(signature, corchetes, nombreVariable, loggable));
			break;
		case "debug":
			if (LOG.isDebugEnabled())
				LOG.debug(toLogMsg(signature, corchetes, nombreVariable, loggable));
			break;
		case "info":
			if (LOG.isInfoEnabled())
				LOG.info(toLogMsg(signature, corchetes, nombreVariable, loggable));
			break;
		case "warn":
			if (LOG.isWarnEnabled())
				LOG.warn(toLogMsg(signature, corchetes, nombreVariable, loggable));
			break;
		case "error":
			if (LOG.isErrorEnabled())
				LOG.error(toLogMsg(signature, corchetes, nombreVariable, loggable));
			break;
		}
	}

	private String toLogMsg(String signature, String corchetes, String nombreVariable, Loggable loggable) {
		StringBuffer s = new StringBuffer(signature);
		if (corchetes != null)
			s.append(" " + corchetes);
		if (nombreVariable != null)
			s.append(" " + "{ l = " + nombreVariable + " }");
		if (nombreVariable != null && loggable == null)
			s.append(nombreVariable + ": " + loggable);
		else if (nombreVariable != null && loggable != null)
			s.append(loggable.toLogString("l"));
		return s.toString();
	}

}
