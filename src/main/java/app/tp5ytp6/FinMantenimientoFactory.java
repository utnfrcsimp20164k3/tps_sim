package app.tp5ytp6;

import java.math.BigDecimal;
import java.util.Map;

import app.utils.TablaIntegracionNumerica;

public interface FinMantenimientoFactory extends EventoFactory<FinMantenimiento> {

	BigDecimal getA0Proximo();

	void setTablasDeIntegracionNumerica(Map<Object, TablaIntegracionNumerica> tablasDeIntegracionNumerica);

	void setH(String h);

}
