package app.tp5ytp6;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
public class EstadoAlumnoAusente implements EstadoAlumno {
	
	public EstadoAlumnoAusente() {
		super();
	}

	@Bean
	@Scope(value = "prototype")
	static public EstadoAlumno estadoAlumnoAusente() {
		return new EstadoAlumnoAusente();
	}

	@Override
	public String toString() {
		return "Ausente";
	}

	@Override
	public boolean isAusente() {
		return true;
	}

	@Override
	public boolean isEsperandoMaquina() {
		return false;
	}

	@Override
	public boolean isInscribiendose() {
		return false;
	}

	@Override
	public boolean isInscripto() {
		return false;
	}

}
