package app.tp5ytp6;

import app.Loggable;
import app.utils.Observable;
import app.utils.Observer;
import app.utils.Servidor;
import app.utils.Temporal;

public interface Alumno extends Temporal, Observer, Observable, Loggable {
	
	boolean isAusente();
	
	boolean isEsperandoMaquina();
	
	boolean isInscribiendose();
	
	boolean isInscripto();

	void declararseEsperandoMaquina();
	
	void servicioDisponible(Servidor servidor);
	
	void declararseAusente();
	
	String mostrarEstado();

}
