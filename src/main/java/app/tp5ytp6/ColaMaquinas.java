package app.tp5ytp6;

import java.util.Deque;
import java.util.HashSet;
import java.util.Set;

import app.utils.Observable;
import app.utils.Observer;
import app.utils.Cola;

public class ColaMaquinas implements Cola<Maquina, Tecnico>, Observable {

	private Set<Observer> observers;
	private boolean changed;
	private Deque<Maquina> maquinas;
	private Deque<Tecnico> tecnicos;
	private Integer size;
	private String id;

	public ColaMaquinas(Deque<Maquina> maquinas, Deque<Tecnico> tecnicos) {
		super();
		observers = new HashSet<>();
		this.maquinas = maquinas;
		this.tecnicos = tecnicos;
		id = "Cola de máquinas";
	}

	public ColaMaquinas(Deque<Maquina> maquinas, Deque<Tecnico> tecnicos, String id) {
		this(maquinas, tecnicos);
		this.id = id;
	}

	public ColaMaquinas(Deque<Maquina> maquinas, Deque<Tecnico> tecnicos, Integer size) {
		this(maquinas, tecnicos);
		this.size = size;
	}

	public ColaMaquinas(Deque<Maquina> maquinas, Deque<Tecnico> tecnicos, String id, Integer size) {
		this(maquinas, tecnicos, id);
		this.size = size;
	}

	@Override
	public void setSize(Integer size) {
		this.size = size;
	}

	@Override
	public Integer getOcupacion() {
		return maquinas.size();
	}

	@Override
	public void next() {
		if (!tecnicos.isEmpty() && maquinas.isEmpty()) {
			for (Tecnico tecnico : tecnicos) {
				if (tecnico.isEsperandoMaquina()) {
					tecnico.declararseAusente();
					tecnicos.remove(tecnico);
				}
			}
		} else if (!tecnicos.isEmpty() && !maquinas.isEmpty()) {
			for (Tecnico tecnico : tecnicos)
				if (tecnico.isEsperandoMaquina()) {
					Maquina maquinaARemover = null;
					for (Maquina maquina : maquinas)
						if (maquina.isEsperandoMantenimiento()) {
							maquinaARemover = maquina;
							tecnico.ofrecerServicio(maquina);
							break;
						}
					maquinas.remove(maquinaARemover);
				}
		}
	}

	@Override
	public boolean hayLugar() {
		int ocupacion = getOcupacion();
		return size - ocupacion > 0;
	}

	@Override
	public boolean isEmpty() {
		return maquinas.isEmpty();
	}

	@Override
	public void agregarCliente(Maquina maquina) {
		if (hayLugar()) {
			maquinas.addLast(maquina);
		}
	}

	@Override
	public void agregarServidor(Tecnico tecnico) {
		if (!tecnicos.contains(tecnico))
			tecnicos.addLast(tecnico);
	}

	@Override
	public Object getId() {
		return id;
	}

	@Override
	public String toString() {
		return id;
	}

	@Override
	public void addObserver(Observer o) {
		observers.add(o);
	}

	@Override
	public void removeObserver(Observer o) {
		observers.remove(o);
	}

	@Override
	public int countObservers() {
		return observers.size();
	}

	@Override
	public void deleteObserver(Observer o) {
		observers.remove(o);
	}

	@Override
	public void deleteObservers() {
		observers.clear();
	}

	@Override
	public boolean hasChanged() {
		return changed;
	}

	@Override
	public void notifyObservers() {
		if (hasChanged())
			for (Observer obs : observers)
				obs.update(this, null);
		clearChanged();
	}

	@Override
	public void notifyObservers(Object o) {
		if (hasChanged())
			for (Observer obs : observers)
				obs.update(this, o);
		clearChanged();
	}

	protected void clearChanged() {
		changed = false;
	}

}
