package app.tp5ytp6;

import app.utils.VectorEstados;

public interface VectorEstadosLlegadaAlumnoFactory extends VectorEstados<EventoFactory<LlegadaAlumno>> {

	Object getClaveRnd();

	Object getClaveTiempoProximo();

	Object getClaveHoraProximo();

}
