package app.tp5ytp6;

import java.util.HashSet;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import app.utils.Observable;
import app.utils.Observer;
import app.Ejercicio;
import app.Loggable;
import app.utils.Cola;
import app.utils.DetalleTabla;
import app.utils.GestorColas;
import app.utils.TablaVectorEstados;
import app.utils.VectorEstados;

@Component
public class VectorEstadosGestorColasImpl implements VectorEstados<GestorColas> {

	@Autowired
	private ApplicationContext applicationContext;
	@Autowired
	private GestorColas gestorColas;
	static final Logger LOG = LoggerFactory.getLogger(VectorEstadosGestorColasImpl.class);
	private Set<Observer> observers;
	private boolean changed;
	private DetalleTabla detalleTabla;

	public VectorEstadosGestorColasImpl() {
		super();
		observers = new HashSet<>();
	}

	@Bean
	@Scope(value = "prototype")
	static public VectorEstados<GestorColas> vectorEstadosGestorColas() {
		return new VectorEstadosGestorColasImpl();
	}

	@Override
	public Object[] getClaves() {
		return gestorColas.getClaves();
	}

	@Override
	public void update(Observable o, Object arg) {
		doLog("trace", "update(Observable o, Object arg)", "[{]", null, null);
		if (detalleTabla == null)
			detalleTabla = (DetalleTabla) applicationContext.getBean("detalleTabla");
		if (o instanceof Ejercicio && arg != null && arg instanceof TablaVectorEstados)
			update((TablaVectorEstados) arg);
		else if (o instanceof GestorColas && arg != null && arg instanceof Cola<?,?>)
			update((Cola<?,?>) arg);
		doLog("trace", "update(Observable o, Object arg)", "[}]", null, null);
	}
	
	private void update(TablaVectorEstados tabla) {
		doLog("trace", "update(TablaVectorEstados tabla)", "[{]", null, null);
		for (int i = 0; i < getClaves().length; i++)
			tabla.crearColumna(getClaves()[i]);
		doLog("trace", "update(TablaVectorEstados tabla)", "[}]", null, null);
	}

	private void update(Cola<?,?> cola) {
		doLog("trace", "update(GestorColas gestorColas, Cola<?,?> cola)", "[{]", null, null);
		detalleTabla.getValores().put(cola.getId(), cola.getOcupacion());
		changed = true;
		notifyObservers();
		doLog("trace", "update(GestorColas gestorColas, Cola<?,?> cola)", "[}]", null, null);
	}

	@Override
	public DetalleTabla parse(GestorColas gestorColas) {
		throw new RuntimeException();
	}

	@Override
	public void addObserver(Observer o) {
		observers.add(o);
	}

	@Override
	public void removeObserver(Observer o) {
		observers.remove(o);
	}

	@Override
	public int countObservers() {
		return observers.size();
	}

	@Override
	public void deleteObserver(Observer o) {
		observers.remove(o);
	}

	@Override
	public void deleteObservers() {
		observers.clear();
	}

	@Override
	public boolean hasChanged() {
		return changed;
	}

	@Override
	public void notifyObservers() {
		if (hasChanged())
			for (Observer obs : observers)
				obs.update(this, detalleTabla);
		clearChanged();
	}

	@Override
	public void notifyObservers(Object o) {
		if (hasChanged())
			for (Observer obs : observers)
				obs.update(this, o);
		clearChanged();
	}

	protected void clearChanged() {
		changed = false;
		detalleTabla = null;
	}

	private void doLog(String level, String signature, String corchetes, String nombreVariable, Loggable loggable) {
		switch (level) {
		case "trace":
			if (LOG.isTraceEnabled())
				LOG.trace(toLogMsg(signature, corchetes, nombreVariable, loggable));
			break;
		case "debug":
			if (LOG.isDebugEnabled())
				LOG.debug(toLogMsg(signature, corchetes, nombreVariable, loggable));
			break;
		case "info":
			if (LOG.isInfoEnabled())
				LOG.info(toLogMsg(signature, corchetes, nombreVariable, loggable));
			break;
		case "warn":
			if (LOG.isWarnEnabled())
				LOG.warn(toLogMsg(signature, corchetes, nombreVariable, loggable));
			break;
		case "error":
			if (LOG.isErrorEnabled())
				LOG.error(toLogMsg(signature, corchetes, nombreVariable, loggable));
			break;
		}
	}

	private String toLogMsg(String signature, String corchetes, String nombreVariable, Loggable loggable) {
		StringBuffer s = new StringBuffer(signature);
		if (corchetes != null)
			s.append(" " + corchetes);
		if (nombreVariable != null)
			s.append(" " + "{ l = " + nombreVariable + " }");
		if (nombreVariable != null && loggable == null)
			s.append(nombreVariable + ": " + loggable);
		else if (nombreVariable != null && loggable != null)
			s.append(loggable.toLogString("l"));
		return s.toString();
	}

}
