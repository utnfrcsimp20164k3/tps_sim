package app.tp5ytp6;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
public class EstadoMaquinaInscribiendo implements EstadoMaquina {
	
	public EstadoMaquinaInscribiendo() {
		super();
	}

	@Bean
	@Scope(value = "prototype")
	static public EstadoMaquina estadoMaquinaInscribiendo() {
		return new EstadoMaquinaInscribiendo();
	}

	@Override
	public String toString() {
		return "Inscribiendo";
	}

	@Override
	public boolean isInscribiendo() {
		return true;
	}

	@Override
	public boolean isLibre() {
		return false;
	}

	@Override
	public boolean isRecibiendoMantenimiento() {
		return false;
	}

	@Override
	public boolean isEsperandoMantenimiento() {
		return false;
	}

	@Override
	public boolean equals(Object obj) {
		return obj instanceof EstadoMaquinaInscribiendo;
	}

}
