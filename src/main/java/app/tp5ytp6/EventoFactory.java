package app.tp5ytp6;

import java.math.BigDecimal;
import java.util.Map;

import app.utils.Evento;
import app.utils.Observable;
import app.utils.Observer;
import app.utils.distribucion.TipoDistribucion;
import app.utils.generador.GeneradorPseudoaleatorio;

public interface EventoFactory<T extends Evento> extends Observer, Observable {

	Double getRndProximo();
	
	BigDecimal getHoraProximo();
	
	Map<?, BigDecimal> getHorasProximos();
	
	BigDecimal getTiempoProximo();

	T getObject() throws Exception;

	Class<?> getObjectType();

	boolean isSingleton();
	
	void setTipoDistribucion(TipoDistribucion tipoDistribucion);
	
	GeneradorPseudoaleatorio getGeneradorPseudoaleatorio();

}
