package app.tp5ytp6;

public interface EstadoAlumno {
	
	boolean isAusente();
	
	boolean isEsperandoMaquina();
	
	boolean isInscribiendose();
	
	boolean isInscripto();

}
