package app.tp5ytp6;

import java.util.ArrayDeque;
import java.util.Deque;
import java.util.HashSet;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import app.utils.Observable;
import app.utils.Observer;
import app.Loggable;
import app.utils.Cola;
import app.utils.GestorColas;

@Component
public class GestorColasImpl implements GestorColas {

	static final Logger LOG = LoggerFactory.getLogger(GestorColasImpl.class);
	private Set<Observer> observers;
	private boolean changed;
	private Deque<Cola<?,?>> colas;
	private Set<Maquina> maquinas;
	private Set<Tecnico> tecnicos;

	public GestorColasImpl() {
		super();
		observers = new HashSet<>();
		maquinas = new HashSet<>();
		tecnicos = new HashSet<>();
		colas = new ArrayDeque<>();
	}

	@Bean
	@Scope(value = "prototype")
	static public GestorColas gestorColas() {
		return new GestorColasImpl();
	}

	@Override
	public void agregarCola(Cola<?,?> cola) {
		if (colas == null)
			colas = new ArrayDeque<Cola<?,?>>();
		colas.addLast(cola);
	}

	@Override
	public void removerCola(Cola<?,?> cola) {
		colas.remove(cola);
		if (colas.isEmpty())
			colas = null;
	}

	@Override
	public Object[] getClaves() {
		Object[] claves = new Object[colas.size()];
		int i = 0;
		for (Cola<?,?> cola : colas)
			claves[i] = cola.getId();
		return claves;
	}

	@Override
	public void update(Observable o, Object arg) {
		doLog("trace", "update(Observable o, Object arg)", "[{]", null, null);
		if (arg instanceof LlegadaAlumno)
			update((LlegadaAlumno) arg);
		else if (arg instanceof LlegadaTecnico)
			update((LlegadaTecnico) arg);
		else if (arg instanceof FinInscripcion)
			update((FinInscripcion) arg);
		else if (arg instanceof FinMantenimiento)
			update((FinMantenimiento) arg);
		else if (o instanceof MaquinaFactory && arg instanceof Maquina)
			update((MaquinaFactory) o,(Maquina) arg);
		else if (o instanceof TecnicoFactory && arg instanceof Tecnico)
			update((TecnicoFactory) o,(Tecnico) arg);
		else if (o instanceof Alumno)
			update((Alumno) o);
		doLog("trace", "update(Observable o, Object arg)", "[}]", null, null);
	}

	private void update(LlegadaAlumno llegadaAlumno) {
		doLog("trace", "update(LlegadaAlumno llegadaAlumno)", "[{]", null, null);
		Alumno alumno = llegadaAlumno.getAlumno();
		alumno.addObserver(this);
		addObserver(alumno);
		// TODO Implementar Strategy para diferentes políticas de ingreso a colas.
		for (Cola<?,?> cola : colas)
			if (cola instanceof ColaAlumnos && cola.hayLugar()) {
				((ColaAlumnos) cola).agregarCliente(alumno);
				cola.next();
				changed = true;
				notifyObservers(alumno);
			}
			else if (cola instanceof ColaAlumnos && !cola.hayLugar()) {
				alumno.declararseAusente();
				changed = true;
				notifyObservers(alumno);
			}
		notificar();
		doLog("trace", "update(LlegadaAlumno llegadaAlumno)", "[}]", null, null);
	}

	private void update(LlegadaTecnico llegadaTecnico) {
		doLog("trace", "update(LlegadaTecnico llegadaTecnico)", "[{]", null, null);
		for (Cola<?,?> cola : colas)
			if (cola instanceof ColaMaquinas) {
				((ColaMaquinas) cola).addObserver(llegadaTecnico.getTecnico());
				llegadaTecnico.getTecnico().declararseEsperandoMaquina();
				((ColaMaquinas) cola).agregarServidor(llegadaTecnico.getTecnico());
				for (Maquina maquina : maquinas) {
					maquina.declararseEsperandoMantenimiento();
					((ColaMaquinas) cola).agregarCliente(maquina);
				}
				cola.next();
			}
		notificar();
		doLog("trace", "update(LlegadaTecnico llegadaTecnico)", "[}]", null, null);
	}

	private void update(FinInscripcion finInscripcion) {
		doLog("trace", "update(FinInscripcion finInscripcion)", "[{]", null, null);
		changed = true;
		notifyObservers(finInscripcion);
		for (Cola<?,?> c : colas)
			c.next();
		notificar();
		doLog("trace", "update(FinInscripcion finInscripcion)", "[}]", null, null);
	}

	private void update(FinMantenimiento finMantenimiento) {
		doLog("trace", "update(FinMantenimiento finMantenimiento)", "[{]", null, null);
		changed = true;
		notifyObservers(finMantenimiento);
		for (Cola<?,?> c : colas)
			c.next();
		notificar();
		doLog("trace", "update(FinMantenimiento finMantenimiento)", "[}]", null, null);
	}

	private void update(Alumno alumno) {
		if (alumno.isInscripto()) {
			removeObserver(alumno);
			alumno.removeObserver(this);
		}
	}

	private void notificar() {
		for (Cola<?,?> cola : colas) {
			changed = true;
			notifyObservers(cola);
		}
	}

	private void update(MaquinaFactory maquinaFactory, Maquina maquina) {
		maquinas.add(maquina);
		addObserver(maquina);
		for (Cola<?,?> cola : colas)
			if (cola instanceof ColaAlumnos)
				((ColaAlumnos) cola).agregarServidor(maquina);
		notificar();
	}

	private void update(TecnicoFactory tecnicoFactory, Tecnico tecnico) {
		tecnicos.add(tecnico);
		addObserver(tecnico);
	}

	@Override
	public void addObserver(Observer o) {
		observers.add(o);
	}

	@Override
	public void removeObserver(Observer o) {
		observers.remove(o);
	}

	@Override
	public int countObservers() {
		return observers.size();
	}

	@Override
	public void deleteObserver(Observer o) {
		observers.remove(o);
	}

	@Override
	public void deleteObservers() {
		observers.clear();
	}

	@Override
	public boolean hasChanged() {
		return changed;
	}

	@Override
	public void notifyObservers() {
		if (hasChanged())
			for (Observer obs : observers)
				obs.update(this, null);
		clearChanged();
	}

	@Override
	public void notifyObservers(Object o) {
		if (hasChanged())
			for (Observer obs : observers)
				obs.update(this, o);
		clearChanged();
	}

	protected void clearChanged() {
		changed = false;
	}

	private void doLog(String level, String signature, String corchetes, String nombreVariable, Loggable loggable) {
		switch (level) {
		case "trace":
			if (LOG.isTraceEnabled())
				LOG.trace(toLogMsg(signature, corchetes, nombreVariable, loggable));
			break;
		case "debug":
			if (LOG.isDebugEnabled())
				LOG.debug(toLogMsg(signature, corchetes, nombreVariable, loggable));
			break;
		case "info":
			if (LOG.isInfoEnabled())
				LOG.info(toLogMsg(signature, corchetes, nombreVariable, loggable));
			break;
		case "warn":
			if (LOG.isWarnEnabled())
				LOG.warn(toLogMsg(signature, corchetes, nombreVariable, loggable));
			break;
		case "error":
			if (LOG.isErrorEnabled())
				LOG.error(toLogMsg(signature, corchetes, nombreVariable, loggable));
			break;
		}
	}

	private String toLogMsg(String signature, String corchetes, String nombreVariable, Loggable loggable) {
		StringBuffer s = new StringBuffer(signature);
		if (corchetes != null)
			s.append(" " + corchetes);
		if (nombreVariable != null)
			s.append(" " + "{ l = " + nombreVariable + " }");
		if (nombreVariable != null && loggable == null)
			s.append(nombreVariable + ": " + loggable);
		else if (nombreVariable != null && loggable != null)
			s.append(loggable.toLogString("l"));
		return s.toString();
	}

}
