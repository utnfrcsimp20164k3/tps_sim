package app.tp5ytp6;

import app.utils.DetalleTabla;

public interface ResultadosSimulacion extends ResumenFinal {

	void setDetalleTablaFinal(DetalleTabla detalleTablaFinal);
	
	boolean hasDetalleTablaFinal();

}
