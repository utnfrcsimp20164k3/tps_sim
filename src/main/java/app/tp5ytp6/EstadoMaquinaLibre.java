package app.tp5ytp6;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
public class EstadoMaquinaLibre implements EstadoMaquina {
	
	public EstadoMaquinaLibre() {
		super();
	}

	@Bean
	@Scope(value = "prototype")
	static public EstadoMaquina estadoMaquinaLibre() {
		return new EstadoMaquinaLibre();
	}

	@Override
	public String toString() {
		return "Libre";
	}

	@Override
	public boolean isInscribiendo() {
		return false;
	}

	@Override
	public boolean isLibre() {
		return true;
	}

	@Override
	public boolean isRecibiendoMantenimiento() {
		return false;
	}

	@Override
	public boolean isEsperandoMantenimiento() {
		return false;
	}

	@Override
	public boolean equals(Object obj) {
		return obj instanceof EstadoMaquinaLibre;
	}

}
