package app.tp5ytp6;

public class FinInscripcionImpl implements FinInscripcion {
	
	Maquina maquina;

	public FinInscripcionImpl() {
		super();
	}

	@Override
	public Maquina getMaquina() {
		return maquina;
	}

	@Override
	public void setMaquina(Maquina maquina) {
		this.maquina = maquina;
	}

	@Override
	public String toString() {
		return "Fin de inscripción";
	}

}
