package app.tp5ytp6;

import app.utils.Observable;

public interface AlumnoFactory extends Observable {
	
	Alumno getObject() throws Exception;

	Class<?> getObjectType();

	boolean isSingleton();

}
