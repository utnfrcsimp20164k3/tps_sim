package app.tp5ytp6;

import app.utils.Evento;

public interface FinMantenimiento extends Evento {

	Tecnico getTecnico();

	void setTecnico(Tecnico tecnico);

}
