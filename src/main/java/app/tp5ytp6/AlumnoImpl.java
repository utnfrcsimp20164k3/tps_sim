package app.tp5ytp6;

import java.util.HashSet;
import java.util.Set;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import app.utils.Observer;
import app.utils.Servidor;
import app.utils.TipoServicio;
import app.Loggable;
import app.utils.Cliente;
import app.utils.Observable;

@Component
public class AlumnoImpl implements Alumno {

	static final Logger LOG = LoggerFactory.getLogger(AlumnoImpl.class);
	static int ultimoNumero;
	private int numero;
	private EstadoAlumno estado;
	@Autowired
	private Cliente cliente;
	@Autowired
	private EstadoAlumno estadoAlumnoAusente;
	@Autowired
	private EstadoAlumno estadoAlumnoEsperandoMaquina;
	@Autowired
	private EstadoAlumno estadoAlumnoInscribiendose;
	@Autowired
	private EstadoAlumno estadoAlumnoInscripto;
	@Autowired
	private TipoServicio inscripcion;
	private Set<Observer> observers;
	private boolean changed;

	static {
		ultimoNumero = 0;
	}

	public AlumnoImpl() {
		super();
		observers = new HashSet<>();
		numero = ultimoNumero++;
	}
	
	@Bean
	@Scope(value = "prototype")
	static public Alumno alumno() {
		return new AlumnoImpl();
	}

	@PostConstruct
	public void init() {
		cliente.addObserver(this);
		cliente.setTipoServicio(inscripcion);
		declararseEsperandoMaquina();
	}

	@Override
	public Integer getNumero() {
		return numero;
	}

	@Override
	public boolean equals(Object obj) {
		return obj != null && obj instanceof Alumno && ((Alumno) obj).getNumero().equals(numero);
	}

	@Override
	public void declararseEsperandoMaquina() {
		cliente.declararseEsperandoServicio();
	}

	@Override
	public void servicioDisponible(Servidor servidor) {
		doLog("trace", "servicioDisponible(Servidor servidor)", "[{]", null, null);
		cliente.servicioDisponible(servidor);
		doLog("trace", "servicioDisponible(Servidor servidor)", "[}]", null, null);
	}

	@Override
	public void declararseAusente() {
		cambiarEstado(estadoAlumnoAusente);
	}

	@Override
	public boolean isAusente() {
		return estado != null && estado.isAusente();
	}

	@Override
	public boolean isEsperandoMaquina() {
		return estado != null && estado.isEsperandoMaquina();
	}

	@Override
	public boolean isInscribiendose() {
		return estado != null && estado.isInscribiendose();
	}

	@Override
	public boolean isInscripto() {
		return estado != null && estado.isInscripto();
	}

	@Override
	public String mostrarEstado() {
		return estado.toString();
	}

	@Override
	public void addObserver(Observer o) {
		observers.add(o);
	}

	@Override
	public void removeObserver(Observer o) {
		observers.remove(o);
	}

	@Override
	public int countObservers() {
		return observers.size();
	}

	@Override
	public void deleteObserver(Observer o) {
		observers.remove(o);
	}

	@Override
	public void deleteObservers() {
		observers.clear();
	}

	@Override
	public boolean hasChanged() {
		return changed;
	}

	@Override
	public void notifyObservers() {
		if (hasChanged())
			for (Observer obs : observers)
				obs.update(this, null);
		clearChanged();
	}

	@Override
	public void notifyObservers(Object o) {
		if (hasChanged())
			for (Observer obs : observers)
				obs.update(this, o);
		clearChanged();
	}

	protected void clearChanged() {
		changed = false;
	}

	@Override
	public void update(Observable o, Object arg) {
		doLog("trace", "update(Observable o, Object arg)", "[{]", null, null);
			if (o instanceof Cliente)
				update((Cliente) o);
		doLog("trace", "update(Observable o, Object arg)", "[}]", null, null);
	}

	private void update(Cliente cliente) {
		doLog("trace", "update(Cliente cliente)", "[{]", null, null);
		if (cliente.isEsperandoServicio())
			cambiarEstado(estadoAlumnoEsperandoMaquina);
		else if (cliente.isRecibiendoServicio())
			cambiarEstado(estadoAlumnoInscribiendose);
		else
			cambiarEstado(estadoAlumnoInscripto);
		doLog("trace", "update(Cliente cliente)", "[}]", null, null);
	}

	private void cambiarEstado(EstadoAlumno estado) {
		changed = this.estado != estado;
		this.estado = estado;
		notifyObservers();
	}

	private void doLog(String level, String signature, String corchetes, String nombreVariable, Loggable loggable) {
		switch (level) {
		case "trace":
			if (LOG.isTraceEnabled())
				LOG.trace(toLogMsg(signature, corchetes, nombreVariable, loggable));
			break;
		case "debug":
			if (LOG.isDebugEnabled())
				LOG.debug(toLogMsg(signature, corchetes, nombreVariable, loggable));
			break;
		case "info":
			if (LOG.isInfoEnabled())
				LOG.info(toLogMsg(signature, corchetes, nombreVariable, loggable));
			break;
		case "warn":
			if (LOG.isWarnEnabled())
				LOG.warn(toLogMsg(signature, corchetes, nombreVariable, loggable));
			break;
		case "error":
			if (LOG.isErrorEnabled())
				LOG.error(toLogMsg(signature, corchetes, nombreVariable, loggable));
			break;
		}
	}

	private String toLogMsg(String signature, String corchetes, String nombreVariable, Loggable loggable) {
		StringBuffer s = new StringBuffer(signature);
		if (corchetes != null)
			s.append(" " + corchetes);
		if (nombreVariable != null)
			s.append(" " + "{ l = " + nombreVariable + " }");
		if (nombreVariable != null && loggable == null)
			s.append(nombreVariable + ": " + loggable);
		else if (nombreVariable != null && loggable != null)
			s.append(loggable.toLogString("l"));
		return s.toString();
	}

	@Override
	public String toLogString(String nombre) {
		nombre = nombre != null ? nombre : "l";
		StringBuffer s = new StringBuffer();
		s.append(" " + nombre + ": " + this);
		s.append(", " + nombre + ".numero: " + numero);
		s.append(", " + nombre + ".estado: " + estado);
		return s.toString();
	}


}
