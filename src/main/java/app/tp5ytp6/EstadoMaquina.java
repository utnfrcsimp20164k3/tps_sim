package app.tp5ytp6;

import app.utils.Estado;

public interface EstadoMaquina extends Estado {
	
	boolean isLibre();
	
	boolean isRecibiendoMantenimiento();

	boolean isInscribiendo();
	
	boolean isEsperandoMantenimiento();

}
