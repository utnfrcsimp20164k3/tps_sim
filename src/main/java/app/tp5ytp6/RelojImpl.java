package app.tp5ytp6;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import app.Loggable;
import app.utils.Observable;
import app.utils.Observer;

@Component
public class RelojImpl implements Reloj {

	static final Logger LOG = LoggerFactory.getLogger(RelojImpl.class);
	private BigDecimal horaActual;
	private List<BigDecimal> horasProximas;
	private Set<EventoFactory<?>> eventosDisparadores;

	private Set<Observer> observers;
	private boolean changed;

	public RelojImpl() {
		super();
		observers = new HashSet<>();
		horaActual = new BigDecimal("0");
		horasProximas = new ArrayList<>();
		horasProximas.add(new BigDecimal("0"));
		eventosDisparadores = new HashSet<>();
		changed = true;
	}

	@Bean
	@Scope(value = "prototype")
	static public Reloj reloj() {
		return new RelojImpl();
	}

	@Override
	public void next() {
		if (isInicializado() && !horasProximas.isEmpty()) {
			horaActual = horasProximas.remove(0);
			changed = true;
			notifyObservers(horaActual);
		}
	}

	@Override
	public void update(Observable o, Object arg) {
		doLog("trace", "update(Observable o, Object arg)", "[{]", null, null);
		if (o instanceof EventoFactory && arg != null && arg instanceof BigDecimal)
			update((EventoFactory<?>) o, (BigDecimal) arg);
		else if (o instanceof EventoFactory && arg != null && arg instanceof Collection<?>)
			update((EventoFactory<?>) o, (Collection<?>) arg);
		doLog("trace", "update(Observable o, Object arg)", "[}]", null, null);
	}

	private void update(EventoFactory<?> eventoFactory, BigDecimal horaProxima) {
		doLog("trace", "update(EventoFactory<?> eventoFactory, BigDecimal horaProxima)", "[{]", null, null);
		LOG.debug("update(EventoFactory<?> eventoFactory, BigDecimal horaProxima) [0] eventosDisparadores: "
				+ eventosDisparadores);
		LOG.debug("update(EventoFactory<?> eventoFactory, BigDecimal horaProxima) [1] eventoFactory: " + eventoFactory);
		LOG.debug("update(EventoFactory<?> eventoFactory, BigDecimal horaProxima) [2] horasProximas: " + horasProximas);
		horasProximas.add(horaProxima);
		Collections.sort(horasProximas);
		eventosDisparadores.remove(eventoFactory);
		LOG.debug("update(EventoFactory<?> eventoFactory, BigDecimal horaProxima) [3] isInicializado(): "
				+ new Boolean(isInicializado()));
		doLog("trace", "update(EventoFactory<?> eventoFactory, BigDecimal horaProxima)", "[}]", null, null);
	}

	private void update(EventoFactory<?> eventoFactory, Collection<?> horasProximas) {
		doLog("trace", "update(EventoFactory<?> eventoFactory, Collection<?> horasProximos)", "[{]", null, null);
		LOG.debug("update(EventoFactory<?> eventoFactory, Collection<?> horasProximas) [0] eventosDisparadores: "
				+ eventosDisparadores);
		LOG.debug("update(EventoFactory<?> eventoFactory, Collection<?> horasProximas) [1] eventoFactory: "
				+ eventoFactory);
		LOG.debug("update(EventoFactory<?> eventoFactory, Collection<?> horasProximas) [2] horasProximas: "
				+ horasProximas);
		this.horasProximas.addAll((Collection<BigDecimal>) horasProximas);
		Collections.sort(this.horasProximas);
		eventosDisparadores.remove(eventoFactory);
		LOG.debug("update(EventoFactory<?> eventoFactory, Collection<?> horasProximas) [3] isInicializado(): "
				+ new Boolean(isInicializado()));
		doLog("trace", "update(EventoFactory<?> eventoFactory, Collection<?> horasProximas)", "[}]", null, null);
	}

	private boolean isInicializado() {
		return eventosDisparadores.isEmpty();
	}

	@Override
	public void addObserver(Observer o) {
		doLog("trace", "addObserver(Observer o)", "[{]", null, null);
		observers.add(o);
		doLog("trace", "addObserver(Observer o)", "[}]", null, null);
	}

	@Override
	public void removeObserver(Observer o) {
		observers.add(o);
	}

	@Override
	public int countObservers() {
		return observers.size();
	}

	@Override
	public void deleteObserver(Observer o) {
		observers.remove(o);
	}

	@Override
	public void deleteObservers() {
		observers.clear();
	}

	@Override
	public boolean hasChanged() {
		return changed;
	}

	@Override
	public void notifyObservers() {
		doLog("trace", "notifyObservers()", "[{]", null, null);
		if (hasChanged())
			for (Observer obs : observers)
				obs.update(this, null);
		clearChanged();
		doLog("trace", "notifyObservers()", "[}]", null, null);
	}

	@Override
	public void notifyObservers(Object o) {
		doLog("trace", "notifyObservers(Object o)", "[{]", null, null);
		if (hasChanged())
			for (Observer obs : observers)
				obs.update(this, o);
		clearChanged();
		doLog("trace", "notifyObservers(Object o)", "[}]", null, null);
	}

	protected void clearChanged() {
		changed = false;
	}

	@Override
	public void resetear(Set<EventoFactory<?>> eventosDisparadores) {
		doLog("trace", "resetear(Set<EventoFactory<?>> eventosDisparadores)", "[{]", null, null);
		LOG.debug(
				"resetear(Set<EventoFactory<?>> eventosDisparadores) [0] eventosDisparadores: " + eventosDisparadores);
		parar();
		changed = true;
		this.eventosDisparadores.addAll(eventosDisparadores);
		LOG.debug("resetear(Set<EventoFactory<?>> eventosDisparadores) [1] this.eventosDisparadores: "
				+ this.eventosDisparadores);
		doLog("trace", "resetear(Set<EventoFactory<?>> eventosDisparadores)", "[}]", null, null);
	}

	@Override
	public void iniciar() {
		horaActual = horasProximas.remove(0);
		notifyObservers(horaActual);
	}

	@Override
	public void parar() {
		horasProximas = new ArrayList<>();
		horasProximas.add(new BigDecimal("0"));
	}

	@Override
	public BigDecimal getHoraActual() {
		return horaActual;
	}

	@Override
	public List<BigDecimal> getHorasProximas() {
		return horasProximas;
	}

	private void doLog(String level, String signature, String corchetes, String nombreVariable, Loggable loggable) {
		switch (level) {
		case "trace":
			if (LOG.isTraceEnabled())
				LOG.trace(toLogMsg(signature, corchetes, nombreVariable, loggable));
			break;
		case "debug":
			if (LOG.isDebugEnabled())
				LOG.debug(toLogMsg(signature, corchetes, nombreVariable, loggable));
			break;
		case "info":
			if (LOG.isInfoEnabled())
				LOG.info(toLogMsg(signature, corchetes, nombreVariable, loggable));
			break;
		case "warn":
			if (LOG.isWarnEnabled())
				LOG.warn(toLogMsg(signature, corchetes, nombreVariable, loggable));
			break;
		case "error":
			if (LOG.isErrorEnabled())
				LOG.error(toLogMsg(signature, corchetes, nombreVariable, loggable));
			break;
		}
	}

	private String toLogMsg(String signature, String corchetes, String nombreVariable, Loggable loggable) {
		StringBuffer s = new StringBuffer(signature);
		if (corchetes != null)
			s.append(" " + corchetes);
		if (nombreVariable != null)
			s.append(" " + "{ l = " + nombreVariable + " }");
		if (nombreVariable != null && loggable == null)
			s.append(nombreVariable + ": " + loggable);
		else if (nombreVariable != null && loggable != null)
			s.append(loggable.toLogString("l"));
		return s.toString();
	}

	@Override
	public String toLogString(String nombre) {
		nombre = nombre != null ? nombre : "l";
		StringBuffer s = new StringBuffer();
		s.append(" " + nombre + ": " + this);
		s.append(", " + nombre + ".horaActual: " + horaActual);
		s.append(", " + nombre + ".horasProximas: " + horasProximas);
		return s.toString();
	}

}
