package app.tp5ytp6;

import app.utils.Observable;
import app.utils.Observer;
import app.utils.Permanente;
import app.utils.Servidor;

import java.math.BigDecimal;

import app.Loggable;
import app.utils.Ubicacion;

public interface Maquina extends Comparable<Maquina>, Ubicacion, Permanente, Observer, Observable, Loggable {

	Integer getNumero();
	
	Integer getContadorInscripciones();
	
	String mostrarEstado();

	Boolean isRevisada();
	
	Boolean isLibre();
	
	void ofrecerServicio(Alumno alumno);
	
	void servicioDisponible(Servidor servidor);

	boolean isInscribiendo();
	
	boolean isEsperandoMantenimiento();

	boolean isRecibiendoMantenimiento();

	void declararseEsperandoMantenimiento();
	
	void entrarEnServicio(BigDecimal hora);

	void salirDeServicio(BigDecimal hora);
	
	BigDecimal getTiempoEnServicio();

}
