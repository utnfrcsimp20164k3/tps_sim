package app.tp5ytp6;

import java.util.HashSet;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import app.utils.Observable;
import app.utils.Observer;
import app.Ejercicio;
import app.Loggable;
import app.tp5.ejerciciod.TP5EjercicioD;
import app.utils.DetalleTabla;
import app.utils.TablaVectorEstados;

@Component
public class VectorEstadosMaquinaImpl implements VectorEstadosMaquina {

	@Autowired
	private ApplicationContext applicationContext;
	static final Logger LOG = LoggerFactory.getLogger(VectorEstadosMaquinaImpl.class);
	private Set<Observer> observers;
	private boolean changed;
	private TablaVectorEstados tabla;
	private DetalleTabla detalleTabla;
	private Set<Maquina> maquinas;

	public VectorEstadosMaquinaImpl() {
		super();
		observers = new HashSet<>();
		maquinas = new HashSet<>();
	}

	@Bean
	@Scope("prototype")
	static public VectorEstadosMaquina vectorEstadosMaquina() {
		return new VectorEstadosMaquinaImpl();
	}

	@Override
	public Object[] getClaves() {
		return new Object[] { getClaveEstado(), getClaveRevisada(), getClaveContadorInscripciones() };
	}

	@Override
	public Object getClaveEstado() {
		return "Estado";
	}

	@Override
	public Object getClaveRevisada() {
		return "Revisada";
	}

	@Override
	public Object getClaveContadorInscripciones() {
		return "Contador de inscripciones";
	}

	@Override
	public void update(Observable o, Object arg) {
		doLog("trace", "update(Observable o, Object arg)", "[{]", null, null);
		if (detalleTabla == null)
			detalleTabla = (DetalleTabla) applicationContext.getBean("detalleTabla");
		if (o instanceof Ejercicio && arg instanceof TablaVectorEstados)
			update((TablaVectorEstados) arg);
		else if (o instanceof Ejercicio && arg instanceof Reloj)
			update((Reloj) arg);
		else if (o instanceof MaquinaFactory && arg instanceof Maquina)
			update((MaquinaFactory) o, (Maquina) arg);
		else if (o instanceof Maquina)
			update((Maquina) o);
		doLog("trace", "update(Observable o, Object arg)", "[}]", null, null);
	}

	private void update(TablaVectorEstados tabla) {
		doLog("trace", "update(TablaVectorEstados tabla)", "[{]", null, null);
		this.tabla = tabla;
		doLog("trace", "update(TablaVectorEstados tabla)", "[}]", null, null);
	}

	private void update(Reloj arg) {
		doLog("trace", "update(Reloj arg)", "[{]", null, null);
		actualizarMaquinas();
		doLog("trace", "update(Reloj arg)", "[}]", null, null);
	}

	private void actualizarMaquinas() {
		doLog("trace", "actualizarMaquinas()", "[{]", null, null);
		for (Maquina maquina : maquinas) {
			update(maquina);
			detalleTabla = (DetalleTabla) applicationContext.getBean("detalleTabla");
		}
		doLog("trace", "actualizarMaquinas()", "[}]", null, null);
	}

	private void update(MaquinaFactory maquinaFactory, Maquina maquina) {
		doLog("trace", "update(MaquinaFactory maquinaFactory, Maquina maquina)", "[{]", null, null);
		maquina.addObserver(this);
		registrar(maquina);
		update(maquina);
		doLog("trace", "update(MaquinaFactory maquinaFactory, Maquina maquina)", "[}]", null, null);
	}

	private void registrar(Maquina maquina) {
		doLog("trace", "registrar(Maquina maquina)", "[{]", null, null);
		maquinas.add(maquina);
		for (Object clave : getClaves()) {
			Set<Object> claves = new HashSet<>();
			claves.add(maquina);
			claves.add(clave);
			tabla.crearColumna(claves);
		}
		tabla.registrarPermanente(maquina);
		doLog("trace", "registrar(Maquina maquina)", "[}]", null, null);
	}
	
	private void update(Maquina maquina) {
		doLog("trace", "actualizar(Maquina maquina)", "[{]", null, null);
		Set<Object> claves = new HashSet<>();
		claves.add(getClaveEstado());
		claves.add(maquina);
		detalleTabla.getValores().put(claves, maquina.mostrarEstado());
		claves = new HashSet<>();
		claves.add(getClaveRevisada());
		claves.add(maquina);
		detalleTabla.getValores().put(claves, maquina.isRevisada());
		claves = new HashSet<>();
		claves.add(getClaveContadorInscripciones());
		claves.add(maquina);
		detalleTabla.getValores().put(claves, maquina.getContadorInscripciones());
		changed = true;
		notifyObservers();
		doLog("trace", "actualizar(Maquina maquina)", "[}]", null, null);
	}

	@Override
	public DetalleTabla parse(Maquina maquina) {
		throw new RuntimeException();
	}

	@Override
	public void addObserver(Observer o) {
		observers.add(o);
	}

	@Override
	public int countObservers() {
		return observers.size();
	}

	@Override
	public void deleteObserver(Observer o) {
		observers.remove(o);
	}

	@Override
	public void deleteObservers() {
		observers.clear();
	}

	@Override
	public boolean hasChanged() {
		return changed;
	}

	@Override
	public void notifyObservers() {
		if (hasChanged())
			for (Observer obs : observers)
				obs.update(this, detalleTabla);
		clearChanged();
	}

	@Override
	public void notifyObservers(Object o) {
		if (hasChanged())
			for (Observer obs : observers)
				obs.update(this, o);
		clearChanged();
	}

	protected void clearChanged() {
		changed = false;
		detalleTabla = null;
	}

	private void doLog(String level, String signature, String corchetes, String nombreVariable, Loggable loggable) {
		switch (level) {
		case "trace":
			if (LOG.isTraceEnabled())
				LOG.trace(toLogMsg(signature, corchetes, nombreVariable, loggable));
			break;
		case "debug":
			if (LOG.isDebugEnabled())
				LOG.debug(toLogMsg(signature, corchetes, nombreVariable, loggable));
			break;
		case "info":
			if (LOG.isInfoEnabled())
				LOG.info(toLogMsg(signature, corchetes, nombreVariable, loggable));
			break;
		case "warn":
			if (LOG.isWarnEnabled())
				LOG.warn(toLogMsg(signature, corchetes, nombreVariable, loggable));
			break;
		case "error":
			if (LOG.isErrorEnabled())
				LOG.error(toLogMsg(signature, corchetes, nombreVariable, loggable));
			break;
		}
	}

	private String toLogMsg(String signature, String corchetes, String nombreVariable, Loggable loggable) {
		StringBuffer s = new StringBuffer(signature);
		if (corchetes != null)
			s.append(" " + corchetes);
		if (nombreVariable != null)
			s.append(" " + "{ l = " + nombreVariable + " }");
		if (nombreVariable != null && loggable == null)
			s.append(nombreVariable + ": " + loggable);
		else if (nombreVariable != null && loggable != null)
			s.append(loggable.toLogString("l"));
		return s.toString();
	}

	@Override
	public void removeObserver(Observer o) {
		observers.remove(o);
	}
	
}
