package app.tp5ytp6;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
public class EstadoAlumnoInscripto implements EstadoAlumno {
	
	public EstadoAlumnoInscripto() {
		super();
	}

	@Bean
	@Scope(value = "prototype")
	static public EstadoAlumno estadoAlumnoInscripto() {
		return new EstadoAlumnoInscripto();
	}

	@Override
	public String toString() {
		return "Inscripto";
	}

	@Override
	public boolean isAusente() {
		return false;
	}

	@Override
	public boolean isEsperandoMaquina() {
		return false;
	}

	@Override
	public boolean isInscribiendose() {
		return false;
	}

	@Override
	public boolean isInscripto() {
		return true;
	}

}
