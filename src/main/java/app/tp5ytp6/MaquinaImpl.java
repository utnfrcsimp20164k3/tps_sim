package app.tp5ytp6;

import java.math.BigDecimal;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Set;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import app.utils.Observable;
import app.utils.Observer;
import app.Loggable;
import app.utils.Cliente;
import app.utils.GestorColas;
import app.utils.Servidor;
import app.utils.TipoServicio;

@Component
public class MaquinaImpl implements Maquina, Observable {

	static final Logger LOG = LoggerFactory.getLogger(MaquinaImpl.class);
	static int ultimoNumero;
	private int numero;
	private BigDecimal tiempoEnServicio, horaUltimaEntradaEnServicio;
	private int contadorInscripciones;
	private EstadoMaquina estado;
	private Set<Observer> observers;
	private boolean changed;
	@Autowired
	private Cliente cliente;
	@Autowired
	private Servidor servidor;
	@Autowired
	private EstadoMaquina estadoMaquinaLibre;
	@Autowired
	private EstadoMaquina estadoMaquinaInscribiendo;
	@Autowired
	private EstadoMaquina estadoMaquinaRecibiendoMantenimiento;
	@Autowired
	private EstadoMaquina estadoMaquinaEsperandoMantenimiento;
	@Autowired
	private TipoServicio inscripcion;
	@Autowired
	private TipoServicio mantenimiento;

	static {
		ultimoNumero = 0;
	}

	public MaquinaImpl() {
		super();
		observers = new HashSet<>();
		numero = ultimoNumero++;
		contadorInscripciones = 0;
		tiempoEnServicio = new BigDecimal("0");
	}

	@Bean
	@Scope(value = "prototype")
	static public Maquina maquina() {
		return new MaquinaImpl();
	}

	@PostConstruct
	public void init() {
		estado = estadoMaquinaLibre;
		cliente.addObserver(this);
		cliente.setTipoServicio(mantenimiento);
		servidor.addObserver(this);
		servidor.setTipoServicio(inscripcion);
	}

	@Override
	public Integer getNumero() {
		return numero;
	}

	@Override
	public Integer getContadorInscripciones() {
		return contadorInscripciones;
	}

	@Override
	public Boolean isRevisada() {
		return !cliente.isEsperandoServicio();
	}

	@Override
	public boolean isInscribiendo() {
		return estado.equals(estadoMaquinaInscribiendo);
	}

	@Override
	public void declararseEsperandoMantenimiento() {
		cliente.declararseEsperandoServicio();
	}

	@Override
	public void entrarEnServicio(BigDecimal hora) {
		horaUltimaEntradaEnServicio = hora;
	}

	@Override
	public void salirDeServicio(BigDecimal hora) {
		if (horaUltimaEntradaEnServicio == null)
			throw new RuntimeException("La máquina ya estaba fuera de servicio.");
		else
			tiempoEnServicio = tiempoEnServicio.add(hora.subtract(horaUltimaEntradaEnServicio));
	}

	@Override
	public BigDecimal getTiempoEnServicio() {
		return tiempoEnServicio;
	}

	@Override
	public boolean equals(Object obj) {
		return obj != null && obj instanceof Maquina && ((Maquina) obj).getNumero().equals(numero);
	}

	@Override
	public int hashCode() {
		return numero;
	}

	@Override
	public String mostrarEstado() {
		return estado.toString();
	}

	@Override
	public Boolean isLibre() {
		return estado.isLibre();
	}

	@Override
	public boolean isEsperandoMantenimiento() {
		return estado.isEsperandoMantenimiento();
	}

	@Override
	public boolean isRecibiendoMantenimiento() {
		return estado.isRecibiendoMantenimiento();
	}

	@Override
	public void ofrecerServicio(Alumno alumno) {
		doLog("trace", "ofrecerServicio(Alumno alumno)", "[{]", null, null);
		alumno.servicioDisponible(servidor);
		doLog("trace", "ofrecerServicio(Alumno alumno)", "[}]", null, null);

	}

	@Override
	public void servicioDisponible(Servidor servidor) {
		cliente.servicioDisponible(servidor);
	}

	@Override
	public void addObserver(Observer o) {
		observers.add(o);
	}

	@Override
	public void removeObserver(Observer o) {
		observers.remove(o);
	}

	@Override
	public int countObservers() {
		return observers.size();
	}

	@Override
	public void deleteObserver(Observer o) {
		observers.remove(o);
	}

	@Override
	public void deleteObservers() {
		observers.clear();
	}

	@Override
	public boolean hasChanged() {
		return changed;
	}

	@Override
	public void notifyObservers() {
		if (hasChanged())
			for (Observer obs : observers)
				obs.update(this, null);
		clearChanged();
	}

	@Override
	public void notifyObservers(Object o) {
		if (hasChanged())
			for (Observer obs : observers)
				obs.update(this, o);
		clearChanged();
	}

	protected void clearChanged() {
		changed = false;
	}

	@Override
	public void update(Observable o, Object arg) {
		doLog("trace", "update(Observable o, Object arg)", "[{]", null, null);
		if (o instanceof Cliente)
			update((Cliente) o);
		else if (o instanceof Servidor)
			update((Servidor) o);
		else if (o instanceof GestorColas && arg != null && arg instanceof FinInscripcion)
			update((GestorColas) o, (FinInscripcion) arg);
		else if (o instanceof GestorColas && arg != null && arg instanceof FinMantenimiento)
			update((GestorColas) o, (FinMantenimiento) arg);
		doLog("trace", "update(Observable o, Object arg)", "[}]", null, null);
	}

	private void update(Cliente cliente) {
		doLog("trace", "update(Cliente cliente)", "[{]", null, null);
		if (cliente.isEsperandoServicio() && isLibre())
			cambiarEstado(estadoMaquinaEsperandoMantenimiento);
		else if (cliente.isRecibiendoServicio())
			cambiarEstado(estadoMaquinaRecibiendoMantenimiento);
		else if (!cliente.isEsperandoServicio() && !cliente.isRecibiendoServicio())
			cambiarEstado(estadoMaquinaLibre);
		doLog("trace", "update(Cliente cliente)", "[}]", null, null);
	}

	private void cambiarEstado(EstadoMaquina estado) {
		changed = !this.estado.equals(estado);
		this.estado = estado;
		notifyObservers();
	}

	private void update(Servidor servidor) {
		doLog("trace", "update(Servidor servidor)", "[{]", null, null);
		if (servidor.isSirviendo())
			cambiarEstado(estadoMaquinaInscribiendo);
		else if (cliente.isEsperandoServicio())
			cambiarEstado(estadoMaquinaEsperandoMantenimiento);
		else 
			cambiarEstado(estadoMaquinaLibre);
		doLog("trace", "update(Servidor servidor)", "[}]", null, null);
	}

	private void update(GestorColas gestorColas, FinInscripcion finInscripcion) {
		doLog("trace", "update(GestorColas gestorColas, FinInscripcion finInscripcion)", "[{]", null, null);
		if (finInscripcion.getMaquina().equals(this)) {
			servidor.terminar();
			contadorInscripciones++;
			notifyObservers();
		}
		doLog("trace", "update(GestorColas gestorColas, FinInscripcion finInscripcion)", "[}]", null, null);
	}

	private void update(GestorColas gestorColas, FinMantenimiento finMantenimiento) {
		doLog("trace", "update(GestorColas gestorColas, FinMantenimiento finMantenimiento)", "[{]", null, null);
		cliente.servicioTerminado(finMantenimiento.getTecnico().getServidor());
		doLog("trace", "update(GestorColas gestorColas, FinMantenimiento finMantenimiento)", "[}]", null, null);
	}

	private void doLog(String level, String signature, String corchetes, String nombreVariable, Loggable loggable) {
		switch (level) {
		case "trace":
			if (LOG.isTraceEnabled())
				LOG.trace(toLogMsg(signature, corchetes, nombreVariable, loggable));
			break;
		case "debug":
			if (LOG.isDebugEnabled())
				LOG.debug(toLogMsg(signature, corchetes, nombreVariable, loggable));
			break;
		case "info":
			if (LOG.isInfoEnabled())
				LOG.info(toLogMsg(signature, corchetes, nombreVariable, loggable));
			break;
		case "warn":
			if (LOG.isWarnEnabled())
				LOG.warn(toLogMsg(signature, corchetes, nombreVariable, loggable));
			break;
		case "error":
			if (LOG.isErrorEnabled())
				LOG.error(toLogMsg(signature, corchetes, nombreVariable, loggable));
			break;
		}
	}

	private String toLogMsg(String signature, String corchetes, String nombreVariable, Loggable loggable) {
		StringBuffer s = new StringBuffer(signature);
		if (corchetes != null)
			s.append(" " + corchetes);
		if (nombreVariable != null)
			s.append(" " + "{ l = " + nombreVariable + " }");
		if (nombreVariable != null && loggable == null)
			s.append(nombreVariable + ": " + loggable);
		else if (nombreVariable != null && loggable != null)
			s.append(loggable.toLogString("l"));
		return s.toString();
	}

	@Override
	public String toLogString(String nombre) {
		nombre = nombre != null ? nombre : "l";
		StringBuffer s = new StringBuffer();
		s.append(" " + nombre + ": " + this);
		s.append(", " + nombre + ".numero: " + numero);
		s.append(", " + nombre + ".contadorInscripciones: " + contadorInscripciones);
		s.append(", " + nombre + ".estado: " + estado);
		return s.toString();
	}

	@Override
	public int compareTo(Maquina maquina) {
		return new BigDecimal(new Integer(numero)).compareTo(new BigDecimal(new Integer(maquina.getNumero())));
	}

	public static Comparador comparador() {
		return new MaquinaImpl().new Comparador();
	}

	public class Comparador implements Comparator<Maquina> {

		@Override
		public int compare(Maquina o1, Maquina o2) {
			return o1.compareTo(o2);
		}

	}

}
