package app.tp5ytp6;

import java.util.Set;

import app.utils.VectorEstados;

public interface VectorEstadosFinInscripcionFactory extends VectorEstados<EventoFactory<FinInscripcion>> {
	
	Set<Maquina> getMaquinas();

	Object getClaveRnd();

	Object getClaveTiempoProximo();

	Object getClaveHoraProximo();

}
