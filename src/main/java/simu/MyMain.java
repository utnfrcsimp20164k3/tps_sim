package simu;

import simu.generador.Generator;
import simu.generador.JavaGenerator;
import simu.generador.PoissonGenerator;

/**
 * @author mauricio.heredia
 */
public class MyMain {
    public static void main(String[] args) {
        //        Generator gen = new CongruenciaLinealGenerator(37,19,33,100);
        //        for(int i =0;i<50;i++){
        //            System.out.format("Valor: %s \n", Double.toString(gen.getNextNumber()));
        //        }

        Generator gen2 = new PoissonGenerator(new JavaGenerator(), 3, 20);

        for (int i = 0; i < 100; i++) {
            System.out.format("Valor: %s \n", Double.toString(gen2.getNextNumber()));
        }
    }
}
