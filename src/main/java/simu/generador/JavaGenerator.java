package simu.generador;

import java.util.Random;

/**
 * @author mauricio.heredia
 */
public class JavaGenerator implements Generator {

    private Random random;
    private final long raiz;
    public JavaGenerator(long raiz){
        this.raiz = raiz;
        random = new Random(raiz);
    }

    public JavaGenerator(){
        raiz = -1;
        random = new Random();
    }

    @Override
    public double getNextNumber() {
        return random.nextDouble();
    }

    @Override
    public void reset() {
        random.setSeed(raiz);
    }
}
