package simu.generador;

/**
 * Created by norberto.app on 3/23/16.
 */
public class ConvolucionGenerator implements Generator {

    private final double k;
    private final Generator generator;

    public ConvolucionGenerator(double k, Generator random) {
        this.k = k;
        this.generator = random;
    }

    @Override
    public double getNextNumber() {
        double accumValue = 0;
        for (int i = 0; i < k; i++) {
            accumValue = +(generator.getNextNumber() - (k / 2));
        }

        return (accumValue / Math.sqrt(k/12));
    }

    @Override
    public void reset() {

    }
}
