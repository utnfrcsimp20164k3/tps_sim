package simu.generador;

/**
 * @author mauricio.heredia
 */
public class ExponencialGenerator implements Generator {
    private final Generator randomGenerator;
    private final double lambda;

    public ExponencialGenerator(Generator randomGenerator, double lambda) {
        this.randomGenerator = randomGenerator;
        this.lambda = lambda;
    }

    @Override
    public double getNextNumber() {
        return (- 1 / lambda) * Math.log(1 - randomGenerator.getNextNumber());
    }

    @Override
    public void reset() {
        randomGenerator.reset();
    }
}
