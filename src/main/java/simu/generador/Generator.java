package simu.generador;

/**
 * @author mauricio.heredia
 */
public interface Generator {
    double getNextNumber();
    void reset();
}
