package simu.generador;

import java.util.concurrent.atomic.AtomicBoolean;

/**
 * @author mauricio.heredia
 */
public class BoxMullerGenerator implements Generator {
    private final Generator randomGenerator;
    private AtomicBoolean isFirstValue = new AtomicBoolean(true);
    private double rndValue1;
    private double rndValue2;

    public BoxMullerGenerator(Generator randomGenerator) {
        this.randomGenerator = randomGenerator;
    }

    @Override
    public double getNextNumber() {
        if (isFirstValue.getAndSet(!isFirstValue.get())) {
            this.rndValue1 = randomGenerator.getNextNumber();
            this.rndValue2 = randomGenerator.getNextNumber();
        }
        return Math.sqrt(-2 * Math.log(rndValue1)) * (isFirstValue.get() ? Math.cos(2 * Math.PI * rndValue2) : Math.sin(2 * Math.PI * rndValue2));
    }

    @Override
    public void reset() {
        randomGenerator.reset();
        isFirstValue.set(true);
    }
}
