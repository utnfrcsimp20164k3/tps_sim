package simu.generador;

/**
 * @author mauricio.heredia.
 */
public class CongruenciaLinealGenerator implements Generator {

    private final int root;
    private final int a;
    private final int c;
    private final int m;
    private double rootAux;

    public CongruenciaLinealGenerator(int root, int a, int c, int m){
        this.root =root;
        this.a = a;
        this.c= c;
        this.m = m;
        this.rootAux = root;
    }

    @Override
    public double getNextNumber() {
        rootAux = (((a * rootAux) + c)% m);
        return rootAux / (m - 1);
    }

    @Override
    public void reset() {
        rootAux = root;
    }
}
