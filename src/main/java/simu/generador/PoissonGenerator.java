package simu.generador;

/**
 * @author mauricio.heredia
 */
public class PoissonGenerator implements Generator {
    private final Generator randomGenerator;
    private final double a;
    private final double b;


    public PoissonGenerator(Generator randomGenerator, double a, double b) {
        this.randomGenerator = randomGenerator;
        this.a = a;
        this.b = b;
    }

    @Override
    public double getNextNumber() {
        int accumValue = -1;
        double auxB = b;
        do {
            auxB = auxB * randomGenerator.getNextNumber();
            accumValue++;
        } while (auxB >= a);
        return accumValue;
    }

    @Override
    public void reset() {
        randomGenerator.reset();
    }
}
