package simu.generador;

/**
 * @author mauricio.heredia
 */
public class TransformadaInversoGenerator implements Generator {
    private final Generator randomGenerator;
    private final int a;
    private final int b;

    public TransformadaInversoGenerator(Generator randomGenerator, int a, int b) {
        this.randomGenerator = randomGenerator;
        this.a = a;
        this.b = b;
    }

    @Override
    public double getNextNumber() {
        return randomGenerator.getNextNumber() * (b - a) + a;
    }

    @Override
    public void reset() {
        randomGenerator.reset();
    }
}
