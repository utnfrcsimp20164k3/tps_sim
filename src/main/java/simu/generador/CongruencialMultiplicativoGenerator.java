package simu.generador;

/**
 * @author mauricio.heredia
 */
public class CongruencialMultiplicativoGenerator implements Generator {

    private final int raiz;
    private final int a;
    private final int m;
    private double raizAUX;

    public CongruencialMultiplicativoGenerator(int raiz, int a, int m) {
        this.raiz = raiz;
        this.a = a;
        this.m = m;
        this.raizAUX = raiz;
    }

    @Override
    public double getNextNumber() {
        raizAUX = (raizAUX * a) % m;
        return raizAUX / (m - 1);
    }

    @Override
    public void reset() {
        raizAUX = raiz;
    }
}
